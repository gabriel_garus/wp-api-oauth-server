// This script is neccessary for 'webshim' validation
// to work with 'contact form 7' plugin
$(document).ready(function(){

$('form').removeAttr('novalidate').addClass('contact-form');
$('input, textarea').each(function(){

  if ($(this).attr('aria-required') == 'true'){
    $(this).attr('required', 'true');

    $("label[for='"+$(this).attr('id')+"']").addClass('required');
  }});


})

webshim.setOptions({
        'loadStyles': false, // if 'true', loading 3 .css files in the head
        'forms': {
        replaceValidationUI: true,
        lazyCustomMessages: true,
        ival: {
        //wether webshim should show a bubble on invalid event: "hide" | true | false
        "handleBubble": true,
        //show/hide effect: 'no' | 'slide' | 'fade'
        "fx": "fade"
            }
        },
        'forms-ext': {
            // Replace native UI behaviour on Chrome, etc.
            replaceUI: 'true',
            types: 'date color',
            "widgets": {
                "openOnFocus": false,
                "calculateWidth": false
              }
            }
        });

     // Start WEBSHIM and disable polyfill on touch devices
        if (!$('html').hasClass('touch')) {
            webshim.polyfill('forms');
        }
