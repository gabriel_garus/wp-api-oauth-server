/**
 * AJAX script for Go Further
 */

(function($){
    $('#submit_query').on( 'click', function(event) {
        event.preventDefault();

        // Remove "Get Related Posts" button
       var user_input = $('#user_query').val();
        // Display loader
        $('.ajax-loader').show();
        
        // Get REST URL and post ID from WordPress
        var json_url = postdata.json_url;
        console.log(json_url);

        // The AJAX
        $.ajax({
            dataType: 'json',
            url: 'http://wpapi.dev/wp-json/'
        })

        .done(function(response){

            // Display "Related Posts:" header
            $('#query_result').html('');
            $('#query_result').append('<h1 class="related-header">Results:</h1>');

            console.log(response);

            var ht = '<ul>';
            var routes = response.routes;
             $.each(routes, function(index, object) {
                    ht += '<li>' + index + '</li>';

            });
            ht += '</ul>'

             $('#query_result').append( ht );
              $('.ajax-loader').remove();
        })

        .fail(function(){
            // Hide loader
            $('.ajax-loader').remove();
            // If something goes wrong, say so
            $('#related-posts').append('<div>Something went wrong</div>');
            console.log('Error');
        })

        .always(function(){

        });

    });











    
})(jQuery);
