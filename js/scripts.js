
$(function() {
	window.addEventListener('push', snap);
	var desktop = true,
			viewport = 900,
			width = $(window).width();
	if (width < viewport) {
		if (!window.snapper) {
			snap();
		}
		desktop = false;
	}
	$(window).resize(function() {
		if ($(this).width() != width) {
			width = $(this).width();
			if ((width < viewport) && desktop) {
				if (!window.snapper) {
					snap();
				}
				else {
					window.snapper.enable();
				}
				desktop = false;
			}
			else if ((width >= viewport) && !desktop) {
				if (window.snapper) {
					if (snapper.state().state == 'right') {
						window.snapper.close();
					}
					window.snapper.disable();
				}
				desktop = true;
			}
		}
	});
});
var snap = function() {
		window.snapper = new Snap({
			element: document.getElementById('page'),
			disable: 'left'
		});
		var addEvent = function addEvent(element, eventName, func) {
			if (element.addEventListener) {
					return element.addEventListener(eventName, func, false);
				}
				else if (element.attachEvent) {
					return element.attachEvent('on' + eventName, func);
				}
		};
		addEvent(document.getElementById('snap-open'), 'click', function() {
			if (snapper.state().state == 'right') {
				window.snapper.close();
			}
			else {
				window.snapper.open('right');
			}
		});
		addEvent(document.getElementById('snap-close'), 'click', function() {
			window.snapper.close();
		});
		(function (a, b, c) {
				if (c in b && b[c]) {
						var d, e = a.location,
								f = /^(a|html)$/i;
						a.addEventListener('click', function (a) {
								d = a.target;
								while (!f.test(d.nodeName)) d = d.parentNode;
								'href' in d && (d.href.indexOf('http') || ~d.href.indexOf(e.host)) && (a.preventDefault(), e.href = d.href)
						}, !1)
				}
		})(document, window.navigator, 'standalone');
}
