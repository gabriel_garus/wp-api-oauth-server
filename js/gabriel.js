$(document).ready(function()
{
	// console.log('WindowH' + $(window).width());
	if($(window).width() > 1020 )
	{		
		sticky_nav('#main-header');
		nav_drop();
	
	}


	$(window).resize( function() 
	{
		//console.log($(window).width());

		if($(window).width() > 1020 )
		{		
			sticky_nav('#main-header');
			nav_drop();
			$('.sub-menu').each(function(){
				if( $(this).css('display') == 'block' )
				{
					$(this).css('display', 'none');
				}
			});
				
		}
		else
		{
			$(window).off('scroll');

			$('.sub-menu').each(function(){
				if( $(this).css('display') == 'none' )
				{
					$(this).css('display', 'block');
				}
			});


			$('.with-drop').unbind('mouseenter mouseleave');
			$('.with-drop').removeClass('drop-active');
	
		}
	});
});

function sticky_nav(selector)
{
	
	
		
	$(window).on('scroll' , function()
		{
		  	var scrolled = $(this).scrollTop();
		  	var breakPt = $(window).height() - 200;
		  	var head = $(selector);
		  	var headH = head.outerHeight();
		  	var smallBp = headH + 10;

		  	// console.log('scrolled: ' + scrolled);
			// console.log('bp: ' + breakPt + ' // smallBp: ' + smallBp );



			if(scrolled < smallBp )
			{
		  		if(head.hasClass('hidden'))
		  		{
		  			head.removeClass('hidden').fadeIn(5);
		  		}

		  		if(head.hasClass('sticky-top'))
		  		{
		  			head.removeClass('sticky-top');
		  		}
		  	}

		  	if(scrolled >= smallBp && scrolled < breakPt)
		  	{
		  			
		  		if(!head.hasClass('sticky-top') && !head.hasClass('hidden'))
		  		{
		  			head.css('display','none').addClass('sticky-top').addClass('hidden');
		  			//console.log('small scroll main header Faded Out');
		  		}

		  		if(head.hasClass('sticky-show'))
		  		{
		  			head.addClass('hidden').removeClass('sticky-show').fadeOut(400);
		  		}
		  	
		  			
		  	}

		  		if(scrolled >= breakPt)
		  		{
		  			//console.log('big scroll');
			  		if(!head.hasClass('sticky-show'))
		  			{
			  			head.addClass('sticky-show').fadeIn(600);	
			  		}		

			  		if(!head.hasClass('sticky-top'))
			  		{
			  			head.addClass('sticky-top');
			  		}  	
		  		}
		  		
		  });
		
}






function nav_drop()
{	

	if($('.with-drop').hasClass('drop-active'))
	{
		return true;
	}
	else
	{
		$('.with-drop').on('mouseenter mouseleave', function (e) 
		{
				$(this).find('.sub-menu').toggle();
				
		});

		$('.with-drop').addClass('drop-active');
	}
}