<?php
/*
Plugin Name: WP php Clean Up
Plugin URI: 
Description: Clean up wordpress code
Author: Gabriel Garus
Version: 1.0.0.
Author URI: https://github.com/Ryshard
*/

// If this file is called directly, abort.

defined('ABSPATH') or die('Access Denied!');


define( 'PCU_PLUGIN_PATH', plugin_dir_path(__FILE__) );
define( 'PCU_PLUGIN_URL', str_replace('index.php','',plugins_url( 'index.php', __FILE__ )));

include_once(PCU_PLUGIN_PATH . 'admin.inc.php');

$pcu = new php_clean_up_options();




/*
 * Disable the emoji's
 * ref: https://geek.hellyer.kiwi/plugins/disable-emojis/
 * Author: Ryan Hellyer
* Author URI: https://geek.hellyer.kiwi/
 */
function disable_emojis() 
{
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}

function disable_emojis_tinymce( $plugins ) 
{
	return array_diff( $plugins, array( 'wpemoji' ) );
}


$disable_emojis = $pcu->check_option('emoji');

if($disable_emojis)
{
	add_action( 'init', 'disable_emojis' );
}
//--------------------------------------------------------------




// Ref: http://bhoover.com/remove-unnecessary-code-from-your-wordpress-blog-header/

/** To remove the EditURI/RSD link from your header
* <link rel="EditURI" type="application/rsd xml" title="RSD" href="http://bhoover.com/wp/xmlrpc.php?rsd
*/
$disable_rsd = $pcu->check_option('rsd');

if($disable_rsd)
{
	remove_action('wp_head', 'rsd_link');
}
//--------------------------------------------------------------




/** Windows Live Writer Manifest Link
* <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://bhoover.com/wp/wp-includes/wlwmanifest.xml">
*/

$wlwmanifest = $pcu->check_option('wlwmanifest');

if($wlwmanifest)
{
	remove_action( 'wp_head', 'wlwmanifest_link');
}
//--------------------------------------------------------------



/** WordPress Page/Post Shortlinks
* <link rel='shortlink' href="http://bhoover.com/?p=42">
*/

$shortlinks = $pcu->check_option('shortlinks');

if($shortlinks)
{
	remove_action( 'wp_head', 'wp_shortlink_wp_head');
}


//--------------------------------------------------------------


/** WordPress Generator (with version information)
* <meta name="generator" content="WordPress 3.4.2">
*/

$wp_generator = $pcu->check_option('wp_generator');

if($wp_generator)
{
	remove_action('wp_head', 'wp_generator');
}
//--------------------------------------------------------------









$css_id =$pcu->check_option('css_id');
// Remove ID from css link tag

if($css_id)
{
	add_filter( 'style_loader_tag', function( $html, $handle ) {

    return str_replace( " id='$handle-css'", '', $html );
}, 10, 2 );
}

//-----------------------------------------










$thumb_class = $pcu->check_option('thumb_class');

//remove class from thumbs
// http://wordpress.stackexchange.com/questions/82011/remove-image-classes-from-post-thumbnail-output

function thumbs_cleaning($output) 
{
    $output = preg_replace('/class=".*?"/', '', $output);
    return $output;
}

if($thumb_class)
{
	add_filter('post_thumbnail_html', 'thumbs_cleaning');
}


//-----------------------------------------











$yoast_comments =  $pcu->check_option('yoast_comments');


// Remove All Yoast HTML Comments
// https://gist.github.com/paulcollett/4c81c4f6eb85334ba076

if($yoast_comments)
{
	if (defined('WPSEO_VERSION'))
	{
	  add_action('get_header',function (){ ob_start(function ($o){
	  return preg_replace('/\n?<.*?yoast.*?>/mi','',$o); }); });
	  add_action('wp_head',function (){ ob_end_flush(); }, 999);
	}
}

//-----------------------------------------












// Flush Bug
// Notice: ob_end_flush(): failed to send buffer of zlib output compression

function flush_bug()
{
  remove_action( 'shutdown', 'wp_ob_end_flush_all', 1 );
}

$flush_bug = $pcu->check_option('flush_bug');

if($flush_bug)
{
	add_action('init', 'flush_bug' );
}





function on_wp_footer_action()
{
    wp_dequeue_script( 'wp-embed' );
}
add_action( 'wp_footer', 'on_wp_footer_action', 9 );

