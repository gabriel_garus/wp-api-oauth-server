<?php


class php_clean_up_options
{
	public $current_fields = array();

	public function __construct()
	{

		$this->set_current_fields();
		$this->initialize();
		
	}
	//----//----//----//----//----//----//----//----//----//----//----//




	private function initialize()
	{
		add_action('admin_menu', array($this, 'register_menu_page') );
		add_action('admin_init', array($this, 'register_options')  );
	}
	//----//----//----//----//----//----//----//----//----//----//----//



	private function set_current_fields()
	{
		$this->current_fields = get_option('pcu_options');
	}
	//----//----//----//----//----//----//----//----//----//----//----//


	public function register_menu_page() 
	{

 		add_submenu_page( 'options-general.php', 
 					'PHP Clean Up', 
 					'Clean Up', 
 					'manage_options', 
 					'php_clean_up', 
 					array( $this, 'options_display' ) 
 					); 

	}

	//----//----//----//----//----//----//----//----//----//----//----//




	public function options_display()
	{
		


		echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
		echo '<h2>PHP Clean Up</h2>';

		echo '<form action="options.php" method="post">';

		settings_fields('pcu_options');

	  	echo '<table>';
	    echo '<tr>';
	    echo '<th> Option </th>';
	    echo '<th> On </th>';
	    echo '</tr>' . PHP_EOL;


	    $this->pcu_tick_row('Disable Emoji', 'emoji');
	    $this->pcu_tick_row('Disable Rsd Link ', 'rsd');
	    $this->pcu_tick_row('Disable wlwmanifest Link ', 'wlwmanifest');
	    $this->pcu_tick_row('Disable Page/Post Shortlinks ', 'shortlinks');
	    $this->pcu_tick_row('Disable wp_generator ', 'wp_generator');
	    $this->pcu_tick_row('remove class from thumbnails', 'thumb_class');
	    $this->pcu_tick_row('Remove ID from css link tag', 'css_id');
	    $this->pcu_tick_row('Solve Flush Bug', 'flush_bug');
	    $this->pcu_tick_row('Remove All Yoast HTML Comments', 'yoast_comments');
//
	    echo '</table>' . PHP_EOL;

	    echo '<input type="submit" value="Save" />' . PHP_EOL;
	    echo '</form>' . PHP_EOL;

		echo '</div>';
	}
	//----//----//----//----//----//----//----//----//----//----//----//


	private function pcu_tick_row($label, $opt_name )
	{
		$current_fields = $this->current_fields;

		if(is_array($current_fields) AND in_array($opt_name, $current_fields) )
		{
			$checked = ' checked';
		}
		else
		{
			$checked = null;
		}
		echo '<tr>' . PHP_EOL;
		echo '<td>'.$label.'</td>';

		echo '<td>';
		echo '<input type="checkbox" name="pcu_options[]" value="'.$opt_name.'"';
		echo $checked;
		echo ' />';
	  	echo '</td>';

		echo '</tr>' . PHP_EOL;
	}


	public function register_options()
	{
		register_setting('pcu_options','pcu_options');
	}

	public function check_option($option)
	{
		$current_fields = $this->current_fields;

		if(is_array($current_fields) AND in_array($option, $current_fields) )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

//----//----//----//----//----//----//----//----//----//----//----//
//----//----//----//----//----//----//----//----//----//----//----//
//----//----//----//----//----//----//----//----//----//----//----//
}













