<?php


class css_wizard_options
{
	public $plugin_data = array();
	public $stylesheets = array();

	public $css_queue = array(); 

	public function __construct()
	{

		$this->plugin_data = get_option('css_wizard_options');
		$this->css_queue = $this->get_queue();
		$this->initialize();
		
	}
	//----//----//----//----//----//----//----//----//----//----//----//




	private function initialize()
	{
		add_action('admin_menu', array($this, 'register_menu_page') );
		add_action('admin_init', array($this, 'register_options')  );
		add_action('wp_enqueue_scripts', array($this, 'gather_stylesheets') , 99);
	}
	//----//----//----//----//----//----//----//----//----//----//----//






	public function register_menu_page() 
	{

 		add_submenu_page( 'options-general.php', // parent_slug
 					'CSS Wizard', //page_title
 					'CSS Wizard', // menu_title
 					'manage_options', // capability
 					'css_wizard', // menu_slug
 					array( $this, 'options_display' ) 
 					); 

	}

	//----//----//----//----//----//----//----//----//----//----//----//

	public function gather_stylesheets()
	{
		if(!empty($_GET['css_wizard']) AND $_GET['css_wizard'] == 'queue_scan')
		{
			global	$wp_styles;
			$styles = $wp_styles;

			$data = get_option('css_wizard_options');

			$data['queue'] = $styles->queue;
			
			//error_log('Existing Data: ' . print_r($existing_data, true));

			update_option( 'css_wizard_options' , $data );

		}
		
		return true; 
	}


	public function get_queue()
	{	
		$options = $this->plugin_data;
		if(empty($options))
		{
			$options = get_option('css_wizard_options');
		}

		if(!empty($options['queue']))
		{
			return $options['queue'];
		}
		else
		{
			return array();
		}
	}


	public function options_display()
	{
		


		echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
		echo '<h2>CSS Wizard</h2>';

		echo '<form action="options.php" method="post">';
		settings_fields('css_wizard_options');

		$styles = $this->css_queue;

		
		echo '<input type="text" name="css_wizard_options[test_value]" />';
	  	echo '<table>';
	    echo '<tr>';
	    echo '<th> Style </th>';
	    echo '<th> Switch </th>';
	    echo '</tr>' . PHP_EOL;
	    if(!empty($styles))
		{
			foreach ($styles as $slug)
			{
				$this->style_tick_row( $slug );
			}
		}

	  //  
	  
//
	    echo '</table>' . PHP_EOL;

	    echo '<input type="submit" value="Save" />' . PHP_EOL;
	    echo '</form>' . PHP_EOL;

		echo '</div>';
	}
	//----//----//----//----//----//----//----//----//----//----//----//


	private function style_tick_row( $opt_name )
	{

		echo '<tr>' . PHP_EOL;
		echo '<td>'.$opt_name.'</td>';

		echo '<td>';
		echo '<label>On ';
		echo '<input type="radio" name="css_wizard_options[styles-list]['. $opt_name .']" value="on" />';
		echo '</label>';
		echo '<label>Off ';
		echo '<input type="radio" name="css_wizard_options[styles-list]['. $opt_name .']" value="off" />';
		echo '</label>';
	  	echo '</td>';

		echo '</tr>' . PHP_EOL;
	}


	public function register_options()
	{
		register_setting('css_wizard_options','css_wizard_options');
	}

	public function check_option($option)
	{
		$current_fields = $this->current_fields;

		if(is_array($current_fields) AND in_array($option, $current_fields) )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

//----//----//----//----//----//----//----//----//----//----//----//
//----//----//----//----//----//----//----//----//----//----//----//
//----//----//----//----//----//----//----//----//----//----//----//
}













