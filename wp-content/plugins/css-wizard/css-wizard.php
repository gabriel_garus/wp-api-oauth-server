<?php
/*
Plugin Name: Css Wizard
Plugin URI: 
Description: Control your CSS files
Author: Gabriel Garus (Rysgard)
Version: 1.0.0.
Author URI: https://github.com/Ryshard
*/

// If this file is called directly, abort.
defined('ABSPATH') or die('Access Denied!');


define( 'CSSWIZ_PLUGIN_PATH', plugin_dir_path(__FILE__) );
define( 'CSSWIZ_PLUGIN_URL', str_replace('index.php','',plugins_url( 'index.php', __FILE__ )));

include_once(CSSWIZ_PLUGIN_PATH . 'admin.inc.php');

$pcu = new css_wizard_options();
