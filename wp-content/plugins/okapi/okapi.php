<?php
/*
Plugin Name: Okapi
Plugin URI: 
Description: WP REST API endpoint
Author: Gabriel Garus (Rysgard)
Version: 0.1
Author URI: https://github.com/Ryshard
*/

// If this file is called directly, abort.
session_start();

defined('ABSPATH') or die('Access Denied!');

define( 'OKAPI_PLUGIN_PATH', plugin_dir_path(__FILE__) );
define( 'OKAPI_PLUGIN_URL', str_replace('index.php','',plugins_url( 'index.php', __FILE__ )));
define( 'OKAPI_INC', OKAPI_PLUGIN_PATH . '/inc/');

if(!class_exists('bb_rest_api'))
{
	include_once OKAPI_INC . 'bb-rest-api-class.php';
}

if(!class_exists('ggPage'))
{
	include_once OKAPI_INC . 'gg-page-class.php';
}

if(!class_exists('ggPost'))
{
	include_once OKAPI_INC . 'gg-post-class.php';
}

if(!class_exists('ggImage'))
{
	include_once OKAPI_INC . 'gg-image-class.php';
}

if(!class_exists('ggFireBase'))
{
	require_once 'inc/gg-firebase-class.php';
}
include_once OKAPI_INC . 'admin.php';

require_once 'firebase/firebaseLib.php';

require_once 'vendor/autoload.php';

$admin = new okapi_options();

$b_rest = new bb_rest_api();

if(!empty($admin->plugin_data['route_base']))
{
	$b_rest->set_route_base( $admin->plugin_data['route_base'] );
}

if(!empty($admin->plugin_data['api_base']))
{
	$b_rest->set_api_base( $admin->plugin_data['api_base'] );
}





$b_rest->init();



$admin->init();







function okapi_admin_style()
{


	$js_src = OKAPI_PLUGIN_URL . '/js/okapi.js';

	wp_register_script( 'okapi', $js_src, array('jquery'), null, true );
	wp_enqueue_script('okapi');


	   wp_enqueue_style('okapi-style', OKAPI_PLUGIN_URL .'css/okapi.css');
}
add_action('admin_enqueue_scripts', 'okapi_admin_style');
//--------------------------------------------------------------------





function okapi_fire_contact_fields( $contactmethods ) 
{
  $contactmethods['firebase_id'] = 'Firebase User ID';
  return $contactmethods;
}
add_filter('user_contactmethods','okapi_fire_contact_fields',10,1);




add_action( 'save_post', 'okapi_save_to_fire' );

function okapi_save_to_fire($id)
{
		if ( wp_is_post_revision( $id ) )
		return;

		global $b_rest;
		

		$api_base   = $b_rest->rest_prefix; 
		$route_base = $b_rest->namespace;

		if(empty($firebase) OR !is_object($firebase))
		{
			$firebase = new ggFireBase();
		}

		if($firebase->error)
		{
			 error_log('Saving post to Firebase: ' . $firebase->error);
			return;
		}

		$post_type = get_post_type($id);
		$api_url = WP_HOME . '/'. $api_base .'/' . $route_base .'/'.$post_type. '/' . $id ;

		$data = json_decode( file_get_contents($api_url));
		 //error_log('$data: ' . $data);
		$path = '/'.rtrim($post_type, 's') . 's' . '/'. $id;

		$result = $firebase->fire_set($data, $path);

	  // error_log('Saving post: ');
	  // error_log('$result: ' . $result);
	   // error_log('$route_base: ' . $route_base);
}