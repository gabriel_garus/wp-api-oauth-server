<?php
session_start();
require( '../../../wp-blog-header.php');
if(!class_exists('ggFireBase'))
{
	require_once 'inc/gg-firebase-class.php';
}
//echo var_dump($_SESSION);
$domain = $_SERVER['SERVER_NAME'];
if(!empty($_SERVER['HTTPS']) AND $_SERVER['HTTPS'] != 'off')
{
	$scheme = 'https://';
}
else
{
	$scheme = 'http://';
}

$scheme = 'http://';


$home = $scheme . $domain;



$back = $_SERVER['HTTP_REFERER'];

$plugin_data = get_option('okapi_options');

$api_base   = (!empty($plugin_data['api_base'])) ? $plugin_data['api_base'] : null; 
$route_base = (!empty($plugin_data['route_base'])) ? $plugin_data['route_base'] : null;

if(empty($firebase) OR !is_object($firebase))
{
	$firebase = new ggFireBase();
}

// If user not logged in - redirect to login page
if(!$firebase->current_user)
{
	header('Location: ' . '/wp-admin/');
	exit;
}

if($firebase->error)
{
	echo var_dump($firebase->error);
	echo '<br><a href="'.$back.'">Back</a>';
	die('---');
}



if(!empty($_GET['export_post_type']))
{
	$post_type_0 = $_GET['export_post_type'];
	$_SESSION['exported_type'] = $post_type_0;

	$data_url = $home.'/'.$api_base .'/' . $route_base .'/'.$post_type_0.'?per_page=99';
	//error_log('data_url: ' . $data_url );
	$data = json_decode( file_get_contents($data_url));

	$path = '/'.rtrim($post_type_0, 's') . 's';

	$result = $firebase->fire_set($data, $path);

	if($firebase->error)
	{
		$_SESSION['okapi_error'] = $firebase->error;
	}
	else
	{
		$_SESSION['okapi_success'] = $result;
	}

	header('Location: ' . $back);
	exit;
}


if(!empty($_GET['action']) AND $_GET['action'] == 'index')
{	
	if(!empty($plugin_data['post_types']))
	{
		$post_types = $plugin_data['post_types'];
	}
	else
	{
		$post_types = array('post', 'page');
		$message = 'Post Types empty - using default (post and page)';

	}

	$the_index = array();

	foreach ($post_types as $type) 
	{
		$posts = get_posts(array('post_type' => $type, 'posts_per_page' => -1));
		$type_index = array();

		foreach($posts as $post)
		{
			$type_index[$post->post_name] = $post->ID;
		}

		$the_index[$type] = $type_index;

	}

	echo var_dump($the_index);
	
}


//----------------------------









//var_dump($result);








// --- storing a string ---
//$firebase->set(DEFAULT_PATH . '/name/contact001', "John Doe");

// --- reading the stored string ---
//$name = $firebase->get(DEFAULT_PATH . '/name/contact001');