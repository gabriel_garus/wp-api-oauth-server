<?php

class bb_rest_api
{

	public $namespace = '';
	public $rest_prefix = '';
	public $post_types = array();
	public $custom_post_types = array();
	private $db_prefix = '';
	
	public $home_id = 0;
	public $blog_id = 0; 

	public $query_atts = '';
	public $query_args = '';
	public $query_params = '';


	public function __construct()
	{
		
		
		global $table_prefix;
		$this->db_prefix = $table_prefix;

		$this->blog_id = get_option('page_for_posts');  
		$this->home_id = get_option('page_on_front');

		return true;
	}




	public function init()
	{
		//add_action( 'rest_api_init', array( $this, 'register_fields'));
		add_action( 'rest_api_init', array( $this, 'register_routes'));
		
		if(!empty($this->rest_prefix))
		{
			add_filter( 'rest_url_prefix', array( $this,'return_api_prefix'));
		}
		
		add_action('init', array( $this, 'init_actions'));

		
	}


	public function init_actions()
	{
		if(is_admin())
		{
			flush_rewrite_rules();
		}
		
	}

	public function set_route_base( $base )
	{	
		$new_namespace = trim( strtolower( $base ));
		$this->namespace = $new_namespace;

	}


	public function set_api_base( $base )
	{	
		$new_rest_prefix = trim( strtolower( $base ));
		$this->rest_prefix = $new_rest_prefix;

	}




	public function return_api_prefix()
	{	
		//error_log('api prefixos: '.  $this->rest_prefix);
		return $this->rest_prefix;
	}

	public function register_routes()
	{	
		//error_log('Post types: ' . print_r( get_post_types(), true));
		$this->post_types = get_post_types();
		$this->custom_post_types = get_post_types(array( '_builtin' => false ));
		if(sizeof($this->post_types) > 0)
		{
			
			foreach ($this->post_types as $type)
			{
					if($type == 'page' || $type == 'post')
					{
						//$type .= 's';
					}

					register_rest_route( $this->namespace, '/' . $type . '/', array(
					'methods' => 'GET',
					'callback' => array($this, 'posts_list'),
					'args' => array('post_type' => $type )
					));

					register_rest_route( $this->namespace, '/' . $type . '/(?P<id>\d+)/', array(
					'methods' => 'GET',
					'callback' => array($this, 'general_info'),
					'args' => array('post_type' => $type )
					));


					register_rest_route( $this->namespace, '/' . $type . '/search/', array(
					'methods' => 'GET',
					'callback' => array($this, 'search'),
					'args' => array('post_type' => $type )
					));
			}

			
		}
	


	}

    /*


	public function register_fields()
	{ }


    */






	public function search($request)
	{
		//error_log('Request: ' .print_r($request, true)  );
		
		$params = $request->get_params();
		if(empty($params['q']))
		{
			return new WP_Error('missing parameter', 'Search cannot start - parameter q is required in query string');
		}

		$query = $params['q'];
		
		global $wpdb;


		$result = array();


		$meta_sql  = $wpdb->prepare( 'SELECT post_id FROM '.$this->db_prefix .'postmeta WHERE meta_value LIKE %s', '%%'.$query .'%%');
		$meta_rows = $wpdb->get_results( $meta_sql );

		foreach ($meta_rows as $post) 
		{
			$id = $post->post_id;

			if(!in_array($id, $result))
			{
				$result[] = $id;
			}
		}
	
		$posts_sql = array();
		$posts_sql[] = $wpdb->prepare( 'SELECT ID FROM '.$this->db_prefix .'posts WHERE post_content LIKE %s', '%%'.$query .'%%');
		$posts_sql[] = $wpdb->prepare( 'SELECT ID FROM '.$this->db_prefix .'posts WHERE post_title LIKE %s', '%%'.$query .'%%');
		$posts_sql[] = $wpdb->prepare( 'SELECT ID FROM '.$this->db_prefix .'posts WHERE post_excerpt LIKE %s', '%%'.$query .'%%');

		
		foreach ($posts_sql as $the_sql) 
		{
			$posts_rows = $wpdb->get_results( $the_sql );
			error_log('result: ' . print_r( $posts_rows, true));
			
			foreach ($posts_rows as $post) 
			{
				$id = $post->ID;

				if(!in_array($id, $result))
				{
					$result[] = $id;
				}
			}

		}
		
		error_log('result: ' . print_r( $result, true));



		$final = array();

		$atts = $request->get_attributes();
		$args = $atts['args'];
		$post_type = (!empty($args['post_type'])) ? $args['post_type'] : 'page';

		foreach ($result as $id)
		{
			$data = $this->get_post_data( $id );
			$data['lang'] = $this->get_lang_info($id, $post_type);

			error_log('data type: ' . $data['post_type'] . ' -- get_type: ' . $post_type);
			if($data['post_type'] == $post_type )
			{
				$final[] = $data;
			}
			
		}

		if(empty($final))
		{
			return new WP_ERROR('Nothing Found', 'No results have been found in post type of: <'.$post_type .'>, for query: <'.$query.'>');

		}

		return $final;

	}










	public function posts_list( $request )
	{
		$atts = $request->get_attributes();
		$args = $atts['args'];
		$params = $request->get_params();

		$this->query_atts = $atts;
		$this->query_args = $args; 
		$this->query_params = $params;


		$post_type = (!empty($args['post_type'])) ? $args['post_type'] : 'page';
		$per_page = (!empty($args['per_page'])) ? $args['per_page'] : 10;
		$per_page = (!empty($params['per_page'])) ? $params['per_page'] : $per_page;

		$query_args = array(
			'post_type' => $post_type,
			'suppress_filters' => 0,
			'posts_per_page' => $per_page

			);

		if( !empty( $params['slug'] ) )
		{
			$query_args['name'] = $params['slug'];
		}

		$posts = get_posts( $query_args );

		$out = array();
		foreach ($posts as $post) 
		{
			$id = $post->ID;
			$info_A = $this->get_post_data( $id );
			$info_A['lang'] = $this->get_lang_info($id, $post_type);

			$out[$post->ID] =  $info_A;
		
		}

		return $out; 

	}

	//public function 

	public function general_info( $request )
	{
		//error_log('Request: ' .print_r($request, true)  );
		$atts = $request->get_attributes();
		$args = $atts['args'];
		$params = $request->get_params();

		$this->query_atts = $atts;
		$this->query_args = $args; 
		$this->query_params = $params;

		$post_type = (!empty($args['post_type'])) ? $args['post_type'] : 'page';

		//error_log('Post type: ' . print_r( $post_type, true));
		$id = $request['id'];

		if(!empty($request['lang']))
		{
			$tr_id = icl_object_id( $id, $post_type, false, $request['lang'] );

			if($tr_id)
			{
				$id = $tr_id;
			}

		}

		$info = $this->get_post_data( $id );
	//	$info['lang'] = $this->get_lang_info($id, $post_type);

		return $info; 
	}



	/*
	*   Collect and prepare post/page data
	*
	*/

	public function get_post_data($id)
	{
		$info = array();

		$atts   = $this->query_atts;
		$args   = $this->query_args;
		$params = $this->query_params;

		//error_log('params: ' . print_r($params, true));
		$page = new ggPost($id);
		$raw  = $page->meta_raw;
		$post = $page->post;

		$th_image = new ggImage($page->thumbnail);
		
		$th_image_sizes = $th_image->all_urls;




		$yoast = $this->get_yoast_from_meta($raw);

		
		$info['id'] = $id;
		$info['post_type'] = $page->post_type;
		$info['slug'] = $page->slug;

		if($page->post_type == 'post' || in_array($page->post_type , $this->custom_post_types))
		{
			$info['date']          = $post->post_date;
			$info['date_gmt']      = $post->post_date_gmt;
			$info['modified']      = $post->post_modified;
			$info['modified_gmt']  = $post->post_modified_gmt;
		
			$tags =	get_the_tags($id);
			if($tags)
			{
				$info['tags'] = array();
				//$info['tags'] = $tags ;
				foreach ($tags as $tag) 
				{
					$info['tags'][$tag->name] = $tag->slug;
				}
			}




			$terms = wp_get_post_terms($id, array('category', 'industry'));

			//error_log('Terms: ' . print_r($terms, true));
			if(!is_wp_error($terms) AND !empty($terms[0]))
			{
				$info['category'] = $terms[0]->slug;
			}
				

			if($terms)
			{
				$info['categories'] = array();
				//$info['categories'] = $terms;
				foreach ($terms as $term)
				{
					$info['categories'][$term->name] = $term->slug;
				}
			}

			$info['author'] = array(
				'name_first' => $page->author['first_name'],
				'name_last'  => $page->author['last_name']
				);
			

		}
		//--------------------


		$info['title']     = strip_tags( $page->title );
		$info['excerpt']   = strip_tags( $page->excerpt );
		$info['content']   = $page->content;

		if( $post->post_parent > 0 )
		{
			$info['parent'] = get_permalink( $post->post_parent );
		}


		


	//	$info['post']      = $post;
		$info['meta']      = array();

		$info['meta']['title'] = (!empty($yoast['title'])) ? $yoast['title'] : $page->title;

		$info['meta']['tags'] = array();
		$info['meta']['tags']['description'] = (!empty($yoast['description'])) ? $yoast['description'] : $page->excerpt;

		$robots = '';

		if(!empty($yoast['robots-index']))
		{
			$robots .= $yoast['robots-index'];
		}

		if(!empty($yoast['robots-follow']))
		{
			$robots .= ' '. $yoast['robots-follow'];
		}

		$info['meta']['tags']['robots'] = $robots;


		$info['meta']['properties'] =  array(
		'og:title'            => (!empty($yoast['og-title'])) ? $yoast['og-title'] : $info['meta']['title'],
		'og:description'      => (!empty($yoast['og-description'])) ? $yoast['og-description'] : $info['meta']['tags']['description'],
		//'og:image'            => (!empty($yoast['og-image'])) ? $yoast['og-image'] : $th_image_sizes['full'],
		'twitter:title'       => (!empty($yoast['twitter-title'])) ? $yoast['twitter-title'] : $info['meta']['title'],
		'twitter:description' => (!empty($yoast['twitter-description'])) ? $yoast['twitter-description'] : $info['meta']['tags']['description'],
		//'twitter:image'       => (!empty($yoast['twitter-image'])) ? $yoast['twitter-image'] : $th_image_sizes['full'],
			);
		

		$info['meta']['links'] = array(
			'canonical' => (!empty($yoast['canonical'])) ? $yoast['canonical'] : $page->url,
			'author'	=> $page->author['url']

			);

		$info['meta']['languages'] = $this->get_lang_info( $id, $page->post_type );


		if($id == $this->blog_id)
		{
			$info['posts'] = array();
			$per_page = (!empty($params['per_page'])) ? $params['per_page'] : -1;

			$posts = get_posts(array('posts_per_page' => $per_page));

			foreach ($posts as $post) 
			{
				$info['posts'][] = $this->get_post_data($post->ID);
			}

		}
		$info['sections']  = $page->meta;
		$info['thumbnail']  = $th_image_sizes;
		return $info;
	}

	public function get_yoast_from_meta($raw)
	{
		$info = array();
		//error_log('RAW: ' . print_r($raw, true));


		$yoast_meta_map = array(
			'title'               => '_yoast_wpseo_title',
			'description'         => '_yoast_wpseo_metadesc',
			'og-title'	          => '_yoast_wpseo_opengraph-title',
			'og-description'      => '_yoast_wpseo_opengraph-description',
			'og-image'            => '_yoast_wpseo_opengraph-image',
			'twitter-title'       => '_yoast_wpseo_twitter-title',
			'twitter-description' => '_yoast_wpseo_twitter-description',
			'twitter-image'       => '_yoast_wpseo_twitter-image',
			'focuskeyword'        => '_yoast_wpseo_focuskw',
			'lindex'              => '_yoast_wpseo_linkdex',
			'canonical'			  => '_yoast_wpseo_canonical',
			'robots-index'		  => '_yoast_wpseo_meta-robots-noindex',
			'robots-follow'		  => '_yoast_wpseo_meta-robots-nofollow',
			);


		foreach ($yoast_meta_map as $key => $value) 
		{
			if(!empty($raw[$value]))
			{
				$info[$key] = $raw[$value][0];
			}
			
		}

		if(!empty($info['robots-index']))
		{
			if($info['robots-index'] == 2)
			{
				$info['robots-index'] = 'index';
			}
			elseif($info['robots-index'] == 1)
			{
				$info['robots-index'] = 'noindex';
			}

		}

		if(!empty($info['robots-follow']) AND $info['robots-follow'] == 1)
		{
			$info['robots-follow'] = 'nofollow';
		}
		else
		{
			$info['robots-follow'] = 'follow';
		}

	
		return $info; 
	}


	public function get_lang_info( $id, $type )
	{
	  //setup_postdata( $object );

	  if(!function_exists('icl_get_languages'))
	  {
	  	return null;
	  }


	  $languages = icl_get_languages('skip_missing=1');
	  $lang_info = array();

	//  error_log('Languages: ' . print_r($languages, true));

	  if(1 < count($languages))
	  {
	     
	    foreach($languages as $key=>$l)
	    {
	      $tr_id = icl_object_id( $id, $type, false, $l['code'] );
	      
	      if($tr_id)
	      {	
	      	$url = get_permalink($tr_id); 
	      	$lang_code = $l['tag'];

	      	 $lang_info[$lang_code] = $url;
	     	 // $lang_info[ $l['code'] ]['translated_url'] = $url;
	     	 // $lang_info[ $l['code'] ]['default_locale'] = $l['default_locale'];
	     	 // $lang_info[ $l['code'] ]['tag'] = $l['tag'];
	     	 // $lang_info[ $l['code'] ]['slug'] = basename( $url );
	      }
	     
	    }
	  
	   
	    return $lang_info; 
	  }
	  else
	  {
	  	return null; 
	  }


	}


}


