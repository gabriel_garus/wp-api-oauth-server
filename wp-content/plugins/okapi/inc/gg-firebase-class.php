<?php


use Firebase\Token\TokenException;
use Firebase\Token\TokenGenerator;

class ggFireBase 
{
	public $current_user = array();
	public $current_user_meta = array();
	public $firebase_user_id = null;
	public $plugin_data = array();
	public $firebase_secret = null;
	public $firebase_url = null;

	private $token = null;

	public $error = null;

	public function __construct()
	{	
		$this->current_user      = $this->get_current_user();
		$this->current_user_meta = $this->get_current_user_meta();
		$this->firebase_user_id  = $this->get_firebase_user_id();
		$this->plugin_data 		 = $this->get_plugin_data();
		$this->firebase_secret   = $this->get_firebase_Secret();
		$this->firebase_url 	 = $this->get_firebase_url();
		$this->token 			 = $this->get_token();
	}

	public function get_current_user()
	{
		$wp_user = wp_get_current_user();

		if($wp_user->ID == 0)
		{	
			$this->error = 'WP User not logged in';
			return null; 
		}
		else
		{
			return $wp_user;
		}
	}

	public function get_current_user_meta()
	{
		if($this->current_user)
		{
			return get_user_meta( $this->current_user->ID );
		}
		else
		{
			return null;
		}
	}

	public function get_firebase_user_id()
	{
		if($this->current_user_meta and !empty($this->current_user_meta['firebase_id']))
		{
			return $this->current_user_meta['firebase_id'][0];
		}
		else
		{
			$this->error = 'FireBase User ID is missing in WP_USER fields';
			return null;
		}
	}

	public function get_firebase_Secret()
	{
		$data = $this->plugin_data;
		if(!empty($data['db_secret']))
		{
			return $data['db_secret'];
		}
		else
		{	
			$this->error = 'Firebase Secret is missing';
			return null; 
		}
	}


	public function get_plugin_data()
	{
		$data = get_option('okapi_options');

		return ($data)? $data : null;

	}

	public function get_token()
	{

		
		$error = null; 
		$token = null;

		if(!empty($_SESSION['firebase_token']))
		{
			$token = $_SESSION['firebase_token'];
		}
		else
		{	
			$generator = new TokenGenerator($this->firebase_secret);
		    $generator->setData(array('uid' => $this->firebase_user_id));

			try 
			{
		     	$token = $generator->create();
		     	$_SESSION['firebase_token'] = $token;
			} 
			catch (TokenException $e) 
			{
			     $error = "Token Error: ".$e->getMessage();
			}

		}

		if($error === null AND $token !== null)
		{
			return $token;
		}
		else
		{
			$this->error = $error;
			return false; 
		}
	}


	public function get_firebase_url()
	{
		$data = $this->plugin_data;
		if(!empty($data['firebase_url']))
		{
			return $data['firebase_url'];
		}
		else
		{	
			$this->error = 'Firebase Url is missing';
			return null; 
		}
	}

	public function fire_set($data, $path = '/')
	{
		$firebase = new \Firebase\FirebaseLib($this->firebase_url, $this->token);

		$response = $firebase->set($path, $data);

		$result = json_decode($response);

		if(!empty($result->error))
		{   
			$this->error = $result->error;
			return $result->error;
		}
		else
		{
			return 'Success - ' . count((array)$result) . ' entries added';
		}
	}
	

}