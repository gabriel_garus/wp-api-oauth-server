<?php


class okapi_options
{
	public $plugin_data = array();
	public $post_types = array();

	public function __construct()
	{
		$data = get_option('okapi_options');
		$this->plugin_data = $data;
		$this->post_types = $data['post_types'];
		
		
	}
	//----//----//----//----//----//----//----//----//----//----//----//




	public function init()
	{
		add_action('admin_menu', array($this, 'register_menu_page') );
		add_action('admin_init', array($this, 'register_options')  );

		add_action( 'wp_loaded', array($this, 'after_load')  );
	}
	//----//----//----//----//----//----//----//----//----//----//----//




	public function after_load()
	{

		$data = $this->plugin_data;

		$data['post_types'] = get_post_types();

		update_option( 'okapi_options', $data); 

	}

	public function register_menu_page() 
	{

 		add_submenu_page( 'options-general.php', // parent_slug
 					'Ok-Api', //page_title
 					'OkApi', // menu_title
 					'manage_options', // capability
 					'okapi', // menu_slug
 					array( $this, 'options_display' ) 
 					); 

	}

	//----//----//----//----//----//----//----//----//----//----//----//



	public function options_display()
	{
		session_start();
		$data = $this->plugin_data;
		$post_types = $this->post_types;

		if(empty($post_types ))
	    {	
	        	
	    	echo 'Saving....';
	    	echo '<script>window.location = "/wp-admin/options-general.php?page=okapi"; </script>';

	    	return;
	    }
		//error_log('Okapi Data: ' . print_r( $data, true ) );


		echo '<div id="okapi" class="wrap"><div id="icon-tools" class="icon32"></div>';

		echo '<img id="okapi-img" src="'.OKAPI_PLUGIN_URL.'/img/okapi_doodle_by_mbpanther-d4hp7o7.png" ';
		echo 'alt="image credit: mbpanther.deviantart.com/art/Okapi-Doodle-271594519" width="180" height="60">';
		echo '<h2 class="o-title">Okapi - WP REST API custom interface</h2>';


if(!is_plugin_active('rest-api/plugin.php'))
{
	echo '<h2> WP REST API Plugin is not active</h2>';
	echo '<p>Please Install WP REST API plugin (v2)</p>';
	echo '<br><a href="https://wordpress.org/plugins/rest-api/" target="_blank">https://wordpress.org/plugins/rest-api/</a>';
	return; 
}

		echo '<h3>Settings:</h3>';
		echo '<form action="options.php" method="post">';
		settings_fields('okapi_options');

		echo $this->text_input( 'api_base', 'Rest API Base' );
		echo '<br>';

		echo $this->text_input( 'route_base', 'Route Base' );

		echo '<hr />';

		echo $this->text_input( 'firebase_url', 'Firebase Url Base' );
		
		echo $this->text_input( 'db_secret', 'Firebase Secret' );

	// echo $this->text_input( 'user_id', 'User Id (UID)' );

		echo '<br />';
	    echo '<input type="submit" value="Save" />' . PHP_EOL;
	    echo '</form>' . PHP_EOL;
	
		echo '<br />';

		
		echo '<hr />';
		echo '<br />';
		//echo var_dump($_SESSION);


	    $rest_api_base = (!empty($data['api_base'] )) ? $data['api_base'] : 'wp-json';
	    $route_base = (!empty($data['route_base'] )) ? $data['route_base'] : 'wp/v2';

	    $base = WP_HOME . '/'.$rest_api_base . '/' . $route_base .'/';
	    $not_show = array(
	    		'attachment',
	    		'revision',
	    		'nav_menu_item',
	    		'json_consumer',
	    		'wpcf7_contact_form'
	    	);
	    //echo var_dump($post_types);

	    foreach ($post_types as $type) 
	    {
	    	if(!in_array($type,  $not_show))
	    	{
	    		echo '<a href="'.OKAPI_PLUGIN_URL.'export.php?';
	    		echo 'export_post_type='.$type;
	    		echo '">Export '.$type .'</a>';


	    		if( !empty($_SESSION['okapi_success']) 
	    			AND !empty($_SESSION['exported_type']) 
	    			AND $_SESSION['exported_type'] == $type)
	    		{
	    			echo '<span style="color: green;"> Success - ' .$_SESSION['okapi_success'] . '</span>';
	    			unset($_SESSION['okapi_success']);

	    		}
	    		if( !empty($_SESSION['okapi_error']) 
	    			AND !empty($_SESSION['exported_type']) 
	    			AND $_SESSION['exported_type'] == $type)
	    		{
	    			echo '<span style="color: red;"> Error - ' .$_SESSION['okapi_error'] . '</span>';
	    			unset( $_SESSION['okapi_error']);

	    		}
	    		echo '<br><br>';

	    	}
	    }
	    $_SESSION['exported_type'] = null;


	    echo '<a href="'.OKAPI_PLUGIN_URL.'export.php?';
	    		echo 'action=index';
	    		echo '">Save Index</a>';

	    echo '<h3>Routes</h3>';
	    echo '<ul>';

	    foreach ($post_types as $type) 
	    {
	    	if(!in_array($type,  $not_show))
	    	{
	    		$sample_posts = get_posts(array(
	    				'post_type' => $type,
	    				'posts_per_page' => 1
	    			));
	    		//echo var_dump($sample_posts);
	    		$sample_post = $sample_posts[0];

	    		echo '<li>';
	    		echo '<h3>'. $type . '</h3>';

	    		echo '<ul>';

	    		echo '<li class="querer">';
	    		echo '<strong>List: </strong> ';
	    		echo '<span class="query">';
	    		echo '<a target="_blank" href="' . $base . $type . '/'. '">';
	    		echo $base . $type . '/';
	    		echo '</span></a>';
	    		//echo '<input class="add-on" type="hidden" value=""/>';
	    		echo '<br />';
	    		//echo '<button class="send">Send</button>';
	    		echo '</li>';


	    		echo '<li class="querer">';
	    		echo '<strong>By Id:</strong> ';
	    		echo '<span class="query">';
	    		echo '<a target="_blank" href="' . $base . $type . '/'. $sample_post->ID . '">';
	    		echo $base . $type . '/' . $sample_post->ID;
	    		echo '</span></a>';
	    		//echo '<input class="add-on" value=""/>';
	    		echo '<br />';
	    		//echo '<button class="send">Send</button>';
	    		echo '</li>';


	    		echo '<li class="querer">';
	    		echo '<strong>By Slug:</strong> ';
	    		echo '<span class="query">';
	    		echo '<a target="_blank" href="' . $base . $type . '/?slug=' . $sample_post->post_name . '">';
	    		echo $base . $type . '/?slug=' . $sample_post->post_name;
	    		echo '</span></a>';
	    		//echo '<input class="add-on" value="" />';
	    		echo '<br />';
	    		//echo '<button class="send">Send</button>';
	    		echo '</li>';



	    		echo '</ul>';
	    		echo '<hr />';
	    		echo '</li>';
	    	}
	    }
	    echo '</ul>';

	    $home_path = get_home_path();


	    $img_src = 'https://s3-eu-west-1.amazonaws.com/www.bigbang.ie/img/m7.jpg';
	   	$img_path = $home_path .'/img/m7.jpg';
	    echo '<img src="'.$img_src.'" width="200" />';

	    $img_cont = file_get_contents($img_src);
	    $img_64 = base64_encode($img_cont);

	    $new_src = 'data: '.mime_content_type($img_path).';base64,'.$img_64;

	    echo var_dump($new_src);

	    echo '<img src="' .$new_src .'" />';

	    echo '<div id="result"></div>';
		echo '</div>';
	}
	//----//----//----//----//----//----//----//----//----//----//----//


	private function text_input( $opt_name, $label )
	{
		$data = $this->plugin_data;
		$h = '';
		$h .= '<label for="'.$opt_name .'">' . $label  . '</label>';
		$h .= '<input type="text" name="okapi_options['.$opt_name.']" id="'.$opt_name.'" ';
		if(!empty($data[$opt_name]))
		{
			$h .=  'value="' . $data[$opt_name] .'"';
			$_SESSION[$opt_name] = $data[$opt_name];
		}
		else
		{
			if($_SESSION[$opt_name])
			{
				unset($_SESSION[$opt_name]);
			}
		}
		$h .=  '/>';
	
		return $h;
		
	}


	public function register_options()
	{
		register_setting('okapi_options','okapi_options');
	}

	public function check_option($option)
	{
		$current_fields = $this->current_fields;

		if(is_array($current_fields) AND in_array($option, $current_fields) )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

//----//----//----//----//----//----//----//----//----//----//----//
//----//----//----//----//----//----//----//----//----//----//----//
//----//----//----//----//----//----//----//----//----//----//----//
}













