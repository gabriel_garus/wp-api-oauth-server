<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Single services
*/


if ( function_exists( 'wpcf7_enqueue_scripts' ) ) 
{
   wpcf7_enqueue_scripts();
}

get_header();


$bbPage = new bbPage( PAGE_ID );



echo '<div class="single-career">' . PHP_EOL;



echo '<div class="section content">' . PHP_EOL;
echo '<div class="container cf">' . PHP_EOL;

the_breadcrumb();


echo '<div class="left">' . PHP_EOL;

echo '<div class="post-content">' . PHP_EOL;

bb_title($bbPage->title,1);
bb_content($bbPage->content);

echo '</div>'; // .post-content
echo '</div>'; // .left


echo '</div>'; // .container.cf
echo '</div>' . PHP_EOL; // .section.content



////////////////////////////////////////////////////////////////////////

echo '<div class="section apply">' . PHP_EOL;
echo '<div class="container cf">' . PHP_EOL;

echo '<div class="left">' . PHP_EOL;
echo '<div class="post-content">' . PHP_EOL;

$title = $bbPage->get_field('apply','title');
$intro = $bbPage->get_field('apply', 'text');

$form_id = $bbPage->get_field('apply', 'form_id');

bb_title($title,2);

if($intro)
{
	echo '<p>'.$intro .'</p>' . PHP_EOL;
}


if($form_id)
{
	$formH =  '[contact-form-7 id="'. $form_id .'" ]';
	$apply_form = do_shortcode($formH);

	echo $apply_form; 
}


echo '</div>'; // .post-content
echo '</div>'; // .left
echo '</div>'; // .container.cf
echo '</div>' . PHP_EOL;// .section.content

//--------------------------------


include_once PARTIALS_DIR . 'cta.php';

echo '</div>' . PHP_EOL; // .single-career


get_footer();




