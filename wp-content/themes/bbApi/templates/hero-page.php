<?php
defined('ABSPATH') or die('Access Denied!');
/**
*Template Name: Hero Page
 */

 

get_header();

if(!isset($bbPage))
{
  $bbPage = new bbPage(PAGE_ID);
}


include_once PARTIALS_DIR . 'hero.php';

$text =  $bbPage->get_field('middle', 'text');

if($text !== null)
{
	echo '<div class="section full-width">' . PHP_EOL;
	echo '<div class="container">' . PHP_EOL;

	echo do_shortcode(wpautop( $text ) );

	echo '</div></div>' . PHP_EOL;

}




get_footer();