<?php
defined('ABSPATH') or die('Access Denied!');

/*
*Template Name: Careers
 */

get_header();


$bbPage = new bbPage(PAGE_ID) ;


echo '<div class="careers-page">' . PHP_EOL;


//--------------------------------

include_once PARTIALS_DIR . 'hero.php';





/////////  JOBS LIST ///////////////////////////////////////////////////

$jobs_heading = $bbPage->get_field('jobs_list', 'title');
$jobs_nr =  $bbPage->get_field('jobs_list', 'nr');


$jobs = get_posts(array(
		'post_type' => 'careers',
		'posts_per_page' => $jobs_nr
	));

//dump($jobs);

if(is_array($jobs) AND sizeof($jobs) > 0)
{	

	echo '<div id="careers-list" >' . PHP_EOL;
	echo '<div class="container">' . PHP_EOL;

	bb_title($jobs_heading, 2);

	//echo '<h2></h2>' . PHP_EOL;
	$i = 0; 
	foreach($jobs as $job)
	{
		$jobPost = new bbPost($job->ID);
		if(($i+1) % 2 == 0)
		{
			$even = true;
			$art_class = 'right';
		}
		else
		{
			$even = false;
			$art_class = 'left';
		}

		if(!$even)
		{
			echo '<div class="list cf news-feed">';
		}

		//
		echo '<article class="'.$art_class.' job-list-item">';
		if(!empty($jobPost->thumbnail) AND $jobPost->thumbnail != '0')
		{
			$img = new bbImage($jobPost->thumbnail);
			echo $img->html;
		}
		echo '<h4>' . $jobPost->leadin_title .'</h4>'. PHP_EOL;
		echo '<div class="post-date"> ' . PHP_EOL;
		echo 'Posted: ' . $jobPost->get_date() . PHP_EOL;
		echo '</div>' . PHP_EOL;
		echo '<p>'.$jobPost->excerpt . '</p>' . PHP_EOL;

		echo '<div class="read-more">';
		echo '<a href="'.$jobPost->url .'" class="">Read More</a>';
		echo '</div>'. PHP_EOL; 
		echo '</article>' . PHP_EOL;

		if($even OR ($i+1) == count($jobs))
		{
			echo '</div>';
		}

		$i++;
	}

	echo '</div></div>';
}
/////////////////////////////////////////////////////////////////////////////////////////



/////////  MIDDLE  ///////////////////////////////////////////////////


$middle_title = $bbPage->get_field('middle', 'title');
$middle_text  = $bbPage->get_field('middle', 'text');
$video_title  = $bbPage->get_field('middle', 'video_title');
$video_url    = $bbPage->get_field('middle', 'video_url');


if( $middle_title || $middle_text || $video_url)
{
	echo '<div class="section middle">' . PHP_EOL;
	echo '<div class="container">' . PHP_EOL;
	
	if($middle_title)
	{
		bb_title($middle_title, 2);
	}


	if($middle_text)
	{
		echo wpautop( do_shortcode( $middle_text ) );

	}
	


	if($video_url)
	{
		if(!defined('VIDEO_CONTENT'))
		{
			define('VIDEO_CONTENT', true);
		}
	
		bb_title($video_title, 3);
		include_once PARTIALS_DIR . 'video.php';
	}
	
	echo '</div>' . PHP_EOL;
	echo '</div>' . PHP_EOL;
}




/////////////////////////////////////////////////////////////////////////////////////////



/////////  BOTTOM   ///////////////////////////////////////////////////

$bottom_title = $bbPage->get_field('bottom', 'title');
$bottom_text  = $bbPage->get_field('bottom', 'text');

echo '<div class="section bottom">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;
if( $bottom_title || $bottom_text )
{
	bb_title($bottom_title, 3);

	bb_content($bottom_text);
}

echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL;



/////////////////////////////////////////////////////////////////////////////////////////
include_once PARTIALS_DIR . 'cta.php';


echo '</div>' . PHP_EOL; // .careers-page
get_footer();


