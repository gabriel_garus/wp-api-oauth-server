<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Template Name: Team
 */

get_header();


$bbPage = new bbPage(PAGE_ID) ;


//--------------------------------

include_once PARTIALS_DIR . 'hero.php';

$deps = get_terms('department');


$exec_team = get_posts( array ( 'post_type' => 'team', 'posts_per_page' => -1, 'department'=> 'executive') );


echo '<div class="section team-page">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;

if(!empty($exec_team))
{
	echo '<h2>Executive</h2>' . PHP_EOL;

	echo '<div class="grid staff-list">' . PHP_EOL;

	foreach ($exec_team as $person)
	{
		bbTeam($person->ID);
	}

	echo '</div>' . PHP_EOL;

}


foreach ($deps as $dep)
{
	//dump($dep);
	if($dep->slug == 'executive')
	{
		continue;
	}

	$dep_team = get_posts( array ( 'post_type' => 'team', 'posts_per_page' => -1, 'department'=> $dep->slug) );

	if(!empty($exec_team))
	{
		echo '<h2>'. $dep->name .'</h2>' . PHP_EOL;

		echo '<div class="grid staff-list">' . PHP_EOL;

		foreach ($dep_team as $person)
		{
			bbTeam($person->ID);
		}

		echo '</div>' . PHP_EOL;

	}

}

echo '</div></div>' . PHP_EOL;


include PARTIALS_DIR .'cta.php';

get_footer();




