<?php
defined('ABSPATH') or die('Access Denied!');
/**
*Template Name: CPT List 
 */

 

get_header();

if(!isset($bbPage))
{
  $bbPage = new bbPage(PAGE_ID);
}

$post_type = $bbPage->get_field('post-type','id');


echo '<div class="'.$post_type .'-page">' . PHP_EOL;

if(is_file(  PARTIALS_DIR .'hero.php' ))
{
	include_once PARTIALS_DIR . 'hero.php';
}



if($post_type !== null AND !empty($post_type)) : 

$posts = get_posts( array( 'posts_per_page' => -1, 'post_type' => $post_type, 'suppress_filters' => 0  ) );


if(is_file( PARTIALS_DIR . $post_type . '-list.php' ))
{
	include PARTIALS_DIR . $post_type . '-list.php';
}



if(is_file(  PARTIALS_DIR .'cta.php' ))
{
	include PARTIALS_DIR .'cta.php';
}

echo '</div>' . PHP_EOL;

endif;

get_footer();