<?php
defined('ABSPATH') or die('Access Denied!');
/**
 * The template for displaying archive pages.
 * Template Name: Case Studies
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package bigbang
 */



get_header();

$bbPage = new bbPage(PAGE_ID);



include_once PARTIALS_DIR . 'hero.php';

include_once PARTIALS_DIR . 'cs-catlist-2.php';

//include_once PARTIALS_DIR . 'cs-featured.php';



////////////////////////////////////////////////////////////////////////
$args = array(
		'post_type' => 'studies',
		'posts_per_page' => -1, 
	);
$the_query = get_posts( $args );



include_once PARTIALS_DIR . 'cs-posts-2.php';

get_footer();
