<?php
/*
*Template Name: About
*
*/
defined('ABSPATH') or die('Access Denied!');

get_header();

$bbPage = new bbPage(PAGE_ID) ;


include PARTIALS_DIR . 'hero.php';



















/////////////////////      MIDDLE CONTENT      ////////////////////////////////////////////

$middle_title = $bbPage->get_field('middle','title');
$middle_text  = $bbPage->get_field('middle','text');

if( $middle_text != null || $middle_title != null )
{
	echo '<div class="section middle">' . PHP_EOL;
	echo '<div class="container">' . PHP_EOL;

	if($middle_title != null )
	{
		echo '<h3>' . $middle_title . '</h3>';
	}


	if($middle_text != null)
	{
		echo wpautop( do_shortcode( $middle_text ) );
	}

	echo '</div>' . PHP_EOL;
	echo '</div>' . PHP_EOL;

}
/////////////////////      --------------      ////////////////////////////////////////////



/////////////////////      VIDEO SECTION      ////////////////////////////////////////////

$video_url = $bbPage->get_field('video','link');

if($video_url !== null)
{
	if(!defined("VIDEO_CONTENT"))
	{
		define("VIDEO_CONTENT", true);
	}
	echo '<div class="section video-content">' . PHP_EOL;
	echo '<div class="container"><div class="grey-box">' . PHP_EOL;

	include PARTIALS_DIR . 'video.php';

	echo '</div></div>' . PHP_EOL;
	echo '</div>' . PHP_EOL;
}



/////////////////////      --------------      ////////////////////////////////////////////















/////////////////////      BOTTOM CONTENT      ////////////////////////////////////////////

$bottom_title = $bbPage->get_field('bottom','title');
$bottom_text  = $bbPage->get_field('bottom','text');
$bottom_link  = $bbPage->get_field('bottom','cta_custom_link');
$bottom_link_label = $bbPage->get_field('bottom','cta_label');

if($bottom_link === null)
{
	$bottom_link = $bbPage->get_field('bottom','cta_link');

	if($bottom_link !== null)
	{
		$bottom_link =  get_permalink( $bottom_link );
	}
}


if( $bottom_text != null || $bottom_title != null )
{
	echo '<div class="section bottom">' . PHP_EOL;
	echo '<div class="container">' . PHP_EOL;
	if($bottom_title != null )
	{
		echo '<h3>' . $bottom_title . '</h3>';
	}
	echo '<div class="block cf">' . PHP_EOL;

	echo '<div class="left">' . PHP_EOL;
	if($bottom_text != null)
	{
		echo wpautop( do_shortcode( $bottom_text ) );
	}
	echo '</div>' . PHP_EOL; // .left


	echo '<div class="right">' . PHP_EOL;
	if($bottom_link !== null)
	{
		echo '<a href="'.$bottom_link .'" class="btn primary">' . $bottom_link_label .'</a>' . PHP_EOL;
	}
	echo '</div>' . PHP_EOL; //.right


	echo '</div>' . PHP_EOL; // .block cf


	echo '</div></div>' . PHP_EOL; // .container //.section

}
/////////////////////      --------------      ////////////////////////////////////////////











/////////////////////      TEAM SECTION       ////////////////////////////////////////////


$team_intro = $bbPage->get_field('team','intro');
$team_title = $bbPage->get_field('team','title');

$team = array();

for($r=1;$r<=4;$r++)
{
	$tt = $bbPage->get_field('team','team_'.$r);

	if($tt !== null)
	$team[] = $tt;
}



if(sizeof($team) > 0)
{

	echo '<div class="section team">' . PHP_EOL;
	echo '<div class="container">' . PHP_EOL;
	
	if($team_intro !== null)
	{
		echo '<h4>' . $team_intro . '</h4>';
	}

	if($team_title !== null)
	{
		echo '<h3>' . $team_title . '</h3>';
	}



	echo '<div id="team-list" class="the-grid">';

	foreach ($team as $t_id)
	{
		bbTeam($t_id);
	}

	echo '</div>' . PHP_EOL;
	echo '<div class="center block">';
	echo '<a href="/about/team/" class="btn tertiary">See All</a>' . PHP_EOL;
	echo '</div>' . PHP_EOL;
	echo '</div>' . PHP_EOL;
	echo '</div>' . PHP_EOL;
}
/////////////////////      --------------      ////////////////////////////////////////////





/////////////////////      WIDE IMAGE      ////////////////////////////////////////////

$wide_image_id = $bbPage->get_field('wide','image');

if($wide_image_id !== null)
{
	$wide_image = new bbImage($wide_image_id, 'full');

	echo '<div class="section wide" style="background-image: url(\''. $wide_image->src .'\');"></div>' . PHP_EOL;
}

/////////////////////      --------------      ////////////////////////////////////////////





/////////////////////      CAREERS      ////////////////////////////////////////////

$careers_title = $bbPage->get_field('careers','title');

$careers = array();

for($a=1;$a<=2;$a++)
{
	$ca = $bbPage->get_field('careers','job_'. $a);

	if($ca != null)
	$careers[] = $ca;
}



if(sizeof($careers)>0)
{


	echo '<div class="section careers news-feed">' . PHP_EOL;
	echo '<div class="container">' . PHP_EOL;

	if($careers_title)
	{
		echo '<h3>' . $careers_title . '</h3>';
	}
	echo '<div id="careers-list">' . PHP_EOL;

	for ($i=0; $i < sizeof($careers); $i++) 
	{ 
		$career = new bbPost($careers[$i]);

		


		if(($i+1) % 2 == 0)
		{
			$even = true;
			$art_class = 'right';
		}
		else
		{
			$even = false;
			$art_class = 'left';
		}

		if(!$even)
		{
			echo '<div class="list cf">' . PHP_EOL;
		}
		

		echo '<article class="'.$art_class.' job-list-item">' . PHP_EOL;

		if(!empty($career->thumbnail) AND $career->thumbnail != '0')
		{
			$img = new bbImage($career->thumbnail);
			echo $img->html;
		}
		echo '<h4>' . $career->title .'</h4>'. PHP_EOL;
		echo '<div class="post-date"> ' . PHP_EOL;
		echo 'Posted: ' . $career->get_date() . PHP_EOL;
		echo '</div>' . PHP_EOL;
		echo '<p>'.$career->excerpt . '</p>' . PHP_EOL;

		echo '<div class="read-more">';
		echo '<a href="'.$career->url .'" class="">Read More</a>';
		echo '</div>'. PHP_EOL; 
		echo '</article>' . PHP_EOL;

		if($even)
		{
			echo '</div>' ; 
		}
	}
	

	echo '</div>' . PHP_EOL;

	echo '<div class="center block">';
	echo '<a href="/about/careers/" class="btn tertiary">See All</a>' . PHP_EOL;
	echo '</div>' . PHP_EOL;

	echo '</div>' . PHP_EOL;
	echo '</div>' . PHP_EOL;
}

/////////////////////      --------------      ////////////////////////////////////////////




include PARTIALS_DIR . 'cta.php';




get_footer();
