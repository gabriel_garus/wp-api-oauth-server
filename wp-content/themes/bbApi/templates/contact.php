<?php
/*
Template Name: Contact
*/



if ( function_exists( 'wpcf7_enqueue_scripts' ) ) 
{
   wpcf7_enqueue_scripts();
}

get_header();

$bbPage = new bbPage(PAGE_ID) ;


echo '<div class="section contact">'.PHP_EOL;
echo '<div class="container cf">' . PHP_EOL;
the_breadcrumb();

echo '<div class="left">'.PHP_EOL;


$options = bb_get_options();

$the_telephone = $bbPage->get_field('contact','telephone');

if($the_telephone === null)
{
	$the_telephone     = (!empty($options['head_telephone'])) ? $options['head_telephone'] : null;
}


$openHours = $bbPage->get_field('contact','hours');



echo '<h1>'.$bbPage->title .'</h1>' . PHP_EOL;

echo wpautop( do_shortcode( $bbPage->content ) );
//**** Intro





//**************************************************
// ---- Call Us -----------------
echo '<div class="call-us">';

echo '<div class="telephone">';
echo '<h3>Call Us</h3>';
	bb_telephone_link($the_telephone);
echo '</div>'.PHP_EOL; // .telephone

if($openHours !== null)
{
	echo '<div class="opening">' . $openHours . '</div>'.PHP_EOL;
}

echo '</div>'.PHP_EOL; // .call-us

//----------------------





echo '</div>'.PHP_EOL; // .left



$form_id = $bbPage->get_field('the_form','id');
$form_title = $bbPage->get_field('the_form','title');

if($form_id !== null)
{
	echo '<div class="contact-form right">'.PHP_EOL;

	bb_title($form_title,3);

	$formH =  '[contact-form-7 id="'. $form_id .'" ]';
	$hero_form = do_shortcode($formH);

	echo $hero_form;

	echo '<script>';
	echo ' $("#password").removeAttr("type").attr("type","password");';
	echo ' $(".wpcf7-form").removeAttr("name");';
	echo '</script>';

	echo '</div>'.PHP_EOL; // .contact-form

}

echo '</div>' . PHP_EOL; // .container
echo '</div>'.PHP_EOL; //.section



$quote_title  = $bbPage->get_field('the_quote','title');
$quote_text   = $bbPage->get_field('the_quote','text');
$quote_author = $bbPage->get_field('the_quote','author');
$qa_position  = $bbPage->get_field('the_quote','position');


echo '<div class="section contact-quote feature">' . PHP_EOL;
echo '<div class="container cf">' . PHP_EOL;

echo '<div class="left">' . PHP_EOL;

if($quote_title !== null)
{
	echo '<h3>' . $quote_title . '</h3>';
}

if($quote_text !== null)
{
	echo '<div class="the-quote">';
	echo wpautop(do_shortcode( $quote_text ));
	echo '</div>' . PHP_EOL;
}

if($quote_author !== null)
{
	echo '<div class="author">';
	echo '<strong class="name">' . $quote_author .'</strong>';

	if($qa_position !== null)
	{
		echo '<p class="position">' . $qa_position . '</p>';
	}

	echo '</div>';


}

echo '</div>' . PHP_EOL;




echo '<div class="right">';
echo '</div>' . PHP_EOL;

echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL;

$cta_white = true;
include_once PARTIALS_DIR . 'cta.php';


get_footer();
