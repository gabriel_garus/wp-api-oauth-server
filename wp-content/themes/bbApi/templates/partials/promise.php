<?php
defined('ABSPATH') or die('Access Denied!');
/* Cloudstrong Promise Template

*/



if(empty($options))
{
	$options = bb_get_options();
}

$bg_img = array();
if(!empty($options['promise-background']))
{
	$bg_img = $options['promise-background'];
}

echo '<div class="section promise"';

if(!empty($bg_img['url']))
{
	echo ' style="background-image: url(\''. $bg_img['url'] .'\');"';
}

echo '>' . PHP_EOL;
echo '<div class="overlay">' . PHP_EOL;
echo '<div class="container txt-center">' . PHP_EOL;

if(!empty( $options['promise-title'] ))
{
  echo '<h2>' .  $options['promise-title'] .'</h2>';
}

if(!empty( $options['promise-subtitle'] ))
{
  echo '<h3>' . $options['promise-subtitle'] . '</h3>';
}

if(($options['promise-nr']) > 0)
{
  echo '<ul class="perks">';

  foreach($options['promise-list'] as $text)
  {
    echo '<li>'.$text . '</li>';
  }
  echo '</ul>';
}

echo '</div>';
echo '</div>';
echo '</div>' . PHP_EOL;