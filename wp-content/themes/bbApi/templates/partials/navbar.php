<?php
defined('ABSPATH') or die('Access Denied!');
/*
*To be included in the header
*/


if(empty($options))
{
	$options = bb_get_options();
}

$the_telephone     		= (!empty($options['head_telephone'])) ? $options['head_telephone'] : null;
$telephone_title 		= (!empty($options['head_telephone_title'])) ? $options['head_telephone_title'] : null;




// echo var_dump($options);

if(PAGE_TEMPLATE == 'contact' || is_search() || is_singular('studies') || is_singular('careers') || is_singular('post') || 
	(PAGE_TEMPLATE == 'default' AND !IS_FRONT_PAGE AND !IS_HOME)  || is_404() )
{
	$header_class = ' navy'; 
}
else
{
	$header_class = '';

}


echo '<header id="main-header" class="snap-drawers section'.$header_class.'">' . PHP_EOL;
echo '<div class="container">';
echo '<div class="top cf">';

echo '<div class="the-telephone">';
bb_title($telephone_title,4);
bb_telephone_link($the_telephone);
echo '</div>' . PHP_EOL;

echo '</div>'; 
echo '<div class="bottom cf">';
include PARTIALS_DIR . 'logo.php';
echo '<div class="snap-drawer snap-drawer-right">';

echo '<div id="mobile-menu"';
//echo ' class="ws-sticky"';
echo '>' . PHP_EOL;
echo '<button id="snap-close" type="button" aria-label="Close"><span aria-hidden="true">×</span></button>' . PHP_EOL;
//bb_menu( 'primary', array('search' => false) );
wp_nav_menu( array(
	'menu' => 'Main',
	'container' => 'nav',
	'container_id' => 'primary',
	'menu_class' => 'cf',
	'walker' => new BB_Walker_Nav_Menu()
	) );
echo '</div>'; // #mobile-menu
echo '</div>'; // .snap-drawer
echo '</div>'; // .bottom
echo '</div>'; // . container
echo '</header>' . PHP_EOL; // END: .snap-drawers

//------------------------------------------------
