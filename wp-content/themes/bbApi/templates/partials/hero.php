<?php
defined('ABSPATH') or die('Access Denied!');
/*
*hero partial
*/


//--------------Get Meta Data----------------
if(empty($bbPage) OR !is_object($bbPage))
{
  $bbPage = new bbPage(PAGE_ID);
}
//----------bb_extract()  located in the inc folder-----------

$hero_title     =  $bbPage->title;

$hero_subtitle  = $bbPage->get_field('headline', 'hero-subtitle'); 


if(is_singular('services'))
{
	$hero_text = $bbPage->get_field('headline','intro');
}
else
{
	$hero_text = $bbPage->content;
}


$hero_btn_href  = $bbPage->get_field('headline', 'cta_hero_link'); 
$hero_btn_lbl   = $bbPage->get_field('headline', 'cta_hero_label');

$hero_img_id = $bbPage->thumbnail;


//----------------------------------------------------------------------------


//***********************************************************************************************


echo '<section id="hero" class="';
if(IS_FRONT_PAGE)
{
	echo 'home';
}
else
{
	echo trim( strtolower( $bbPage->title ) );
}
echo ' section"';

if( !empty($hero_img_id))
{
	$hero_img =  new bbImage( $bbPage->thumbnail, 'hero' );
	echo ' style="background-image: url(\''.$hero_img->src . '\');" ';
}

echo '>' ;
echo '<div class="overlay">';
echo '<div class="container cf">' . PHP_EOL;

if(!IS_FRONT_PAGE)
the_breadcrumb();


if($hero_title !== null)
{
	if(IS_FRONT_PAGE)
	{
		echo '<h2>' . $hero_title . '</h2>';
	}
	else
	{
		echo '<h1>' . $hero_title . '</h1>';
	}
	
}

if($hero_subtitle !== null)
{
	echo '<h3>' . $hero_subtitle .'</h3>';
}

if(!empty($hero_text))
{
	echo '<div class="hero-text">';
	echo wpautop( do_shortcode( $hero_text ), true );
	echo '</div>';
}

if($hero_btn_href != null)
{
  echo '<div class="cta-btn btn-wrap">';
  echo '<a href="' . get_permalink($hero_btn_href) . '"  class="btn btn-primary green">' . $hero_btn_lbl . '</a>';
  echo '</div>';
}



// ------




// ------

echo '</div>' . PHP_EOL; // .container
echo '</div>' ; // .overlay
echo '</section>' . PHP_EOL; // .hero


