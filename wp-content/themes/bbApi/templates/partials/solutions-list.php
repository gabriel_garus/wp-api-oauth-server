<?php
defined('ABSPATH') or die('Access Denied!');
/*
*News list partial
*/
if(!isset($posts) OR !is_array($posts))
{
	die('posts array not exists');
}



for ($i = 0; $i < count($posts); $i++) :



$post = $posts[$i];

$article_id = $post->ID;



$blog_post 	= new bbPost( $article_id );

$post_title   			= $blog_post->leadin_title;

$post_href  			= $blog_post->url;

$thumb_id  				= $blog_post->leadin_image;

$overlay_id 			= $blog_post->get_field('leadin','icon');
$post_excerpt 			= strip_tags($blog_post->excerpt);

if(($i+1) % 2 == 0)
{
	$even = true;
	$art_class = 'right';
}
else
{
	$even = false;
	$art_class = 'left';
}


if(!$even)
{
	echo '<div class="section solution"><div class="container cf">';
}

echo '<article class="'. $art_class .'">' . PHP_EOL;
echo '<h2 itemprop="headline"><a href="' . $post_href . '">' . $post_title . '</a></h2>'.PHP_EOL;





echo '<div class="overlay-set">';
if(!empty($thumb_id))
{	
	 $thumb_img = new bbImage($thumb_id, 'solution');
	 if(!empty($thumb_img->html))
	 {
	 	echo $thumb_img->html;
	 }
	 else
	 {
	 	echo '<img src="/img/solutions-placeholder.png" alt="solution" width="445" height="215" />';
	 }

}
else
{
	echo '<img src="/img/solutions-placeholder.png" alt="solution" width="445" height="215" />';
}
echo '<div class="overlay">';
if ($overlay_id !== null)
{
	$overlay = new bbImage($overlay_id, 'solution');
	echo $overlay->html;
}
echo '</div>';
echo '</div>' . PHP_EOL;




echo wpautop(do_shortcode($post_excerpt));

if(!empty($post_href))
	{
		echo '<div class="read-more">';
		echo '<a itemprop="url" href="'. $post_href .'" >Read More</a>';
		echo '</div>' . PHP_EOL;
	}

echo '</article>'.PHP_EOL;


if($even OR ($i+1) == count($posts))
{
	echo '</div></div>';
}


$article_id = null; 




endfor;