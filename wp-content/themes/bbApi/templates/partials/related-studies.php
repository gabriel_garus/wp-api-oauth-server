<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Related articles partial
*/


//-----Get meta_data---------


//bb_extract()  located in the inc folder

if(empty($bbPage))
{
	$bbPage = new bbPage(PAGE_ID);
}

$meta_data = $bbPage->meta;

$related 	= (!empty($meta_data['related-cases'])) ? $meta_data['related-cases'] : null;
$heading 	= 'Related Case Studies';
$nr 		= 4;


	echo '<div class="related ws-sticky studies">' . PHP_EOL;


$togo = array();

if(!empty($related ))
{
	foreach ($related as $rel_id) 
	{
		if(!empty($rel_id) AND $rel_id != '0')
		{
			$p = new bbPost($rel_id);
			$a = array();
			$a['title'] = $p->leadin_title;
		

			$thu = $p->get_field('company-logo','image');

			if(!empty($thu))	
			{
				$image = new bbImage($thu);
				$a['image'] = $image->html;
			}
			elseif(!empty($p->thumbnail))
			{
				$image = new bbImage($p->thumbnail);
				$a['image'] = $image->html;	
			}

			$a['link'] = $p->url;

			$togo[] = $a;	
		}
	}

}





	if( sizeof($togo) > 0 )
	{

		if(	$a['title'] !== null)
		{
			echo '<h2 class="caps">'. $heading .'</h2>'. PHP_EOL;
		}

		foreach ($togo as $a) 
		{
			
			echo '<article>' . PHP_EOL;

			echo '<a href="' . $a['link'] . '">' . PHP_EOL;
			echo '<h3>'. $a['title'] .'</h3></a>' . PHP_EOL;
			

			if(!empty($a['image']))
			{
				echo '<a href="' . $a['link'] . '">' . PHP_EOL;
				echo $a['image'];
				echo '</a>';
			}
		
			echo '</article>' . PHP_EOL;
		}
		
		

	}
	

	echo '</div>' . PHP_EOL;


