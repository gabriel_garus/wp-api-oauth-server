<?php

$section =  $bbPage->get_section('white-paper');

if($section):
echo '<div class="section white-paper"';

if(!empty($section['bg_image']))
{
	$bg_image = new bbImage($section['bg_image'], 'full');
	echo ' style="background-image: url(\''.$bg_image->src . '\');"';
}

echo '>' . PHP_EOL;

echo '<div class="overlay">' . PHP_EOL;

echo '<div class="container cf">' . PHP_EOL;

echo '<div class="left">';
if(!empty($section['image']))
{
  $img = new bbImage($section['image']);
  echo $img->html;
}
echo '</div>' . PHP_EOL;

echo '<div class="right">' . PHP_EOL;

if(!empty($section['sub-title']))
{
  echo '<p class="subtitle">' . $section['sub-title'] . '</p>';
}

if(!empty($section['title']))
{
  echo '<h2>' . $section['title'] . '</h2>';
}


if(!empty($section['button_label']))
{
	
	echo '<button ';
	if(!empty($section['button_id']))
	{

		echo 'id="'. str_replace(' ', '-', $section['button_id'])  .'" ';
	}

	echo 'class="btn secondary bloom';
	if(!empty($section['butotn_class']))
	{
		echo ' '.$section['butotn_class'];
	}

	echo '">';
	echo $section['button_label'];
	echo '</button>' . PHP_EOL;
}


echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL;

endif;