<?php
defined('ABSPATH') or die('Access Denied!');
/*
*News list partial
*/




// initialize post object.
if(empty($article_id))
{
	$article_id = $post->ID;
}




$blog_post 	= new bbPost( $article_id );
//dump($blog_post);


// get variables
$meta = $blog_post->meta;

$post_title   			= $blog_post->leadin_title;
$post_excerpt 			= strip_tags($blog_post->leadin_text);
$post_date  			= $blog_post->get_date(array('sup'=> false ));
$post_href  			= $blog_post->url;

$post_author_first_name = $blog_post->author['first_name'];
$post_author_last_name  = $blog_post->author['last_name'];

$the_author 			= $blog_post->author;
$thumb_id  				= $blog_post->leadin_image;






echo '<article class="cf">' . PHP_EOL;


echo '<div class="left">' . PHP_EOL;

////---- header ----
echo '<header>'.PHP_EOL;
echo '<h2 itemprop="headline"><a href="' . $post_href . '">' . $post_title . '</a></h2>'.PHP_EOL;

// --- post meta ---
echo '<div class="article-meta cf">'.PHP_EOL;

echo '<div class="post-author">';
echo '<span class="pre">By </span>';
if(!empty($the_author['nick_name']))
{
	echo '<span class="name">'. $the_author['nick_name'] . '</span>' .PHP_EOL;
}
//echo '<span class="first name">' . $post_author_first_name . '</span>'; 
//echo ' '. '<span class="last name">'. $post_author_last_name . '</span>';
echo '</div>' ; // .post-author

echo '<div class="article-date">' . PHP_EOL;
echo $post_date;
echo '</div>' . PHP_EOL; // .article-date
echo '</div>'; // .article-meta
//---------------------------

echo '</header>'.PHP_EOL;
//// ----------------------------------




echo '<p class="intro">' . $post_excerpt . '</p>'. PHP_EOL;


echo '<div class="block read-more">';
echo '<a itemprop="url" href="'. $post_href .'"  class="blue">Read More</a>';
echo '</div>' . PHP_EOL;


echo '</div>' . PHP_EOL; //.left
//---------------------------


if(!empty($thumb_id))
{	
	echo '<div class="right post-thumbnail">' . PHP_EOL;
	$img = new bbImage($thumb_id);
	echo $img->html;
	echo '</div>'.PHP_EOL;
}



//---------------------------
// --- Read More button



echo '</article>'.PHP_EOL;

$article_id = null; 
