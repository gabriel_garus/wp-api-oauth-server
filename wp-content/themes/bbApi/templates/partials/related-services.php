<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Related articles partial
*/


//-----Get meta_data---------


//bb_extract()  located in the inc folder

if(empty($meta_data))
{
  $meta_data = bb_extract( get_post_meta(PAGE_ID));
}



$related = $thePost->get_section('related-services');

$heading = $thePost->get_field('sidebar','title');
$hide_title = $thePost->get_field('sidebar','hide_title'); 

if($heading === null AND $hide_title !== 'true')
{
	if(!empty($post_type) AND $post_type == 'solutions')
	{
		$heading = 'Our Solutions';
	}
	else
	{
		$heading = 'Our Services';
	}
	
}


$nr 		= 4;


echo '<div class="related services">' . PHP_EOL;


$togo = array();



if(!empty($related ))
{
	foreach ($related as $rel_id) 
	{
		if(!empty($rel_id) AND $rel_id != '0')
		{
			$p = new bbPost($rel_id);
			$a = array();
			$a['title'] = $p->leadin_title;
			$a['intro'] = $p->excerpt;
			
			if(!empty($p->thumbnail))	
			{
				$image = new bbImage($p->thumbnail);
				$a['image'] = $image->html;
			}

			$a['link'] = $p->url;

			$togo[] = $a;	
		}
	}

}
else
{
	$related = wp_get_recent_posts( array( 'post_type' => $post_type, 'numberposts' => 4, 'exclude' => array(PAGE_ID) ) );
	

	foreach ($related as $rel) 
	{
			$rel_id = $rel['ID'];


			$p = new bbPost($rel_id);
			$a = array();
			$a['title'] = $p->leadin_title;
			$a['intro'] = $p->excerpt;
			
			if(!empty($p->thumbnail))	
			{
				$image = new bbImage($p->thumbnail);
				$a['image'] = $image->html;
			}

			$a['link'] = $p->url;

			$togo[] = $a;	
	}
}





	if( sizeof($togo) > 0 )
	{

		if ($hide_title !== 'true')
		{
			echo '<h2 class="related-title">'. $heading .'</h2>'. PHP_EOL;
		}

		foreach ($togo as $a) 
		{
			
			echo '<article>' . PHP_EOL;

			echo '<a href="' . $a['link'] . '">' . PHP_EOL;
			echo '<h3>'. $a['title'] .'</h3></a>' . PHP_EOL;
			echo '<p>'. $a['intro'] . '</p>' . PHP_EOL;

			if(!empty($a['image']))
			{
				echo $a['image'];
			}
		
			echo '</article>' . PHP_EOL;
		}
		
		

	}
	

	echo '</div>' . PHP_EOL;


