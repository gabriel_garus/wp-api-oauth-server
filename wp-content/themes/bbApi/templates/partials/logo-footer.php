<?php
/*
*  logo-footer.php
* 
*  Footer Logo
*/

defined('ABSPATH') or die('Access Denied!');

$footer_logo_url = (!empty($options['footerLogo-image']['url'])) ? $options['footerLogo-image']['url'] : null;
$footer_logo_width = (!empty($options['footerLogo-size']['width'])) ? $options['footerLogo-size']['width'] : '200';
$footer_logo_height = (!empty($options['footerLogo-size']['height'])) ? $options['footerLogo-size']['height'] : '50';

echo '<div class="footer-logo">';
if($footer_logo_url !== null)
{
	echo '<a href="' . esc_url(home_url('/')) . '" rel="home">';
	echo '<img alt="' . WP_HOME .' Logo" ';
	echo 'src="' . $footer_logo_url . '" ';
	echo 'width="'.$footer_logo_width.'" ';
	echo 'height="'.$footer_logo_height.'" />';
	echo '</a>';
}
echo '</div>'; 