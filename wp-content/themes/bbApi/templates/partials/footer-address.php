<?php
defined('ABSPATH') or die('Access Denied!');
/*
* footer-address
*
*/

//-----------Options from Redux
if(empty($options))
{
	$options = (!empty($_SESSION['bb_options'])) ? $_SESSION['bb_options'] : get_option('bigbang');
}



// ------------------microdata --------------------
if ( IS_FRONT_PAGE OR !empty($address_itemprop))
{
	$md_address       = 'itemprop="address" itemscope itemtype="https://schema.org/PostalAddress"';
	$md_streetAddress = 'itemprop="streetAddress"';
	$md_addressLocale = 'itemprop="addressLocality"';
 	$md_addressRegion = 'itemprop="addressRegion"';
	$md_code          = 'itemprop="postalCode"';
	if(!empty($options['countryCode']))
	{
		$md_country_meta  = '<meta itemprop="addressCountry" content="' . $options['countryCode'] . '">';
	}
	else
	{
		$md_country_meta  = null;
	}
}
else
{
	$md_address       = null;
	$md_streetAddress = null;
	$md_addressLocale = null;
	$md_addressRegion = null;
	$md_code          = null;
	$md_country_meta  = null;
}



$title = ( !empty($options['title']) ) ? $options['title'] : null;

$street_line_1 		 = ( !empty($options['addressL1']) ) ? $options['addressL1'] : null;
$street_line_2 		 = ( !empty($options['addressL2']) ) ? $options['addressL2'] : null;
$street_line_3 		 = ( !empty($options['addressL3']) ) ? $options['addressL3'] : null;
$city 				 = ( !empty($options['city'])) ? $options['city'] : null;
$code 				 = ( !empty($options['areaCode']) ) ? $options['areaCode'] : null;
$county			 	 = ( !empty($options['county']) ) ? $options['county'] : null;
$country 			 = ( !empty($options['country']) ) ? $options['country'] : null;
$countryCode 	 	 = ( !empty($options['countryCode']) ) ? $options['countryCode'] : null;



//----------------------------------------------





echo '<address ' . $md_address . ' >'.PHP_EOL;



$street_address = array();

if(!empty($street_line_1))
{
	$street_address[] =  $street_line_1;
}

if( !empty($street_line_2) )
{
	//$street_address[] = $street_line_2;
}

if( !empty($street_line_3 ) )
{
	$street_address[] = $street_line_3;
}


$addressRest = array();


if( !empty($city) )
{
	$addressRest[0]['name'] = $city;
	$addressRest[0]['md'] = $md_addressLocale;
	$addressRest[0]['class'] = 'addressLocality';
}

if( !empty($code) )
{
	$addressRest[1]['name'] = $code;
	$addressRest[1]['md'] = $md_code;
	$addressRest[1]['class'] = 'postalCode';
}

if( !empty($county) )
{
	$addressRest[2]['name'] = $county;
	$addressRest[2]['md'] = $md_addressRegion;
	$addressRest[2]['class'] = 'addressRegion';
}

if( !empty($country) )
{
	$addressRest[3]['name'] = $country;
	$addressRest[3]['md'] = null;
	$addressRest[3]['class'] = 'addressCountry';
}



echo '<div class="street" '.$md_streetAddress.' >' . PHP_EOL;

for ($i=0; $i < sizeof($street_address) ; $i++)
{
	if(!empty($street_address[$i]))
	{

		$class = 'street_address_'. ($i+1);
   	   echo '<span class="'.$class.'">' . $street_address[$i];
	   if( $i < (sizeof($street_address) - 1 ) OR !empty($addressRest))
	   {
	   	if(isset($address_commas) AND $address_commas)
	   		echo ', ';
	   	else
	   		echo ' ';
	   }
   		
   	   echo '</span>';
	}
}

echo '</div>' . PHP_EOL; // .street


for ($i=0; $i < sizeof($addressRest) ; $i++)
{

	if(!empty($addressRest[$i]))
	{	
		$cex = explode('"', $addressRest[$i]['md']);
		$class = (!empty($addressRest[$i]['class'])) ? $addressRest[$i]['class'] : 'field';

		echo '<span class="'.$class.'" ' . $addressRest[$i]['md'] .'>' . $addressRest[$i]['name'];
		if( $i < (sizeof($addressRest) - 1 ) )
		   {
		   	if(isset($address_commas) AND $address_commas)
		   		echo ', ';
		   	else
		   		echo ' ';
		   }
	   echo '</span>';
	}
}
echo '<span>'.$country . '</span>' . PHP_EOL;

if( !empty($countryCode) )
{
	echo $md_country_meta.PHP_EOL;
}

echo '</address>'.PHP_EOL;
