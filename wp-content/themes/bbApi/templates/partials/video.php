<?php
defined('ABSPATH') or die('Access Denied!');
/* HTML5 Video Markup
* require $video_url
*/

//wp_enqueue_script( 'shimVideo' );

include_once  INC_DIR . 'bb-video-class.php';

$companyName = '';
$companySite = WP_HOME;

// Assign Raw link from DB

$videoLink = $video_url;

$V = new bbVideo($videoLink);




if( empty($videoPoster) )
{

	$videoPoster = ( !empty($video_poster) ) ? $video_poster : null;
}


if($V->ID !== null)
{
	$videoName 		= 'Name';
	$embedUrl 		=  $V->get_embed_url();
	$videoSrc 		= $V->get_watch_url();
	$contentUrl 	= $V->get_watch_url();
	$thumbnailUrl = $V->get_thumbnail('mq');
	$videoPoster 	= (empty($videoPoster)) ? ($V->get_thumbnail('hq')) : $videoPoster;
	$time 				= $V->get_time();
	$publish_date = $V->get_publish_date();
	$description = $V->get_description();
	$video_name = $V->get_name();

		$V->set_width('805');
		$V->set_height('513');

	$description = str_replace('"', "'", $description);
	$video_name = str_replace('"', "'", $video_name);
	if(!is_front_page())
	{
		$vidItemProp = 'itemprop="video"';
	}

	else{
		$vidItemProp = null;
	}




	//$agent = strtolower( $_SERVER['HTTP_USER_AGENT'] );

	
	$the_id = 'yt_video';
	
	echo '<div id="'.$the_id.'" class="video" '. $vidItemProp .' itemscope itemtype="http://schema.org/VideoObject" >'.PHP_EOL;

	echo '<div itemprop="publisher" itemscope itemtype="http://schema.org/Organization">';
	echo '<meta itemprop="name" content="'. $companyName .'">'.PHP_EOL;
	echo '<link itemprop="url" href="'.$companySite.'">'.PHP_EOL;
	echo '</div>'.PHP_EOL;

	
	if( $the_id == 'yt_video' )
	{
		echo $V->embedCode;
	}
	else
	{

			echo '<div class="wrap">'.PHP_EOL;
			echo '<div class="chc-video">'.PHP_EOL;

			echo '<meta itemprop="thumbnailUrl" content="'.$thumbnailUrl.'" />'.PHP_EOL;
			echo '<meta itemprop="contentUrl" content="' . $contentUrl . '" />'.PHP_EOL;
			echo '<meta itemprop="embedURL" content="'. $embedUrl . '" />'.PHP_EOL;
			echo '<meta itemprop="duration" content="'. $time . '" />'.PHP_EOL;
			echo '<meta itemprop="description" content="'. $description . '" />'.PHP_EOL;
			echo '<meta itemprop="name" content="'. $video_name . '" />'.PHP_EOL;
			echo '<meta itemprop="uploadDate" content="' . $publish_date . '"  />' . PHP_EOL;

			echo '<div class="mediaplayer ratio-16-9" style="width: 100%;">'.PHP_EOL;
			
			
			echo '<video poster="'.$videoPoster.'" width="'.$V->width.'" height="'.$V->height. '" style="height: 100%;" controls>'.PHP_EOL;
			echo '<source type="video/youtube" src="' . $videoSrc . '" /> ' .PHP_EOL;

			echo'</video>'.PHP_EOL . '</div>'.PHP_EOL;
			

			echo'</div></div>'.PHP_EOL;
	}
	echo'</div>'.PHP_EOL;

}