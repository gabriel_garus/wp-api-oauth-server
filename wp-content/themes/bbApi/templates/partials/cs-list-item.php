<?php
defined('ABSPATH') or die('Access Denied!');
/*
*News list partial
*/




// initialize post object.
if(empty($article_id))
{
	$article_id = $post->ID;
}





$blog_post 	= new bbPost( $article_id );
//dump($blog_post);


// get variables
$meta = $blog_post->meta;

$post_title   			= $blog_post->leadin_title;
$post_excerpt 			= $blog_post->excerpt;

$post_href  			= $blog_post->url;

$thumb_id  				= $blog_post->thumbnail;





$terms = wp_get_post_terms( $article_id, 'industry');

if(!empty($terms))
{
	$taxonomy = $terms[0];
}
else
{
	$taxonomy = null;
}




echo '<div class="section service cs">';
echo '<div class="container">' . PHP_EOL;







echo '<article class="cf">' . PHP_EOL;

echo '<div class="left">' . PHP_EOL;


// HEADER
echo '<header class="cf">'.PHP_EOL;

echo '<h2 itemprop="headline"><a href="' . $post_href . '">' . $post_title . '</a></h2>'.PHP_EOL;

if($taxonomy)
{	// --- post meta
	echo '<div class="article-meta">'.PHP_EOL;
	echo $taxonomy->name . PHP_EOL;
	echo '</div>';
	//---------------
}

echo '</header>'.PHP_EOL;
// ---------------------------


echo '<p>' . $post_excerpt .'</p>';

if(!empty($post_href))
	{
		echo '<div class="read-more">';
		echo '<a itemprop="url" href="'. $post_href .'" >Read More</a>';
		echo '</div>' . PHP_EOL;
	}

echo '</div>' . PHP_EOL; //.left
//---------------------------


echo '<div class="right">' . PHP_EOL;
if(!empty($thumb_id))
{	
	 $thumb_img = new bbImage($thumb_id, 'thumbnail');
	 echo $thumb_img->html;
}
echo '</div>' . PHP_EOL; // .right
//---------------------------

echo '</article>'.PHP_EOL; // article





echo '</div></div>' . PHP_EOL;  // .section   // .container
//---------------------------

	

$article_id = null; 