<?php
defined('ABSPATH') or die('Access Denied!');


if(empty($bbPage))
{
  $bbPage =  new bbPage(PAGE_ID);
}


$section =  $bbPage->get_section('feature-section');




if( $section !== null ) :


if(empty($options))
{
	$options = bb_get_options();
}



echo '<div class="section feature';
if(isset($cta_white) AND $cta_white ) { echo ' white'; }
echo '">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;


///         TOP       ////////////////////////////////

echo '<div class="top">' . PHP_EOL;

if(!empty($section['title']))
{
  echo '<h2>' . $section['title'] .'</h2>';
}

if(!empty($section['text']))
{
  echo wpautop( do_shortcode( $section['text'] ));
}
echo '</div>'; // .top

//---------------------------------------------



///         Bottom       ////////////////////////////////

echo '<div class="bottom cf">' . PHP_EOL;

// -- left --
echo '<div class="left">' . PHP_EOL;
if(!empty( $options['cta_logo'] ))
      {
       	$cta_image = $options['cta_logo'];
        echo '<img src="'.$cta_image['url'].'" width="280" height="65"  alt="Cloudstrong" />';
      }

if(!empty( $options['cta_name'] ))
{
    echo '<p class="cta-name">' . $options['cta_name'] . '</p>';
}
//dump($options);
if(!empty($options['cta_button_url']) OR !empty($options['cta_button_custom_url']))
{
    $cta_btn_href = (!empty($options['cta_button_custom_url'])) ? $options['cta_button_custom_url'] : get_permalink($options['cta_button_url']);
    
    echo '<div class="cta-btn btn-wrap">';
    echo '<a href="' . $cta_btn_href . '"  class="btn btn-primary">' . $options['cta_button_label'] . '</a>';
    echo '</div>';
}

echo '</div>'; // .left
//---------

// -- right --
echo '<div class="right">' . PHP_EOL;
if(!empty($options['cta_image']))
{
    	$cta_logo = $options['cta_image'];
    	echo '<img src="'.$cta_logo['url'].'" width="700" height="260" alt="Logo Image" />';
}
    

echo '</div>'; //.right
//---------

echo '</div>' ; //. bottom
//---------------------------------------------




echo '</div>' . PHP_EOL; // .container
echo '</div>' . PHP_EOL; // .section
unset($section);

endif;