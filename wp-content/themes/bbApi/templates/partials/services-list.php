<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Services  list partial
*/
if(!isset($posts) OR !is_array($posts))
{
	die('posts array not exists');
}




$query_Args =  array(
	'meta_query' => array(
			array(
			'key' => 'leadin',
			'value' => 'alien',
			'compare'	 => 'LIKE'
			)
	),
	'post_type'  => 'services',
	
	);

$qq = new WP_Query($query_Args);



$meta = get_post_meta(54);

//echo var_dump($meta);

$posts = $qq->posts;
echo '<div class="services-list">'. PHP_EOL;

foreach ($posts as $post):



$article_id = $post->ID;



$blog_post 	= new bbPost( $article_id );

$meta = $blog_post->meta;

$post_title   			= $blog_post->leadin_title;

$post_sub_title         = $blog_post->get_field('leadin','subtitle');

$post_excerpt 			= strip_tags($blog_post->excerpt);

$post_href  			= $blog_post->url;

$thumb_id  				= $blog_post->leadin_image;




echo '<div class="section service switch"><div class="container">' . PHP_EOL;

echo '<article class="cf">' . PHP_EOL;

echo '<div class="left">' . PHP_EOL;

echo '<h2 itemprop="headline"><a href="' . $post_href . '">' . $post_title . '</a></h2>'.PHP_EOL;

bb_title($post_sub_title,3);

echo '<p>' . $post_excerpt .'</p>';

echo '<div class="block read-more">';
echo '<a itemprop="url" href="'. $post_href .'"  class="blue">Read More</a>';
echo '</div>' . PHP_EOL;

echo '</div>' . PHP_EOL; //.left
//---------------------------

if(!empty($thumb_id))
{	
	$serv_th = new bbImage($thumb_id, 'service');

	echo '<div class="right">' . PHP_EOL;
	echo $serv_th->html;
	echo '</div>'.PHP_EOL;
}




	

echo '</article>'.PHP_EOL;

$article_id = null; 

echo '</div></div>' . PHP_EOL;

//break;
endforeach;

echo '</div>' . PHP_EOL;