<?php
defined('ABSPATH') or die('Access Denied!');
/*
*News list partial
*/
// initialize post object.
if(empty($article_id))
{
	$article_id = $post->ID;
}


$blog_post 	= new bbPost( $article_id );
//dump($blog_post);


// get variables
$meta = $blog_post->meta;

$post_title   			= $blog_post->leadin_title;
$post_excerpt 			= strip_tags( $blog_post->excerpt );
$post_date  			= $blog_post->get_date(array('sup'=> false ));
$post_href  			= $blog_post->url;

$post_author_first_name = $blog_post->author['first_name'];
$post_author_last_name  = $blog_post->author['last_name'];

$the_author 			= $blog_post->author;
$thumb_id  				= $blog_post->leadin_image;


//dump($the_author);



echo '<article>' . PHP_EOL;
if(!empty($thumb_id))
{	
	echo '<div class="post-thumbnail">' . PHP_EOL;
	$img = new bbImage($thumb_id);
	echo $img->html;

	echo '</div>'.PHP_EOL;
}


echo '<h3 itemprop="headline"><a href="' . $post_href . '">' . $post_title . '</a></h3>'.PHP_EOL;

// --- post meta
echo '<div class="post-meta cf">' . PHP_EOL;

	echo '<div class="post-author">';
	echo '<span class="pre">By </span>';
	if(!empty($the_author['nick_name']))
	{
		echo '<span class="name">'. $the_author['nick_name'] . '</span>';
	}
	//echo '<span class="first name">' . $post_author_first_name . '</span>'; 
	//echo ' '. '<span class="last name">'. $post_author_last_name . '</span>';
	echo '</div>' ; //.post-author

	echo '<div class="post-date">' ;
	echo $post_date;
	echo '</div>' ;

echo '</div>'.PHP_EOL; // .post-meta
//---------------------------

echo '<p>'.$post_excerpt .'</p>';


//---------------------------
// --- Read More button
if(isset($read_more_link_revert))
{
	$class = ' reverse';
}
else
{
	$class= ' normal';
}

if(!empty($post_href))
{
	echo '<div class="block read-more'. $class  .'">' . PHP_EOL;
	echo '<a itemprop="url" href="'. $post_href .'" >Read More</a>';
	echo '</div>' . PHP_EOL;
}

echo '</article>'.PHP_EOL;

$article_id = null; 
