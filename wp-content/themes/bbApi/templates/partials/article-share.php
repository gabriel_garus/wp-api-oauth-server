<?php defined('ABSPATH') or die('Access Denied!');



$aLink = get_permalink( PAGE_ID );
$e = get_the_excerpt();

$exc = ( !empty( $e ) ) ? $e : $thePost->excerpt;
$options = bb_get_options();


$socials = array();

$fb = array();
$fb['class'] = 'facebook';
$fb['label'] = 'Facebook';
$fb['link']  = 'https://www.facebook.com/sharer/sharer.php?u=';
$fb['link'] .= $aLink;
$socials[] = $fb;
		
$twit = array();
$twit['class'] = 'twitter';
$twit['label'] = 'Twitter';
$twit['link']  = 'https://twitter.com/intent/tweet?text=';
$twit['link'] .= urlencode($title);
$twit['link'] .= '&amp;url=';
$twit['link'] .= urlencode($aLink);
if( !empty($options['twitter']) ) 
{
	$twit['link'] .= '&amp;via=' . $options['twitter'];
}

$socials[] = $twit;

$gp['class'] = 'google';
$gp['label'] = 'Google+';
$gp['link']  = 'https://plus.google.com/share?url=';
$gp['link'] .= urlencode($aLink);
$socials[] = $gp;

$li['class'] = 'linked';
$li['label'] = 'LinkedIn';
$li['link']  = 'https://www.linkedin.com/shareArticle?mini=true&amp;url=';
$li['link'] .=  urlencode($aLink);
$li['link'] .= '&amp;title=';
$li['link'] .= urlencode($title);
$li['link'] .= '&amp;source=';
$li['link'] .= urlencode($aLink);
$li['link'] .= '&amp;summary=';
$li['link'] .= urlencode($exc);
$socials[] = $li;


echo '<ul id="share-links" class="share-links">'.PHP_EOL;
foreach ($socials as $soc)
{
	echo '<li class="'.$soc['class'].'">';
	echo '<a href="' . $soc['link'] .'" ';
	echo 'target="_blank" ';
	echo 'onclick="windowExplode(this.href,500,400); event.preventDefault()" >';
	echo $soc['label'].'</a>';

}

echo '</ul>' ; 


echo '<script>';

echo 'function windowExplode(url, width, height)';
echo '{';

echo 'var leftPosition, topPosition, specs; ';
echo 'leftPosition = (window.screen.width / 2) - ((width / 2) + 10);';
echo 'topPosition = (window.screen.height / 2) - ((height / 2) + 50);';

$specs  = 'status=no,';
$specs .= 'height=" + height + ",';
$specs .= 'width=" + width + ",';
$specs .= 'resizable=yes,';
$specs .= 'left=" + leftPosition + ",';
$specs .= 'top=" + topPosition + ",';
$specs .= 'screenX=" + leftPosition + ",';
$specs .= 'screenY=" + topPosition + ",';
$specs .= 'toolbar=no,';
$specs .= 'menubar=no,';
$specs .= 'scrollbars=no,';
$specs .= 'location=no,';
$specs .= 'directories=no';

echo 'specs = "'.$specs.'";';
echo 'window.open(url, "_blank", specs)'; 
echo '}';
echo '</script>';