<?php
defined('ABSPATH') or die('Access Denied!');
/*
** Google tag Manager Partial
*/

/* @done:  options names */

if(empty($options))
{
	$options = bb_get_options();
}

$gtm_id = (!empty($options['gtmId'])) ? $options['gtmId'] : null;
$prod_url = (!empty($options['bb_prod_url'])) ? $options['bb_prod_url'] : null;
$prod_url = ltrim($prod_url,'http://');
$prod_url = ltrim($prod_url,'https://');
$prod_url = ltrim($prod_url,'www.');
/*
*  HOST defined in functions.php - define('HOST',$_SERVER['SERVER_NAME']);
* 	gtmID and prod_url from redux options.
*/

$host = ltrim(HOST, 'www.');
$host = rtrim(HOST, '/');


$prod_url = ltrim($prod_url, 'www.');
$prod_url = rtrim($prod_url, '/');

if ( $host === $prod_url AND !empty($gtm_id))
{
	echo '<!-- Google Tag Manager -->'.PHP_EOL;
	echo '<noscript><iframe src="//www.googletagmanager.com/ns.html?id='. $gtm_id .'"'.PHP_EOL;
	echo '  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>'.PHP_EOL;
	echo '  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':'.PHP_EOL;
	echo '  new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],'.PHP_EOL;
	echo '  j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src='.PHP_EOL;
	echo '  \'//www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);'.PHP_EOL;
	echo '})(window,document,\'script\',\'dataLayer\',\''. $gtm_id. '\');</script>'.PHP_EOL;
	echo '<!-- End Google Tag Manager -->'.PHP_EOL;
}
elseif(WP_DEBUG)
{
	//echo '<!-- host:' . $host .' || prod-url:'.$prod_url .' || gtm-id: '.$gtm_id .' -->';
}