<?php


$case_cats = get_terms('industry', array( 'orderby'=>'count' ));


if(!empty($case_cats))
{
	echo '<div class="section cs-categories">' . PHP_EOL;
	echo '<div class="container cf">' . PHP_EOL;
	echo '<h3>Filter by Industry:</h3>';

	if(defined('QUERIED_TAX'))
	{
		$current_tax = strtolower( QUERIED_TAX );
	}
	else
	{
		$current_tax = 'all';
	}

	echo '<nav id="cs-buttons">';


	if($current_tax == 'all')
	{
		$class = ' current';
	}
	else
	{
		$class = ' disabled'; 
	}

	echo '<a class="btn-tertiary all'.$class.'" ng-click="industry={}" >All</a>';
	
	foreach ($case_cats  as $cat) 
	{
		if($cat->slug == 'other')
		{
			continue;
		}
		
		$cat_link = WP_HOME . '/case-studies/' . $cat->slug .'/';

		if( $current_tax == $cat->slug)
		{
			$class = ' current';
		}
		else
		{
			$class = ' disabled';
		}

		echo '<a class="btn-tertiary cat'.$class.'" ng-click="industry={category:\''.$cat->slug.'\'}">'.$cat->name .'</a>';

	}
	
	echo '</nav>';
	echo '<input type="text" id="query" name="query" ng-model="query">';

	echo '<hr />' . PHP_EOL;
	echo '</div>' . PHP_EOL;
	echo '</div>' . PHP_EOL;

}