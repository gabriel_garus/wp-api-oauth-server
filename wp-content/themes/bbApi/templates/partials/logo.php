<?php
/*
* TOP Logo
*/

defined('ABSPATH') or die('Access Denied!');

if(empty($options))
{
	$options = bb_get_options();
}


$logo_url = (!empty($options['topLogo-image']['url'])) ? $options['topLogo-image']['url'] : null;
$logo_width = (!empty($options['topLogo-size']['width'])) ? $options['topLogo-size']['width'] : '200';
$logo_height = (!empty($options['topLogo-size']['height'])) ? $options['topLogo-size']['height'] : '50';



if ( IS_FRONT_PAGE AND !defined('logo_h1'))
{
	$logo_wrapper_tag = 'h1';
	define('logo_h1', true);
}
else
{
	$logo_wrapper_tag = 'aside';
}

if($logo_url !== null)
{
	echo '<' . $logo_wrapper_tag . ' class="logo">' . PHP_EOL;
	echo '<a href="' . esc_url(home_url('/')) . '" itemprop="url" rel="home">';
	echo '<img alt="' . COMPANY_NAME . ' Logo" ';
	echo 'itemprop="logo" ';
	echo 'title="' . COMPANY_NAME . '" ';
	// echo 'id="logo-big" ';
	echo 'src="'. $logo_url  . '" ';
	echo 'width="' . $logo_width .'" ';
	echo 'height="' . $logo_height .'" />';
	echo '</a>' . PHP_EOL;
	echo '<meta itemprop="name" content="' . COMPANY_NAME . '">' . PHP_EOL;
	echo '</'.$logo_wrapper_tag.'>';
}