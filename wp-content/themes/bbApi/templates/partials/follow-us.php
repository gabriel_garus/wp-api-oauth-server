<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Flat section with left side text and img to the right
*/

$blog_page_id = (defined('BLOG_ID')) ? BLOG_ID : get_option( 'page_for_posts' );

$blog_index_meta  = bb_extract( get_post_meta($blog_page_id ) );
$follow_head = ( !empty($blog_index_meta['follow']['head']) ) ? $blog_index_meta['follow']['head'] : null;
$follow_text =  ( !empty($blog_index_meta['follow']['text']) ) ? $blog_index_meta['follow']['text'] : null;


echo '<div class="social invert">'.PHP_EOL;

if(!empty($follow_head))
{
	echo '<h3>' . $follow_head . '</h3>'.PHP_EOL;
}
if( !empty($follow_text) )
{
	echo '<p>' . $follow_text . '</p>'.PHP_EOL;
}

include PARTIALS_DIR . 'social-links-invert.php';

echo '</div>'.PHP_EOL;
