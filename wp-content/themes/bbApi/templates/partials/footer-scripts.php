<?php
defined('ABSPATH') or die('Access Denied!');
/*
*JScript for loading in the footer
*/
$page_meta 	= bb_extract(get_post_meta(PAGE_ID));
//error_log( print_r($page_meta, true) );
if(empty($options))
{
	$options = bb_get_options();
}

$template_file = get_post_meta(get_the_id(),'_wp_page_template',TRUE);

//echo '<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>' . PHP_EOL;



///------------ Video Scripts

/*
if( defined('VIDEO_CONTENT') )
{
	echo '<script>' . PHP_EOL;
	// Expanded Version
	echo '$(document).ready(function(){ ' . PHP_EOL;
	echo 'webshim.setOptions({\'loadStyles\': false });' . PHP_EOL;
	//echo 'webshim.setOptions("track", {override: "auto"});' . PHP_EOL;
	echo 'webshim.setOptions("mediaelement", {replaceUI: "auto"}); ' . PHP_EOL;
	echo 'webshim.polyfill(); ' . PHP_EOL;
	echo '});' . PHP_EOL;

	// Minified Version
	//echo 'webshim.setOptions({loadStyles:!1}),webshim.setOptions("track",{override:"auto"}),$("html").hasClass("touch")?webshim.setOptions("mediaelement",{replaceUI:"auto"}):webshim.setOptions("mediaelement",{replaceUI:"auto"}),webshim.polyfill("mediaelement"); ' . PHP_EOL;
	echo '</script>'  . PHP_EOL;
}

*/


///------------ Form scripts

if( !empty( $page_meta['form']['form_id'] ) )
{


	echo '<script>' . PHP_EOL;
	/*
	// Expanded Version

	echo '$(document).ready(function(){' . PHP_EOL;
	echo '$("form").removeAttr("novalidate");' . PHP_EOL;
	echo '$("input, textarea").each(function(){' . PHP_EOL;
	echo '  if ($(this).attr("aria-required") == "true"){' . PHP_EOL;
	echo '    $(this).attr("required", "true");' . PHP_EOL;
	echo '    $("label[for=\'"+$(this).attr(\'id\')+"\']").addClass(\'required\');' . PHP_EOL;
	echo '  }});' . PHP_EOL;
	echo '})' . PHP_EOL;
	echo 'webshim.setOptions({' . PHP_EOL;
	echo '        \'loadStyles\': false,' . PHP_EOL; // if "true", loading 3 .css files in the head'
	echo '        \'forms\': {' . PHP_EOL;
	echo '        replaceValidationUI: true,' . PHP_EOL;
	echo '        lazyCustomMessages: true,' . PHP_EOL;
	echo '        addValidators: true,' . PHP_EOL;
	echo '        ival: {' . PHP_EOL;
	        //wether webshim should show a bubble on invalid event: "hide" | true | false'
	echo '        "handleBubble": true,' . PHP_EOL;
	        //show/hide effect: 'no' | 'slide' | 'fade''
	echo '        "fx": "fade"' . PHP_EOL;
	echo '            }' . PHP_EOL;
	echo '        },' . PHP_EOL;
	echo '        \'forms-ext\': {' . PHP_EOL;
	            // Replace native UI behaviour on Chrome, etc.'
	echo '            replaceUI: \'true\',' . PHP_EOL;
	echo '            types: \'date color\',' . PHP_EOL;
	echo '            "widgets": {' . PHP_EOL;
	echo '                "openOnFocus": false,' . PHP_EOL;
	echo '                "calculateWidth": false' . PHP_EOL;
	echo '              }' . PHP_EOL;
	echo '            }' . PHP_EOL;
	echo '        });' . PHP_EOL;
	echo '             webshim.polyfill(\'forms forms-ext\');' . PHP_EOL;
	echo '    webshim.ready(\'form-validators\', function () {' . PHP_EOL;
	    //change default message ('') for the 'dependent' error type'
	echo '    webshim.customErrorMessages.dependent[\'\'] = "Email Mismatch";' . PHP_EOL;
	echo '});' . PHP_EOL;
	*/


	// Minified Version

	echo ' $(document).ready(function(){$("form").removeAttr("novalidate"),$("input, textarea").each(function(){';
	echo '"true"==$(this).attr("aria-required")&&($(this).attr("required","true"),$("label[for=\'"+$(this).attr("id")+"\']").addClass("required"))})})';
	echo ',webshim.setOptions({loadStyles:!1,forms:{replaceValidationUI:!0,lazyCustomMessages:!0,addValidators:!0,ival:{handleBubble:!0,fx:"fade"}},';
	echo '"forms-ext":{replaceUI:"true",types:"date color",widgets:{openOnFocus:!1,calculateWidth:!1}}}),webshim.polyfill("forms forms-ext"),';
	echo 'webshim.ready("form-validators",function(){webshim.customErrorMessages.dependent[""]="Email Mismatch"}); ' . PHP_EOL;
	//------------------------------

	echo '</script>'. PHP_EOL;

	echo '<script type="text/javascript" src="/js/jquery.form.min.js"></script>' . PHP_EOL;
	echo '<script>' . PHP_EOL;
	echo '/* <![CDATA[ */ ' . PHP_EOL;
	echo 'var _wpcf7 = {"loaderUrl":"\/img\/icons\/ajax-loader.gif","sending":"Sending ..."};' . PHP_EOL;
	echo '/* ]]> */ ' . PHP_EOL;
	echo '</script>' . PHP_EOL;
	echo '<script type="text/javascript" src="/js/scripts.cf.min.js"></script>' . PHP_EOL;

}


///------------ Chosen
/*
if(!empty( $page_meta['form']['form_id'] )  )
{

	echo '<script>' . PHP_EOL;
	echo '  $(function () {' . PHP_EOL;
	echo '    ' . PHP_EOL;
	echo '    $(\'select\').chosen();' . PHP_EOL;
	echo '  });' . PHP_EOL;
	

	// Minified Version
	echo '$(function(){$("select").chosen()});' . PHP_EOL;

	echo '</script>' . PHP_EOL;

}

*/

///------------ share button pop-up window
/*
if(is_singular('post'))
{
echo '<script>';



// Expanded Version
echo 'function windowExplode(url, width, height)';
echo '{';

echo 'var leftPosition, topPosition, specs; ';
echo 'leftPosition = (window.screen.width / 2) - ((width / 2) + 10);';
echo 'topPosition = (window.screen.height / 2) - ((height / 2) + 50);';

$specs  = 'status=no,';
$specs .= 'height=" + height + ",';
$specs .= 'width=" + width + ",';
$specs .= 'resizable=yes,';
$specs .= 'left=" + leftPosition + ",';
$specs .= 'top=" + topPosition + ",';
$specs .= 'screenX=" + leftPosition + ",';
$specs .= 'screenY=" + topPosition + ",';
$specs .= 'toolbar=no,';
$specs .= 'menubar=no,';
$specs .= 'scrollbars=no,';
$specs .= 'location=no,';
$specs .= 'directories=no';

echo 'specs = "'.$specs.'";';
echo 'window.open(url, "_blank", specs)';
echo '}';


// Minified Version
echo 'function windowExplode(n,o,e){var i,r,t;i=window.screen.width/2-(o/2+10),r=window.screen.height/2-(e/2+50),t="status=no,height="+e+",width="+o+",resizable=yes,left="+i+",top="+r+",screenX="+i+",screenY="+r+",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no",window.open(n,"_blank",t)} ' . PHP_EOL;

echo '</script>' . PHP_EOL;
}

*/



///------------ SVG script

echo '<script>' . PHP_EOL;

// Expanded Version
/*
echo '	if (!Modernizr.svg) {' . PHP_EOL;
echo '	    var imgs = document.getElementsByTagName(\'img\');' . PHP_EOL;
echo '	    var endsWithDotSvg = /.*\.svg$/' . PHP_EOL;
echo '	    var i = 0;' . PHP_EOL;
echo '	    var l = imgs.length;' . PHP_EOL;
echo '	    for (; i != l; ++i) {' . PHP_EOL;
echo '	        if(imgs[i].src.match(endsWithDotSvg)) {' . PHP_EOL;
echo '	            imgs[i].src = imgs[i].src.slice(0, -3) + \'png\';' . PHP_EOL;
echo '	        }' . PHP_EOL;
echo '	    }' . PHP_EOL;
echo '	}' . PHP_EOL;
echo '</script>' . PHP_EOL;
*/

// Minified Version
echo 'if(!Modernizr.svg)for(var imgs=document.getElementsByTagName("img"),endsWithDotSvg=/.*\.svg$/,i=0,l=imgs.length;i!=l;++i)imgs[i].src.match(endsWithDotSvg)&&(imgs[i].src=imgs[i].src.slice(0,-3)+"png"); ' . PHP_EOL;
echo '</script>' . PHP_EOL;



//------------------------








/// --- gallery
/*
*   http://www.idangero.us/swiper/demos/#.VbtpgPlVhBd
*/

if(defined('gallery'))
{

	echo '<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.1.0/js/swiper.min.js"></script>'.PHP_EOL;

	echo '<script> '.PHP_EOL;

	/*
	// Expanded Version
	echo '$(document).ready(function(){ '.PHP_EOL;
	echo 'var galleryTop = new Swiper(".swiper-container", { '.PHP_EOL;
	echo '		pagination: ".swiper-pagination", ' .PHP_EOL;
	echo '		paginationClickable: true, ' . PHP_EOL;
	echo '      nextButton: ".swiper-button-next", '.PHP_EOL;
	echo '      prevButton: ".swiper-button-prev", '.PHP_EOL;
	echo '      spaceBetween: 40,'.PHP_EOL;
	echo '  }); '.PHP_EOL;
	echo '}); '.PHP_EOL;
	*/

	// Minified Version
	echo '$(document).ready(function(){new Swiper(".swiper-container",{pagination:".swiper-pagination",paginationClickable:!0,nextButton:".swiper-button-next",prevButton:".swiper-button-prev",spaceBetween:40})}); ' . PHP_EOL;
	echo '</script> '.PHP_EOL;
}













///--------------------Snap Open/Close
/* NAV script */
echo '<script>' . PHP_EOL;
/*
// Expanded Version
echo '$(function() {' . PHP_EOL;
echo '	window.addEventListener(\'push\', snap);' . PHP_EOL;
echo '	var desktop = true,' . PHP_EOL;
echo '			viewport = 900,' . PHP_EOL;
echo '			width = $(window).width();' . PHP_EOL;
echo '	if (width < viewport) {' . PHP_EOL;
echo '		if (!window.snapper) {' . PHP_EOL;
echo '			snap();' . PHP_EOL;
echo '		}' . PHP_EOL;
echo '		desktop = false;' . PHP_EOL;
echo '	}' . PHP_EOL;
echo '	$(window).resize(function() {' . PHP_EOL;
echo '		if ($(this).width() != width) {' . PHP_EOL;
echo '			width = $(this).width();' . PHP_EOL;
echo '			if ((width < viewport) && desktop) {' . PHP_EOL;
echo '				if (!window.snapper) {' . PHP_EOL;
echo '					snap();' . PHP_EOL;
echo '				}' . PHP_EOL;
echo '				else {' . PHP_EOL;
echo '					window.snapper.enable();' . PHP_EOL;
echo '				}' . PHP_EOL;
echo '				desktop = false;' . PHP_EOL;
echo '			}' . PHP_EOL;
echo '			else if ((width >= viewport) && !desktop) {' . PHP_EOL;
echo '				if (window.snapper) {' . PHP_EOL;
echo '					if (snapper.state().state == \'right\') {' . PHP_EOL;
echo '						window.snapper.close();' . PHP_EOL;
echo '					}' . PHP_EOL;
echo '					window.snapper.disable();' . PHP_EOL;
echo '				}' . PHP_EOL;
echo '				desktop = true;' . PHP_EOL;
echo '			}' . PHP_EOL;
echo '		}' . PHP_EOL;
echo '	});' . PHP_EOL;
echo '});' . PHP_EOL;
echo 'var snap = function() {' . PHP_EOL;
echo '		window.snapper = new Snap({' . PHP_EOL;
echo '			element: document.getElementById(\'page\'),' . PHP_EOL;
echo '			disable: \'left\'' . PHP_EOL;
echo '		});' . PHP_EOL;
echo '		var addEvent = function addEvent(element, eventName, func) {' . PHP_EOL;
echo '			if (element.addEventListener) {' . PHP_EOL;
echo '					return element.addEventListener(eventName, func, false);' . PHP_EOL;
echo '				}' . PHP_EOL;
echo '				else if (element.attachEvent) {' . PHP_EOL;
echo '					return element.attachEvent(\'on\' + eventName, func);' . PHP_EOL;
echo '				}' . PHP_EOL;
echo '		};' . PHP_EOL;
echo '		addEvent(document.getElementById(\'snap-open\'), \'click\', function() {' . PHP_EOL;
echo '			if (snapper.state().state == \'right\') {' . PHP_EOL;
echo '				window.snapper.close();' . PHP_EOL;
echo '			}' . PHP_EOL;
echo '			else {' . PHP_EOL;
echo '				window.snapper.open(\'right\');' . PHP_EOL;
echo '			}' . PHP_EOL;
echo '		});' . PHP_EOL;
echo '		addEvent(document.getElementById(\'snap-close\'), \'click\', function() {' . PHP_EOL;
echo '			window.snapper.close();' . PHP_EOL;
echo '		});' . PHP_EOL;
echo '		(function (a, b, c) {' . PHP_EOL;
echo '				if (c in b && b[c]) {' . PHP_EOL;
echo '						var d, e = a.location,' . PHP_EOL;
echo '								f = /^(a|html)$/i;' . PHP_EOL;
echo '						a.addEventListener(\'click\', function (a) {' . PHP_EOL;
echo '								d = a.target;' . PHP_EOL;
echo '								while (!f.test(d.nodeName)) d = d.parentNode;' . PHP_EOL;
echo '								\'href\' in d && (d.href.indexOf(\'http\') || ~d.href.indexOf(e.host)) && (a.preventDefault(), e.href = d.href)' . PHP_EOL;
echo '						}, !1)' . PHP_EOL;
echo '				}' . PHP_EOL;
echo '		})(document, window.navigator, \'standalone\');' . PHP_EOL;
echo '}' . PHP_EOL;
*/


// Minified Version
echo '$(function(){window.addEventListener("push",snap);var n=!0,e=900,t=$(window).width();e>t&&(window.snapper||snap(),n=!1),$(window).resize(function(){$(this).width()!=t&&(t=$(this).width(),e>t&&n?(window.snapper?window.snapper.enable():snap(),n=!1):t>=e&&!n&&(window.snapper&&("right"==snapper.state().state&&window.snapper.close(),window.snapper.disable()),n=!0))})});var snap=function(){window.snapper=new Snap({element:document.getElementById("page"),disable:"left"});var n=function(n,e,t){return n.addEventListener?n.addEventListener(e,t,!1):n.attachEvent?n.attachEvent("on"+e,t):void 0};n(document.getElementById("snap-open"),"click",function(){"right"==snapper.state().state?window.snapper.close():window.snapper.open("right")}),n(document.getElementById("snap-close"),"click",function(){window.snapper.close()}),function(n,e,t){if(t in e&&e[t]){var a,i=n.location,o=/^(a|html)$/i;n.addEventListener("click",function(n){for(a=n.target;!o.test(a.nodeName);)a=a.parentNode;"href"in a&&(a.href.indexOf("http")||~a.href.indexOf(i.host))&&(n.preventDefault(),i.href=a.href)},!1)}}(document,window.navigator,"standalone")};' . PHP_EOL;
echo '</script>' . PHP_EOL;
//-------------------------------------------------

//------------------Scroll In Page Anchor-------------------------
/*
echo '<script>' . PHP_EOL;

echo '$(document).ready(function(){' . PHP_EOL;
echo '	$(\'a[href^="#"]\').on(\'click\',function (e) {' . PHP_EOL;
echo '	    e.preventDefault();' . PHP_EOL;

echo '	    var target = this.hash;' . PHP_EOL;
echo '	    var $target = $(target);' . PHP_EOL;

echo '	    $(\'html, body\').stop().animate({ ' . PHP_EOL;
echo '	        \'scrollTop\': $target.offset().top ' . PHP_EOL;
echo '	    }, 300, \'swing\', function () { ' . PHP_EOL;
echo '	        window.location.hash = target;' . PHP_EOL;
echo '	    }); ' . PHP_EOL;
echo '	});' . PHP_EOL;
echo '});' . PHP_EOL;
echo ' </script>' . PHP_EOL;
*/


/*
if( is_single() )
{
	echo ' <script>  ' ;
	echo ' $(document).ready(function(){ ' ;
	echo ' 	webshim.polyfill("sticky"); ' ;
	echo ' }); ' ;
	echo ' </script> ' ;
}

*/


// OTHER
/*
echo '<script> ';
echo ' function window_fit(){ ';
echo ' var bodyH = $( window ).height(); ';
echo ' var navH = $("#head").height();' ;
echo ' var footH = $("#the-footer").height(); ';
echo ' var contH = bodyH - (navH + footH);';
echo ' $("#the-content").css("min-height" ,contH); ' ;
echo '}';
echo '$(document).ready(function(){ ';


// btn height fix
echo '		$(".btn-wrap").each(function(){ ';
echo '			var btnH = $(this).find(".btn").outerHeight(); ';
echo '			$(this).css("height", btnH); ';
echo '				}); ';

echo 'window_fit(); ';

echo '}); ';

echo ' $( window ).resize( function(){ window_fit(); } ); ';
echo '</script> ';

*/
