<?php


$page_url = WP_HOME . $_SERVER['REQUEST_URI']; 
$page_url_ex = explode('?', $page_url);
$page_url = $page_url_ex[0];


$cases_per_page = $bbPage->get_field('studies_per_page','nr');

if(empty($cases_per_page))
{
	$cases_per_page = get_option('posts_per_page');
}

$cases_size = sizeof($the_query);


$current = (!empty($_GET['currentpage'])) ? $_GET['currentpage'] : 1;

$boxes = array();

if($cases_size > $cases_per_page)
{
	$boxes = array_chunk($the_query, $cases_per_page);
}
else
{
	$boxes[0] = $the_query;
}


$pages = sizeof($boxes);







echo '<div id="paginated" class="cs-list">' . PHP_EOL;

echo '<div id="ini-box">'.PHP_EOL;

$c = $current - 1;

$ini_set = $boxes[$c];


if(!empty($ini_set))
{
	foreach ($ini_set as $cpost) 
	{
		$article_id = $cpost->ID;
		include PARTIALS_DIR . 'cs-list-item.php';
	}
}
else
{
	echo 'No Post Found';
}


echo '</div>' . PHP_EOL;


echo '</div>' . PHP_EOL;


//dump($boxes[1]);

if($current == 1)
{
	$prevLink = '';
	$nextLink = $page_url . '?currentpage=2';
}
elseif($current > 1 && $current < $pages )
{	
	$pp = $current - 1;
	$prevLink = $page_url . '?currentpage='. $pp;

	$np = $current + 1;
	$nextLink = $page_url . '?currentpage=' . $np;
}
elseif( $current >= $pages)
{
	$pp = $current - 1;
	$prevLink = $page_url . '?currentpage='. $pp;
	$nextLink = '';
}


$l_state = (empty($prevLink)) ? 'disabled' : 'active';
$r_state = (empty($nextLink)) ? 'disabled' : 'active';

if( $pages > 1 )

{
	echo '<div class="section pagination"><div class="container">' . PHP_EOL;
	echo '<nav>'.PHP_EOL;
	echo '<div class="navi navi-left '.$l_state.'">';
	if(!empty($prevLink ))
	{
		echo '<a href="' . $prevLink . '"></a>';
	}
	
	echo '</div>'.PHP_EOL;

	for($e=1; $e <= $pages; $e++)
	{

		if( $e == $current )
		{
			$state = 'disabled';
			$in = '<span>' . $e . ' </span>';
		}
		elseif($e === 1)
		{
			$state = 'active';
			$pagi_link = $page_url;
			$in = '<a href="'.$pagi_link.'" >'. $e . '</a> ';
		}
		else
		{
			$state = 'active';
			$pagi_link = '?currentpage=' .$e;
			$in = '<a href="'.$pagi_link.'" >'. $e . '</a> ';
		}



		echo '<div class="nr '.$state.'">'.$in.'</div>' . PHP_EOL;

	}
	echo '<div class="navi navi-right '.$r_state.'">';
	if(!empty($nextLink))
	{
		echo '<a href="' . $nextLink . '"></a>';
	}
	echo '</div>'.PHP_EOL;
	echo '</nav>';
	
	echo '</div></div>' . PHP_EOL;

}