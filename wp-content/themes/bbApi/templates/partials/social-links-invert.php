<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Social links partial
*/

//-----------Options from Redux---------

if( empty($options) )
{
	$options  = bb_get_options();
}

$fb_url = ( !empty($options['facebook']) ) ? $options['facebook'] : null;
$in_url = ( !empty($options['linkedin']) ) ? $options['linkedin'] : null;
$tw_url = ( !empty($options['twitter']) ) ? $options['twitter']   : null;
$gp_url = ( !empty($options['goolge+']) ) ? $options['goolge+']   : null;
$yt_url = ( !empty($options['youtube']) ) ? $options['youtube']   : null;
$rs_url  = ( !empty($options['rss']) ) ? $options['rss'] : null;
//-----------------------




$socials = array();

if($fb_url !== null)
{
	$socials['facebook']['url']        = $fb_url;
	$socials['facebook']['svg_icon']   = '/img/icons/social/facebook-white.svg';
	$socials['facebook']['class']      = 'fb';
}

if( $in_url )
{
	$socials['linkedin']['url']        = $in_url;
	$socials['linkedin']['svg_icon']   = '/img/icons/social/linkedin-white.svg';
	$socials['linkedin']['class']      = 'in';
}

if( $tw_url !== null )
{
	$socials['twitter']['url']         = ( strpos($tw_url, 'http') ) ? $tw_url : 'https://twitter.com/'.$tw_url;
	$socials['twitter']['svg_icon']    = '/img/icons/social/twitter-white.svg';
	$socials['twitter']['class']       = 'twitter';
}


if( $gp_url !== null )
{
	$socials['googlePlus']['url']      = $gp_url;
	$socials['googlePlus']['svg_icon'] = '/img/icons/social/google-plus-white.svg';
	$socials['googlePlus']['class']    = 'gplus';
}

if($yt_url !== null)
{
	$socials['YouTube']['url']         = $yt_url;
	$socials['YouTube']['svg_icon']    = '/img/icons/social/you-tube-white.svg';
	$socials['YouTube']['class']       = 'youtube';
}



if( $rs_url !== null AND (isset($add_rss) AND $add_rss) )
{
	$socials['rss']['url']        = $rs_url;
	$socials['rss']['svg_icon']   = '/img/icons/social/rss.svg';
	$socials['rss']['class']      = 'in';
}






//--------------------------------------
//echo '<p>Follow us for regular insights on brand building and online growth</p>' . PHP_EOL;
echo '<ul class="social invert">';


foreach($socials as $label=>$link)
{
	if($link['url'] !== null)
	{
		echo '<li class="'.$link['class'].'">';
		echo '<a href="'.$link['url'].'" target="_blank">';
		echo '<img width="45" height="45" src="' . $link['svg_icon'] .'" alt="' . $label . '" />';
		echo '</a></li>';
	}
}

echo '</ul>'.PHP_EOL;
