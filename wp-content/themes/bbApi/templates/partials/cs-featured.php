<?php


if(empty($bbPage))
{
	$bbPage = new bbPage(PAGE_ID);
}

// dump($current_tax);
if(empty($current_tax) OR $current_tax == 'all')
{
     $featured_id = $bbPage->get_field('featured_study','study_id');
}
else
{
	 $featured_id = $bbPage->get_field('featured_study','study_id_'.$current_tax);
}

if(!isset($current_tax))
{
		$current_tax = '';
}

$csPost = new bbPost($featured_id);

//dump($csPost);

if($featured_id) :
echo '<div class="section featured-cs">' . PHP_EOL;

	$bg_image_id = $bbPage->get_field('featured_study','image');

	if($bg_image_id === null)
	{
		$bg_image_id = $csPost->leadin_image;
	}

	echo '<div class="container"';

	if($bg_image_id !== null)
	{
		$bg_image = new bbImage($bg_image_id, 'container');
		//dump($bg_image);
		echo ' style="background-image: url(\'';
		echo $bg_image->src;
		echo '\')"';

	}
	echo '>' . PHP_EOL;

	echo '<div class="overlay">' . PHP_EOL;

	$terms = wp_get_post_terms( $featured_id , 'industry');
	

	if(!empty($terms) AND !is_wp_error($terms))
	{
		$taxonomy = $terms[0];
		echo '<h4>' . $taxonomy->name . '</h4>' . PHP_EOL;
	}
	elseif(WP_DEBUG)
	{
		echo '<!-- $terms: empty -->';
	}


	$featured_title_main = $bbPage->get_field('featured_study','title');
	$featured_tax_title = $bbPage->get_field('featured_study','title_'.$current_tax);

	if($featured_tax_title)
	{
		$featured_title = $featured_tax_title;
	}
	elseif($featured_title_main)
	{
		$featured_title = $featured_title_main;
	}
	elseif( !empty($csPost->leadin_title) )
	{
		$featured_title =  $csPost->leadin_title;
	}
	else
	{
		$featured_title = null;
	}

	if($featured_title !== null)
	{
		echo '<h3>' . $featured_title .'</h3>';
	}
	elseif(WP_DEBUG)
	{
		echo '<!-- $featured_title: empty -->';
	}

	

	$logo = get_post_meta( $featured_id , 'company-logo' );
	$th = get_post_thumbnail_id($featured_id );
	if(!empty($logo[0]['image']) AND $logo[0]['image'] != 0)
	{
		$logo_id = $logo[0]['image'];
		$logo_img = new bbImage($logo_id, 'logo');
		echo $logo_img->html;
	}
	elseif($th AND $th != 0)
	{
		$logo_img = new bbImage($th, 'logo');
		echo $logo_img->html;
	}
	elseif(WP_DEBUG)
	{
		echo '<!-- $logo: empty -->';
	}

	echo '<div class="block">' . PHP_EOL;
	echo '<a href="' . get_permalink($featured_id) .'" class="btn tertiary">Read Case Study</a>';
	echo '</div>' . PHP_EOL;
	echo '</div>' . PHP_EOL; //.overlay
	echo '</div>' . PHP_EOL; //.container
	echo '</div>' . PHP_EOL;//.section
else:
	if(WP_DEBUG)
	{
		//dump($featured_id);
		//dump($current_tax);
	}
endif;