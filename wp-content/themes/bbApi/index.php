<?php
defined('ABSPATH') or die('Access Denied!');
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package bigbang
 */

get_header();


$theBlogId = (defined('BLOG_ID')) ? BLOG_ID : get_option( 'page_for_posts' );
$url = get_permalink($theBlogId);


if(empty($bbPage))
{
	$bbPage = new bbPage(PAGE_ID);
}

include_once PARTIALS_DIR . 'hero.php';





echo '<div class="section blog" itemscope itemtype="http://schema.org/Blog">' . PHP_EOL;
echo '<meta itemprop="name" content="' . COMPANY_NAME . '">';
echo '<meta itemprop="url" content="'.$url.'">';


echo '<div class="container">' . PHP_EOL;



  if(empty($options['blog_list_intro_blog']) OR $options['blog_list_intro_blog'] == '0')
  {
    $blog_list_intro = false;
  }
  else
  {
    $blog_list_intro = true; 
  }

include_once PARTIALS_DIR . 'blog-list.php';
include_once PARTIALS_DIR . 'pagination.php';



echo '</div>' . PHP_EOL; // .container
echo '</div>' . PHP_EOL; // .section




get_footer();
