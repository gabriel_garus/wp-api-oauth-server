<?php
defined('ABSPATH') or die('Access Denied!');
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package bigbang
 */

//$meta_data = bb_extract(get_post_meta(PAGE_ID));
//dump($meta_data);
/*
if( !have_posts() )
{	
	}

*/
get_header();

if(empty($options))
{
	$options = bb_get_options();
}

if(!empty($options['studies_page']))
{
	$cs_page_id = $options['studies_page'];
}
elseif(  get_page_by_path('case-studies') )
{
	$cs_page = get_page_by_path('case-studies');
	$cs_page_id = $cs_page_id = $cs_page->ID;
}
else
{
	$cs_page_id = null;
}

if($cs_page_id !== null)
{
	$bbPage = new bbPage($cs_page_id);
	include_once PARTIALS_DIR . 'hero.php';



	$queried_object = get_queried_object();
	//var_dump( $queried_object );

	$args = array(
			'post_type' => 'studies',
			'posts_per_page' => -1, 
			'tax_query' => array( array(
				'taxonomy' => $queried_object->taxonomy,
				'field'    => 'slug',
				'terms'    => $queried_object->slug,
				)));
	$the_query = get_posts( $args );

	define('QUERIED_TAX', $queried_object->slug);

	include_once PARTIALS_DIR . 'cs-catlist.php';

	include_once PARTIALS_DIR . 'cs-featured.php';
	
	include_once PARTIALS_DIR . 'cs-posts.php';
}
else
{
	die('Case Studies Page not Found');
}


get_footer();
