<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Single services
*/

get_header();




include_once PARTIALS_DIR . 'facebook-share-script.php';

echo '<script src="https://apis.google.com/js/platform.js" async defer></script>'; 


$bbPage = new bbPage( PAGE_ID );
$thePost = $bbPage; 


$disable_sidebar = $bbPage->get_field('sidebar','disable_sidebar'); 


if($disable_sidebar == 'true')
{
	$have_sidebar = false; 
}
else
{
	$have_sidebar = true; 
}



$search_title =      $bbPage->get_field('sidebar','search_title'); 
$hide_search_title = $bbPage->get_field('sidebar','hide_search_title'); 
//--------------------------------

if(empty($search_title))
{
	$search_title = 'Search Solutions';
}

include_once PARTIALS_DIR . 'hero.php';



echo '<div class="section over">' . PHP_EOL;
echo '<div class="container cf">' . PHP_EOL;

if($have_sidebar)
{
	echo '<div class="left">';
}


////////////     MIDDLE   ////////////////////////////////////////
$mid_title = $bbPage->get_field('middle','title');

$parts = array();


for ($i=1; $i <=3 ; $i++) 
{ 	
	$part = array();
	$part_title = $bbPage->get_field('middle', 'title_'. $i);
	$part_text  = $bbPage->get_field('middle', 'text_'. $i);

	if($part_title !== null) {	$part['title'] = $part_title; }

	if($part_text !== null) { $part['text'] = $part_text; }
	
	if(!empty($part)){ 	$parts[] = $part; }

}



if( sizeof($parts) > 0 || $mid_title )
{
	echo '<div class="section middle single-solution">' . PHP_EOL;
	echo '<div class="cf">' . PHP_EOL;

	if($mid_title !== null)
	{
		echo '<h2>'. $mid_title .'</h2>' . PHP_EOL;
	}

	if(!empty($parts))
	{
		echo '<div class="parts cf">' . PHP_EOL;

		foreach ($parts as $part)
		{
			echo '<div class="part">' . PHP_EOL;

			if(empty($part['title']) === false )
			{
				echo '<h3>' . $part['title']. '</h3>';
			}

			if(empty($part['text']) === false )
			{
				echo '<p>' . $part['text']. '</p>';
			}

			echo '</div>' . PHP_EOL;
		}

		echo '</div>' . PHP_EOL;
	}



	echo '</div>' . PHP_EOL;
	echo '</div>' . PHP_EOL;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////







////////////     WIDE    ////////////////////////////////////////

$image_top_id   = $bbPage->get_field('wide', 'image_top');
$wide_title     = $bbPage->get_field('wide', 'title');
$image_bot_id_1 = $bbPage->get_field('wide', 'image_bottom_1');
$image_bot_id_2 = $bbPage->get_field('wide', 'image_bottom_2');

if($image_top_id || $wide_title || $image_bot_id_1 || $image_bot_id_2 )
{
	echo '<div class="section single-solution wide-images">' . PHP_EOL;
	echo '<div class="cf">' . PHP_EOL;
	
	if($image_top_id)
	{
		$image_top = new bbImage($image_top_id, 'container');
		$image_top->set_class('tele');
		echo $image_top->html;
	}


	if($wide_title)
	{
		echo '<h3>'.$wide_title .'</h3>' . PHP_EOL;
	}



	echo '<div class="bot-images cf">' . PHP_EOL;
	if($image_bot_id_1)
	{
		$image_bottom_1 = new bbImage($image_bot_id_1, 'full');
		$image_bottom_1->set_class('left');
		echo $image_bottom_1->html;
	}

	if($image_bot_id_2)
	{
		$image_bottom_2 = new bbImage($image_bot_id_2, 'full');
		$image_bottom_2->set_class('right');
		echo $image_bottom_2->html;
	}
	echo '</div>' . PHP_EOL;

	echo '</div>' . PHP_EOL;
	echo '</div>' . PHP_EOL;

}




/////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////     BOTTOM    ////////////////////////////////////////

$bot_title = $bbPage->get_field('bottom','title');
$bot_content = $bbPage->get_field('bottom','last');


if($bot_title || $bot_content)
{

echo '<div class="section bottom single-solution post-content';

if($have_sidebar == false)
{
	echo ' full-width';
}


echo'">' . PHP_EOL;
echo '<div class="cf">' . PHP_EOL;

if($bot_title)
{
	echo '<h3>'.$bot_title .'</h3>' . PHP_EOL;
}


if($bot_content)
{
	echo wpautop( do_shortcode( $bot_content ) );
}

echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL;

}




/////////////////////////////////////////////////////////////////////////////////////////////////////////
if($have_sidebar)
{
	echo '</div>' . PHP_EOL; // .left

	//-----------  SIDEBAR    ---------------------------------
	echo '<div class="right sidebar">' . PHP_EOL;

	if($hide_search_title !== 'true' AND $search_title !== null)
	{
		echo '<h2 class="search-title">' . $search_title . '</h2>';
	}

	get_search_form();

	$post_type = 'solutions';
	include_once PARTIALS_DIR . 'related-services.php';


	include_once PARTIALS_DIR . 'social-links.php';



	$rs_url  = ( !empty($options['rss']) ) ? $options['rss'] : null;
	if($rs_url !== null)
	{
		echo '<h2>Keep Up To Date</h2>';
		echo '<ul class="social">';
		echo '<li class="rss">';
		echo '<a href="'. $rs_url .'" target="_blank">';
		echo '<img width="45" height="45" src="/img/icons/social/rss.svg" alt="rss icon" />';
		echo '</a></li>';
		echo '</ul>' . PHP_EOL;
	}

	echo '</div>' . PHP_EOL; // .sidebar
	//--------------------------------------------

}


echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL;



/////////////////////////////////////////////////////////////////////////////////////////////////////////


include PARTIALS_DIR . 'white-section.php';


include PARTIALS_DIR . 'cta.php';

include PARTIALS_DIR . 'promise.php';



//////   RELATED STUDY   ////////////////////

$section = $bbPage->get_section('related');

if($section)
{
	echo '<div class="section solutions-article">' . PHP_EOL;
	echo '<div class="container align-left blog-list">' . PHP_EOL;
	if(!empty($section['title']))
	{
	  echo '<h2>' . $section['title'] . '</h2>';
	}


	if(!empty($section['id']))
	{
	    $article_id = $section['id'];

	    

		$blog_post 	= new bbPost( $article_id );
		//dump($blog_post);
	
		$post_title   			= $blog_post->leadin_title;
		$post_excerpt 			= strip_tags($blog_post->leadin_text);
		$post_href  			= $blog_post->url;
		$thumb_id  				= $blog_post->leadin_image;



		echo '<article class="cf">' . PHP_EOL;
		echo '<div class="left">' . PHP_EOL;

		////---- header ----
		echo '<header>'.PHP_EOL;
		echo '<h2 itemprop="headline"><a href="' . $post_href . '">' . $post_title . '</a></h2>'.PHP_EOL;
		echo '</header>'.PHP_EOL;
		//// ----------------------------------



		echo '<p class="intro">' . $post_excerpt . '</p>'. PHP_EOL;


		echo '<div class="block read-more">';
		echo '<a itemprop="url" href="'. $post_href .'"  class="btn tertiary">Read More</a>';
		echo '</div>' . PHP_EOL;


		echo '</div>' . PHP_EOL; //.left
		//---------------------------


		if(!empty($thumb_id))
		{	
			echo '<div class="right post-thumbnail">' . PHP_EOL;
			$img = new bbImage($thumb_id);
			echo $img->html;
			echo '</div>'.PHP_EOL;
		}



		//---------------------------
		// --- Read More button



		echo '</article>'.PHP_EOL;

		$article_id = null; 

	}
	    
	echo '</div>' . PHP_EOL;
	echo '</div>' . PHP_EOL;
}



get_footer();





