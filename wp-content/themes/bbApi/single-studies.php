<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Single
*/

get_header();


include_once PARTIALS_DIR . 'facebook-share-script.php';

echo '<script src="https://apis.google.com/js/platform.js" async defer></script>'; 


$thePost 	= new bbPost( PAGE_ID );
$meta_data  = $thePost->meta;
$title 		= $thePost->title;
$content  	=  $thePost->content;


$thePost->set_img_itemprop('image');

$post_tags = $thePost->get_tag_list();
$gallery   = $thePost->gallery;

$middle_content = $thePost->get_field('middle_content', 'text');
$bottom_content = $thePost->get_field('bottom_content', 'text');

//dump($meta_data);
$theBlogId = (defined('BLOG_ID')) ? BLOG_ID : get_option( 'page_for_posts' );
$url = get_permalink(PAGE_ID);

$post_url = get_permalink(PAGE_ID);


//--------------------------------




echo '<div class="section single-study" itemscope itemtype="http://schema.org/Blog">' . PHP_EOL;
echo '<div class="container cf">' . PHP_EOL;
the_breadcrumb();
echo '<meta itemprop="name" content="' . COMPANY_NAME . '">';
echo '<meta itemprop="url" content="'.$url.'">';

//--------- LEFT -----------------------
echo '<div class="left">' . PHP_EOL;

//-------- Article ---------
echo '<article itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">'.PHP_EOL;
echo '<meta itemprop="url" content="'.$post_url.'">'.PHP_EOL;

//--------- Header -----------------------

if(!empty($title))
{	
	echo '<header>' . PHP_EOL;
	echo '<h1 itemprop="headline">' . $title . '</h1>' . PHP_EOL;
	echo '</header>' . PHP_EOL;
}

//--------------------------------

//----------- CONTENT ---------------------
echo '<div class="post-content">' . PHP_EOL;
echo  do_shortcode( wpautop($content) );

$call_us = $thePost->get_section('call-us');

if(!empty($call_us))
{
	echo '<div class="call-us">' . PHP_EOL;

	bb_title($call_us['title'], 3);

	echo '<p>'. $call_us['text'] . '</p>'. PHP_EOL;

	echo '<div class="cf">' . PHP_EOL;
	echo '<div class="left">' ;

	if(!empty($call_us['heading']))
	{	
		echo '<p class="heading">' .$call_us['heading'] . '</p>'; 
    }
    echo '</div>' . PHP_EOL; //.left

    echo '<div class="right">' ;
    if(!empty($call_us['phone']))
	{	
		$telephone = trim($call_us['phone']);
		$tele_short = str_replace(' ', '', $telephone);
		echo '<a href="tel:'.$tele_short.'" class="telephone">'. $telephone . '</a> '.PHP_EOL;
	}
    echo '</div>' . PHP_EOL; // .right
	echo '</div>' . PHP_EOL; // .cf
	echo '</div>' . PHP_EOL; // .call-us
}



echo '</div>' . PHP_EOL; // .post-contetn
echo '<hr />';
echo '<div class="article-share">' . PHP_EOL;
echo '<h3>Share This Case Study</h3>' . PHP_EOL;
include_once PARTIALS_DIR . 'article-share.php';
echo '</div>' . PHP_EOL;
//--------------------------------

echo '</article>' . PHP_EOL;





echo '</div>' . PHP_EOL; // . left
//--------------------------------




//----------Right Aside ----------------------
echo '<div class="right sidebar">' . PHP_EOL;

//----------- Related Articles ---------------------
include_once PARTIALS_DIR . 'related-studies.php';
//--------------------------------


//--------------- Social Follow -----------------


include_once PARTIALS_DIR . 'social-links.php';


echo '</div>';// .right sidebar
//--------------------------------


echo '</div>'; //.container
echo '</div>' . PHP_EOL; // .section

include_once PARTIALS_DIR . 'cta.php';

get_footer();


