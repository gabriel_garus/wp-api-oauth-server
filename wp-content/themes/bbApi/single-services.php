<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Single services
*/

get_header();


include_once PARTIALS_DIR . 'facebook-share-script.php';

echo '<script src="https://apis.google.com/js/platform.js" async defer></script>'; 


$thePost 	= new bbPost( PAGE_ID );
$meta_data  = $thePost->meta;
$title 		= $thePost->title;
$content  	=  $thePost->content;
$the_author = $thePost->author;

$thePost->set_img_itemprop('image');

$post_tags = $thePost->get_tag_list();
$gallery   = $thePost->gallery;




//dump($meta_data);
$theBlogId = (defined('BLOG_ID')) ? BLOG_ID : get_option( 'page_for_posts' );
$url = get_permalink(PAGE_ID);

$post_url = get_permalink(PAGE_ID);

include_once PARTIALS_DIR . 'hero.php';
//--------------------------------





////////////////////////  CONTENT  //////////////////////////////////////////////////

$middle_content = $bbPage->content;
$search_title =      $bbPage->get_field('sidebar','search_title'); 
$hide_search_title = $bbPage->get_field('sidebar','hide_search_title'); 


if($search_title === null AND $hide_search_title !== 'true')
{
	$search_title = 'Search Our Services';
}

echo '<div class="section single-service">' . PHP_EOL;
echo '<div class="container cf">' . PHP_EOL;

echo '<div class="left">' . PHP_EOL;
echo '<div class="post-content">' . PHP_EOL;
echo wpautop( do_shortcode($middle_content ));
echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL;



//-----------  SIDEBAR    ---------------------------------
echo '<div class="right sidebar">' . PHP_EOL;

if($hide_search_title !== 'true' AND $search_title !== null)
{
	echo '<h2 class="search-title">' . $search_title . '</h2>';
}

get_search_form();

$post_type = 'services';

include_once PARTIALS_DIR . 'related-services.php';


include_once PARTIALS_DIR . 'social-links.php';



$rs_url  = ( !empty($options['rss']) ) ? $options['rss'] : null;
if($rs_url !== null)
{
	echo '<h2>Keep Up To Date</h2>';
	echo '<ul class="social">';
	echo '<li class="rss">';
	echo '<a href="'. $rs_url .'" target="_blank">';
	echo '<img width="45" height="45" src="/img/icons/social/rss.svg" alt="rss icon" />';
	echo '</a></li>';
	echo '</ul>' . PHP_EOL;
}

echo '</div>' . PHP_EOL; // .sidebar
//--------------------------------------------
	
echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL;

///////////////////////////////////////////////////////////////////////////////




////////////////////////  ARTICLES  //////////////////////////////////////////////////
$related_artcles = array();

for($a=1;$a<=2; $a++)
{
	$related_artcles[] = $thePost->get_field('related-articles', 'article_'.$a);
}

$related_articles_title = $thePost->get_field('related-articles', 'title');

if($related_articles_title == null)
{$related_articles_title = 'Related Blog Posts'; }
if( array_sum($related_artcles) > 0 )
{

echo '<div class="section news-feed single-service">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;
echo '<h3>' . $related_articles_title . '</h3>' . PHP_EOL;

echo '<div class="list cf">' . PHP_EOL;
	foreach ($related_artcles as $article_id)
	{
		if(!empty($article_id))
		{
			$read_more_link_revert = true;
			include PARTIALS_DIR . 'blog-list-item-home.php';
		}
		
	}
echo '</div>' . PHP_EOL;

echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL;
}

include PARTIALS_DIR . 'cta.php';


///////////////////////////////////////////////////////////////////////////////

get_footer();