<?php
defined('ABSPATH') or die('Access Denied!');
//*********** Contact Details*************

$section_id = 'footer';


$sections[] = array( 
        'id'      => $section_id, 
        'title'   => 'Footer',
        'desc'    => '',
        'heading' => 'Page Footer <br /> <hr/>',
        'icon'    => 'el el-arrow-down', // http://elusiveicons.com/icons/
    );

//--------------------------------------------------


redux_image($fields,$section_id,'footerLogo', 'Footer Logo' );

redux_image($fields,$section_id,'footerAffiliation1', 'Footer Affiliation 1');
redux_image($fields,$section_id,'footerAffiliation2', 'Footer Affiliation 2' );

        $fields[] =  array( 
            'id'         => 'footer_email', 
            'type'       => 'text',
            'section_id' => $section_id,
            'title'      => 'Contact Email',
            'validate'   => 'text'
        );

        $fields[] =  array( 
            'id'         => 'footer_telephone', 
            'type'       => 'text',
            'section_id' => $section_id,
            'title'      => 'Contact Telephone',
            'validate'   => 'text'
        );

//--------------------------------------------------


    $fields[] =  array( 
        'id'         => 'button'.'-start', 
        'type'       => 'section',
        'section_id' => $section_id,
        'title'      => 'Button',
        'indent' => true 
     );


        $fields[] =  array( 
            'id'         => 'footer_button_label', 
            'type'       => 'text',
            'section_id' => $section_id,
            'title'      => 'Button Label',
            'validate'   => 'text'
        );

       $fields[] =  array( 
            'id'         => 'footer_button_url', 
            'type'       => 'select',
            'section_id' => $section_id,
            'data'       => 'page',
            'title'      => 'Link'
        );

      $fields[] =  array( 
            'id'         => 'footer_button_custom_url', 
            'type'       => 'text',
            'section_id' => $section_id,
            'title'      => 'Button Custom Url',
            'subtitle'   => 'Will override selection above',
            'validate'   => 'text'
        );





        $fields[] =  array( 
            'id'         => 'button'.'-end', 
            'type'       => 'section',
            'section_id' => $section_id,
            'indent'     => false 
         );

        // --Trade Name

$fields[] =  array( 
        'id'         => 'tradeName',
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'Trade Name',
        'validate'   => 'text'
);

//--------------------------------------------------

// --Registration Number

$fields[] =  array( 
        'id'         => 'registration', 
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'Company Number',
        'validate'   => 'text'
);

//--------------------------------------------------

// --Disclaimer

$fields[] =  array( 
        'id'         => 'disclaimer', 
        'type'       => 'textarea',
        'section_id' => $section_id,
        'title'      => 'Disclaimer Text',
        'validate'   => 'text'
);


$fields[] =  array( 
        'id'         => 'legal-info', 
        'type'       => 'info',
        'section_id' => $section_id,
        'title'      => 'Legal Menu',
        'desc'       => 'Legal Menu can be edited in the <a href="/wp-admin/nav-menus.php?action=edit&menu=3">Menus Section</a>'
);


//--------------------------------------------------
