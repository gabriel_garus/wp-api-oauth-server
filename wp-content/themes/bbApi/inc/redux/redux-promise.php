<?php
defined('ABSPATH') or die('Access Denied!');
//*********** Contact Details*************

$section_id = 'promise';


$sections[] = array( 
        'id'      => $section_id, 
        'title'   => 'Cloudstrong Promise',
        'desc'    => '',
        'heading' => '<br /> <hr/>',
        'icon'    => 'el el-flag', // http://elusiveicons.com/icons/
    );

//--------------------------------------------------


$fields[] =  array( 
        'id'         => 'promise-title', 
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'Title',
        'validate'   => 'text'
);

$fields[] =  array( 
        'id'         => 'promise-subtitle', 
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'Sub Title',
        'validate'   => 'text'
);


$fields[] =  array( 
        'id'         => 'promise-nr', 
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'List Elements Number',
        'validate'   => 'numeric'
);

$items = array();
for($a=1;$a<=$options['promise-nr']; $a++)
{
    $items['text_'.$a] = 'Item '.$a;
}

$fields[] =  array( 
        'id'         => 'promise-list', 
        'type'       => 'sortable',
        'section_id' => $section_id,
        'title'      => 'List',
         'mode'     => 'text',
         'options'  => $items
         );


$fields[] =  array( 
        'id'         => 'promise-background', 
        'type'       => 'media',
        'section_id' => $section_id,
        'title'      => 'Background Number',
        
);