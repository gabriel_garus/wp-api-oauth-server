<?php

defined('ABSPATH') or die('Access Denied!');

/****** SOCIAL LINKS **///

$section_id = 'header';


$sections[] = array( 
        'id'      => $section_id,
        'title'   => 'Header',
        'desc'    => '',
        'heading' => '',
        'icon'    => 'el el-arrow-up', // http://elusiveicons.com/icons/
    );


//---------------------- Top Logo -------------------------------------------
$fields[] =  array( 
        'id'         => 'topLogo-start', 
        'type'       => 'section',
        'section_id' => $section_id,
        'title'      => 'Top Logo',
        'indent' => true 
     );


    $fields[] =  array( 
            'id'         => 'topLogo-image', 
            'type'       => 'media',
            'section_id' => $section_id,
            'title'      => 'The Image',
            'url'        => true
        );


    $fields[] =  array( 
            'id'         => 'topLogo-size', 
            'type'       => 'dimensions',
            'section_id' => $section_id,
            'unit'       => false,
            'default'    => array('width' => '200', 'height'=>'50'),
            'title'      => 'Logo Size Attribute'

        );


$fields[] =  array( 
        'id'         => 'topLogo-end', 
        'type'       => 'section',
        'section_id' => $section_id,
        'indent'     => false 
     );
//-----------------------------------------------------------------

$fields[] =  array( 
            'id'         => 'head_telephone_title', 
            'type'       => 'text',
            'section_id' => $section_id,
            'title'      => 'Telephone Title',
            'subtitle'   => '',
            'validate'   => 'text'
        );

  $fields[] =  array( 
            'id'         => 'head_telephone', 
            'type'       => 'text',
            'section_id' => $section_id,
            'title'      => 'Telephone Number',
            'validate'   => 'text'
        );

