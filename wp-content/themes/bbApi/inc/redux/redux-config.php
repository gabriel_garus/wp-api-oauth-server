<?php

defined('ABSPATH') or die('Access Denied!');

//*************** Config *****************

$section_id = 'config';


$sections[] = array( 
        'id'      => $section_id, 
        'title'   => 'Config',
        'desc'    => '',
        'heading' => '',
        'icon'    => 'el el-adjust-alt', // http://elusiveicons.com/icons/
    );

//--------------------------------------------------




/*
$fields[] =  array( 
        'id'         => 's3bucket', 
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'S3 Bucket',
        'subtitle'   => 'url to images folder on S3 bucket',
        'validate'   => 'url'
);

*/

$fields[] =  array( 
        'id'         => 'bb_prod_url', 
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'Production URL',
        'subtitle'   => 'Server name ("example.com")',
        'desc'       => ''
       // 'validate'   => 'url'
);

$fields[] =  array( 
        'id'         => 'gtmId', 
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'Google Tag Manager ID',
        'validate'   => 'text'
);


//--------------------------------------------------

// --Disqus ID
/*
$fields[] =  array( 
        'id'         => 'disqusId',
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'Disqus ID',
        'validate'   => 'text'
);
*/


if(!empty($options['custom_post_types']))
{
    $custom_post_types = $options['custom_post_types'];
    foreach ($custom_post_types as $type) 
    {
        $fields[] =  array( 
                'id'         => $type.'_page', 
                'type'       => 'select',
                'section_id' => $section_id,
                'data'       => 'page',
                'title'      => 'Page for: <b>' . $type . '</b>'
        );
    }
}

