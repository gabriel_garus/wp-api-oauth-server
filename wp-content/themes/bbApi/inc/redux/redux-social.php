<?php

defined('ABSPATH') or die('Access Denied!');

/****** SOCIAL LINKS **///
$section_id = 'social';



$sections[] = array( 
        'id'    => $section_id, 
        'title' => 'Social Links',
        'desc' => 'Links to social media accounts .',
        'heading' => '',
        'icon' => 'el el-heart', // http://elusiveicons.com/icons/
    );












$fields[] =  array( 
        'id'         => 'linkedin',
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'Linkedin',
        'desc'       => 'Linkedin URL',
        'validate'   => 'url'
);

//--------------------------------------------------


// -- Google+
$fields[] =  array( 
        'id'         => 'goolge+', 
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'Google+',
        'desc'       => 'Google+ URL',
        'validate'   => 'url'
);

//--------------------------------------------------

// -- Twitter
$fields[] =  array( 
        'id'         => 'twitter', 
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'Twitter',
        'desc'       => 'Twitter Username'
);

//--------------------------------------------------

// -- Facebook
$fields[] =  array( 
        'id'         => 'facebook', 
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'Facebook',
        'desc'       => 'Facebook Page URL',
        'validate'   => 'url'
);

//--------------------------------------------------

// -- Youtube
$fields[] =  array( 
        'id'         => 'youtube',
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'YouTube',
        'desc'       => 'YouTube URL',
        'validate'   => 'url'
);


// -- RSS
$fields[] =  array( 
        'id'         => 'rss',
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'RSS link',
        'desc'       => 'RSS feed link',
        'validate'   => 'text'
);

//--------------------------------------------------
