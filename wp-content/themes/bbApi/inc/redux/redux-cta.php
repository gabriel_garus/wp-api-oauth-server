<?php
defined('ABSPATH') or die('Access Denied!');
//*********** Contact Details*************

$section_id = 'cta';


$sections[] = array( 
        'id'      => $section_id, 
        'title'   => 'CTA Section',
        'desc'    => '',
        'heading' => '<br /> <hr/>',
        'icon'    => 'el el-bullhorn', // http://elusiveicons.com/icons/
    );

//--------------------------------------------------




$fields[] =  array( 
        'id'         => 'cta_image', 
        'type'       => 'media',
        'section_id' => $section_id,
        'title'      => 'The Image',
        'url'        => true
    );


$fields[] =  array( 
        'id'         => 'cta_logo', 
        'type'       => 'media',
        'section_id' => $section_id,
        'title'      => 'The Logo',
        'url'        => true
    );


  $fields[] =  array( 
            'id'         => 'cta_name', 
            'type'       => 'text',
            'section_id' => $section_id,
            'title'      => 'Text Under Logo',
            'validate'   => 'text'
        );

$fields[] =  array( 
        'id'         => 'ctaButton-start', 
        'type'       => 'section',
        'section_id' => $section_id,
        'title'      => 'Link',
        'indent' => true 
     );

  $fields[] =  array( 
            'id'         => 'cta_button_label', 
            'type'       => 'text',
            'section_id' => $section_id,
            'title'      => 'The Link Label',
            'validate'   => 'text'
        );

       $fields[] =  array( 
            'id'         => 'cta_button_url', 
            'type'       => 'select',
            'section_id' => $section_id,
            'data'       => 'page',
            'title'      => 'The Link Itself'
        );

      $fields[] =  array( 
            'id'         => 'cta_button_custom_url', 
            'type'       => 'text',
            'section_id' => $section_id,
            'title'      => 'Link Custom Url',
            'subtitle'   => 'Will override selection above',
            'validate'   => 'text'
        );

      $fields[] =  array( 
        'id'         => 'ctaButton-end', 
        'type'       => 'section',
        'section_id' => $section_id,
        'indent'     => false 
     );