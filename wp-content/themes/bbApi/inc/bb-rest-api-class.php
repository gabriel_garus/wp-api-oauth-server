<?php

class bb_rest_api
{

	public $namespace = 'okapi';
	public $post_types = array();
	private $db_prefix = '4po77o_';
	



	public function __construct()
	{
		$the_types = get_post_types(array( '_builtin' => false ));
		return true;
	}





	public function launch()
	{
		add_action( 'rest_api_init', array( $this, 'register_fields'));
		add_action( 'rest_api_init', array( $this, 'register_routes'));
	}



	public function register_routes()
	{	
		//error_log('Post types: ' . print_r( get_post_types(), true));
		$this->post_types = get_post_types();
		if(sizeof($this->post_types) > 0)
		{
			
			foreach ($this->post_types as $type)
			{
					if($type == 'page' || $type == 'post')
					{
						//$type .= 's';
					}

					register_rest_route( $this->namespace, '/' . $type . '/', array(
					'methods' => 'GET',
					'callback' => array($this, 'posts_list'),
					'args' => array('post_type' => $type )
					));

					register_rest_route( $this->namespace, '/' . $type . '/(?P<id>\d+)/', array(
					'methods' => 'GET',
					'callback' => array($this, 'general_info'),
					'args' => array('post_type' => $type )
					));


					register_rest_route( $this->namespace, '/' . $type . '/search/', array(
					'methods' => 'GET',
					'callback' => array($this, 'search'),
					'args' => array('post_type' => $type )
					));
			}

			
		}
	


	}

	public function register_fields()
	{

	}







	public function search($request)
	{
		//error_log('Request: ' .print_r($request, true)  );
		
		$params = $request->get_params();
		if(empty($params['q']))
		{
			return new WP_Error('missing parameter', 'Search cannot start - parameter q is required in query string');
		}

		$query = $params['q'];
		
		global $wpdb;


		$result = array();


		$meta_sql  = $wpdb->prepare( 'SELECT post_id FROM '.$this->db_prefix .'postmeta WHERE meta_value LIKE %s', '%%'.$query .'%%');
		$meta_rows = $wpdb->get_results( $meta_sql );

		foreach ($meta_rows as $post) 
		{
			$id = $post->post_id;

			if(!in_array($id, $result))
			{
				$result[] = $id;
			}
		}
	
		$posts_sql = array();
		$posts_sql[] = $wpdb->prepare( 'SELECT ID FROM '.$this->db_prefix .'posts WHERE post_content LIKE %s', '%%'.$query .'%%');
		$posts_sql[] = $wpdb->prepare( 'SELECT ID FROM '.$this->db_prefix .'posts WHERE post_title LIKE %s', '%%'.$query .'%%');
		$posts_sql[] = $wpdb->prepare( 'SELECT ID FROM '.$this->db_prefix .'posts WHERE post_excerpt LIKE %s', '%%'.$query .'%%');

		
		foreach ($posts_sql as $the_sql) 
		{
			$posts_rows = $wpdb->get_results( $the_sql );
			error_log('result: ' . print_r( $posts_rows, true));
			
			foreach ($posts_rows as $post) 
			{
				$id = $post->ID;

				if(!in_array($id, $result))
				{
					$result[] = $id;
				}
			}

		}
		
		error_log('result: ' . print_r( $result, true));



		$final = array();

		$atts = $request->get_attributes();
		$args = $atts['args'];
		$post_type = (!empty($args['post_type'])) ? $args['post_type'] : 'page';

		foreach ($result as $id)
		{
			$data = $this->get_post_data( $id );
			$data['lang'] = $this->get_lang_info($id, $post_type);

			error_log('data type: ' . $data['post_type'] . ' -- get_type: ' . $post_type);
			if($data['post_type'] == $post_type )
			{
				$final[] = $data;
			}
			
		}

		if(empty($final))
		{
			return new WP_ERROR('Nothing Found', 'No results have been found in post type of: <'.$post_type .'>, for query: <'.$query.'>');

		}

		return $final;

	}










	public function posts_list( $request )
	{
		$atts = $request->get_attributes();
		$args = $atts['args'];
		$params = $request->get_params();

		$post_type = (!empty($args['post_type'])) ? $args['post_type'] : 'page';
		$per_page = (!empty($args['per_page'])) ? $args['per_page'] : 10;

		$query_args = array(
			'post_type' => $post_type,
			'suppress_filters' => 0,
			'posts_per_page' => $per_page

			);

		if( !empty( $params['slug'] ) )
		{
			$query_args['name'] = $params['slug'];
		}

		$posts = get_posts( $query_args );

		$out = array();
		foreach ($posts as $post) 
		{
			$id = $post->ID;
			$info_A = $this->get_post_data( $id );
			$info_A['lang'] = $this->get_lang_info($id, $post_type);

			$out[$id] =  $info_A;
		
		}

		return $out; 

	}

	//public function 

	public function general_info( $request )
	{
		//error_log('Request: ' .print_r($request, true)  );
		$atts = $request->get_attributes();
		$args = $atts['args'];

		$post_type = (!empty($args['post_type'])) ? $args['post_type'] : 'page';

		//error_log('Post type: ' . print_r( $post_type, true));
		$id = $request['id'];

		if(!empty($request['lang']))
		{
			$tr_id = icl_object_id( $id, $post_type, false, $request['lang'] );

			if($tr_id)
			{
				$id = $tr_id;
			}

		}

		$info = $this->get_post_data( $id );
		$info['lang'] = $this->get_lang_info($id, $post_type);

		return $info; 
	}


	public function get_post_data($id)
	{
		$info = array();

		$info['id'] = $id;

		$page = new bbPage($id);
		$raw = $page->meta_raw;
		$info['post_type'] = $page->post_type;
		$info['title']     = strip_tags( $page->title );
		$info['excerpt']   = strip_tags( $page->excerpt );
		$info['content']   = strip_tags( $page->content );
		$info['sections']  = $page->meta;
		$info['yoast_seo'] = $this->get_yoast_from_meta($raw);

		return $info;
	}

	public function get_yoast_from_meta($raw)
	{
		$info = array();

		if(!empty($raw['_yoast_wpseo_title']))
			$info['title'] = $raw['_yoast_wpseo_title'];
	
		if(!empty($raw['_yoast_wpseo_metadesc']))
			$info['description'] = $raw['_yoast_wpseo_metadesc'];

		if(!empty($raw['_yoast_wpseo_focuskw']))
			$info['focuskeyword'] = $raw['_yoast_wpseo_focuskw'];
		
		if(!empty($raw['_yoast_wpseo_linkdex']))
			$info['lindex'] = $raw['_yoast_wpseo_linkdex'];

		return $info; 
	}


	public function get_lang_info( $id, $type )
	{
	  //setup_postdata( $object );

	  if(!function_exists('icl_get_languages'))
	  {
	  	return null;
	  }


	  $languages = icl_get_languages('skip_missing=1');
	  $lang_info = array();

	//  error_log('Languages: ' . print_r($languages, true));

	  if(1 < count($languages))
	  {
	     
	    foreach($languages as $key=>$l)
	    {
	      $tr_id = icl_object_id( $id, $type, false, $l['code'] );
	      
	      if($tr_id)
	      {	
	      	$url = get_permalink($tr_id); 
	      	 $lang_info[ $l['code'] ]['translated_id'] = $tr_id;
	     	 $lang_info[ $l['code'] ]['translated_url'] = $url;
	     	 $lang_info[ $l['code'] ]['default_locale'] = $l['default_locale'];
	     	  $lang_info[ $l['code'] ]['tag'] = $l['tag'];
	     	  $lang_info[ $l['code'] ]['slug'] = basename( $url );
	      }
	     
	    }
	  
	   
	    return $lang_info; 
	  }
	  else
	  {
	  	return null; 
	  }


	}


}


$b_rest = new bb_rest_api();
$b_rest->launch();