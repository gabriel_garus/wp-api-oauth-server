<?php
defined('ABSPATH') or die('Access Denied!');
/*
*menu-functions
*/

function bb_menu ($menu_name = 'loggedin', $args = array())
{
	if ( ($locations = get_nav_menu_locations() ) && isset( $locations[$menu_name]) )
	{
		
		$blog_id = (defined('BLOG_ID')) ? BLOG_ID : get_option( 'page_for_posts' );
		
		$page 		 = get_post();

		// $custom_post_types = get_post_types( array('_builtin' => false) );
	

		$menu 			 = wp_get_nav_menu_object($locations[$menu_name]);

		if(!is_object($menu))
			return;
		
		$items 			 = wp_get_nav_menu_items($menu->term_id);

		echo '<nav id="' . $menu_name . '">';
		echo '<ul class="cf">' . PHP_EOL;


		foreach ($items as $item)
		{

			if($page)
			{
				$page_parent = get_post($page->post_parent);
			}
			else
			{
				$page_parent = null;
			}
			
			if(!empty($page ))
			{
				$menu_item_id = $item->object_id;

				if (
				$menu_item_id == $page->ID ||
				$menu_item_id == $page->post_parent ||
				( $page_parent AND $menu_item_id == $page_parent->post_parent ) ||
				( IS_HOME AND $menu_item_id == $blog_id ) ||
				( $menu_item_id == $blog_id && $page->post_type == 'post' ) ||
				( IS_FRONT_PAGE AND  $menu_item_id == HOME_ID )
				)
				{
					$item->classes[] = 'current ';
				}

			
			}


			$classes = '';

			foreach ($item->classes as $class)
			{
				$classes .= $class . ' ';
			}
			// $classes .= ' id-'.$menu_item_id;


			echo '<li';
			$classes = trim($classes);
			if (!empty($classes))
			{
				echo ' class="' . $classes . '"';
			}
			echo '>';
			echo '<a href="' . $item->url . '"';
			echo !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
			echo !empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
			echo !empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
			echo '>';
			echo _($item->title);
			//echo '['. $item->object_id . ']';
			echo '</a>';
			echo '</li>' . PHP_EOL;


		}

				
		echo '</ul>';
				
		echo '</nav>' . PHP_EOL;
	}
}
add_filter('nav_menu_css_class', 'current_type_nav_class', 10, 2);


function current_type_nav_class($classes, $item)
{
    // Get post_type for this post
    $post_type = get_query_var('properties');

    // Go to Menus and add a menu class named: {custom-post-type}-menu-item
    // This adds a 'current_page_parent' class to the parent menu item
    if( in_array( $post_type.'-menu-item', $classes ) )
        array_push($classes, 'current_page_parent');
    return $classes;
}
