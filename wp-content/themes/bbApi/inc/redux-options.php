<?php
defined('ABSPATH') or die('Access Denied!');

$opt_name = 'bigbang';

$redux11 = new Redux();
$redux11->setArgs(
    $opt_name, // This is your opt_name,
    array( // This is your arguments array
       'display_name'    => 'Theme Options (by Big Bang)',
       'display_version' => '1.0',
       'menu_title'      => 'Control Panel',
       'admin_bar'       => 'true',
       'page_slug'       => 'bigbang_options_page', // Must be one word, no special characters, no spaces
       'menu_type'       => 'menu', // Menu or submenu
       'allow_sub_menu'  => true,
       'dev_mode'        => false

   ));


$sections = array();
$fields = array();

$options = bb_get_options();


//----------------------------------------------------------------------------
include_once INC_DIR . 'redux' . DIRECTORY_SEPARATOR . 'redux-header.php';
include_once INC_DIR . 'redux' . DIRECTORY_SEPARATOR . 'redux-promise.php';
include_once INC_DIR . 'redux' . DIRECTORY_SEPARATOR . 'redux-cta.php';
include_once INC_DIR . 'redux' . DIRECTORY_SEPARATOR . 'redux-footer.php';
include_once INC_DIR . 'redux' . DIRECTORY_SEPARATOR . 'redux-social.php';
include_once INC_DIR . 'redux' . DIRECTORY_SEPARATOR . 'redux-config.php';

// Setup Sections
foreach ($sections as $section)
{
  $redux11->setSection( $opt_name, $section); 
}


// Setup Fields
foreach ($fields as $field)
{
  $redux11->setField( $opt_name, $field);
}



function redux_image(&$fields, $section_id, $field_id, $field_title)
{

$fields[] =  array( 
        'id'         => $field_id.'-start', 
        'type'       => 'section',
        'section_id' => $section_id,
        'title'      => $field_title,
        'indent'     => true 
     );


$fields[] =  array( 
        'id'         => $field_id .'-image', 
        'type'       => 'media',
        'section_id' => $section_id,
        'title'      => 'The Image',
        'url'        => true
    );


$fields[] =  array( 
        'id'         => $field_id.'-size', 
        'type'       => 'dimensions',
        'section_id' => $section_id,
        'unit'       => false,
        'default'    => array('width' => '200', 'height'=>'50'),
        'title'      => 'Image Size Attribute'
    );


$fields[] =  array( 
        'id'         => $field_id.'-end', 
        'type'       => 'section',
        'section_id' => $section_id,
        'indent'     => false 
     );



}

