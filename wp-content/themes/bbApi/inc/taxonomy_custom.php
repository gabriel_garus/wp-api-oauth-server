<?php


defined('ABSPATH') or die('Access Denied!');


//*************************************************
//**  Extra Taxonomy for Custom POst Type
//**
//**
//*************************************************

function create_test_taxonomies()
{
    // Add new taxonomy, make it hierarchical (like categories)
	$singular = 'Test Type';
	$plural = 'Test Types'
	$slug = 'test_types';

    $labels = array(
        'name'              => _x( $plural, 'taxonomy general name' ),
        'singular_name'     => _x( $singular, 'taxonomy singular name' ),
        'search_items'      => __( 'Search '.$plural ),
        'all_items'         => __( 'All ' . $plural ),
        'parent_item'       => __( 'Parent ' . $singular ),
        'parent_item_colon' => __( 'Parent :' . $singular ),
        'edit_item'         => __( 'Edit ' . $singular ),
        'update_item'       => __( 'Update ' . $singular ),
        'add_new_item'      => __( 'Add New ' . $singular ),
        'new_item_name'     => __( 'New ' . $singular ),
        'menu_name'         => __( $plural ),
    );


    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'hierarchical' => true, //default false
                                       'slug' => $slug ,
                                     'with_front' => false // default: true
         ),
    );

  register_taxonomy( $slug , null, $args );
  register_taxonomy_for_object_type( $slug , 'robots' );

}


add_action( 'init', 'create_test_taxonomies', 0 );