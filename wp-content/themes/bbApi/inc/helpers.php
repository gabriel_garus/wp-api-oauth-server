<?php
defined('ABSPATH') or die('Access Denied!');





function dump($sth, $line=true)
{
    if( is_array($sth) OR is_object($sth) )
    {
        echo '<ul>'.PHP_EOL;
        foreach ($sth as $key=>$value)
        {
            echo '<li>'.PHP_EOL;
            echo '["' . $key . '"] => ';
            if(is_array($value) OR is_object($value))
            {
               echo '<em> array </em>{ <br>'.PHP_EOL;
               dump($value, false ).PHP_EOL;
               echo ' }'.PHP_EOL;
            }
            elseif( is_null($value) )
            {
                echo '<em>null</em>';
            }
            elseif( (strpos($value,'http://') !== false) OR (strpos($value,'https://') !== false ) )
            {
             echo '<a href="'. $value .'" target="_blank">'.$value .'</a>';
            }
            else
            {
            echo var_dump($value);
            }
            echo '</li>'.PHP_EOL;

        }
        echo '</ul>'.PHP_EOL;
    }
   else
   {
     echo '<pre>' . var_dump($sth) . '</pre>';
   }

   if($line) echo'<hr>';

   return;
}


//--------------------


function bb_extract($meta)
{
    if( !is_array($meta) )
        return array('error' => 'Meta is not an array');;

    if( empty($meta['box_index'][0]) )
        return array('error' => 'Please re-save the post.');

    $index = unserialize($meta['box_index'][0]);

    $deserialised = array();
    foreach ($index as $key )
    {
        $string = $meta[$key][0];
        $arra = unserialize($string);
        $deserialised[$key] = $arra;

    }

    return $deserialised;
}




//----

function bb_change_post_type($old, $new)
{

    $posts = get_posts(array(
            'post_type' => $old,
            'post_per_page' => -1
        ));
    $result = array();
       foreach ($posts as $post )
        {
          $result[] = set_post_type( $post->ID, $new );
        }

    return $result;

}





function bb_get_page_id()
{

    if(is_home())
    {
        $thePostId = (defined('BLOG_ID')) ? BLOG_ID : get_option( 'page_for_posts' );
    }
    else
    {
        $thePostId = get_the_ID();
    }

    return $thePostId;

}

//-------------------------------------------


function bb_get_body_schema($page_title)
{
    if( IS_FRONT_PAGE )
    {
        $bodySchema = 'Organization';
    }
    elseif($page_title === 'Contact' )
    {
        $bodySchema = 'ContactPage';
    }
    elseif($page_title === 'Company' )
    {
        $bodySchema = 'AboutPage';
    }
    else
    {
        $bodySchema = 'WebPage';
    }

    return $bodySchema;
}


//-------------------------------------------


function bb_get_canonical()
{
    if( IS_FRONT_PAGE )
    {
      $canonical = WP_HOME.'/';
    }
    else
    {
      $canonical = get_permalink();
    }

    return $canonical;
}


//-------------------------------------------







function bb_get_page_class($page_id, $page_title )
{
    
    $front_page = get_option( 'page_on_front' );
    if($front_page == $page_id)
    {
        return 'home';
    }
    else
    {
       $blog_page = (defined('BLOG_ID')) ? BLOG_ID : get_option( 'page_for_posts' );
       if( $blog_page == $page_id)
       {
            return 'blog';
       }
       elseif( is_search() )
       {
            return 'search';
       }
       elseif( is_tag() )
       {
            return 'tag';
       }
       elseif( is_singular() )
       {
            return get_post_type();
       }
       else
       {
           return str_replace(' ', '-', trim($page_title) );
       }
    }
    
}





function get_user_browser()
{
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $ub = '';
    if(preg_match('/MSIE/i',$u_agent))
    {
        $ub = "ie";
    }
    elseif(preg_match('/Firefox/i',$u_agent))
    {
        $ub = "firefox";
    }
     elseif(preg_match('/Chrome/i',$u_agent))
    {
        $ub = "chrome";
    }
    elseif(preg_match('/Safari/i',$u_agent))
    {
        $ub = "safari";
    }
    elseif(preg_match('/Flock/i',$u_agent))
    {
        $ub = "flock";
    }
    elseif(preg_match('/Opera/i',$u_agent))
    {
        $ub = "opera";
    }

    return $ub;
}


function bb_get_options()
{   
    
    if(defined('bb_options'))
    {

        $options = unserialize(bb_options);
    } 
    else
    {
         $options =  get_option('bigbang');
         define('bb_options', serialize($options));

    } 

    return $options; 
}


function bb_telephone_link($telephone, $md_tele = null, $direction = null, $class = null)
{
    if($telephone)
    {
        $tele_short = str_replace(' ', '', $telephone);
        $tele_short = str_replace('-', '', $tele_short);

        $tele_short = ltrim($tele_short, '0');

        if($direction !== null)
        {
            $pre = $direction;
        }
        elseif( ( strpos( $tele_short, '1') === 0 ) OR ( strpos($tele_short, '8')  === 0 ) )
        {
             $pre = '+353';
        }
        else
        {
            $pre = '+';
        }

        $tele_short = $pre . $tele_short; 

        echo '<a href="tel:'.$tele_short.'" class="telephone '.$class.'"><span ' . $md_tele . '>' . $telephone . '</span></a> '.PHP_EOL;
    }
}




function bb_get_page_template($id)
{
    $template_file = get_post_meta($id,'_wp_page_template',TRUE);

    $template_file = rtrim($template_file, '.php');

    $ex = explode('/', $template_file);

    if(sizeof($ex) > 1)
    {
        return end($ex);
    }
    else
    {
        return $template_file; 
    }

}



function bbTeam($t_id)
{
    
        
         $t = new bbPage($t_id);

        $timage_id = $t->thumbnail;
        $timage = new bbImage($timage_id, 'box');
        $pos = $t->get_field('employee','position');

        $fname = $t->get_field('employee','first_name');
        $lname = $t->get_field('employee','family_name');

        if($fname !== null AND $lname !== null)
        {
            $full_name = '<span>' . $fname . '</span> ';
            $full_name .= '<span>' . $lname . '</span>';
        }
        else
        {
            $full_name = $t->title;
        }

        echo '<div class="person grid-element">' . PHP_EOL;
        echo $timage->html . PHP_EOL;
        echo '<h5>'. $full_name  . '</h5>' . PHP_EOL;
        echo '<h6>' . $pos . '</h6>';
        echo '</div>';
}


function bb_content($content)
{
    if($content !== null AND !empty($content))
    {
        echo wpautop( do_shortcode( $content ) );
    }
}


function bb_title($title, $h='h1')
{

    if(!is_string($h))
    {
        $h = 'h'.$h;
    }

    if($title !== null AND !empty($title) AND is_string($title))
    {
        echo '<'.$h.'>' . $title . '</'.$h.'>';
    }
}



function bb_template($file)
{
    if(WP_DEBUG)
    {   
        echo '<!-- template-file: '. $file .' -->' . PHP_EOL; 
    }
}




