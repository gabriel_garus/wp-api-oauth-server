<?php
defined('ABSPATH') or die('Access Denied!');
/*
*	
*	author: Gabriel Garus for Big Bang Design
*   gabriel.garus@gmail.com	
*/


class bbImage
{
	public $ID = 0;

	public $size_name = '';

	public $resized = false;

	public $src = '';

	public $width = 0;

	public $height = 0;

	public $class = '';

	public $alt = '';

	public $itemprop = '';

	public $html = '';

	public $caption = '';

	public $sizes = array();
	public $wp_image_src = array();

	public $all_urls = '';

	public $image_post = array();

	public $fixed_width = 150;
	public $fixed_height = 40;

	public $mime = '';

	public function __construct($id, $size = 'full')
	{	
		$this->ID = $id;

		$this->size_name = $size;

		$the_img_src =  wp_get_attachment_image_src( $id , $size);

		$this->image_post = get_post($id);

		$this->mime = $this->get_mime();

		$this->wp_image_src = $the_img_src;

		$this->src = $the_img_src[0];

		$this->sizes = $this->get_image_sizes();

		$this->width = $this->get_width();

		$this->height = $this->get_height();

		$this->alt = $this->get_alt();

		$this->html = $this->get_html();

		$this->resized = $this->get_resized();

		$this->all_urls = $this->get_all_urls();
		


	}



	public function get_all_urls()
	{


		if($this->mime == 'image/svg+xml' )
		{
			$all_urls = $this->src;
		}
		else
		{
			$urls = array();
			$urls['full'] = $this->src;
			foreach ($this->sizes as $sizeName=>$sizeArr) 
			{
				$urls[$sizeName] = $sizeArr['url'];
			}

			$all_urls = $urls;
		}

		return $all_urls;

	}




	public function get_image_sizes() 
	{
	    global $_wp_additional_image_sizes;

	    $sizes = array();

	    foreach ( get_intermediate_image_sizes() as $_size ) 
	    {

	    	$image_srcs = wp_get_attachment_image_src($this->ID,  $_size );
	        if ( in_array( $_size, array('thumbnail', 'medium', 'medium_large', 'large') ) ) 
	        {
	            $sizes[ $_size ]['width']  = get_option( "{$_size}_size_w" );
	            $sizes[ $_size ]['height'] = get_option( "{$_size}_size_h" );
	            $sizes[ $_size ]['crop']   = (bool) get_option( "{$_size}_crop" );
	            $sizes[ $_size ]['url']    = $image_srcs[0];
	        } 
	        elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) 
	        {
	            $sizes[ $_size ] = array(
	                'width'  => $_wp_additional_image_sizes[ $_size ]['width'],
	                'height' => $_wp_additional_image_sizes[ $_size ]['height'],
	                'crop'   => $_wp_additional_image_sizes[ $_size ]['crop'],
	                'url'    => $image_srcs[0]
	            );
	        }
	    }

	    return $sizes;
	}



    public function get_image_size( $size ) 
    {
	    $sizes = $this->sizes;

	    if ( isset( $sizes[ $size ] ) ) 
	    {
	        return $sizes[ $size ];
	    }

	    return false;
	}




	public function get_width()
	{
		$the_img_src = $this->wp_image_src;
		$sizes = $this->sizes;

		    if($the_img_src[1]>1)
		    {
		        $the_width = $the_img_src[1];
		    }
		    elseif(!empty($sizes[$this->size_name]))
		    {
		        $the_width = $sizes[$this->size_name]['width'];
		    }
		    else
		    {
		         $the_width = $this->fixed_width;
		    }

		return $the_width;
	}

	public function get_height()
	{
			$the_img_src = $this->wp_image_src;
			$sizes = $this->sizes;

		    if($the_img_src[2]>1)
		    {
		        $the_height = $the_img_src[2];
		    }
		    elseif(!empty($sizes[$this->size_name]))
		    {
		        $the_height = $sizes[$this->size_name]['height'];
		    }
		    else
		    {
		         $the_height = $this->fixed_height;
		    }

		return $the_height;
	}

	public function get_sizes()
	{

		$the_img_src = $this->wp_image_src;

		if($the_img_src[3])
	    {
	       $sizes = $this->get_image_size($this->size_name);
	    }
	    else
	    {
	        $sizes = false;
	    }

	    return $sizes; 
	}

	public function get_alt()
	{

		$alt = get_post_meta($this->ID, '_wp_attachment_image_alt', true);

		if(empty($alt))
		{
			$post = $this->image_post;
			if(!empty($post))
			{
				$alt = $post->post_title;
			}
			
		}
		return $alt; 
	}


	public function get_html()
	{
		$opts = array();

		$out_html = '<img src="'.$this->src.'" ';
		$out_html .= 'width="'.$this->width.'" ';
		$out_html .= 'height="'.$this->height . '" ';
		
		if(!empty( $this->alt ))
		{
			$out_html .= 'alt="'. $this->alt .'" ';
			$opts['alt'] = $this->alt;
		}
		
		if(!empty( $this->class ))
		{
			$out_html .= 'class="' . $this->class .'" ';
			$opts['class'] = $this->class;
		}
		else
		{
			$opts['class'] = false;
		}

		if(!empty( $this->itemprop))
		{
			$out_html .= 'itemprop="' . $this->itemprop .'" ';
		}


		$out_html .= '/>';

		
		$icon = false; 

		if(!empty($this->size_name))
		{
			$wp_size = $this->size_name;
		}
		else
		{
			$wp_size = array($this->width, $this->height);
		}
		
		$wp_image = wp_get_attachment_image($this->ID, $wp_size, $icon, $opts);


		if($opts['class'] === false)
		{
			$wp_image = str_replace('class=""', '', $wp_image);
		}

		if($this->mime == 'image/svg+xml')
		{
			return $out_html;
		}
		else
		{
			return $wp_image;
		}
		
	}

	public function get_resized()
	{
		$wp_o = $this->wp_image_src;

		if(isset($wp_o[3]))
		{
			return $wp_o[3];
		}
		else
		{
			return false;
		}
		
	}




	public function get_mime()
	{
		$image_post = $this->image_post;
		
		if (!empty($image_post->post_mime_type))
		{
			return $image_post->post_mime_type;
		}
		else
		{
			return null;
		}
		
	}

	public function set_class($class)
	{
		$this->class = $class;
		$this->html = $this->get_html();
	}

	public function set_itemprop($itemprop)
	{
		$this->itemprop = $itemprop;
		$this->html = $this->get_html();
	}

	public function set_width($width)
	{
		$this->width = $width;
		$this->html = $this->get_html();
	}

	public function set_height($height)
	{
		$this->height = $height;
		$this->html = $this->get_html();
	}
}


