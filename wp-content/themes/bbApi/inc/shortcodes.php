<?php
/*
*   Shortcodes 
*
*/

add_shortcode( 'main_image' , 'main_post_image' );

function main_post_image( $atts )
{
    global $post;

    $imaginarium = '';
    
    $thePost = new bbPost($post->ID);

    if(empty($thePost->thumbnail))
    {
        return null;
    }

    if(isset($atts['size']))
    {
    	$size = $atts['size'];
    }
    else
    {
    	$size = 'thumbnail';
    }

     $img = new bbImage($thePost->thumbnail, $size);


    $imaginarium .= '<div class="image">';


    if(isset($atts['link']) AND $atts['link'] == true AND !empty($img->src))
    {
    	 $imaginarium .= '<a href="'. $img->src . '" rel="nofollow" target="_blank">';
    }

      $imaginarium .= $img->html;

    if(isset($atts['link']) AND $atts['link'] == true AND !empty($img['url']))
   	{
   		$imaginarium .= '</a>';
   	}

    if(!empty($img->caption))
    {
        $imaginarium .= '<p class="caption">' . $img->caption . '</p>';
    }

    $imaginarium .= '</div>';

    return $imaginarium;
}
//---------------------------------------------
//---------------------------------------------






add_shortcode('movie', 'post_video');

function post_video($atts)
{
    require_once INC_DIR . 'yt_video.php';

    $echo = '';

    if(!empty($atts['url']))
    {
        $video_url = $atts['url']; 
       
      // wp_enqueue_script( 'webshim' );
      
      /*
       if(!defined('VIDEO_CONTENT'))
        {
            define('VIDEO_CONTENT',true);
        }
    */

        $companyName = '';
        $companySite = WP_HOME;

        // Assign Raw link from DB

        $videoLink = $video_url;

        $V = new YT_Video($videoLink);

        if( empty($videoPoster) )
        {

            $videoPoster = ( !empty($video_poster) ) ? $video_poster : null;
        }


        if($V->ID !== null)
        {
            $videoName      = 'Name';
            $embedUrl       =  $V->get_embed_url();
            $videoSrc       = $V->get_watch_url();
            $contentUrl     = $V->get_watch_url();
            $thumbnailUrl = $V->get_thumbnail('mq');
            $videoPoster    = (empty($videoPoster)) ? ($V->get_thumbnail('hq')) : $videoPoster;
            $time               = $V->get_time();
            $publish_date = $V->get_publish_date();
            $description = $V->get_description();
            $video_name = $V->get_name();


            $description = str_replace('"', "'", $description);
            $video_name = str_replace('"', "'", $video_name);
            if(!IS_FRONT_PAGE)
            {
                $vidItemProp = 'itemprop="video"';
            }

            else{
                $vidItemProp = null;
            }

            $echo .= '<div id="video" class="video" '. $vidItemProp;
            $echo .= ' itemscope itemtype="http://schema.org/VideoObject" >'.PHP_EOL;
            
            $echo .= '<div itemprop="publisher" itemscope itemtype="http://schema.org/Organization">';
            $echo .= '<meta itemprop="name" content="'. $companyName .'">'.PHP_EOL;
            $echo .= '<link itemprop="url" href="'.$companySite.'">'.PHP_EOL;
            $echo .= '</div>'.PHP_EOL;

            $echo .= '<div class="wrap">'.PHP_EOL;
            $echo .= '<div class="chc-video">'.PHP_EOL;
            $echo .= '<meta itemprop="thumbnailUrl" content="'.$thumbnailUrl.'" />'.PHP_EOL;
            $echo .= '<meta itemprop="contentUrl" content="' . $contentUrl . '" />'.PHP_EOL;
            $echo .= '<meta itemprop="embedURL" content="'. $embedUrl . '" />'.PHP_EOL;
            $echo .= '<meta itemprop="duration" content="'. $time . '" />'.PHP_EOL;
            $echo .= '<meta itemprop="description" content="'. $description . '" />'.PHP_EOL;
            $echo .= '<meta itemprop="name" content="'. $video_name . '" />'.PHP_EOL;
            $echo .= '<meta itemprop="uploadDate" content="' . $publish_date . '"  />' . PHP_EOL;
        //    $echo .= '<div class="mediaplayer ratio-16-9" >'.PHP_EOL;
            $echo .= '<iframe ';
            $echo .= 'width="512" height="360" ';
            $echo .=  'src="' . $embedUrl . '" ';
            $echo .= 'frameborder="0" allowfullscreen></iframe>';
            /*
            $echo .= '<video ';
            $echo .= 'poster="'.$videoPoster.'" ';
            $echo .= 'height="360" width="640" ';
            $echo .= 'style="height: 100%;" ';
            $echo .= 'controls preload ';
            $echo .= 'src="' . $videoSrc . '" type="video/youtube" ';
            $echo .= '>'.PHP_EOL;
            //$echo .= '<source type="video/mp4" src="' . $videoSrc . '" /> ' .PHP_EOL;
  			//$echo .= '<source type="video/ogg" src="' . $videoSrc . '">' . PHP_EOL;
  			//$echo .= '<source type="video/webm" src="' . $videoSrc . '" >' . PHP_EOL;
  			$echo .= '<source type="video/youtube" src="' . $videoSrc . '" >' . PHP_EOL;
            $echo .='</video>'.PHP_EOL . '</div>'.PHP_EOL;
            */
            $echo .='</div></div>'.PHP_EOL;
            $echo .='</div>'.PHP_EOL;

        }
    }

    return $echo; 
}
//---------------------------------------------
//---------------------------------------------




/*

add_shortcode('quote', 'post_quote');

function post_quote($atts, $content = null) 
{
	return '<div class="quote">' . $content . '</div>';
}

*/


add_shortcode( 'address' , 'address_sc' );

function address_sc() 
{

    if(empty($options))
    {
       $options  = bb_get_options();
    }

    $title = ( !empty($options['title']) ) ? $options['title'] : null;
    $street_line_1       = ( !empty($options['addressL1']) ) ? $options['addressL1'] : null;
    $street_line_2       = ( !empty($options['addressL2']) ) ? $options['addressL2'] : null;
    $street_line_3       = ( !empty($options['addressL3']) ) ? $options['addressL3'] : null;
    $city                = ( !empty($options['city'])) ? $options['city'] : null;
    $code                = ( !empty($options['areaCode']) ) ? $options['areaCode'] : null;
    $county              = ( !empty($options['county']) ) ? $options['county'] : null;
    $country             = ( !empty($options['country']) ) ? $options['country'] : null;
    $countryCode         = ( !empty($options['countryCode']) ) ? $options['countryCode'] : null;



    //----------------------------------------------




    $echo = '';
    $echo .= '<address ' . $md_address . ' >'.PHP_EOL;



    $street_address = array();

    if(!empty($street_line_1))
    {
        $street_address[] =  $street_line_1;
    }

    if( !empty($street_line_2) )
    {
        //$street_address[] = $street_line_2;
    }

    if( !empty($street_line_3 ) )
    {
        $street_address[] = $street_line_3;
    }


    $addressRest = array();


    if( !empty($city) )
    {
        $addressRest[0]['name'] = $city;
        $addressRest[0]['md'] = $md_addressLocale;
        $addressRest[0]['class'] = 'addressLocality';
    }

    if( !empty($code) )
    {
        $addressRest[1]['name'] = $code;
        $addressRest[1]['md'] = $md_code;
        $addressRest[1]['class'] = 'postalCode';
    }

    if( !empty($county) )
    {
        $addressRest[2]['name'] = $county;
        $addressRest[2]['md'] = $md_addressRegion;
        $addressRest[2]['class'] = 'addressRegion';
    }

    if( !empty($country) )
    {
        $addressRest[3]['name'] = $country;
        $addressRest[3]['md'] = null;
        $addressRest[3]['class'] = 'addressCountry';
    }



    $echo .= '<div class="street" '.$md_streetAddress.' >' . PHP_EOL;

    for ($i=0; $i < sizeof($street_address) ; $i++)
    {
        if(!empty($street_address[$i]))
        {

            $class = 'street_address_'. ($i+1);
           $echo .= '<span class="'.$class.'">' . $street_address[$i];
           if( $i < (sizeof($street_address) - 1 ) OR !empty($addressRest))
           {
            if(isset($address_commas) AND $address_commas)
                $echo .= ', ';
            else
                $echo .= ' ';
           }
            
           $echo .= '</span>';
        }
    }

    $echo .= '</div>' . PHP_EOL; // .street


    for ($i=0; $i < sizeof($addressRest) ; $i++)
    {

        if(!empty($addressRest[$i]))
        {   
            $cex = explode('"', $addressRest[$i]['md']);
            $class = (!empty($addressRest[$i]['class'])) ? $addressRest[$i]['class'] : 'field';

            $echo .= '<span class="'.$class.'" ' . $addressRest[$i]['md'] .'>' . $addressRest[$i]['name'];
            if( $i < (sizeof($addressRest) - 1 ) )
               {
                if(isset($address_commas) AND $address_commas)
                    $echo .= ', ';
                else
                    $echo .= ' ';
               }
           $echo .= '</span>';
        }
    }
    $echo .= '<span>'.$country . '</span>' . PHP_EOL;

    if( !empty($countryCode) )
    {
        $echo .= $md_country_meta.PHP_EOL;
    }

    $echo .= '</address>'.PHP_EOL;

    return $echo; 
}



add_shortcode( 'lorem' , 'lorem_ipsum' );

function lorem_ipsum()
{
    $lorem = 'Lorem ipsum dolor sit amet, esse probo posidonium ius ad. Mei ea affert inermis, sanctus nostrum comprehensam vix et. Nam dolorum voluptaria id, alia exerci ne ius. Modus praesent repudiare cum ut, sea insolens signiferumque ut.';

    return $lorem;
}