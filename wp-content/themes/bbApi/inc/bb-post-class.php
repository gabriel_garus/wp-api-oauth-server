<?php
defined('ABSPATH') or die('Access Denied!');
if(!class_exists('bbPage'))
{
	include_once 'bb-page-class.php';
}


class bbPost extends bbPage
{

	public $url = '';
	public $leadin_title = '';
	public $leadin_image = '';
	public $leadin_text = '';
	public $gallery = array();
	public $author = array();
	public $cta_bottom = array();
	public $rel_posts = array();
	public $video_url = '';
	public $video_poster = '';
	public $middle_content = '';
	public $bottom_content = '';
	public $gallery_img_size = 'large';
	public $excerpt = '';
	//----//----//----//----//----//----//----//----//----//----//----//

	public function __construct($thePostId)
	{
		parent::__construct($thePostId);
		
		$this->url 			   = $this->get_url();
		$this->leadin_title    = $this->get_leadin_title();
		$this->leadin_text     = $this->get_leadin_text();
		$this->leadin_image    = $this->get_leadin_image_id();
		$this->rel_posts 	   = $this->get_rel_posts();
		$this->author 		   = $this->get_author();
		//$this->gallery 		   = $this->get_gallery_srcs();
		//$this->video_url 	   = $this->get_video_url();
		//$this->video_poster    = $this->get_video_poster();
		//$this->middle_content  = $this->get_field('middle_content','text');
		//$this->bottom_content  = $this->get_field('bottom_content','text');
		$this->excerpt 		   = $this->get_leadin_text();
	}
	//----//----//----//----//----//----//----//----//----//----//----//



	public function get_url()
	{
		$url = get_permalink( $this->ID );
		return $url;
	}
	//----//----//----//----//----//----//----//----//----//----//----//



	/*
	*   get_author()
	*	arguments: none;
	*	returns: array('first_name','last_name', 'img_id', 'social_url', 'position' )
	*/
	public function get_author()
	{
		$author 			= array();

		if(is_object($this->post))
		{
			$author['id'] = $this->post->post_author;
		}
		else
		{
			return null;
		}

		$user_meta 	= get_user_meta( $author['id'] );

		$user_id_arr = array('id'=> $author['id'] );
		$user_id_serial = serialize($user_id_arr );
		$staff = get_posts(array('post_type'=> 'team', 'meta_key'=>'user', 'meta_value'=> $user_id_serial, 'post_status' => 'any'));


		// try get from staff 
		if(!empty($staff))
		{
			$staff = $staff[0];
			$staff_meta =  bb_extract( get_post_meta($staff->ID) );

			//dump($staff_meta );
			$author['first_name'] = (!empty($staff_meta['employee']['first_name'])) ? $staff_meta['employee']['first_name'] : null;
			$author['last_name'] = (!empty($staff_meta['employee']['family_name'])) ? $staff_meta['employee']['family_name'] : null;
			$author['position']  = (!empty($staff_meta['employee']['position'])) ? $staff_meta['employee']['position'] : null;
			$author['bio'] = (!empty($staff->post_content)) ? $staff->post_content : null;
			$author['img_id'] = get_post_thumbnail_id($staff->ID);
			

			$author['socials'] =  (!empty($staff_meta['social_profiles'])) ? $staff_meta['social_profiles'] : null;
			if($author['first_name'] === null or $author['last_name'] === null)
			{
				$ex = explode(' ', $staff->post_title);
				if(count($ex) > 1)
				{
					if($author['first_name'] === null)
					{	
						$author['first_name'] = $ex[0];
					}

					if($author['last_name'] === null)
					{
						$author['last_name'] = $ex[1];
					}
				}
			}
			
		
		}


		if(empty($author['first_name'] ))
		{
			$author['first_name'] = (!empty( $user_meta['first_name'][0] )) ? $user_meta['first_name'][0] : null;
		}
		
		if(empty($author['last_name'] ))
		{
			$author['last_name'] = (!empty( $user_meta['last_name'][0] )) ? $user_meta['last_name'][0] : null;
		}

		if(empty($author['position'] ))
		{
			$author['position'] = (!empty($user_meta['position'][0])) ? $user_meta['position'][0] : null;
		}
	  	
	  	if(empty($author['bio']))
	  	{
	  		$author['bio'] = (!empty($user_meta['description'][0])) ? $user_meta['description'][0] : null;
	  	}
	  	
	  	if(empty($author['img_id'] ))
	  	{
	  		$author['img_id']   = (!empty($user_meta['user_image'][0])) ? $user_meta['user_image'][0] : null;
	  	}
	  
	  	
	 	$author['nick_name'] = (!empty( $user_meta['nickname'][0] )) ? $user_meta['nickname'][0] : null;
	  	$author['user_name'] = get_the_author_meta('user_login', $author['id']);
	  			
	  
		return $author;
	}
	//----//----//----//----//----//----//----//----//----//----//----//



	public function set_gallery_img_size($size)
	{
		$this->gallery_img_size = $size;
	}
	//----//----//----//----//----//----//----//----//----//----//----//



	/*
	*   get_date()
	*	arguments: array('format', 'itemprop');
	*	returns: html string - complete html for <time> tag
	*/
	public function get_date($args=array())
	{

		$id = $this->ID;


		// itemprop
		if(empty($args['itemrpop']))
		{
			$itemprop = ' itemprop="datePublished"';
		}
		elseif($args['itemrpop'] == false)
		{
			$itemprop = null;
		}
		else
		{
			$itemprop = ' itemprop="'.$args['itemrpop'].'"';
		}


		$sup = (isset($args['sup']) ) ? $args['sup'] : true;
		
		//------------------------------

		// -- format - short
		$format 		= 'Y-m-d';
		$date_short = get_the_date( $format, $id );
		//------------------------------



			// itemprop
		if(empty($args['format']))
		{
			$format_long = 'D-j-S-M-Y';
			$date 		 = get_the_date( $format_long, $id);
			$dateEx 	 = explode('-', $date);

			$date_long   = '';
			$date_long  .= '<span class="day">'   . $dateEx[1] . '</span>';

			if($sup)
			{
				$date_long .= '<sup>' . $dateEx[2] . '</sup> ';
			}
			else
			{
				$date_long .= '<span class="sup">' . $dateEx[2] . '</span> ';
			}
			
			$date_long .= '<span class="month">' . $dateEx[3] . ' </span> ';
			$date_long .= '<span class="year">'  . $dateEx[4] . '</span> ';
		}
		else
		{
			$format_long = $args['format'];
			$date_long   = get_the_date( $format_long, $id);
		}

		$theTime 		= '<time'.$itemprop .' datetime="' . $date_short . '" >'.$date_long.'</time>';

		return $theTime;
	}
	//----//----//----//----//----//----//----//----//----//----//----//
	



	static function cut_string($content, $length = 150)
	{	
		//$pattern = get_shortcode_regex();

		$content = strip_shortcodes($content);
		$content = strip_tags($content); 
		if (strlen($content) > $length)
		{
			$cut_off = $length - 5;
			$excerpt  = substr( $content, 0, $cut_off ) . ' ...';
		}
		else
		{
			$excerpt = $content;
		}

		//$excerpt = $pattern;
		return $excerpt;
	}
	//----//----//----//----//----//----//----//----//----//----//----//



	/*
	*   get_tag_list()
	*	arguments: none
	*	returns: html string  - ul>li>a with comas
	*/
	public function get_tag_list($opts = array())
	{
		$posttags = get_the_tags($this->ID);

		$links = ( isset($opts['links']) ) ? $opts['links'] : true;
		$title_li = ( isset($opts['title_li']) ) ? $opts['title_li'] : false;

		if ($posttags)
		{
			$tags_html = '<ul class="tags">';

			if($title_li)
			{
				$tags_html .= '<li class="title">'.$title_li .'</li>';
			}

			$counter = 0;
			foreach ($posttags as $tag)
			{
			  $counter ++;

			   $tags_html .= '<li>';
			   if($links)
			   {
			   	$tag_link = get_tag_link($tag->term_id);
			   	$tags_html .= '<a href="'.$tag_link.'" rel="tag" >';
			   }
			   
			   $tags_html .= $tag->name;

			   if($links)
			   {
			   	$tags_html .= '</a>';
			   }
			   
			   if($counter < sizeof($posttags))
			   {
			   $tags_html .= '<span>,</span>';
			   }
			   $tags_html .= "</li>";
			 }

			 $tags_html .= '</ul>'.PHP_EOL;
		}
		else
		{
			$tags_html = null;
		}
		 return $tags_html;
	}
	//--------------------------------



	/*
	*   get_icons()
	*	arguments: none
	*	returns: html string  - ul>li>a>img with comas
	*   prerequisites:
	*   *      - /img/icons/gallery.svg
	*      - /img/icons/event.svg
	* 	   - /img/icons/video.svg
	*/
	public function get_icons($size = 22, $args = array())
	{

		$meta = $this->meta;

		$icons_size = $size;

		$inner = '';

		$li_class = (isset($args['li_class']) ) ? $args['li_class'] : true;
		

	// @TODO - upload proper icons into /img/icons/

		if(!empty($this->gallery))
		{
			$inner .= '<li';
			$inner .= ($li_class) ? ' class="gallery-icon"' : '';
			$inner .= ' >';
			$inner .= '<a href="' . $this->url . '#gallery">';
			$inner .= '<img alt="gallery icon" width="' . $icons_size . '" height="' . $icons_size . '" ';
			$inner .= 'src="/img/icons/gallery.svg" />';
			$inner .= '</a></li>';
		}

		if(!empty($meta['event']['name']))
		{
				$inner .= '<li';
			$inner .= ($li_class) ? ' class="event-icon"' : '';
			$inner .= ' >';
			$inner .= '<a href="' . $this->url . '#event">';
			$inner .= '<img alt="event icon" width="' . $icons_size . '" height="' . $icons_size . '" ';
			$inner .= 'src="/img/icons/event.svg" />';
			$inner .= '</a></li>';
		}

		if(!empty($meta['video']['url']))
		{
			$inner .= '<li';
			$inner .= ($li_class) ? ' class="video-icon"' : '';
			$inner .= ' >';
			$inner .= '<a href="' . $this->url . '#video">';
			$inner .= '<img alt="video icon" width="' . $icons_size . '" height="' . $icons_size . '" ';
			$inner .= 'src="/img/icons/video.svg" />';
			$inner .= '</a></li>';
		}


		if(!empty($inner))
		{
			$icons_list = '<ul class="icons">' . $inner . '</ul>';
		}
		else
			$icons_list = null;


		return $icons_list;

	}
	//--------------------------------



	/*
	*   get_title()
	*	arguments: none
	*	returns: string
	*   prerequisites:
	*   $meta['headline']['title']
	*/
	public function get_title()
	{
		$meta = $this->meta;
		$post = $this->post;

		if(!empty($meta['headline']['title']))
		{
			$title = $meta['headline']['title'];
		}
		else
		{
			$title = $post->post_title;
		}

		return $title;

	}
	//--------------------------------



	/*
	*   get_title()
	*	arguments: none
	*	returns: string
	*   prerequisites:
	*   $meta['leadin']['title']
	*/

	public function get_leadin_title()
	{
		$meta = $this->meta;
		$post = $this->post;

		if(!empty($meta['leadin']['title']))
		{
			$title = $meta['leadin']['title'];
		}
		elseif(is_object($this->post))
		{
			$title = $post->post_title;
		}
		else
		{
			$title = null;
		}

		return $title;


	}
	//--------------------------------


	public function get_leadin_text()
	{
		$meta = $this->meta;
		$post = $this->post;

		if(!empty($meta['leadin']['text']))
		{
			$text = $meta['leadin']['text'];
		}
		elseif(!empty($post->excerpt))
		{
			$text = $post->excerpt;
		}
		elseif(is_object($this->post))
		{
			$text = $this->cut_string( $post->post_content );
		}
		else
		{
			$text = '';
		}
	
		return strip_tags( strip_shortcodes($text) );
	}


	public function get_leadin_image_id()
	{
		$img_id = '';
		$meta = $this->meta;


		if(!empty($meta['leadin']['image']) AND $meta['leadin']['image'] != 0 )
		{
			$img_id = $meta['leadin']['image'];
		}
		else
		{
			$img_id = $this->thumbnail;
		}

		return $img_id;

	}


	public function get_leadin_image()
	{
		return $this->get_image( $this->leadin_image_id );
	}

	public function get_wp_leadin_image()
	{
		return $this->get_wp_image( $this->leadin_image_id, $this->thumbnail_size );
	}




	public function get_rel_posts()
	{
		$rel_p =  array();

		if(!empty($this->meta['related_posts'][0])) :
		$rel_pp = unserialize($this->meta['related_posts'][0]);

		foreach ($rel_pp as $key => $value)
		{
			if($value != '0' AND (strlen($value) < 5))
			$rel_p[$key] = 	$value;

		}

		else :
		$rel_p = null;
		endif;

		return $rel_p;

	}


	public function get_gallery_srcs($content = null)
	{

		$gallery_size = $this->gallery_img_size;

		if($content == null)
		{
			$gal_content = (!empty($this->meta['gallery']['images'])) ? $this->meta['gallery']['images'] : null;
		}
		else
		{
			$gal_content = $content;
		}

		if(null === $gal_content)
		{
			//error_log('gallery not in meta');
			return null;

		}

		$core = rtrim( ltrim($gal_content,'[gallery ids="' ) , '"]' );

		$coreX = explode(',' , $core);

		$sources = array();

		if(is_array($coreX) AND $coreX[0] != '0')
		{
			foreach ($coreX as $key)
			{
				$img = wp_get_attachment_image( $key , 	$gallery_size , false , array( 'class' => 'switch-image') );
				$sources[] = $img;
			}
			return $sources;
		}
		else
		{
			//error_log('gallery empty');
			return null;

		}


	}


	public function set_img_itemprop($prop)
	{
		$this->img_itemprop = $prop;
	}

	public function set_img_class($class)
	{
		$this->img_class = $class;
	}

	public function set_img_id($id)
	{
		$this->img_id = $id;
	}



	public function get_the_image()
	{
		$img_id = get_post_thumbnail_id( $this->id );
		return $img_id;
	}


	public function get_video_url()
	{

		if(!empty($this->meta['video']['url']))
		{
			return $this->meta['video']['url'];
		}
		else
		{
			return null;
		}


	}



	public function get_video_poster()
	{

		if(!empty($this->meta['video']['poster']))
		{
			$poster_id = $this->meta['video']['poster'];
			return wp_get_attachment_url( $poster_id );
		}
		else
		{
			return null;
		}


	}






}
