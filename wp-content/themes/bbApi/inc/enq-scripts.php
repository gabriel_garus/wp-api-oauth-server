<?php
defined('ABSPATH') or die('Access Denied!');
/*
/** Enqueue scripts and styles.
*/



function sbb_scripts()
{
	$cdn = true;
	/*
	* wp_enqueue_style( $handle, $src, $deps, $ver, $media );
	* wp_register_script( $handle, $src, $deps, $ver, $in_footer );
	*

	*/
	 wp_register_script( 'myWpApi', '/js/my-wp-api.js', array('jquery'), '1.0', true );
		// -- Styles ----

		// yt popup
		wp_deregister_style( 'oba_youtubepopup_css');
		
		// Contact Form 7 
		wp_deregister_style('contact-form-7');
		
		//bloom
		wp_deregister_style('et_bloom-css');

		// Main Stylesheet
		

		
		//-----------------------------------





		/* Webshim */

		if($cdn)
		{
			$webshim_src  = '//cdnjs.cloudflare.com/ajax/libs/webshim/1.15.10/dev/polyfiller.js';
		}
		else
		{
			$webshim_src = '/js/polyfiller.js';
		}

		wp_register_script( 'webshim', $webshim_src , array( 'jquery' ), null, false );

		//-------------------------------

		/* Modernizr */


		if($cdn)
		{
			$modernizr_src = '//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js';
		}
		else
		{
			$modernizr_src = '/js/modernizr.js';
		}

		wp_register_script( 'modernizr', $modernizr_src , array(), null);

		//-------------------------------

		/*Navigation Scripts*/
		wp_register_script( 'navScript', '/js/navscript.min.js', array( 'jquery' ), null, true);


		if($cdn)
		{
			$snap_src = '//cdnjs.cloudflare.com/ajax/libs/snap.js/1.9.3/snap.min.js';
		}
		else
		{
			$snap_src = '/js/snap.min.js';
		}
		wp_register_script( 'snapscr', $snap_src , array(), null, true);

		//-------------------------------


		/* Chosen */
		/*
		if($cdn)
		{
			$chosen_src = '//cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.min.js';
		}
		else
		{
			$chosen_src = '/js/chosen.jquery.min.js';
		}
		*/
		//wp_register_script( 'chosen', $chosen_src, array('jquery'), null, true );




		// * video popup */
		
     //  $yt_pp_src = '/js/YouTubePopUp.jquery.min.js';
     //  wp_register_script( 'YouTubePopUp', $yt_pp_src, array('jquery'), null, true );

       wp_dequeue_script('oba_youtubepopup_plugin');
       wp_dequeue_script('oba_youtubepopup_activate');
       	if(IS_FRONT_PAGE)
       	{
       	 	//wp_enqueue_script('YouTubePopUp');	
       	}

       	//-----------------------------------------------------------------------------------



		/* FooTable */
		/*
		if($cdn)
		{
			$footable_src = '//cdnjs.cloudflare.com/ajax/libs/jquery-footable/0.1.0/js/footable.min.js';
		}
		else
		{
			$footable_src = '/js/footable.js';
		}
		*/
		//wp_register_script( 'fooTable', $footable_src  , array('jquery'), null, true );


	

		//wp_register_script( 'jQueryForm', '/js/jquery.form.min.js'  , array('jquery'), null, true );
		//wp_register_script( 'CF7_script', '/js/script_cf.js'  , array('jQueryForm'), null, true );

		

		$page_template = bb_get_page_template(PAGE_ID);

		//*** Enquque Scripts *********************
		wp_enqueue_script( 'modernizr' );
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'navScript' );
		//wp_enqueue_script( 'snapscr' );

		/*
		global $wp_query;
		$post_name = (!empty($wp_query->queried_object->post_name)) ? $wp_query->queried_object->post_name : null;
		*/
		//$meta_data = bb_extract( get_post_meta(PAGE_ID) );


		
		
		 wp_register_script( 'myWpApi', '/js/my-wp-api.js', array('jquery'), '1.0', true );
		
			wp_enqueue_script( 'myWpApi' );

					wp_localize_script( 'myWpApi', 'postdata',
			array(
				'json_url' => rest_url( 'wp/v2/')
			)
		);



	
} // end of bb_scripts

add_action( 'wp_enqueue_scripts', 'sbb_scripts' );

/*
add_action( 'wp_loaded', function() {

});
*/

function ini_scripts()
{
	add_filter( 'wpcf7_load_js', '__return_false' );

	/*
	Add this line in the template
	  if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
        wpcf7_enqueue_scripts();
    }
    */
}
add_action( 'after_setup_theme', 'ini_scripts' );




