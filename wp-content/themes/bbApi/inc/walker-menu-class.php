<?php

/**
 * Custom walker class.
 */
class BB_Walker_Nav_Menu extends Walker_Nav_Menu {
 
    /**
     * Starts the list before the elements are added.
     *
     * Adds classes to the unordered list sub-menus.
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     */
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        // Depth-dependent classes.
       // $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
        $display_depth = ( $depth + 1); // because it counts the first submenu as 0
        $classes = array(
            'sub-menu'
          //  ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
            //( $display_depth >=2 ? 'sub-sub-menu' : '' ),
            //'menu-depth-' . $display_depth
        );
        $class_names = implode( ' ', $classes );
 
        // Build HTML for output.
        $output .= "\n" . '<ul class="' . $class_names . '">' . "\n";
    }
 
    /**
     * Start the element output.
     *
     * Adds main/sub-classes to the list items and links.
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item   Menu item data object.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     * @param int    $id     Current item ID.
     */
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        global $wp_query;
       // $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
        
       

        // Depth-dependent classes.
        /*
        $depth_classes = array(
            ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
            ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
            ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
            'menu-item-depth-' . $depth
        );*/
       // $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );
        $options = bb_get_options();


        // Services Page
        if(!empty($options['services_page']))
        {
            $services_page_id = $options['services_page'];
        }
        else
        {
            $services_page = get_page_by_path('services');
              if($services_page !== null AND is_object($services_page))
              {
                  $services_page_id = $services_page->ID;
              }
              else
              {
                 $services_page_id = 52;
              }

          
        }


        // Solutions Page
        if(!empty($options['solutions_page']))
        {
              $solutions_page_id = $options['solutions_page'];
        }
        else
        {
              $solutions_page = get_page_by_path('solutions');
              if($solutions_page !== null AND is_object($solutions_page))
              {
                  $solutions_page_id =   $solutions_page->ID;
              }
              else
              {
                $solutions_page_id = 54;
              }
        }
      
       // Case Studies Page 
       if(!empty($options['studies_page']))
       {
           $studies_page_id =  $options['studies_page'];
       }
       else
       {
          $studies_page = get_page_by_path('case-studies');
       
          if($studies_page !== null AND is_object($studies_page))
          {
              $studies_page_id = $studies_page->ID;
          }
          else
          {
            $studies_page_id = 70;
          }
        }
        

        // Careers Page
        if(!empty($options['careers_page']))
        {
            $careers_page_id =  $options['careers_page'];
            $careers_page = get_page($careers_page_id );
        }
        else
        {
            $careers_page = get_page_by_path('careers');
            if($careers_page !== null AND is_object($careers_page))
            {
                $careers_page_id = $careers_page->ID;
            }
            else
            {
                $careers_page_id = 115;
                $careers_page = get_page($careers_page_id);
             }
                
          }

        //  $careers_parent = $careers_page->post_parent;
      


        // Passed classes.
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $my_classes = array();
        $page        = get_post();
        $blog_id = (defined('BLOG_ID')) ? BLOG_ID : get_option( 'page_for_posts' );
       // $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
 
        // Build HTML.
        //$output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';
 		
 		if(!empty($classes['dropdown']))
 		{
 			$my_classes[] = "with-drop" ; 
 		}

        if($depth < 1)
        {
            if(!empty($classes['current-menu-item']) OR in_array('current-menu-item', $classes))
            {                   
                $my_classes[] = 'current';
            }
            elseif( is_singular('services') AND $item->object_id == $services_page_id )
            {
                $my_classes[] = 'current';
            }
            elseif(  is_singular('solutions') AND $item->object_id == $solutions_page_id )
            {
                $my_classes[] = 'current';
            }
            elseif(  is_singular('studies') AND $item->object_id == $studies_page_id )
            {
                $my_classes[] = 'current';     
            }
            elseif(  is_singular('careers') AND $careers_page !== null AND $item->object_id == $careers_page->post_parent )
            {
                $my_classes[] = 'current';     
            }
            elseif($page AND $item->object_id == $page->post_parent)
            {
                 $my_classes[] = 'current';
            }
            elseif( ( IS_HOME OR is_singular('post') ) AND  $item->object_id == $blog_id )
            {
                 $my_classes[] = 'current is_single';
            }
            elseif( is_tax('industry') AND $item->object_id == $studies_page_id )
            {
                 $my_classes[] = 'current';
            }

        }
        // $my_classes[] = 'studies-id:'.$studies_page->ID;
        // $my_classes[] = 'item-id:'.$item->object_id ;
        // $my_classes[] = 'depth:' . $depth;


        $output .= '<li';
        if(!empty($my_classes))
        {
            $my_class_names = implode( ' ', $my_classes );
             $output .= ' class="'. $my_class_names . '"';  
        }
        else
        {
           $output .= ' class="empty"'; 
        }
 		$output .= '>';
 		
        // Link attributes.
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
     //   $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';
 
        // Build HTML output and pass through the proper filter.
        $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
            $args->before,
            $attributes,
            $args->link_before,
            apply_filters( 'the_title', $item->title, $item->ID ),
            $args->link_after,
            $args->after
        );
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}