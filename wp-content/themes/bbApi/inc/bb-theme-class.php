<?php
defined('ABSPATH') or die('Access Denied!');

class bbTheme
{

	public $theme_dir = '';
	public $theme_url = '';
	public $styles_dir = '/' . 'style' . '/';

	public $inc_dir = '';
	public $partials_dir = '';

	public $host = '';
	public $company_name = '';

	public $blog_id = '';
	public $home_id = '';

	//-------------

	private $theme_support = array('title-tag' , 'post-thumbnails' );
	private $image_sizes   = array();
	private $editor_image_sizes = array(); 



	public $menus = array();

	public $styles = array();
	public $registered_scripts = array();
	public $enqueued_scripts = array();
	public $script_conditions = array();

	// -------  SETTINGS  ----
	public $settings = array(
		'upload_svg'   => true, 
		'filter_svg'   => true,
	    'filter_pdf'   => true, 
	    'blog_content' => true,
	    'redux' 	   => true,
	    'php_usage'    => true,
	    'reset_editor_image_sizes' => true,
	    'wpml'		   => false,
	    'replace_jquery'   => true
   );


	public $jquery_src = '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js';




   public function __construct()
   {
   	 	$this->set_constants();

   }



   public function activate()
   {
   		$settings = $this->settings;
   		$this->define_constants();

   		add_action( 'after_setup_theme', array( $this, 'after_theme_setup' ));
   		add_action( 'init', array( $this, 'init' ));
   		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ));
   		add_action('admin_footer', array( $this, 'admin_footer'));


   		if(!empty($this->editor_image_sizes))
   		{
   			add_filter( 'image_size_names_choose', array( $this, 'image_size_names_choose' ) );
   		}
   		

   		if($this->settings['upload_svg'] == true)
   		{
   			add_filter('upload_mimes', array( $this, 'svg_upload' ) );
   		}
   	

   		if($this->settings['filter_svg'] == true)
   		{
   			add_filter( 'post_mime_types', array( $this,'modify_svg_post_mime_types') );
   		}
   	
   		if($this->settings['filter_pdf'] == true)
   		{
   			add_filter( 'post_mime_types', array( $this,'modify_pdf_post_mime_types') );
   		}

   		if($this->settings['blog_content'] == true)
   		{
   			add_action('edit_form_after_title', array( $this,'editor_on_posts_page'), 0);
   		}
   		
   }

   private function set_constants()
   {
   		$this->theme_dir    = get_template_directory(). DIRECTORY_SEPARATOR;
   		$this->theme_url    = get_template_directory_uri() . '/';
   		$this->inc_dir      = $this->theme_dir . 'inc/';
   		$this->partials_dir = $this->theme_dir  . 'templates' . DIRECTORY_SEPARATOR . 'partials' . DIRECTORY_SEPARATOR ;
   		$this->host         = $_SERVER['SERVER_NAME'];
   		$this->company_name = get_option('blogname');
   		$this->blog_id      = get_option('page_for_posts');  
   		$this->home_id      = get_option('page_on_front');
   }



   public function svg_upload($mimes)
   {
	  $mimes['svg'] = 'image/svg+xml';
	  return $mimes;
    }





    /* PDF mime filter
	* source: http://code.tutsplus.com/articles/quick-tip-add-extra-media-type-filters-to-the-wordpress-media-manager--wp-25998
	*/

   public function  modify_svg_post_mime_types($post_mime_types)
   {
   		$post_mime_types['image/svg+xml'] = array(
    		__( 'SVG' ),
    		__( 'Manage SVGs' ),
    		_n_noop( 'SVG <span class="count">(%s)</span>', 'SVGs <span class="count">(%s)</span>' 	)
    		);

	    return $post_mime_types;
   }


   public function modify_pdf_post_mime_types($post_mime_types)
   {
   		$post_mime_types['application/pdf'] = array(
    		__( 'PDFs' ),
    		__( 'Manage PDFs' ),
    		_n_noop( 'PDF <span class="count">(%s)</span>', 'PDFs <span class="count">(%s)</span>' 	)
    		);
   		return $post_mime_types;
   }


   //----------------------------------------------------------------------------




   public function admin_footer()
   {

   		if($this->settings['php_usage'])
   		{
   			$memory_limit = $this->return_bytes(ini_get('memory_limit'));
			$memory_usage = memory_get_peak_usage(true);

			$percent = $memory_usage / $memory_limit * 100;
			$percent = round($percent, 2);

			$memory_limit = round($memory_limit /  1000000 ,2);
			$memory_usage  = round($memory_usage /  1000000 ,2);

			echo '<center> PHP memory Usage: <b>' . $percent . '%</b> ('. $memory_usage  . 'MB/' . $memory_limit . 'MB) </center>';	
   		}
		
    }




    public function return_bytes($val) 
    {
	    $val = trim($val);
	    $last = strtolower($val[strlen($val)-1]);
	    switch($last) {
	        // The 'G' modifier is available since PHP 5.1.0
	        case 'g':
	            $val *= 1024;
	        case 'm':
	            $val *= 1024;
	        case 'k':
	            $val *= 1024;
	    }
	    return $val;
     }




	//-- Add content editor to blog page
	//   https://wordpress.org/plugins/add-editor-to-page-for-posts/

   public function editor_on_posts_page($post)
   {
	   	if($post->ID != BLOG_ID || post_type_supports('page', 'editor'))
		{
			return;
		}

		remove_action('edit_form_after_title', '_wp_posts_page_notice');
		add_post_type_support('page', 'editor');
   }
   //----------------------------------------------------------------------------







   public function after_theme_setup()
   {
   		/** Let WordPress manage the document title. **/

   		$this->theme_support();
   		$this->add_image_sizes();
   		

   }




   public function init()
   {

   		if(!empty($this->menus))
   		{	
   		//	error_log( print_r($this->menus, true)) ;
   			register_nav_menus( $this->menus );
   		}
   		else
   		{
   			register_nav_menus( array( 'primary' => 'Primary Menu' ));
   		}

   }


   public function enqueue_scripts()
   {
   		// CSS
   		if(!empty($this->styles))
   		{
   			foreach ($this->styles as $style) 
   			{	
         		wp_register_style( $style['slug'], $style['location'], $style['deps'],  $style['version']);

         		// IE 8 stylesheet
				if($style['ie8'] == true)
				{
					$GLOBALS['wp_styles']->add_data( $style['slug'], 'conditional', 'lte IE 8' );
				}

				wp_enqueue_style( $style['slug'] );
   			}
   		}
   		//--------------------------


   		if($this->settings['replace_jquery'] == true)
   		{
   				wp_deregister_script('jquery');
				wp_register_script( 'jquery', $this->jquery_src,  array(), null, true );
   		}


   		foreach ($this->registered_scripts as $script)
   		{
			wp_register_script( $script['handle'], $script['src'], $script['deps'], $script['ver'], $script['in_footer'] );
   		}

   		$this->check_script_conditions();

   		foreach ($this->enqueued_scripts as $script)
   		{
			wp_enqueue_script($script);
   		}




   }


   private function check_script_conditions()
   {

   		if(IS_FRONT_PAGE)
   		{
   			//error_log('Front page it is');
   		}
   		else
   		{
   			//error_log('NOT a front page');
   		}


   		foreach ($this->enqueued_scripts as $key => $script ) 
   		{
   			if(!empty($this->script_conditions[$script]))
   			{
   				$conditions = $this->script_conditions[$script];
   				foreach ($conditions as $con) 
   				{
   					if(
   						( isset($conditions['is_front_page']) AND $conditions['is_front_page'] !=  IS_FRONT_PAGE  ) ||
   						( isset($conditions['page_id']) AND $conditions['page_id'] != PAGE_ID  ) ||
   						( isset($conditions['page_template']) AND $conditions['page_template'] != PAGE_TEMPLATE  ) ||
   						( isset($conditions['page_slug']) AND $conditions['page_slug'] != PAGE_SLUG  ) 
   					   )
   					{
   						unset($this->enqueued_scripts[$key]);
   					}

   				}

   			}
   			
   		}


   }


   public function add_script($handle, $src, $deps = array(), $ver = null, $in_footer = true)
   {
   		$this->registered_scripts[$handle] = array(
   			'handle' => $handle,
   			'src'  => $src,
   			'deps' => (is_array($deps)) ? $deps : array($deps),
   			'ver'  => $ver,
   			'in_footer' => $in_footer
   			);

   		$this->enqueued_scripts[] = $handle; 
   }

    public function register_script($handle, $src, $deps = array(), $ver = null, $in_footer = true)
    {
    	$this->registered_scripts[$handle] = array(
   			'handle' => $handle,
   			'src'  => $src,
   			'deps' => (is_array($deps)) ? $deps : array($deps),
   			'ver'  => $ver,
   			'in_footer' => $in_footer
   			);
    }


    public function enqueue_script($handle)
    {
    	if(!empty($this->registered_scripts[$handle]))
    	{
    		$this->enqueued_scripts[] = $handle; 
    	}
    	else
    	{
    		error_log('Theme Class - Script '.$handle . ' not registered'); 
    	}
    }

    public function add_script_condition($handle, $conditions)
    {
    	$this->script_conditions[$handle] = $conditions;
    }


   public function add_style($slug, $location, $deps = array(), $version = null)
   {

   		$style['slug']     = $slug;
   		$style['location'] = $this->styles_dir . $location;
   		$style['deps']     = $deps;
   		$style['version']  = $version;

   		$this->styles[$slug] = $style; 
   }

   public function add_ie8_style($slug, $location, $deps = array(), $version = null)
   {

   		$style['slug']     = $slug;
   		$style['location'] = $this->styles_dir . $location;
   		$style['deps']     = $deps;
   		$style['version']  = $version;
   		$style['ie8']       = true;

   		$this->styles[] = $style; 
   }







  /******************
		IMAGE SIZES
		*****************/
   public function add_image_size($slug, $width = 0, $height = 0, $crop = true)
   {	
   		if($width>0 AND $height>0)
   		{
	   		$new_image_size =  array(
	   			'slug'   => $slug,
	   			'width'  => $width,
	   			'height' => $height,
	   			'crop'   => $crop
	   		);

	   		$this->image_sizes[] = $new_image_size;
	  		return $new_image_size;
   		}
   		else
   		{
   			error_log('Theme Class: New image size cannot be 0' );
   			return false; 
   		}
   
   }

   private function add_image_sizes()
   {
   	  if(!empty($this->image_sizes))
   	  {
   	  	foreach ($this->image_sizes as $size)
   	  	{
   	  		add_image_size( $size['slug'],  $size['width'], $size['height'], $size['crop'] );
   	  	}
   	  	
   	  }
   }


   public function get_image_sizes()
   {
   	 return $this->image_sizes;
   }



   public function add_menu($slug, $label = null)
   {
   	$slug = strtolower($slug);
   	$label = ($label === null) ? $slug : $label;

   	$this->menus[$slug] = $label;
   }	


   public function add_editor_image_size($slug, $label)
   {
   		$this->editor_image_sizes[] = array($slug => $label );

   		if($this->settings['wpml'] == true)
   		{
   			do_action( 'wpml_register_single_string', 'bigbang',  'editor-image-size-label', $label );
   		}
   		
   }


	public function image_size_names_choose( $sizes ) 
	{
		$settings = $this->settings;

		if($settings['reset_editor_image_sizes'] == true)
		{
			return $this->editor_image_sizes;
		}
		else
		{
			return array_merge( $sizes, $this->editor_image_sizes );
		}

	}








  /******************
		THEME SUPPORT
		*****************/

  public function add_support($support)
  {
  	$this->add_theme_support($support);
  }		


   public function add_theme_support($support)
   {
	   	if(is_String($support) OR is_array($support))
	   	{
	   		$this->theme_support[] = $support;
	   		return true;
	   	}
	   	else
	   	{
	   		return false; 
	   	}
   }

   public function remove_theme_support($support)
   {
   		$theme_support = $this->theme_support;
   		foreach ($theme_support as $key => $value) 
   		{
   			if($key == $support OR $value == $support)
   			{
   				unset($theme_support[$key]);
   			}
   		}
   		$this->theme_support = $theme_support;
   }



   public function get_theme_support()
   {
   	  return $this->theme_support;
   }



   private function theme_support()
   {	
   		// error check
   		if(empty($this->theme_support) AND !is_array($this->theme_support))
   		{
   			error_log('Theme Class: $this->theme_support is empty' );
   			return false;
   		}
   		//*****************************

   		foreach ( $this->theme_support as $key=>$value )
   		{
   			if(is_array($value ))
   			{
   				add_theme_support( $key, $value );
   			}
   			else
   			{
   				add_theme_support( $value );
   			}
   			
   		}
   }

	/*******************************************************/








   private function define_constants()
   {	
   		
   		if(!defined('THEME_DIR'))
   		{
   			define('THEME_DIR', $this->theme_dir );
   		}
   	
   		if(!defined('THEME_URL'))
   		{
   			define('THEME_URL', $this->theme_url );
   		}
		
		if(!defined('INC_DIR'))
		{
			define('INC_DIR', $this->inc_dir );
		}

		if(!defined('PARTIALS_DIR'))
		{
			define( 'PARTIALS_DIR', THEME_DIR . 'templates' . DIRECTORY_SEPARATOR . 'partials' . DIRECTORY_SEPARATOR );
		}

		if(!defined('HOST'))
		{
			define('HOST', $this->host );
		}

		if(!defined('COMPANY_NAME'))
		{
			define( 'COMPANY_NAME', $this->company_name );
		}

		if(!defined('BLOG_ID'))
		{
			define( 'BLOG_ID', $this->blog_id );
		}

		if(!defined('HOME_ID'))
		{
			define( 'HOME_ID', $this->home_id );
		}
   }





}