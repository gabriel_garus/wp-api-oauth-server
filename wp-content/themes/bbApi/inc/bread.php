<?php
defined('ABSPATH') or die('Access Denied!');

function the_breadcrumb()
{
		global $wp_query;

		$query_vars = $wp_query->query_vars;
		//error_log(print_r($wp_query, true));
		$options = bb_get_options();

		$blog_page = ( defined('BLOG_ID') ) ? BLOG_ID : get_option( 'page_for_posts' );
		

  if ( IS_FRONT_PAGE === false )
	{
		global $post;

		$title = '';
		$count = 2;
		echo '<div class="breadcrumbs section">' . PHP_EOL;
		echo '<div>' . PHP_EOL;
		echo '<nav id="breadcrumbs">';
		echo '<ul>';
		echo '<li>';
		echo '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
		echo '<a href="' . rtrim(get_option('home'), '/') . '/" itemprop="url">';
		echo '<span itemprop="title">Home</span>';
		echo '</a>';
		echo '</span>';

		$is_tag = is_tag();

		if( is_singular('post') || $is_tag )
		{
		
			$page_title =  get_the_title( 	$blog_page );
			$the_post = get_post(	$blog_page );
			$page_slug = $the_post->post_name;

			bread_part('/'.$page_slug.'/', $page_title);
			$count++;

			

		}	
		elseif( is_singular('services'))
		{
			if(!empty($options['services_page']))
			{
				$url = get_permalink( $options['services_page'] );
			}
			else
			{
					$url = WP_HOME . '/services/';
			}
		
			//	error_log('$url: ' . $url);
				bread_part($url , 'Services');
				$count++;
		}
		elseif( is_singular('solutions'))
		{	
			if(!empty($options['solutions_page']))
			{
				$url = get_permalink( $options['solutions_page'] );
			}
			else
			{
				$url = WP_HOME . '/solutions/';
			}
			
			//	error_log('$url: ' . $url);
				bread_part($url , 'Solutions');
				$count++;
		}
		elseif( is_singular('studies'))
		{	
			if(!empty($options['studies_page']))
			{
				$studies_url = get_permalink( $options['studies_page'] );
			}
			else
			{
				$studies_url = WP_HOME . '/case-studies/';
			}
			
			//	error_log('$url: ' . $url);
				bread_part($studies_url , 'Case Studies');
				$count++;

			$cats = get_the_terms(PAGE_ID,'industry');
			$cat = $cats[0];

			$url = $studies_url . $cat->slug . '/';
			//	error_log('$url: ' . $url);
				bread_part($url , $cat->name);
				$count++;

			
		}
		elseif( is_singular('careers'))
		{
			$url = WP_HOME . '/about/';
			bread_part($url , 'About');
			$count++;

			if(!empty($options['careers_page']))
			{
				$url = get_permalink( $options['careers_page'] );
			}
			else
			{
				$url = WP_HOME . '/about/careers/';
			}
		
			bread_part($url , 'Careers');
			$count++;
		}
	   	elseif ( is_single() || is_category() || $is_tag )
		{
		//die('is_single() || is_category() || is_tag() ');
			$categories = get_the_category();


			foreach ($categories as $category)
			{
				$url = WP_HOME . '/'.$category->slug .'/';
			//	error_log('$url: ' . $url);
				bread_part($url , $category->cat_name);
				$count++;
			}
		}
		elseif ( is_page() || IS_HOME )
		{

	 		//print_r($post, false);
			//die('is_page');
			if( IS_HOME )
			{
				$post = get_post( $blog_page );
			}

			if ($post->post_parent)
				{
					$ancestors = get_post_ancestors($post->ID);
					
					$ancestors = array_reverse($ancestors);
					foreach ($ancestors as $ancestor)
					{
						bread_part(get_page_link($ancestor), get_the_title($ancestor) );
						$count++;
					}
				}


		}
		elseif( is_tax('industry') )
		{
			if(!empty($options['studies_page']))
			{
				$url = get_permalink( $options['studies_page'] );
			}
			else
			{
				$url = WP_HOME . '/case-studies/';
			}
			//	error_log('$url: ' . $url);
				bread_part($url , 'Case Studies');
				$count++;
		}

	//----------------------------------------




		if ( $is_tag )
		{
			$title =  single_tag_title('Tag: ');
		}
		else if ( is_day() )
		{
			$title = get_the_time('F jS, Y') . ' Archives';
		}
		else if ( is_month() )
		{
			$title = get_the_time('M') . ' Archives';
		}
		else if ( is_year() )
		{
			$title = get_the_time('Y') . ' Archives';
		}
		else if ( is_search() )
		{
			$title = 'Search Results';
		}
		else if ( is_author() )
		{
			$title = 'Page Not Found';
		}
		else if ( is_404() )
		{
			$title = 'Page Not Found';
		}
		else if ( is_author() )
		{
			//global $author;
			//$userdata = get_userdata($author)
			//$title = $userdata->display_name;
		}
		elseif( is_tax() )
		{
			//die( 'is_tax()');

			$title = $query_vars['industry'];
		}
		else
		{
			$title =  get_the_title(PAGE_ID);
		}
		echo '<ul>';
		echo '<li class="current" itemprop="breadcrumb">';
		echo '<span>' . $title . '</span>';
		echo str_repeat('</li></ul>', $count) . PHP_EOL;
		echo '</nav>' . PHP_EOL;
		echo '</div>' . PHP_EOL;
		echo '</div>' . PHP_EOL;
	}
	

//dump($query_vars);
}



function bread_part($href, $label)
{
		echo '<ul>';
		echo '<li>';
		echo '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
		echo '<a href="'.$href.'" itemprop="url" >';
		echo '<span itemprop="title">'.$label.'</span>';
		echo '</a>';
		echo '</span>';
}
