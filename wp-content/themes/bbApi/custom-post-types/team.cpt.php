<?php
defined('ABSPATH') or die('Access Denied!');
// Register Custom Post Type

function team_post_type()
{

	$text_domain = 'bigbang';
	$singular = 'Team Member';
	$plural = 'Team';

	$labels = array(
		'name'                => _x( $plural, 'Post Type General Name', $text_domain ),
		'singular_name'       => _x( $singular, 'Post Type Singular Name', $text_domain ),
		'menu_name'           => __( $plural, $text_domain ),
		'name_admin_bar'      => __( $singular , $text_domain ),
		'parent_item_colon'   => __( 'Parent Item:', $text_domain ),
		'all_items'           => __( $plural, $text_domain ),
		'add_new_item'        => __( 'Add New ' . $singular, $text_domain ),
		'add_new'             => __( 'Add ' . $singular, $text_domain ),
		'new_item'            => __( 'New ' . $singular, $text_domain ),
		'edit_item'           => __( 'Edit ' . $singular, $text_domain ),
		'update_item'         => __( 'Update ' . $singular, $text_domain ),
		'view_item'           => __( 'View ' . $singular, $text_domain ),
		'search_items'        => __( 'Search ' . $singular, $text_domain ),
		'not_found'           => __( 'Not found', $text_domain ),
		'not_found_in_trash'  => __( 'Not found in Trash', $text_domain ),
	);
	$args = array(
		'label'               => __( $singular, $text_domain ),
		'description'         => __( $plural, $text_domain ),
		'labels'              => $labels,
		'supports'            => array('title', 'thumbnail', 'editor'),
		'taxonomies'          => array('department'),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-groups', // https://developer.wordpress.org/resource/dashicons
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'capability_type'     => 'page',
		'show_in_rest'        => true,
		//'rewrite' 			  => array('slug' =>  $page_slug, 'with_front' => false)
	);
	register_post_type( strtolower($plural), $args );

}

// Hook into the 'init' action
add_action( 'init', 'team_post_type', 0 );



function create_dept_taxony()
{
   
	$singular = 'Department';
	$plural = 'Departments';
	$slug = 'department';

    $labels = array(
        'name'              => _x( $plural, 'taxonomy general name' ),
        'singular_name'     => _x( $singular, 'taxonomy singular name' ),
        'search_items'      => __( 'Search '.$plural ),
        'all_items'         => __( 'All ' . $plural ),
        'parent_item'       => __( 'Parent ' . $singular ),
        'parent_item_colon' => __( 'Parent :' . $singular ),
        'edit_item'         => __( 'Edit ' . $singular ),
        'update_item'       => __( 'Update ' . $singular ),
        'add_new_item'      => __( 'Add New ' . $singular ),
        'new_item_name'     => __( 'New ' . $singular ),
        'menu_name'         => __( $plural ),
    );


    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true
       /*
        'rewrite'           => array( 'hierarchical' => false, //default false
                                       'slug' => $slug ,
                                     'with_front' => false // default: true
         ),*/
    );

  register_taxonomy( $slug , null, $args );
  register_taxonomy_for_object_type( $slug , 'team' );






}


add_action( 'init', 'create_dept_taxony', 0 );



function checktoradio_team()
{
    echo '<script type="text/javascript">
     jQuery("#departmentchecklist input").each(function(){
        this.type="radio"; 
    });
    </script>';
}

add_action('admin_footer', 'checktoradio_team');