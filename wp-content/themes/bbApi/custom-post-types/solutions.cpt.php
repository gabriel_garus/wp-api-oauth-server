<?php
defined('ABSPATH') or die('Access Denied!');
// Register Custom Post Type

function solutions_post_type()
{

	$text_domain = 'bigbang';
	$singular = 'Solution';
	$plural = 'Solutions';

	$labels = array(
		'name'                => _x( $plural, 'Post Type General Name', $text_domain ),
		'singular_name'       => _x( $singular, 'Post Type Singular Name', $text_domain ),
		'menu_name'           => __( $plural, $text_domain ),
		'name_admin_bar'      => __( $singular , $text_domain ),
		'parent_item_colon'   => __( 'Parent Item:', $text_domain ),
		'all_items'           => __( $plural, $text_domain ),
		'add_new_item'        => __( 'Add New ' . $singular, $text_domain ),
		'add_new'             => __( 'Add ' . $singular, $text_domain ),
		'new_item'            => __( 'New ' . $singular, $text_domain ),
		'edit_item'           => __( 'Edit ' . $singular, $text_domain ),
		'update_item'         => __( 'Update ' . $singular, $text_domain ),
		'view_item'           => __( 'View ' . $singular, $text_domain ),
		'search_items'        => __( 'Search ' . $singular, $text_domain ),
		'not_found'           => __( 'Not found', $text_domain ),
		'not_found_in_trash'  => __( 'Not found in Trash', $text_domain ),
	);
	$args = array(
		'label'               => __( $singular, $text_domain ),
		'description'         => __( $plural, $text_domain ),
		'labels'              => $labels,
		'supports'            => array('title', 'thumbnail', 'editor'),
		'taxonomies'          => array(),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-lightbulb', // https://developer.wordpress.org/resource/dashicons
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'show_in_rest'        => true,
		//'rewrite' 			  => array('slug' =>  $page_slug, 'with_front' => false)
	);
	register_post_type( strtolower($plural), $args );

}

// Hook into the 'init' action
add_action( 'init', 'solutions_post_type', 0 );
