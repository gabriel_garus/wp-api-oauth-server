<?php
defined('ABSPATH') or die('Access Denied!');
// Register Custom Post Type

function cs_post_type()
{

	$text_domain = 'bigbang';
	$singular = 'Case Study';
	$plural = 'Case Studies';

	$labels = array(
		'name'                => _x( $plural, 'Post Type General Name', $text_domain ),
		'singular_name'       => _x( $singular, 'Post Type Singular Name', $text_domain ),
		'menu_name'           => __( $plural, $text_domain ),
		'name_admin_bar'      => __( $singular , $text_domain ),
		'parent_item_colon'   => __( 'Parent Item:', $text_domain ),
		'all_items'           => __( $plural, $text_domain ),
		'add_new_item'        => __( 'Add New ' . $singular, $text_domain ),
		'add_new'             => __( 'Add ' . $singular, $text_domain ),
		'new_item'            => __( 'New ' . $singular, $text_domain ),
		'edit_item'           => __( 'Edit ' . $singular, $text_domain ),
		'update_item'         => __( 'Update ' . $singular, $text_domain ),
		'view_item'           => __( 'View ' . $singular, $text_domain ),
		'search_items'        => __( 'Search ' . $singular, $text_domain ),
		'not_found'           => __( 'Not found', $text_domain ),
		'not_found_in_trash'  => __( 'Not found in Trash', $text_domain ),
	);


	$rewrite = array(
    'slug'                => 'case-studies/%industry%',
    'with_front'          => true,
    'pages'               => true,
    'feeds'               => true,
   	'hierarchical'        => false,
);

	$args = array(
		'label'               => __( $singular, $text_domain ),
		'description'         => __( $plural, $text_domain ),
		'labels'              => $labels,
		'supports'            => array('title', 'thumbnail', 'editor'),
		'taxonomies'          => array('industry'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-media-document', // https://developer.wordpress.org/resource/dashicons
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
    'show_in_rest'        => true,
	    'rewrite' 			  => $rewrite
	);
	register_post_type( 'studies', $args );

}

// Hook into the 'init' action
add_action( 'init', 'cs_post_type', 0 );


function create_industry_taxony()
{
   
	$singular = 'Industry';
	$plural = 'Industries';
	$slug = 'industry';

    $labels = array(
        'name'              => _x( $plural, 'taxonomy general name' ),
        'singular_name'     => _x( $singular, 'taxonomy singular name' ),
        'search_items'      => __( 'Search '.$plural ),
        'all_items'         => __( 'All ' . $plural ),
        'parent_item'       => __( 'Parent ' . $singular ),
        'parent_item_colon' => __( 'Parent :' . $singular ),
        'edit_item'         => __( 'Edit ' . $singular ),
        'update_item'       => __( 'Update ' . $singular ),
        'add_new_item'      => __( 'Add New ' . $singular ),
        'new_item_name'     => __( 'New ' . $singular ),
        'menu_name'         => __( $plural ),
    );


    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true
       /*
        'rewrite'           => array( 'hierarchical' => false, //default false
                                       'slug' => $slug ,
                                     'with_front' => false // default: true
         ),*/
    );

  register_taxonomy( $slug , null, $args );
  register_taxonomy_for_object_type( $slug , 'studies' );






}

///%category%/%postname%/
add_action( 'init', 'create_industry_taxony', 0 );




function industry_rewrite()
{

  $all_industries = get_categories( array('taxonomy'=> 'industry'));

  	foreach ($all_industries as $cat) 
  	{
  		$the_slug = 'studies/' . $cat->slug;
		add_rewrite_rule( $the_slug, "index.php?post_type=studies&industry_slug=".$cat->slug );
		
	}




}
add_action( 'init', 'industry_rewrite');

function checktoradio(){
    echo '<script type="text/javascript">
     jQuery("#industrychecklist input").each(function(){
        this.type="radio"; 
    });
    </script>';
}

add_action('admin_footer', 'checktoradio');


function wpa_course_post_link( $post_link, $id = 0 )
{
    $post = get_post($id);
    if ( is_object( $post ) ){
        $terms = wp_get_object_terms( $post->ID, 'industry' );
        if( $terms ){
            return str_replace( '%industry%' , $terms[0]->slug , $post_link );
        }
    }
    return $post_link;
}
add_filter( 'post_type_link', 'wpa_course_post_link', 10, 2 );





    function default_industry_category($post_id, $post)
    {

    	$terms = wp_get_object_terms( $post_id, 'industry');

    	if(empty($terms))
    	{
    		wp_set_object_terms( $post_id, 'other','industry' );
    	}


    }

     add_action( 'save_post', 'default_industry_category', 10, 2 );