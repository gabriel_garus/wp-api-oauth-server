<?php
/**
 * The template for displaying search results pages.
 *@package bigbang
 *
 */


get_header();


if ( have_posts() )
{
	echo '<div class="hero blog">' . PHP_EOL;
	echo '<div class="container">' . PHP_EOL;
	echo '<h1>';
	printf( __( 'Search Results for: %s', 'bigbang' ), '<span>' . get_search_query() . '</span>' );
	echo '</h1>'.PHP_EOL;
	echo '</div>' . PHP_EOL; //.container
	echo '</div>' . PHP_EOL; //.hero blog


	echo '<div class="section blog single-article">' . PHP_EOL;
	echo '<section class="news">' . PHP_EOL;
	echo '<div class="container">' . PHP_EOL;
	echo '<div class="blog-list">' . PHP_EOL;

while ( have_posts() )
{
	the_post();
	require PARTIALS_DIR . 'blog-list-item.php';
}

	echo '</div>' . PHP_EOL;
	echo '</div>' . PHP_EOL; // .container
	echo '</section>' . PHP_EOL; //.news
	echo '</div>' . PHP_EOL; //.section blog single-article
}

else
{
	echo '<div class="legal section">' . PHP_EOL;
	echo '<div class="container">' . PHP_EOL;
	echo '<h1>';
	_e('Nothing Found' , 'bigbang');
	echo '</h1>'.PHP_EOL;
	echo '<div class="result-content">'.PHP_EOL;
	echo '<p> ' . __( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'bigbang') . '</p>'.PHP_EOL;
	echo '<div class="search-404">' . PHP_EOL;
	$submit_label = 'Search';
	get_search_form();
	echo '</div>' . PHP_EOL;
	echo '</div>'.PHP_EOL;// .result-content
	echo '</div>' . PHP_EOL;//.container
	echo '</div>' . PHP_EOL;// .legal section
}

get_footer();
