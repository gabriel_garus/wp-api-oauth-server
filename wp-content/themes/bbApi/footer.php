<?php
defined('ABSPATH') or die('Access Denied!');

$options  = bb_get_options();





$social_title =  ( !empty($options['social_title']) ) ? $options['social_title'] : null;





/*** Footer *********************/
echo '<footer id="theFooter" role="contentinfo">'.PHP_EOL;

echo '<div class="container">' . PHP_EOL;

			//-------------------  TOP ------------ //
echo '<div class="top">'.PHP_EOL;

//----------------  LOGO ------ //
include PARTIALS_DIR . 'logo-footer.php';
//------------------------------------------------------------


//----------------  socialand affiliates wrap--------- //
echo '<div class="footer-top-right">'.PHP_EOL;


//----------------  social --------- //
echo '<div class="footer-social">'.PHP_EOL;

include PARTIALS_DIR . 'social-links-invert.php';

echo '</div>'.PHP_EOL; // .social
//------------------------------------------


//----------------  affili --------- //
echo '<div class="affiliations">'.PHP_EOL;

for ($i=1; $i<=2 ; $i++)
{
	$af_image = ( !empty($options['footerAffiliation'.$i.'-image']) ) ? $options['footerAffiliation'.$i.'-image'] : null;

	if($af_image !== null)
	{
		$af_width  = (!empty($af_image['footerLogo-size']['width'])) ? $af_image['footerLogo-size']['width'] : null;
		$af_height = (!empty($af_image['footerLogo-size']['height'])) ? $af_image['footerLogo-size']['height'] : null;

		$img = new bbImage( $af_image['id']);
		$img->set_class($img->alt);

		if($af_width !== null)
		{
			$img->set_width($af_width);
		}

		if($af_height !== null)
		{
			$img->set_height($af_height);
		}

		echo $img->html;
	}
}


echo '</div>'.PHP_EOL; // .affili
//----------------------------------------------

echo '</div>'.PHP_EOL; // .footer top right
//----------------------------------------------

echo '</div>'.PHP_EOL; // .TOP

/******************************************/

echo '<div class="mid">' . PHP_EOL;
echo '<div class="footer-contact">' . PHP_EOL;
echo '<h5>Get In Touch</h5>' . PHP_EOL;

//----------------------------------


$telephone = (!empty($options['footer_telephone'])) ? $options['footer_telephone'] : null;

if($telephone != null)
{
	$telephone = trim($telephone);
	$tele_short = str_replace(' ', '', $telephone);
	echo '<a href="tel:'.$tele_short.'" class="telephone">'. $telephone . '</a> '.PHP_EOL;
}

$email 	= ( !empty($options['footer_email']) ) ? $options['footer_email'] : null;
if( $email !== null )
{
	echo '<a href="mailto:'.$email.'" class="email">' . $email . '</a>'.PHP_EOL;
}

echo '</div>' . PHP_EOL;


echo '<div class="legal-links">'.PHP_EOL;
bb_menu('legal');
echo '</div>';//closes div leftbot

echo '<div class="footer-btn">' . PHP_EOL;

if(!empty($options['footer_button_custom_url']))
{
	$footer_btn_url =  $options['footer_button_custom_url'];
}
elseif(!empty($options['footer_button_url']))
{
$footer_btn_url = $options['footer_button_url'];
}
$footer_btn_label =  (!empty($options['footer_button_label'])) ? $options['footer_button_label'] : null;

if($footer_btn_url !== null && $footer_btn_label !== null )
{
	echo '<a href="'.$footer_btn_url.'">'. $footer_btn_label .'</a>' . PHP_EOL;
}

echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL; //.mid




			//-------------------  BOTTOM ------------ //
echo '<div class="bottom">'.PHP_EOL;

include PARTIALS_DIR . 'footer-legal.php';

//---------------------------------------------

echo '</div>'.PHP_EOL; // .bottom





//---------------------------------------
echo '</div>' . PHP_EOL;
echo '</footer>'.PHP_EOL;







































echo '</div>' . PHP_EOL; //.page-content

wp_footer();

include PARTIALS_DIR . 'footer-scripts.php';

echo '</div>' . PHP_EOL; // .snap-content

echo '</body>' . PHP_EOL;
echo '</html>' . PHP_EOL;


global $wp_scripts;
//echo var_dump($wp_scripts);
