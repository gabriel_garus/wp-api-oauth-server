<?php
defined('ABSPATH') or die('Access Denied!');
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package bigbang
 */

$browser = get_user_browser();

define( 'PAGE_ID', bb_get_page_id() );
define( 'IS_FRONT_PAGE', is_front_page() );
define( 'IS_HOME', is_home() );

define( 'PAGE_TEMPLATE', bb_get_page_template(PAGE_ID));


$page_title 	= get_the_title( $page_id );

$body_schema 	= bb_get_body_schema($page_title);


$options  = bb_get_options();
if(PAGE_ID)
{
	$bbPage = new bbPage(PAGE_ID);
}
define( 'PAGE_SLUG', $bbPage->post->post_name );





/*
if(!empty($_GET['lang']))
{
	$tr_id = icl_object_id( PAGE_ID, $bbPage->post_type, false, $_GET['lang'] );

	if($tr_id)
	{

		$bbPage = new bbPage($tr_id);
	}
}

$data = array(
	'id' => $bbPage->ID,
	'slug' =>$bbPage->post->post_name,
	'title'=> $bbPage->title,
	'content' => $bbPage->content,
	'sections' => $bbPage->meta
	);

header('Content-Type: application/json');
echo json_encode($data);
die();
*/












echo '<!DOCTYPE html>' . PHP_EOL;
echo '<html lang="en-IE" class="no-js" ng-app="myApp">' . PHP_EOL;

echo '<head>' . PHP_EOL;

echo '<meta charset="utf-8" />' . PHP_EOL;

echo '<meta name="application-name" content="' . COMPANY_NAME . '" />' . PHP_EOL;
echo '<meta name="msapplication-tap-highlight" content="no" />' . PHP_EOL;
echo '<meta name="mobile-web-app-capable" content="yes" />' . PHP_EOL;
echo '<meta name="format-detection" content="telephone=no" />' . PHP_EOL;

echo '<link rel="dns-prefetch" href="//ajax.googleapis.com">' . PHP_EOL;
echo '<link rel="dns-prefetch" href="//cdnjs.cloudflare.com">' . PHP_EOL;
echo '<link rel="dns-prefetch" href="//fonts.googleapis.com">' . PHP_EOL;
echo '<link rel="dns-prefetch" href="//youtube.com">' . PHP_EOL;


////////////   FONTS  //////////////////////////
echo '<link href="https://fonts.googleapis.com/css?family=Lato:';
//echo '100,';
echo '100italic,';
echo '300,';
echo '300italic,';
echo '400,';
echo '400italic,';
echo '700,';
//echo '700italic,';
echo '900,';
//echo '900italic,';
echo '" rel="stylesheet" type="text/css">' . PHP_EOL;
///////////////////////////////////////////////////////////






echo '<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, user-scalable=no" />' . PHP_EOL;

//echo '<link rel="canonical" href="' . $canonical .'" />'.PHP_EOL;
wp_head();

include_once PARTIALS_DIR . 'schema-json.php';

echo '</head>' . PHP_EOL;

flush();

echo '<body itemscope itemtype="https://schema.org/' . $body_schema . '" class="' . $browser .' et_bloom" >'.PHP_EOL;



include_once PARTIALS_DIR . 'navbar.php';

echo '<div id="page" class="snap-content">' . PHP_EOL;

echo '<div id="head-mobile" class="'.trim($header_class).'">' . PHP_EOL;
echo '<div class="top cf">';
echo '<div class="btn-telephone cf">';
bb_title($telephone_title,4);
bb_telephone_link($the_telephone, null, null, 'btn primary');
echo '</div>' . PHP_EOL;
echo '</div>'; 

echo '<div class="container">' . PHP_EOL;

include PARTIALS_DIR . 'logo.php';

echo '<button id="snap-open" type="button" aria-label="Open"></button>' . PHP_EOL;

echo '</div>' . PHP_EOL; // END: .wrap
echo '</div>' . PHP_EOL; // END: .head

if($body_schema == 'WebPage' OR $body_schema == 'Contactpage' OR $body_schema == 'AboutPage')
{
	$content_itemprop = ' itemprop="mainContentOfPage"';
}
else
{
	$content_itemprop = null;
}

$page_class = bb_get_page_class(PAGE_ID, $page_title);
echo '<div id="the-content" class="page-content '.$page_class.'"'. $content_itemprop . '>' . PHP_EOL;
