<?php
defined('ABSPATH') or die('Access Denied!');
/*
* 	Single Case Study specyfic metaboxes
*/

$box_id = 'call-us';
$page_set->add_metabox( array(
        'id' => $box_id,
        'title' => 'Call Us Section',
        ));
  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));

  $page_set->add_field($box_id, array(
     'type'  => 'textarea',
     'name'  => 'text',
     'label' => 'Intro',
     'rows'  => 4
  ));

   $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'heading',
     'label' => 'Phone Header'
  )); 

   $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'phone',
     'label' => 'Phone Number'
  )); 


$box_id = 'related-cases';
$page_set->add_metabox( array(
        'id' => $box_id,
        'title' => 'Related Case Studies',
        ));



   for( $ai=1;$ai<=4;$ai++)
      {

        $page_set->add_field($box_id, array(
          'type'  => 'content_select',
          'name'  => 'related_cs_'.$ai,
          'label' => 'Case Study '.$ai,
          'post_type' => 'studies'
          ));
      }


$box_id = 'company-logo';
  $page_set->add_metabox( array(
        'id' => $box_id,
        'title' => 'Company Logo',
        'priority' => 'high'
        ));

    $page_set->add_field($box_id, array(
     'type'  => 'image',
     'name'  => 'image',
     'label' => 'Image'
  ));


     ///////////////     FEATURE    //////////////////////////////////////////



      

$box_id = 'feature-section';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'Feature Section' ));

  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));


  $page_set->add_field($box_id, array(
     'type'  => 'textarea',
     'name'  => 'text',
     'label' => 'Text',
     'rows'  => 4
  ));



 /////////////////////////////////////////////////////////
