<?php
defined('ABSPATH') or die('Access Denied!');




$box_id = 'post-type';
  $page_set->add_metabox( array(
        'id' => $box_id,
        'title' => 'Post Type',
        'context' => 'side',
        'priority' => ''
        ));

     $page_set->add_field($box_id, array(
          'type'  => 'content_select',
          'name'  => 'id',
          'label' => 'Title',
           'post_type' => 'post-types'
          ));


$box_id = 'feature-section';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'Feature Section' ));

  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));


  $page_set->add_field($box_id, array(
     'type'  => 'textarea',
     'name'  => 'text',
     'label' => 'Text',
     'rows'  => 4
  ));