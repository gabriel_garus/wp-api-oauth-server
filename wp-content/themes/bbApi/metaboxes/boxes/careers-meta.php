<?php
defined('ABSPATH') or die('Access Denied!');
/*
* 	Single Case Study specyfic metaboxes
*/

$box_id = 'apply';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'Apply Section' ));
  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));


$page_set->add_field($box_id, array(
     'type'  => 'textarea',
     'name'  => 'text',
     'label' => 'Intro',
     'rows'  => 4
  ));
$page_set->add_field($box_id , array(
    'type' => 'form',
    'name' => 'form_id'
  ));



$box_id = 'feature-section';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'Feature Section' ));

  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));


  $page_set->add_field($box_id, array(
     'type'  => 'textarea',
     'name'  => 'text',
     'label' => 'Text',
     'rows'  => 4
  ));