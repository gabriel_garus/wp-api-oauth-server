<?php
$box_id = 'middle';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'Content Section' ));



$page_set->add_field($box_id, array(
                    'type'  => 'rich',
                    'label' => 'Text',
                    'name'  => 'text',
                    'rows'	=> 18,
                    'media' => true
                ));