<?php
defined('ABSPATH') or die('Access Denied!');

$box_id = 'contact';

$page_set->add_metabox( array(
	'id' => $box_id,
	'title' => 'Contact Information'
	));

$page_set->add_field($box_id , array(
		'type' => 'text',
		'name' => 'hours',
		'label' => 'Opening Hours'
	));

$page_set->add_field($box_id , array(
		'type' => 'text',
		'name' => 'telephone',
		'label' => 'Contact Telephone'
	));





$box_id = 'the_form';
$page_set->add_metabox( array(
	'id' => $box_id,
	'title' => 'Contact Form'
	));
$page_set->add_field($box_id , array(
		'type' => 'text',
		'name' => 'title',
		'label' => 'Title'
	));

$page_set->add_field($box_id , array(
		'type' => 'form',
		'name' => 'id'
	));


$box_id = 'the_quote';
$page_set->add_metabox( array(
	'id' => $box_id,
	'title' => 'The Quote'
	));

$page_set->add_field($box_id , array(
		'type' => 'text',
		'name' => 'title',
		'label' => 'Title'
	));

$page_set->add_field($box_id , array(
		'type' => 'textarea',
		'name' => 'text',
		'label' => 'Quote Text',
		'rows'	=> 6
	));

$page_set->add_field($box_id , array(
		'type' => 'text',
		'name' => 'author',
		'label' => 'Quote Author'
	));

$page_set->add_field($box_id , array(
		'type' => 'text',
		'name' => 'position',
		'label' => 'Author\'s Position'
	));




 ///////////////     FEATURE    //////////////////////////////////////////



      

$box_id = 'feature-section';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'Feature Section' ));

  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));


  $page_set->add_field($box_id, array(
     'type'  => 'textarea',
     'name'  => 'text',
     'label' => 'Text',
     'rows'  => 4
  ));



 /////////////////////////////////////////////////////////
