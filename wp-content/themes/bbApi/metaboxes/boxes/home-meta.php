<?php
defined('ABSPATH') or die('Access Denied!');

$page_set->add_field('headline', array(
          'type'  => 'text',
          'label' => 'Subtitle',
          'name'  => 'hero-subtitle'
          )
        );

//---------------------------------------------------------------


  $page_set->add_field('headline', array(
  					'type'  => 'cta_btn',
  					'label' => 'Hero Button',
  					'name'  => 'cta_hero'
  			));



  $page_set->add_metabox( array(
        'id' => 'home_video' ,
        'title' => 'Home Video'
        ));

   $page_set->add_field('home_video', array(
          'type'  => 'text',
          'name'  => 'title',
          'label' => 'Title'
          ));

 $page_set->add_field('home_video', array(
          'type'  => 'text',
          'label' => '<span style="color: silver;">Video ID ( for example: https://www.youtube.com/watch?v=<b style="color: white;">DcHKOC64KnE</b> )</span>',
          'name'  => 'id'
          ));



$box_id = 'tagline';
  $page_set->add_metabox( array(
        'id' => $box_id,
        'title' => 'TagLine Section'
        ));

     $page_set->add_field($box_id, array(
          'type'  => 'text',
          'name'  => 'title',
          'label' => 'Title'
          ));

     $page_set->add_field($box_id, array(
          'type'  => 'rich',
          'name'  => 'text',
          'label' => 'text'
          ));








 ///////////////     APPROACH    //////////////////////////////////////////
$box_id = 'approach';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'Approach Section' ));

  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));

  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'subtitle',
     'label' => 'Sub-Title'
  ));


    $page_set->add_field($box_id, array(
     'type'  => 'number',
     'name'  => 'sections_nr',
     'label' => 'Number of Sections',
     'min'  => 1,
     'max'  => 10
  ));



$sec_nr = 4;

for($a=1; $a<=$sec_nr; $a++)
{
     $page_set->add_field($box_id, array(
     'type'  => 'info',
     'name'  => 'section_'.$a.'_info',
     'info'  => '<br><h2>Section ' . $a . '</h2>'
  ));

    $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'section_'.$a.'_title',
     'label' => 'Title'
  ));

    $page_set->add_field($box_id, array(
     'type'  => 'textarea',
     'name'  => 'section_'.$a.'_text',
     'label' => 'Text',
     'rows' => 5
  ));

  $page_set->add_field($box_id, array(
     'type'  => 'image',
     'name'  => 'section_'.$a.'_image',
     'label' => 'Image'
  ));

    $page_set->add_field($box_id, array(
     'type'  => 'info',
     'name'  => 'section_'.$a.'_info',
     'info'  => ''
  ));

}

//**********************************************/






 ///////////////     SOLUTIONS    //////////////////////////////////////////

$box_id = 'solutions';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'Solutions Section' ));
    
     $page_set->add_field($box_id, array(
          'type'  => 'text',
          'name'  => 'title',
          'label' => 'Title'
          ));


      for( $ai=1;$ai<=4;$ai++)
      {

        $page_set->add_field($box_id, array(
          'type'  => 'image',
          'name'  => 'solution_id_'.$ai,
          'label' => 'Solutuion '.$ai,
          'post_type' => 'solutions'
          ));
      }










 ///////////////     FEATURE    //////////////////////////////////////////



      

$box_id = 'feature-section';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'Feature Section' ));

  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));


  $page_set->add_field($box_id, array(
     'type'  => 'textarea',
     'name'  => 'text',
     'label' => 'Text',
     'rows'  => 4
  ));



 /////////////////////////////////////////////////////////






 ///////////////     Case Study    //////////////////////////////////////////
$box_id = 'featured_study';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'Case Study Section' ));


$page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));

$page_set->add_field($box_id, array(
          'type'  => 'content_select',
          'name'  => 'study_id',
          'label' => 'Select Case Study',
          'post_type' => 'studies'
          ));

$page_set->add_field($box_id, array(
                    'type'  => 'image',
                    'label' => 'Background Image',
                    'name'  => 'image'
                ));









 ///////////////     WHITE PAPER    //////////////////////////////////////////
$box_id = 'white-paper';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'White Paper Section' ));

  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'sub-title',
     'label' => 'Sub Title'
  ));


  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));

  $page_set->add_field($box_id, array(
     'type'  => 'image',
     'name'  => 'image',
     'label' => 'Image',
     ));

   $page_set->add_field($box_id, array(
     'type'  => 'image',
     'name'  => 'bg_image',
     'label' => 'Background Image',
     ));


    $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'button_label',
     'label' => 'Button Label'
  ));

    $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'button_id',
     'label' => 'Button id'
  ));

        $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'button_class',
     'label' => 'Button Class'
  ));














$box_id = 'home-blog';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'Blog News Section' ));

  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));

  $page_set->add_field( $box_id , array(
            'type'       => 'number',
            'name'       => 'posts_number',
            'label'      => 'Number of Posts <br><em>( select and save/update to increase number of posts )</em>',
            'default'    => 1,
            'min'        => 0,
            'max'        => 10
          ));

  $nr_of_posts = 2;

for($s=1; $s <=  $nr_of_posts; $s++)
{
  $page_set->add_field($box_id, array(
          'type'  => 'content_select',
          'name'  => 'post_'. $s,
          'label' => 'Select Post ' . $s,
          'post_type' => 'post'
          ));
}




