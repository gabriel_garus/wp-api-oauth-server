<?php
defined('ABSPATH') or die('Access Denied!');
/*
* 	Single Case Study specyfic metaboxes
*/


    $box_id = 'featured_study';
$page_set->add_metabox( array(
        'id' => $box_id,
        'title' => 'Featured Case Study',
        ));

$page_set->add_field($box_id, array(
                    'type'  => 'text',
                    'label' => 'Title',
                    'name'  => 'title'
                ));
$page_set->add_field($box_id, array(
                    'type'  => 'image',
                    'label' => 'Background Image',
                    'name'  => 'image'
                ));


$page_set->add_field($box_id, array(
          'type'  => 'content_select',
          'name'  => 'study_id',
          'label' => 'Case Study - General',
          'post_type' => 'studies'
          ));

$page_set->add_field($box_id, array(
          'type'  => 'info',
          'name'  => 'other_title',
          'label' => '',
          'info' => 'Select Featured Caser Study for each category: '
          ));


$case_cats = get_terms('industry', array( 'orderby'=>'count' ));

if(!empty($case_cats ))
{
  foreach ($case_cats  as $cat) 
  {
    if($cat->slug == 'other')
    {
      continue;
    }

     $page_set->add_field($box_id, array(
          'type'  => 'text',
          'name'  => 'title_' . $cat->slug,
          'label' => 'Title for - ' . $cat->name
          ));


    $page_set->add_field($box_id, array(
          'type'  => 'content_select',
          'name'  => 'study_id_' . $cat->slug,
          'label' => 'Case Study - ' . $cat->name,
          'post_type' => 'studies'
          ));


  }


}



$box_id = 'studies_per_page';
$page_set->add_metabox( array(
        'id' => $box_id,
        'title' => 'Case Studies Per Page',
        ));


    $page_set->add_field($box_id, array(
     'type'  => 'number',
     'name'  => 'nr',
     'label' => 'Number of Case Studies Articles per Page',
     'min'  => 1,
     'max'  => 100
  ));






