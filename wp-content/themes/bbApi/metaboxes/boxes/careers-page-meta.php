<?php defined('ABSPATH') or die('Access Denied!');
/*
* 	
*/

$box_id = 'jobs_list';
$page_set->add_metabox( array(
        'id' => $box_id,
        'title' => 'Positions List',
        ));

$page_set->add_field($box_id, array(
                    'type'  => 'text',
                    'label' => 'Title',
                    'name'  => 'title'
                ));


$page_set->add_field($box_id, array(
                    'type'  => 'number',
                    'label' => 'Number of Jobs to Show',
                    'name'  => 'nr',
                    'min'  => -1,
                    'max'  => 10000,
                    'default' => 4
                ));




/////////////////////////////////////////////////////////
$box_id = 'middle';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'Middle Section' ));



$page_set->add_field($box_id, array(
                    'type'  => 'text',
                    'label' => 'Title',
                    'name'  => 'title'
                ));


$page_set->add_field($box_id, array(
                    'type'  => 'textarea',
                    'label' => 'Text',
                    'name'  => 'text',
                    'rows'	=> 8.5
                ));

$page_set->add_field($box_id, array(
                    'type'  => 'text',
                    'label' => 'Video Title',
                    'name'  => 'video_title'
                ));
$page_set->add_field($box_id, array(
                    'type'  => 'text',
                    'label' => 'Video Url',
                    'name'  => 'video_url'
                ));




/////////////////////////////////////////////////////////  










/////////////////////////////////////////////////////////
$box_id = 'bottom';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'Bottom Section' ));


$page_set->add_field($box_id, array(
                    'type'  => 'text',
                    'label' => 'Title',
                    'name'  => 'title'
                ));


$page_set->add_field($box_id, array(
                    'type'  => 'rich',
                    'label' => 'Text',
                    'name'  => 'text'
                ));


/////////////////////////////////////////////////////////  




$box_id = 'feature-section';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'Feature Section' ));

  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));


  $page_set->add_field($box_id, array(
     'type'  => 'textarea',
     'name'  => 'text',
     'label' => 'Text',
     'rows'  => 4
  ));



 /////////////////////////////////////////////////////////