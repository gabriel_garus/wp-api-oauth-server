<?php
defined('ABSPATH') or die('Access Denied!');
/*
* 	About Page specyfic metaboxes
*/

$page_set->add_field('headline', array(
            'type'  => 'textarea',
            'label' => 'Intro',
            'name'  => 'intro',
            'rows'  => 8
            ));


$box_id = 'sidebar';
$page_set->add_metabox( array(
        'id' => $box_id,
        'title' => 'The Sidebar',
        ));

$page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'search_title',
     'label' => 'Search Title'
  ));
$page_set->add_field($box_id, array(
     'type'  => 'checkbox',
     'name'  => 'hide_search_title',
     'label' => 'Hide Search Title',
     'value' => 'true'
  ));


  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));

  $page_set->add_field($box_id, array(
     'type'  => 'checkbox',
     'name'  => 'hide_title',
     'label' => 'Hide Title',
     'value' => 'true'
  ));

related_posts_mbox($page_set, array('post_type'=> 'services'));
/*
$box_id = 'related-posts';
$page_set->add_metabox( array(
        'id' => $box_id,
        'title' => 'Sidebar - Related Services',
        'context' => 'side',
        'priority'=>'low'
        ));

   for( $ai=1;$ai<=4;$ai++)
      {

        $page_set->add_field($box_id, array(
          'type'  => 'content_select',
          'name'  => 'service_'.$ai,
          'label' => 'Service '.$ai,

          ));
      }
*/
//delete_post_meta($post_id, $box_id);

related_posts_mbox($page_set, array('nr' => 2));


      $box_id = 'feature-section';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'Feature Section' ));

  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));


  $page_set->add_field($box_id, array(
     'type'  => 'textarea',
     'name'  => 'text',
     'label' => 'Text',
     'rows'  => 4
  ));