<?php
defined('ABSPATH') or die('Access Denied!');
/*
* 	Single Article specyfic metaboxes
*/

$box_id = 'feature-section';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'Feature Section' ));

  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));


  $page_set->add_field($box_id, array(
     'type'  => 'textarea',
     'name'  => 'text',
     'label' => 'Text',
     'rows'  => 4
  ));


  $box_id = 'related-articles';
$page_set->add_metabox( array( 'id' => $box_id,   'title' => 'Related Articles'   ));


for( $ai=1;$ai<=4;$ai++)
  {
    $page_set->add_field($box_id, array(
        'type'  => 'content_select',
        'name'  => 'related_'.$ai,
        'label' => 'Article '.$ai,
        'post_type' => 'post'
        ));
   }
