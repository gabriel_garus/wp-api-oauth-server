<?php
defined('ABSPATH') or die('Access Denied!');
/*
* 	About Page specyfic metaboxes
*/

$box_id = 'middle';
$page_set->add_metabox( array(
        'id' => $box_id,
        'title' => 'Middle Content',
        ));


$page_set->add_field($box_id, array(
                    'type'  => 'text',
                    'label' => 'Title',
                    'name'  => 'title'
                ));

$page_set->add_field($box_id, array(
                    'type'  => 'rich',
                    'label' => '',
                    'name'  => 'text'
                ));


$box_id = 'video';
$page_set->add_metabox( array(
        'id' => $box_id,
        'title' => 'Video',
        ));

$page_set->add_field($box_id, array(
                    'type'  => 'text',
                    'label' => 'Video Url',
                    'name'  => 'link'
                ));

//////////////////////////////////////////////////////////////////////////////////////








$box_id = 'bottom';
$page_set->add_metabox( array(
        'id' => $box_id,
        'title' => 'Bottom Content',
        ));


$page_set->add_field($box_id, array(
                    'type'  => 'text',
                    'label' => 'Title',
                    'name'  => 'title'
                ));

$page_set->add_field($box_id, array(
                    'type'  => 'rich',
                    'label' => '',
                    'name'  => 'text'
                ));

  $page_set->add_field($box_id, array(
  					'type'  => 'cta_btn',
  					'label' => 'Hero Button',
  					'name'  => 'cta'
  			));

//////////////////////////////////////////////////////////////////////////////////////









$box_id = 'team';
$page_set->add_metabox( array(
        'id' => $box_id,
        'title' => 'The Team',
        ));

$page_set->add_field($box_id, array(
                    'type'  => 'text',
                    'label' => 'Intro text',
                    'name'  => 'intro'
                ));


$page_set->add_field($box_id, array(
                    'type'  => 'text',
                    'label' => 'Title',
                    'name'  => 'title'
                ));

for($o = 1; $o <= 4; $o++)
{

$page_set->add_field($box_id, array(
          'type'  => 'content_select',
          'name'  => 'team_'.$o,
          'label' => 'Team Memeber ' . $o,
          'post_type' => 'team'
          ));
}

//////////////////////////////////////////////////////////////////////////////////////










$box_id = 'wide';
$page_set->add_metabox( array(
        'id' => $box_id,
        'title' => 'Full Width Image',
        ));

$page_set->add_field($box_id, array(
        'type'  => 'image',
        'label' => '',
        'name'  => 'image'
                ));

//////////////////////////////////////////////////////////////////////////////////////








$box_id = 'careers';
$page_set->add_metabox( array(
        'id' => $box_id,
        'title' => 'Careers',
        ));

$page_set->add_field($box_id, array(
                    'type'  => 'text',
                    'label' => 'Title',
                    'name'  => 'title'
                ));

for($o = 1; $o <= 2; $o++)
{

$page_set->add_field($box_id, array(
          'type'  => 'content_select',
          'name'  => 'job_'.$o,
          'label' => 'Job ' . $o,
          'post_type' => 'careers'
          ));
}

//////////////////////////////////////////////////////////////////////////////////////








$box_id = 'feature-section';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'Feature Section' ));

  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));


  $page_set->add_field($box_id, array(
     'type'  => 'textarea',
     'name'  => 'text',
     'label' => 'Text',
     'rows'  => 4
  ));