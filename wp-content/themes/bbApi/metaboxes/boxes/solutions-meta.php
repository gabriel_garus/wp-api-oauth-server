<?php
defined('ABSPATH') or die('Access Denied!');
/*
* 	Single Solution specyfic metaboxes
*/


$box_id = 'middle';
$page_set->add_metabox( array(
	'id' => $box_id,
	'title' => 'Middle Section',
	'containers' => array('box box_1', 'box box_2','box box_3')
	));

$page_set->add_field($box_id , array(
		'type' => 'text',
		'name' => 'title',
		'label' => 'Title'
	));

for ($i=1; $i <4 ; $i++)
{ 
	$page_set->add_field($box_id , array(
		'type' => 'text',
		'name' => 'title_'.$i,
		'label' => '',
		'container' => 'box box_'.$i
	));


	$page_set->add_field($box_id , array(
		'type' => 'textarea',
		'name' => 'text_'.$i,
		'label' => '',
		'rows'	=> 6,
		'container' => 'box box_'.$i
	));
}




$box_id = 'wide';

$page_set->add_metabox( array(
	'id' => $box_id,
	'title' => 'Images Section'
	));



$page_set->add_field($box_id , array(
		'type' => 'image',
		'name' => 'image_top',
		'label' => 'Top Image'
	));
$page_set->add_field($box_id , array(
		'type' => 'text',
		'name' => 'title',
		'label' => 'Title'
	));

$page_set->add_field($box_id , array(
		'type' => 'image',
		'name' => 'image_bottom_1',
		'label' => 'Bottom Image 1'
	));

$page_set->add_field($box_id , array(
    'type' => 'image',
    'name' => 'image_bottom_2',
    'label' => 'Bottom Image 2'
  ));


$box_id = 'bottom';

$page_set->add_metabox( array(
	'id' => $box_id,
	'title' => 'Bottom Section',
	));

$page_set->add_field($box_id , array(
		'type' => 'text',
		'name' => 'title',
		'label' => 'Title'
	));

	$page_set->add_field($box_id , array(
		'type' => 'rich',
		'name' => 'last',
		'label' => '',
		'rows'	=> 16,
    'media' => 'true'
	));





$box_id = 'related-services';
$page_set->add_metabox( array(
        'id' => $box_id,
        'title' => 'Sidebar - Related Solutions',
        'context' => 'side',
        'priority'=>'low'
        ));

   for( $ai=1;$ai<=4;$ai++)
      {

        $page_set->add_field($box_id, array(
          'type'  => 'content_select',
          'name'  => 'service_'.$ai,
          'label' => 'Solution '.$ai,
          'post_type' => 'solutions'
          ));
      }












$box_id = 'white-paper';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'White Paper Section' ));

  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'sub-title',
     'label' => 'Sub Title'
  ));


  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));

  $page_set->add_field($box_id, array(
     'type'  => 'image',
     'name'  => 'image',
     'label' => 'Image',
     ));


    $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'button_label',
     'label' => 'Button Label'
  ));

    $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'button_id',
     'label' => 'Button id'
  ));

        $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'button_class',
     'label' => 'Button Class'
  ));



 ///////////////     FEATURE    //////////////////////////////////////////



      

$box_id = 'feature-section';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'Feature Section' ));

  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));


  $page_set->add_field($box_id, array(
     'type'  => 'textarea',
     'name'  => 'text',
     'label' => 'Text',
     'rows'  => 4
  ));



 /////////////////////////////////////////////////////////



$box_id = 'related';
$page_set->add_metabox( array( 'id' => $box_id, 'title' => 'Related Case Study' ));

 $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));


 $page_set->add_field($box_id, array(
          'type'  => 'content_select',
          'name'  => 'id',
          'label' => 'Select Case Study',
          'post_type' => 'studies'
          ));




$box_id = 'sidebar';
$page_set->add_metabox( array(
        'id' => $box_id,
        'title' => 'The Sidebar',
        'context' => 'side',
        'priority' => 'low'
        ));

        

        $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'search_title',
     'label' => 'Search Title'
  ));
$page_set->add_field($box_id, array(
     'type'  => 'checkbox',
     'name'  => 'hide_search_title',
     'label' => 'Hide Search Title',
     'value' => 'true'
  ));


  $page_set->add_field($box_id, array(
     'type'  => 'text',
     'name'  => 'title',
     'label' => 'Title'
  ));

  $page_set->add_field($box_id, array(
     'type'  => 'checkbox',
     'name'  => 'hide_title',
     'label' => 'Hide Title',
     'value' => 'true'
  ));

  $page_set->add_field($box_id, array(
          'type'  => 'checkbox',
          'name'  => 'disable_sidebar',
          'label' => 'Disable Sidebar',
          'value' => 'true'
          ));


//update_post_meta($post_id, 'related-services', array());