<?php
defined('ABSPATH') or die('Access Denied!');

add_action( 'load-post.php', 'bb_metaboxes' );
add_action( 'load-post-new.php', 'bb_metaboxes' );


session_start();



function bb_metaboxes()
{
   
	//bb_remove_content_editor();

	// ---- get post ID ----------------------------------------

	$post_id = isset($_GET['post']) ? $_GET['post'] : (isset($_POST['post_ID']) ? $_POST['post_ID'] : '') ;


	if(!defined('POST_ID'))
		{
			define('POST_ID', $post_id);
		}
	//----------------------------------------------

	$post = get_post( $post_id );

	if(!empty($post_id ))
	{
		$bbPage = new bbPage($post_id);
	}
	

	// ---- get post type ----------------------------------------
	if(!empty($post_id))
	{
		$post_type = get_post_type($post_id);
	}
	elseif(isset($_GET['post_type']) && !empty($_GET['post_type']))
	{
		$post_type = $_GET['post_type'];
	}
	else
	{
		$post_type = 'post';
	}



	// get blog page ID
	$blog_page_id = (defined('BLOG_ID')) ? BLOG_ID : get_option( 'page_for_posts' );

	// get front Page ID
	$front_page_id = (defined('HOME_ID')) ? HOME_ID : get_option( 'page_on_front' );

	define('BOXES', THEME_DIR . 'metaboxes/boxes/');

	//$meta_data = bb_extract( get_post_meta($post_id) );

//--------------------------------------------------------------------------------------------
//  											THE SWITCH
//--------------------------------------------------------------------------------------------



//----------------------------------------------
//  				POSTS
//----------------------------------------------

$page_set = new mBoxSet();


if( $post_type != 'attachment' AND $post_type != 'team')
{

	$page_set->add_metabox( array(
				'id' => 'headline' ,
				'title' => 'Headline',
				'priority' => 'high',
			//	'context' => 'side'
				));
	$page_set->add_field('headline', array(
						'type'  => 'text',
						'label' => '',
						'name'  => 'title'
						));

}

//----------------------------------------------
//  				POSTS
//----------------------------------------------


if($post_type === 'post')
{
	include_once BOXES . 'article-meta.php';
}
elseif($post_type === 'page')
{

		//----------------------------------------------
		//  				PAGES
		//----------------------------------------------
		// ---- get page -> get template name ----------------------------------------
		if(!empty($post_id))
		{
			$template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
		}
		else
		{
			$template_file = 'default';
		}



	  	if($post_id  == $front_page_id)
	  	{
	  		include_once BOXES . 'home-meta.php';	//$blog_page_id
	  	}
	  	elseif($post_id == $blog_page_id)
	  	{
	  		include_once BOXES . 'blog-meta.php';	//$blog_page_id
	  	}
	  	elseif ($template_file ==='templates/about.php')
		{
			include BOXES . 'about-meta.php';
		}
		elseif ($template_file ==='templates/cpt-list.php')
		{
			include BOXES . 'cpt-list-meta.php';
		}
		elseif ($template_file ==='templates/case-studies.php')
		{
			include BOXES . 'case-studies-meta.php';
		}
		elseif ($template_file ==='templates/contact.php')
		{
			include BOXES . 'contact-meta.php';
		}
		elseif ($template_file ==='templates/team.php')
		{
			include BOXES . 'team-page-meta.php';
		}
		elseif ($template_file ==='templates/careers.php')
		{
			include BOXES . 'careers-page-meta.php';
		}
		elseif ($template_file ==='templates/hero-page.php')
		{
			include BOXES . 'hero-page-meta.php';
		}
}
else
{
	$custom_post_types = get_post_types( array( '_builtin' => false ));

	foreach ($custom_post_types as $type) 
	{
		if($post_type == $type)
		{
			$filename = $type .'-meta.php';
			if(is_file(BOXES.$filename))
			{
				include_once BOXES.$filename;
			}

		}
	}
}


	if($post_type === 'post' || $post_type === 'solutions' || $post_type === 'studies' || $post_type === 'services' || $post_type ==='careers')
	{
		leadin_metabox($page_set);
	}

	if( $post_type === 'solutions' )
	{
		$page_set->add_field( 'leadin', array(
					'type'  => 'image',
					'label' => 'Overlay',
					'name'  => 'icon'
				));
	}
	
	  	if($post_id  == $front_page_id)
	  	{

				$page_set->add_metabox( array(
							'id' => 'navigation' ,
							'title' => 'Section Index:',
							'priority' => 'low',
							'context' => 'side'
							));
				$page_set->add_field('navigation', array( 'type'  => 'navi' ) );

		}

	$page_set->activate();
}



require 'mBox_class.php';

require 'meta-helpers.php';

if(!class_exists('bbPage'))
{
include_once INC_DIR . 'bb-page-class.php';
}
//----------------------------------------------
// move Yoast SEO to the bottom
//----------------------------------------------

add_filter( 'wpseo_metabox_prio', function() { return 'low';});

//----------------------------------------


//----------------------------------------------
// Reset metabox positions
//----------------------------------------------
$user_ID = get_current_user_id();
$dontResetMeta = get_user_meta($user_ID, 'noBoxReset');

if(empty($dontResetMeta)) :
update_user_meta( $user_ID, 'meta-box-order_post', '' );
update_user_meta( $user_ID, 'meta-box-order_page', '' );
endif;
//----------------------------------------


// Remove Post Tags And Categories 
function wpse120418_unregister_categories() {
    register_taxonomy( 'category', array() );
    register_taxonomy( 'post_tag', array() );
}
//add_action( 'init', 'wpse120418_unregister_categories' );

//add_action( 'add_meta_boxes', 'hide_categories_metabox' ); 
function hide_categories_metabox() { 
	remove_meta_box('categorydiv','post','side');
	remove_meta_box('tagsdiv-post_tag','post','side');


}