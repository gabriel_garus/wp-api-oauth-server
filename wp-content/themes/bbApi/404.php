<?php
/**
 * 404
 */

get_header();
echo '<section class="error-404 not-found">' . PHP_EOL;
echo '<div class="legal section">' . PHP_EOL;
		echo '<div class="container">' . PHP_EOL;
				echo '<br><br>'; 
						echo '<br><br>'; 
								echo '<br><br>'; 
			echo '<h1>';
				_e( 'Oops! That page can&rsquo;t be located.', 'bigbang' );
			echo '</h1>'.PHP_EOL;
			echo '<div class="result-content">'.PHP_EOL;
			_e( 'It looks like nothing was found at this location. Maybe try a search?', 'bigbang' );
			
			echo '<br><br>'; 
			echo '<div class="search-404">' . PHP_EOL;
			$submit_label = 'Search';
			get_search_form();
			echo '</div>' . PHP_EOL;
					echo '<br><br>'; 
							echo '<br><br>'; 
									echo '<br><br>'; 

			echo '</div>'.PHP_EOL;
		echo '</div>' . PHP_EOL;//.container
	echo '</div>' . PHP_EOL; 
echo '</section><!-- .error-404 -->' . PHP_EOL;

get_footer();
