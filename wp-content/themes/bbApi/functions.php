<?php
defined('ABSPATH') or die('Access Denied!');

/**
 * castle functions and definitions
 *
 * @package bigbang
 */
//if ( ! isset( $content_width ) ) $content_width = 600;

function bb_include($dir)
{
    if(is_file($dir))
    {
        include_once $dir; 
    }
    else
    {
    	error_log($dir .'  - cannot be found. ');
    }
}


include_once 'inc/bb-theme-class.php';

$theme = new bbTheme();


$theme->add_image_size( 'wide',  1900,395 );
$theme->add_image_size( 'container',  980,270 );
$theme->add_image_size( 'service',  445,260 );
$theme->add_image_size( 'solution', 445,215 );
$theme->add_image_size( 'cs',    391,194 );
$theme->add_image_size( 'avatar', 125,125 );
$theme->add_image_size( 'small',  170,104 );
$theme->add_image_size( 'box',    170,170 );
$theme->add_image_size( 'aside', 230,140 );
$theme->add_image_size( 'article',  650,550, false );
$theme->add_image_size( 'logo',  315,115, false );
$theme->add_image_size( 'mini', 120,80, false );

$theme->add_editor_image_size('article', 'Article');


$theme->add_menu('primary', 'Primary Menu');
$theme->add_menu('legal', 'Legal Menu');

$theme->add_style('master-style', 'master.css');
$theme->add_ie8_style('ie8-style', 'ie8.css');


$theme->add_support( 'post_tags');

$theme->add_script( 'YouTubePopUp', '/js/YouTubePopUp.jquery.min.js', 'jquery');
$theme->add_script( 'modernizr', '//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js');
$theme->add_script( 'gabriel', '/js/gabriel.min.js', 'jquery' );
$theme->add_script( 'myWpApi', '/js/my-wp-api.js', 'jquery' );
$theme->add_script( 'navScript', '/js/navscript.min.js', 'jquery' );
$theme->add_script( 'snapscr','//cdnjs.cloudflare.com/ajax/libs/snap.js/1.9.3/snap.min.js');
$theme->add_script( 'angular','//ajax.googleapis.com/ajax/libs/angularjs/1.4.9/angular.min.js');
$theme->add_script( 'ngControllers','/js/angular/controllers.js', 'angular');

$theme->add_script_condition('YouTubePopUp', array('is_front_page'=>true));


$theme->activate();



bb_include( INC_DIR . 'helpers.php') ;

function bb_scripts()
{
	wp_dequeue_script('oba_youtubepopup_plugin');
    wp_dequeue_script('oba_youtubepopup_activate');
    // yt popup
	wp_deregister_style( 'oba_youtubepopup_css');
		
	// Contact Form 7 
	wp_deregister_style('contact-form-7');
		
	//bloom
	wp_deregister_style('et_bloom-css');

	wp_localize_script( 'myWpApi', 'postdata',
	array(
				'json_url' => rest_url( 'wp/v2/')
			));
}
add_action( 'wp_enqueue_scripts', 'bb_scripts' );



add_filter( 'wpcf7_load_js', '__return_false' );


//----------------------------------------------------------------------






//------------------------------


// ***  ADMIN CSS  and SCRIPTS **************/
function bb_admin_style()
{
	
   wp_enqueue_style('bb-admin-style', THEME_URL .'admin-style/admin-style.css');

   $jquery_src = '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js';

   
	//$jquery_src = '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js';
   	wp_register_script( 'jquery-admin', $jquery_src, array(), null );

    wp_register_script( 'js-admin', THEME_URL .'admin-style/admin.js', array('chosen-admin'), null, true );


	/* Chosen */
	$chosen_src = '/js/chosen.jquery.min.js';
	//$chosen_src = '//cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.min.js';
	wp_register_script( 'chosen-admin', $chosen_src, array(), null, true );


	if(!empty($_GET['action']) AND $_GET['action'] == 'edit')
	{
		wp_enqueue_script( 'js-admin' );
	}
	//wp_enqueue_script('jquery-admin' );


}

add_action('admin_enqueue_scripts', 'bb_admin_style');
add_action('login_enqueue_scripts', 'bb_admin_style');

//---------------------------------------------------------




if(!class_exists('bbPage'))
{
	bb_include( INC_DIR . 'bb-page-class.php');
}

if(!class_exists('bbPost'))
{
	bb_include( INC_DIR . 'bb-post-class.php');
}





// **** Custom Post types ***/

$cpt_dir =  THEME_DIR . DIRECTORY_SEPARATOR .'custom-post-types' . DIRECTORY_SEPARATOR;


bb_include( $cpt_dir . 'services.cpt.php');
bb_include( $cpt_dir . 'solutions.cpt.php');
bb_include( $cpt_dir . 'case-studies.cpt.php');
bb_include( $cpt_dir . 'careers.cpt.php');
bb_include( $cpt_dir . 'team.cpt.php');
//--------------------------------------------






include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

if(is_plugin_active('redux-framework/redux-framework.php'))
{
	bb_include( INC_DIR . 'redux-options.php');
}





if( is_admin() )
{
	bb_include( THEME_DIR . 'metaboxes' . DIRECTORY_SEPARATOR . 'switch.php') ;
}

if( !is_admin() )
{
	bb_include( INC_DIR . 'menu-function.php');
	bb_include(  INC_DIR . 'bread.php');
	//bb_include(  INC_DIR . 'shortcodes.php');

	
}

if(!class_exists('bbImage'))
{
	include_once(  INC_DIR . 'bb-image-class.php');
}

bb_include(  INC_DIR . 'walker-menu-class.php');



$options = bb_get_options();


function script_tag_defer($tag, $handle) {
    if (is_admin()){
        return $tag;
    }
    if (strpos($tag, 'jquery')) {
        return $tag;
    }

     if (strpos($tag, 'modernizr')) {
     	return $tag;
     }

     if (strpos($tag, 'polyfiller')) {
     	return $tag;
     }
     
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 9.') !==false) {
    return $tag;
    }


    else {
        return str_replace(' src',' defer src', $tag);
    }
}
add_filter('script_loader_tag', 'script_tag_defer',10,2);


// PHP Memory check







add_action('admin_footer', 'save_cpt');

function save_cpt()
{
		$custom_post_types = get_post_types( array( '_builtin' => false ));
   		
   		$bb_options = get_option('bigbang'); 

   		$types = array();


   		if(!empty($custom_post_types))
   		{
	      	foreach ($custom_post_types as $type) 
	      	{
				if($type != 'wpcf7_contact_form')
				{
					$types[] = $type;
				}
	      	}
      	}

      	$bb_options['custom_post_types'] = $types;
      	update_option( 'bigbang', $bb_options );
}




function wpdocs_special_nav_class( $classes, $item ) 
{
	//var_dump($classes);
	//die();

	foreach ($classes as $key=>$class) 
	{
		if (strpos($class, 'current') === false)
		{
			unset($classes[$key]);
		}
		else
		{
			$classes[$key] = $class;
		}
	}
    return $classes;
}


function my_css_attributes_filter($var) 
{
  return is_array($var) ? array() : '';
}
add_filter( 'nav_menu_css_class' , 'wpdocs_special_nav_class' , 10, 2 );
add_filter('nav_menu_item_id','my_css_attributes_filter' , 10, 2 );


function menu_set_dropdown( $sorted_menu_items, $args ) {
    $last_top = 0;
    foreach ( $sorted_menu_items as $key => $obj ) {
        // it is a top lv item?
        if ( 0 == $obj->menu_item_parent ) {
            // set the key of the parent
            $last_top = $key;
        } else {
            $sorted_menu_items[$last_top]->classes['dropdown'] = 'dropdown';
        }
    }
    return $sorted_menu_items;
}
add_filter( 'wp_nav_menu_objects', 'menu_set_dropdown', 10, 2 );


function bb_rewrites()
{
	
	add_rewrite_rule( 'blog/([^/]*)$','index.php?name=$matches[1]',1 );

}
add_action( 'init', 'bb_rewrites', 0 );

/*

add_filter('rest_url_prefix', 'return_api' );
function return_api()
{
	return 'lilia';
}
*/
///  WP REST API




















