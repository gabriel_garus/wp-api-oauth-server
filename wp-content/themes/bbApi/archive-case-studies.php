<?php
defined('ABSPATH') or die('Access Denied!');
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package bigbang
 */

//$meta_data = bb_extract(get_post_meta(PAGE_ID));
//dump($meta_data);


if( !have_posts() )
{	
	echo '<h1>No Posts</h1>';
}


get_header();


$case_studies_page = get_page_by_title('Case Studies');

$page_id = $case_studies_page->ID;

if(!isset($bbPage))
{
  $bbPage = new bbPage($page_id);
}




include_once PARTIALS_DIR . 'hero.php';





	if ( have_posts() )
	{

			echo '<div class="section blog single-article"  itemtype="http://schema.org/Blog" >' . PHP_EOL;
				echo '<meta itemprop="url" content="'.$url.'">';
				echo '<meta itemprop="name" content="' . COMPANY_NAME . '">';

				echo '<section class="news">' . PHP_EOL;
		  			echo '<div class="container">' . PHP_EOL;
			  			echo '<div class="blog-list">' . PHP_EOL;

							while ( have_posts() )
							{
								the_post();
								require PARTIALS_DIR . 'blog-list-item.php';
							}

						echo '</div>' . PHP_EOL; // .blog list
					echo '</div>' . PHP_EOL; // .container
				echo '</section>' . PHP_EOL; //.news
			echo '</div>' . PHP_EOL; //.section blog single-article



	}
	else
	{
		echo 'No Posts Found';
	}


$page_size = 2;

$all =  wp_count_posts('studies');

$all_posts = $all->publish;

$pages = $all_posts / $page_size;
$pages = ceil($pages);

dump($pages);

//$thePostId = get_option( 'page_for_posts' );
$blogUrl = get_permalink( $page_id );

$uri = $_SERVER['REQUEST_URI'];
$ex = explode('page/', $uri);
$current = (int)rtrim( end($ex), '/');
//echo 'Pages: ' . $pages . '<br>';
$prevLink = (get_previous_posts_link('')) ? get_previous_posts_link('') : '' ;
$nextLink = (get_next_posts_link( '' )) ? get_next_posts_link( '' ) : '';

$l_state = (empty($prevLink)) ? 'disabled' : 'active';
$r_state = (empty($nextLink)) ? 'disabled' : 'active';



if($pages > 1 )
{
	echo '<nav class="pagination">'.PHP_EOL;
	echo '<div class="navi navi-left '.$l_state.'">';
	echo $prevLink;
	echo '</div>'.PHP_EOL;

	for($e=1; $e <= $pages; $e++)
	{

		if($e === $current || ($e === 1) && ($current === 0))
		{
			$state = 'disabled';
			$in = '<span>' . $e . ' </span>';
		}
		else
		{
			$state = 'active';
			$pagi_link = ($e === 1) ? $blogUrl : $blogUrl . 'page/'. $e . '/';
			$in = '<a href="'.$pagi_link.'" >'. $e . '</a> ';
		}

		echo '<div class="nr '.$state.'">'.$in.'</div>' . PHP_EOL;

	}



	echo '<div class="navi navi-right '.$r_state.'">';
	echo $nextLink;
	echo '</div>'.PHP_EOL;
	echo '</nav>';
}


get_footer();
