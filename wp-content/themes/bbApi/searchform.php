<?php
defined('ABSPATH') or die('Bang bang, we shot you down...');

/**
 * @package WordPress
 *
 */


// Need homepage conditional
$post_type = get_post_type();


// $submit_label = (is_404()) ? 'Submit' : '';
if(!isset($submit_label))
{
	$submit_label =  '';
}


echo '<nav id="search"  itemscope itemtype="https://schema.org/WebSite">' . PHP_EOL;

echo '<meta itemprop="url" content="'.WP_HOME.'">' . PHP_EOL;

echo '<form role="search" method="get" class="search-form" action="' . home_url('/') . '" itemprop="potentialAction"';
echo ' itemscope itemtype="http://schema.org/SearchAction">' . PHP_EOL;

echo '<meta itemprop="target" content="'.home_url().'/?s={s}">' . PHP_EOL;
/*
echo '<label for="s" class="screen-reader-text">' . PHP_EOL;
echo __('Search for:', 'label');
echo '</label>' . PHP_EOL;
*/
echo '<input type="hidden" name="post_type" value="'.$post_type .'" />';
echo '<input itemprop="query-input" type="search" placeholder="' . esc_attr_x(' ', 'placeholder');
echo '" value="' . get_search_query() . '" name="s" id="s" ';
echo 'title="' . esc_attr_x('Search for:', 'label') . '" required >';
echo '<input type="submit" id="search-btn" value="">' . PHP_EOL;



echo '</form>' . PHP_EOL;
echo '</nav>' . PHP_EOL;
