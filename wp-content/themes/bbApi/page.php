<?php
defined('ABSPATH') or die('Access Denied!');
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package bigbang
 */

 

get_header();

if(empty($bbPage))
{
	$bbPage = new bbPage(PAGE_ID);
}

echo '<div class="page-default">' . PHP_EOL;

echo '<div class="section">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;
the_breadcrumb();
echo '<h1>' .$bbPage->title . '</h1>' . PHP_EOL;

echo do_shortcode( wpautop( $bbPage->content) );

echo '</div></div></div>' . PHP_EOL;

get_footer();