<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Single
*/
///%category%/%postname%/b


get_header();

$thePostId = PAGE_ID;

$thePost = new bbPost($thePostId);

$title = $thePost->title;

$the_author = $thePost->author;

$url = get_permalink($thePostId);



echo '<div class="blog-article section"  itemscope itemtype="http://schema.org/Blog">'.PHP_EOL;

$theBlogId = get_option( 'page_for_posts' );
$url = get_permalink($theBlogId);
echo '<meta itemprop="url" content="'.$url.'">';
echo '<meta itemprop="name" content="TaxHug">';

echo '<div class="container cf">' . PHP_EOL;
the_breadcrumb();
//**************   LEFT 


echo '<div class="left left-content">'.PHP_EOL;
echo '<article itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">'.PHP_EOL;

$url = get_permalink($thePostId);
echo '<meta itemprop="url" content="'.$url.'">'.PHP_EOL;

//-- Header  -----------------------------
echo '<header>'.PHP_EOL;

//-- TITLE -----------------------------
echo '<h1 itemprop="headline">' . $title . '</h1>' . PHP_EOL;
//----------------------------------------


// -- Article Meta ---------------------
echo '<div class="article-meta">'.PHP_EOL;
echo '<div class="post-author">';
echo '<span class="pre">By </span>';
if(!empty($the_author['nick_name']))
{
	echo '<span class="name">'. $the_author['nick_name'] . PHP_EOL;
}
//echo '<span class="first name">' . $post_author_first_name . '</span>'; 
//echo ' '. '<span class="last name">'. $post_author_last_name . '</span>';
echo '</div>' ;
echo '<div class="article-date">' . PHP_EOL;
echo $thePost->get_date(). PHP_EOL;
echo '</div>' . PHP_EOL;

echo '</div>'.PHP_EOL; // .article-meta
//----------------------------------------

echo '</header>' . PHP_EOL;
//---------------------------------------------



// -- Article Body -----------------------------

echo '<div itemprop="articleBody">'.PHP_EOL;

bb_content($thePost->content);

echo '</div>'.PHP_EOL;

echo '<footer>' . PHP_EOL;

echo '<div class="share">'.PHP_EOL;
echo '<h4> Share this article: </h4>'.PHP_EOL;

require PARTIALS_DIR . 'article-share.php';

echo '</div>' .PHP_EOL;

echo '<hr>';


// ***********  Author **************
$author = $thePost->author;

require PARTIALS_DIR . 'article-author.php';

//--------------------------------------

echo '</footer>' . PHP_EOL;
echo '</article>'.PHP_EOL;
echo '</div>'.PHP_EOL; // .left












// **** Aside ***************
echo '<div class="right">'.PHP_EOL;
echo '<aside class="sidebar">'.PHP_EOL;


echo '<h2>Search the blog</h2>'.PHP_EOL;
get_search_form();

$related = array();

$rel = $thePost->get_section('related-articles');

if($rel)
{
	foreach($rel as $r)
	{
		if($r != 0)
		{
			$related[] = $r;
		}
	}

}




if(empty($nr_of_posts) )
{
	$nr_of_posts = 4;
}

if(empty($related))
{
	$posts = wp_get_recent_posts( array( 'posts_per_page' => $nr_of_posts, 'exclude' => array(PAGE_ID) ));


	foreach ($posts as $post) 
	{	
		$related[] = $post['ID'];
	}
}



if(empty($blog_list_title))
{
	$blog_list_title = 'Recommended';
}



echo '<div class="aside-blog related">'.PHP_EOL;

echo '<h2>' . $blog_list_title . '</h2>'.PHP_EOL;

foreach ($related as $post)
{	


	$blog_post 	= new bbPost( $post );
	$post_date = $blog_post->get_date(array('sup'=> false ));
	$the_author = $blog_post->author;

	echo '<article>'.PHP_EOL;

	 

	echo '<h4><a href="' . get_permalink($post) . '" >';
	echo $blog_post->leadin_title . '</a></h4>'.PHP_EOL;


	// -- Article Meta ---------------------
	echo '<div class="article-meta">'.PHP_EOL;
	echo '<div class="post-author">';
	echo '<span class="pre">By </span>';
	if(!empty($the_author['nick_name']))
	{
		echo '<span class="name">'. $the_author['nick_name'] . PHP_EOL;
	}
	//echo '<span class="first name">' . $post_author_first_name . '</span>'; 
	//echo ' '. '<span class="last name">'. $post_author_last_name . '</span>';
	echo '</div>' ;
	echo '<div class="article-date">' . PHP_EOL;
	echo $blog_post->get_date(). PHP_EOL;
	echo '</div>' . PHP_EOL;

	echo '</div>'.PHP_EOL; // .article-meta
	//----------------------------------------

	if(has_post_thumbnail($post))
	{
		echo get_the_post_thumbnail($post, 'aside');
	}
	elseif(defined('DEF_THUMB'))
	{
		echo DEF_THUMB;
	}





	echo '</article>'.PHP_EOL;

}

echo '</div>'.PHP_EOL;





	

include_once PARTIALS_DIR . 'social-links.php';



$rs_url  = ( !empty($options['rss']) ) ? $options['rss'] : null;
if($rs_url !== null)
{
	echo '<h2>Keep Up To Date</h2>';
	echo '<ul class="social">';
	echo '<li class="rss">';
	echo '<a href="'. $rs_url .'" target="_blank">';
	echo '<img width="45" height="45" src="/img/icons/social/rss.svg" alt="rss icon" />';
	echo '</a></li>';
	echo '</ul>' . PHP_EOL;
}


echo '</aside>'.PHP_EOL;
echo '</div>'.PHP_EOL; // .right

echo '</div>' . PHP_EOL;//.container



echo '</div>'.PHP_EOL;


include_once PARTIALS_DIR .'cta.php';





echo '<script>';

echo 'function windowExplode(url, width, height)';
echo '{';

echo 'var leftPosition, topPosition, specs; ';
echo 'leftPosition = (window.screen.width / 2) - ((width / 2) + 10);';
echo 'topPosition = (window.screen.height / 2) - ((height / 2) + 50);';

$specs  = 'status=no,';
$specs .= 'height=" + height + ",';
$specs .= 'width=" + width + ",';
$specs .= 'resizable=yes,';
$specs .= 'left=" + leftPosition + ",';
$specs .= 'top=" + topPosition + ",';
$specs .= 'screenX=" + leftPosition + ",';
$specs .= 'screenY=" + topPosition + ",';
$specs .= 'toolbar=no,';
$specs .= 'menubar=no,';
$specs .= 'scrollbars=no,';
$specs .= 'location=no,';
$specs .= 'directories=no';

echo 'specs = "'.$specs.'";';
echo 'window.open(url, "_blank", specs)'; 
echo '}';
echo '</script>';

get_footer();



