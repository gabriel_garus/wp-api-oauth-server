<?php
defined('ABSPATH') or die('Access Denied!');
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package bigbang
 */

get_header();
/*
global $wp_scripts;
dump($wp_scripts);
die();*/
if(empty($bbPage))
{
	$bbPage = new bbPage(PAGE_ID);
}




include_once PARTIALS_DIR . 'hero.php';




/**************************************
* Video 
*/

$home_vid_id = $bbPage->get_field('home_video','id');
$home_vid_title = $bbPage->get_field('home_video','title');

if($home_vid_id !== null)
{
  echo '<div class="home-video section">';

  if($home_vid_title !== null)
  {
     echo '<h5>' . $home_vid_title . '</h5>';
  }
 


 $video_shortcode = '[video_popup url="'.$home_vid_id.'" text="x" auto="yes"]';


  echo '<div class="video-button">';
	echo do_shortcode( $video_shortcode );
  echo '</div>';

  echo '</div>'; 
}

unset($home_vid_id); unset($home_vid_title);


//echo $bbPage->field_list;

/*
*
**************************************************/
/*

  echo '<div class="section tagline">' . PHP_EOL;
  echo '<div class="container txt-center">' . PHP_EOL;
  echo '<p>' .WP_HOME. 'wp-json/wp/v2/ </p>';
  echo '<input id="user_query" />' . PHP_EOL;
  echo '<a id="submit_query" class="btn btn tertiary">Check</a>' . PHP_EOL;

  echo '<div class="ajax-loader"><img src="/spinner.svg" width="32" height="32" /></div>';
  echo '<code><pre id="query_result"></pre></code>' . PHP_EOL;
  echo '</div>' . PHP_EOL;
  echo '</div>' . PHP_EOL;

*/

/**************************************
* Section Tagline
*/


$section = 'tagline';
$tagline_title = $bbPage->get_field($section,'title');
$tagline_text  = $bbPage->get_field($section,'text');

if($tagline_title !== null || $tagline_text !== null)
{
  echo '<div class="section tagline">' . PHP_EOL;
  echo '<div class="container txt-center">' . PHP_EOL;

  if($tagline_title !== null)
  {
    echo '<h2>' . $tagline_title . '</h2>';
  }

  if($tagline_text !== null)
  {
    echo wpautop( do_shortcode( $tagline_text  ) );
  }

  echo '</div>' . PHP_EOL;
  echo '</div>' . PHP_EOL;

}

unset($tagline_title); unset($tagline_text);

/*
*
**************************************************/













/**************************************
* Approach Section
*/


$section =  $bbPage->get_section('approach');


echo '<div class="section approach">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;

echo '<div class="a-section cf heading">';
echo '<div class="left">';
echo '</div>' . PHP_EOL;

echo '<div class="right">';
if(!empty($section['title']))
{
  echo '<h2>' . $section['title'] .'</h2>';
}

if(!empty($section['subtitle']))
{
  echo '<h3>' . $section['subtitle'] . '</h3>';
}
echo '</div>'; // .right
echo '</div>' . PHP_EOL;




for($i=1;$i<=$section['sections_nr']; $i++)
{
  $img_id = $section['section_'.$i.'_image'];

 echo '<div class="a-section cf">';
 
  echo '<div class="left">' . PHP_EOL;
    if(!empty( $img_id ))
      {
        $img = new bbImage($img_id, 'aside');
       // dump($img->sizes);
        echo $img->html;
        unset($img);
      }
  echo '</div>' . PHP_EOL;

  echo '<div class="right">' . PHP_EOL;
    if(!empty($section['section_'.$i.'_title']))
    {
      echo '<h3>' . $section['section_'.$i.'_title'] . '</h3>';
    }

    if(!empty($section['section_'.$i.'_text']))
    {
      echo wpautop( do_shortcode( $section['section_'.$i.'_text'] ) );
    }


  echo '</div>' . PHP_EOL;

  echo '</div>' . PHP_EOL;
}



echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL;

unset($section);

/*
*
**************************************************/




/**************************************
* Solutions Section
*/
$section = 'solutions';


$solutions_title = $bbPage->get_field($section,'title');

$solutions = array();
for($c=1; $c<=4;$c++)
{
  $im_id = $bbPage->get_field($section,'solution_id_'.$c);
 if($im_id > 0)
  $solutions[] = $im_id;
}


echo '<div class="section solutions">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;

if($solutions_title !== null)
{
  echo '<h2>' . $solutions_title . '</h2>' ;
}

if(!empty($solutions))
{
  echo '<div class="list">' . PHP_EOL;
  foreach ($solutions as $id) 
  {
    echo '<div class="grid">' . PHP_EOL;
    $img = new bbImage($id, 'mini');
    echo $img->html;
    unset($img);
     echo '</div>';
  }
  echo '</div>' . PHP_EOL;
}




echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL;


unset($solutions_title); unset($solutions);

/*
*
**************************************************/








/**************************************
* List Section
*/
include PARTIALS_DIR . 'promise.php';

/*
*
**************************************************/





/**************************************
* F
*/

include PARTIALS_DIR .'cta.php';

/*
*
**************************************************/

include PARTIALS_DIR . 'cs-featured.php';



/**************************************
*  White Paper Section
* 

*/
include PARTIALS_DIR . 'white-section.php';

/*
*
**************************************************/



$section = $bbPage->get_section('home-blog');
//dump($section);


echo '<div class="section home-blog news-feed">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;
if(!empty($section['title']))
{
  echo '<h2>' . $section['title'] . '</h2>';
}

$recent_posts = wp_get_recent_posts(); 

if($section['posts_number'] != 0)
{
  echo '<div class="list cf">' . PHP_EOL;
  for($q=1;$q<=$section['posts_number'];$q++)
  {
   
    if(!empty($section['post_'.$q]))
    {
      $article_id = $section['post_'.$q];
      
    }
    else
    {
      $article_id = $recent_posts[$q-1]['ID'];
    }

    include PARTIALS_DIR . 'blog-list-item-home.php';
  }
  echo '</div>' . PHP_EOL;


}

echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL;

get_footer();
