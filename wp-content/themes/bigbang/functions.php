<?php
defined('ABSPATH') or die('Access Denied!');

/**
 * castle functions and definitions
 *
 * @package bigbang
 */

defined('ABSPATH') or die('Access Denied!');

define('THEME_DIR', get_template_directory(). DIRECTORY_SEPARATOR );
define('INC_DIR', THEME_DIR . 'inc/');
define('PARTIALS_DIR', THEME_DIR . 'templates' . DIRECTORY_SEPARATOR . 'partials' . DIRECTORY_SEPARATOR);
define('HOST',$_SERVER['SERVER_NAME']);

define('COMPANY_NAME', get_option('blogname'));

define('BLOG_ID', get_option('page_for_posts'));





function my_theme_setup()
{
/** Let WordPress manage the document title. **/
	add_theme_support( 'title-tag' );
/** Enable support for Post Thumbnails on posts and pages. **/
	add_theme_support( 'post-thumbnails' );
	
	// for homepage gallery
	add_image_size( 'pan-crop',  760,360, true );
	add_image_size( 'port-crop', 280,360, true );


	//  projects
	add_image_size( 'hero',      1040,600, false );

	// for expertise pages
	add_image_size( 'hero-wide', 1040,400, true );
	// work/projects
	add_image_size( 'th-wide',   475,320, false );

	//staff photos
	add_image_size( 'avatar',    200,200, true );
	add_image_size( 'avatar-small',    85,85, true );
	// related articles and small quote
	add_image_size( 'th-small',  70,70, true );

	// add_image_size( 'article',  614,325, false );
	

// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'TheNavigation' ),
		'legal' => __( 'Legal Menu', 'LegalMenu' )
	) );

/** Switch default core markup for search form, comment form, and comments to output valid HTML5. */
	add_theme_support( 'html5', array(
		'gallery', 'caption'
	) );

	add_filter( 'wpcf7_load_js', '__return_false' );

}

add_action( 'after_setup_theme', 'my_theme_setup' );

//----------------------------------------------------------------------


/** Enqueue scripts and styles. */
function bb_scripts()
{
	$cdn = true;
	/*
	* wp_enqueue_style( $handle, $src, $deps, $ver, $media );
	* wp_register_script( $handle, $src, $deps, $ver, $in_footer );
	*
	* Don't load contact form 7 scripts (moved to header.php - page dependant)

	*/

		// -- Styles ----


		
		wp_deregister_style( 'language-selector' );

		define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);


		wp_enqueue_style( 'master-style', '/style/master.css', array(), null);// @TODO uncomment this line once classes have been properly named.

		// IE 8 stylesheet
		wp_register_style( 'ie8', '/style/ie8.css', array(), null  );
		$GLOBALS['wp_styles']->add_data( 'ie8', 'conditional', 'lte IE 8' );
		wp_enqueue_style( 'ie8' );


		// -- Scripts -----

		/* Jquery */
		wp_deregister_script('jquery');

		if($cdn)
		{
			$jquery_src = '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js';
		}
		else
		{
			$jquery_src = '/js/jquery-1.11.3.min.js';
		}
		//
		wp_register_script( 'jquery', $jquery_src,  array(), null, true );
		//-------------------------------

		/* Webshim */

		if($cdn)
		{
			$webshim_src  = '//cdnjs.cloudflare.com/ajax/libs/webshim/1.15.10/dev/polyfiller.js';
		}
		else
		{
			$webshim_src = '/js/polyfiller.js';
		}

		wp_register_script( 'webshim', $webshim_src , array( 'jquery' ), null, true );

		//-------------------------------

		/* Modernizr */


		if($cdn)
		{
			$modernizr_src = '//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js';
		}
		else
		{
			$modernizr_src = '/js/modernizr.js';
		}

		wp_register_script( 'modernizr', $modernizr_src , array(), null);

		//-------------------------------

		/*Navigation Scripts*/
		wp_register_script( 'navScript', '/js/scripts.js', array( 'jquery' ), null);


		if($cdn)
		{
			$snap_src = '//cdnjs.cloudflare.com/ajax/libs/snap.js/1.9.3/snap.min.js';
		}
		else
		{
			$snap_src = '/js/snap.min.js';
		}
		wp_register_script( 'snapscr', $snap_src , array(), null, true);

		//-------------------------------


		/* Chosen */

		if($cdn)
		{
			$chosen_src = '//cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.min.js';
		}
		else
		{
			$chosen_src = '/js/chosen.jquery.min.js';
		}
		wp_register_script( 'chosen', $chosen_src, array('jquery'), null, true );


		/* FooTable */

		if($cdn)
		{
			$footable_src = '//cdnjs.cloudflare.com/ajax/libs/jquery-footable/0.1.0/js/footable.min.js';
		}
		else
		{
			$footable_src = '/js/footable.js';
		}
		
		//wp_register_script( 'fooTable', $footable_src  , array('jquery'), null, true );


		/* Contact Form 7 */
		//wp_register_script( 'jQueryForm', '/js/jquery.form.min.js'  , array('jquery'), null, true );
		//wp_register_script( 'CF7_script', '/js/script_cf.js'  , array('jQueryForm'), null, true );

		//*** Enquque Scripts *********************
		wp_enqueue_script( 'modernizr' );
		wp_enqueue_script( 'jquery' );
		//wp_enqueue_script( 'navScript' );
		wp_enqueue_script( 'snapscr' );


		global $wp_query;
		$post_name = (!empty($wp_query->queried_object->post_name)) ? $wp_query->queried_object->post_name : null;

		//$meta_data = bb_extract( get_post_meta(PAGE_ID) );



		// wp_enqueue_script( 'webshim' );
	

		

		

} // end of bb_scripts

add_action( 'wp_enqueue_scripts', 'bb_scripts' );


//------------------------------


// ***  ADMIN CSS  and SCRIPTS **************/
function bb_admin_style()
{
	/*
	global $wp_query;

	$the_vars = $wp_query->query_vars;
*/

	$template_dir_url = get_template_directory_uri();

   wp_enqueue_style('bb-admin-style', $template_dir_url .'/admin-style/admin-style.css');

   	$jquery_src = '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js';
	//$jquery_src = '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js';
   	wp_register_script( 'jquery-admin', $jquery_src, array(), null );

    
   	$jquery_ui_src = '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js';

   	wp_register_script( 'jquery-admin-ui', $jquery_ui_src, array(), null );

	/* Chosen */
	//$chosen_src = '/js/chosen.jquery.min.js';
	$chosen_src = '//cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.min.js';

	wp_register_script( 'chosen-admin', $chosen_src, array('jquery-admin'), null, true );

	wp_register_script( 'js-admin', $template_dir_url .'/admin-style/admin.js', array('chosen-admin', 'jquery-admin-ui'), null, true );

		if(!empty($_GET['action']) AND $_GET['action'] == 'edit')
		{
			wp_enqueue_script( 'js-admin' );
		}

}

add_action('admin_enqueue_scripts', 'bb_admin_style');
add_action('login_enqueue_scripts', 'bb_admin_style');

//---------------------------------------------------------

/*
add_action('admin_footer', 'bb_admin_footer');
function bb_admin_footer()
{
	//echo '<script>' . PHP_EOL;

	//echo '</script>'. PHP_EOL;
}

*/
/*   Allow uploading SVG's  */
function bb_mime_types($mimes)
{
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'bb_mime_types');


//-------------------------------------


/* PDF mime filter
* source: http://code.tutsplus.com/articles/quick-tip-add-extra-media-type-filters-to-the-wordpress-media-manager--wp-25998
*
*/
function modify_post_mime_types( $post_mime_types ) {

    // select the mime type, here: 'application/pdf'
    // then we define an array with the label values

    $post_mime_types['application/pdf'] = array(
    		__( 'PDFs' ),
    		__( 'Manage PDFs' ),
    		_n_noop( 'PDF <span class="count">(%s)</span>', 'PDFs <span class="count">(%s)</span>' 	)
    		);

    $post_mime_types['image/svg+xml'] = array(
    		__( 'SVG' ),
    		__( 'Manage SVGs' ),
    		_n_noop( 'SVG <span class="count">(%s)</span>', 'SVGs <span class="count">(%s)</span>' 	)
    		);

    // then we return the $post_mime_types variable
    return $post_mime_types;

}

// Add Filter Hook
add_filter( 'post_mime_types', 'modify_post_mime_types' );
//--------------------------------------------------------------------

//-- Add content editor to blog page
//   https://wordpress.org/plugins/add-editor-to-page-for-posts/
	function crgeary_fix_no_editor_on_posts_page($post)
	{
		if($post->ID != get_option('page_for_posts') || post_type_supports('page', 'editor'))
		{
			return;
		}

		remove_action('edit_form_after_title', '_wp_posts_page_notice');
		add_post_type_support('page', 'editor');
	}
	add_action('edit_form_after_title', 'crgeary_fix_no_editor_on_posts_page', 0);




// **** Custom Post types ***/

$cpt_dir =  THEME_DIR . DIRECTORY_SEPARATOR .'custom-post-types' . DIRECTORY_SEPARATOR;


include_once $cpt_dir . 'employee.php';
include_once $cpt_dir . 'quotes.php';
include_once $cpt_dir . 'projects.php';

//--------------------------------------------




// Clean up scripts


require_once INC_DIR . 'helpers.php';

require_once INC_DIR . 'menu-function.php';

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

if(is_plugin_active('redux-framework/redux-framework.php'))
{
	require INC_DIR . 'redux-options.php';
}

include_once INC_DIR . 'bb-post-class.php';
include_once  INC_DIR . 'bb-page-class.php';


if( is_admin() )
{
	require_once  THEME_DIR . 'metaboxes' . DIRECTORY_SEPARATOR . 'switch.php';
}


include_once  INC_DIR . 'bread.php';

include_once  INC_DIR . 'shortcodes.php';
$options = bb_get_options();


function script_tag_defer($tag, $handle) {
    if (is_admin()){
        return $tag;
    }
    if (strpos($tag, 'jquery')) {
        return $tag;
    }

     if (strpos($tag, 'modernizr')) {
     	return $tag;
     }
     
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 9.') !==false) {
    return $tag;
    }
    else {
        return str_replace(' src',' defer src', $tag);
    }
}
add_filter('script_loader_tag', 'script_tag_defer',10,2);


// PHP Memory check
/*
add_action('admin_footer', 'my_admin_footer_function');
function my_admin_footer_function()
{

		$memory_limit = return_bytes(ini_get('memory_limit'));
		$memory_usage = memory_get_peak_usage(true);

		$percent = $memory_usage / $memory_limit * 100;
		$percent = round($percent, 2);

		$memory_limit = round($memory_limit /  1000000 ,2);
		$memory_usage  = round($memory_usage /  1000000 ,2);

		echo '<center> PHP memory Usage: <b>' . $percent . '%</b> ('. $memory_usage  . 'MB/' . $memory_limit . 'MB) </center>';
}
*/




