<?php
defined('ABSPATH') or die('Access Denied!');
// Register Custom Post Type

function quotes_post_type()
{


	$labels = array(
		'name'                => _x( 'Quotes', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Quote', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Quotes', 'text_domain' ),
		'name_admin_bar'      => __( 'Quote', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'Quotes', 'text_domain' ),
		'add_new_item'        => __( 'Add New Quote', 'text_domain' ),
		'add_new'             => __( 'Add Quote', 'text_domain' ),
		'new_item'            => __( 'New Quote', 'text_domain' ),
		'edit_item'           => __( 'Edit Quote', 'text_domain' ),
		'update_item'         => __( 'Update Quote', 'text_domain' ),
		'view_item'           => __( 'View Quote', 'text_domain' ),
		'search_items'        => __( 'Search Quote', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'Quote', 'text_domain' ),
		'description'         => __( 'Quotes', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array('title', 'thumbnail', 'editor'),
		'taxonomies'          => array(),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-format-quote',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'capability_type'     => 'page',
		//'rewrite' 			  => array('slug' =>  $page_slug, 'with_front' => false)
	);
	register_post_type( 'quotes', $args );

}

// Hook into the 'init' action
add_action( 'init', 'quotes_post_type', 0 );
