<?php
defined('ABSPATH') or die('Access Denied!');
/*
*
*Register this Custom Post Type file in functions.php
*
*/


function employees_post_type()
{

	$labels = array(
		'name'                => _x( 'employees', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'employee', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Team', 'text_domain' ),
		'name_admin_bar'      => __( 'employee', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'Employees', 'text_domain' ),
		'add_new_item'        => __( 'Add New employee', 'text_domain' ),
		'add_new'             => __( 'Add employee', 'text_domain' ),
		'new_item'            => __( 'New employee', 'text_domain' ),
		'edit_item'           => __( 'Edit employee', 'text_domain' ),
		'update_item'         => __( 'Update employee', 'text_domain' ),
		'view_item'           => __( 'View employee', 'text_domain' ),
		'search_items'        => __( 'Search employee', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'employee', 'text_domain' ),
		'description'         => __( 'employees', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array('title', 'thumbnail', 'editor'),
		'taxonomies'          => array(),
		'hierarchical'        => false,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-groups',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'capability_type'     => 'page',
		//'rewrite' 			  => array('slug' =>  $page_slug, 'with_front' => false)
	);
	register_post_type( 'employees', $args );

}

// Hook into the 'init' action
add_action( 'init', 'employees_post_type', 0 );
