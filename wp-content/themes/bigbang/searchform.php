<?php
defined('ABSPATH') or die('Bang bang, we shot you down...');

/**
 * @package WordPress
 *
 */


// Need homepage conditional

$template_file = get_post_meta(get_the_id(),'_wp_page_template',TRUE);


// $submit_label = (is_404()) ? 'Submit' : '';
if(!isset($submit_label))
{
	$submit_label =  '';
}


echo '<nav id="search"  itemscope itemtype="https://schema.org/WebSite">' . PHP_EOL;

echo '<meta itemprop="url" content="'.WP_HOME.'">' . PHP_EOL;

echo '<form role="search" method="get" class="search-form" action="' . home_url('/') . '" itemprop="potentialAction"';
echo ' itemscope itemtype="http://schema.org/SearchAction">' . PHP_EOL;

echo '<meta itemprop="target" content="'.home_url().'/?s={s}">' . PHP_EOL;
/*
echo '<label for="s" class="screen-reader-text">' . PHP_EOL;
echo __('Search for:', 'label');
echo '</label>' . PHP_EOL;
*/
echo '<input itemprop="query-input" type="search" placeholder="' . esc_attr_x(' ', 'placeholder');
echo '" value="' . get_search_query() . '" name="s" id="s" ';
echo 'title="' . esc_attr_x('Search for:', 'label') . '" required>' . PHP_EOL;

echo '<input type="submit" id="search-btn" value="search">' . PHP_EOL;



echo '</form>' . PHP_EOL;
echo '</nav>' . PHP_EOL;
