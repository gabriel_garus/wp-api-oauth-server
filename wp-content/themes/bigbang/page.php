<?php
defined('ABSPATH') or die('Access Denied!');
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package bigbang
 */

 

get_header();

$meta_data = bb_extract( get_post_meta(PAGE_ID) );

include PARTIALS_DIR . 'hero.php';




get_footer();