<?php
defined('ABSPATH') or die('Access Denied!');

class chc_user
{

private $id = 0;
public $first_name = '';
public $last_name = '';
public $position = '';
public $bio = '';

public $img = '';

public $username = '';
public $email = '';

public $facebook = '';
public $twitter = '';
public $linkedin = '';
public $google = '';

private $user = array();
private $userData =  array();

public function __construct($id)
{
$this->userData = get_userdata( $id );
$this->user = get_user_meta( $id ); // user

$user = $this->user; // user
$data = $this->userData;

$this->id = $id;
$this->first_name = $user['first_name'][0];
$this->last_name = $user['last_name'][0];

$this->email = $data->user_email;
$this->username = $data->user_login;

$this->setPosition();
$this->setBio();


$this->setImg();


$this->setSocials();

}

public function setLorem()
{
	$txt = array('video_description','quote','bio','more');
	foreach ($txt as $th)
	{
		if(empty($this->$th))
		$this->$th = myLorem(4);
	}
}

private function setPosition()
{
	$user = $this->user;

	if(isset($user['position']) AND !empty($user['position'][0]))
		{
			$this->position = $user['position'][0];
		}

	return;
}

private function setBio()
{
	$user = $this->user;
	if(isset($user['description']) AND !empty($user['description'][0]))
		{
			$this->bio = trim($user['description'][0]);
		}

	return;
}















private function setImg()
{
		$user = $this->user;
		if(isset($user['user_image']) AND !empty($user['user_image'][0]))
		{
			$imgg = bb_get_the_image($user['user_image'][0]);
			$this->img = $imgg['url'];
		}
	return;
}

private function setSocials()
{
	$user = $this->user;
	if(isset($user['facebook']) AND !empty($user['facebook'][0]))
	{
		$this->facebook = $user['facebook'][0];
	}

	if(isset($user['twitter']) AND !empty($user['twitter'][0]))
	{
		$this->twitter = $user['twitter'][0];
	}

	if(isset($user['linkedin']) AND !empty($user['linkedin'][0]))
	{
		$this->linkedin = $user['linkedin'][0];
	}

	if(isset($user['googleplus']) AND !empty($user['googleplus'][0]))
	{
		$this->google = $user['googleplus'][0];
	}

	return;


}

}
