<?php
defined('ABSPATH') or die('Access Denied!');


class YT_Video
{

public $embedLink = '';
public $watchLink = '';
public $ID = '';
public $meta = '';

public function __construct($video_link)
{
	$this->set_ID($video_link);
	$this->set_meta();
}

private function set_ID($video_link)
{
	$id = '';

	if (strpos($video_link,'watch') !== false)
	{
		$ex = explode('v=', $video_link);
		$id = end($ex);
	}
	elseif(strpos($video_link,'youtu.be') !== false)
	{
		$ex = explode('.be/', $video_link);
		$id = end($ex);
	}
	elseif(strpos($video_link,'embed') !== false)
	{
		$ex = explode('embed/', $video_link);
		if(strpos($ex[1],'"') !== false)
		{
			$ex2 = explode('"', $ex[1]);
			$id = $ex2[0];
		}
		else
		{
		$id = end($ex);
		}
	}
	else
	{
		$id = null;
	}
	$this->ID = $id;

}


public function get_id()
{
	return $this->ID;
}

public function get_thumbnail($size = 'default')
{
	$thumb = '';
	$id = $this->ID;

	if($id === null)
	return;

	$path = 'http://img.youtube.com/vi/'.$id.'/';

	switch ($size) {
		case 'full':
			$img = '0';
			break;

		case 'hq':
			$img = 'hqdefault';
			break;

		case 'mq' :
			$img = 'mqdefault';
			break;

		case 'small' :
			$img = '1';
			break;

		default:
			$img = 'default';
			break;
	}
	$thumb = $path . $img .'.jpg';

	return $thumb;
}

public function get_watch_url()
{
 $id = $this->ID;

	if($id === null)
	return;

 $watchUrl = '//www.youtube.com/watch?v=';
 $watchUrl .= $id. '&amp;vq=large';
 return  $watchUrl;
}


public function get_embed_url()
{
 $id = $this->ID;

 $embedUrl = 'https://www.youtube.com/embed/';
 $embedUrl .= $id;
 return  $embedUrl;
}


public function get_time()
{

	$vidkey = $this->ID;


	if($vidkey === null)
	return;
	$apikey = 'AIzaSyACEbxu3qpTax9gyd2WKCaSuctTyl8qokI';

	$url = "https://www.googleapis.com/youtube/v3/videos?id=".$vidkey;
	$url .= "&part=contentDetails&key=".$apikey;
	$dur = file_get_contents($url);
	$VidDuration =json_decode($dur, true);

	//dump($VidDuration);
	$theTime = $VidDuration['items'][0]['contentDetails']['duration'];




	return $theTime;

}
public function get_publish_date()
{

	if(!empty($this->meta['items'][0]['snippet']['publishedAt']))
	{
		return $this->meta['items'][0]['snippet']['publishedAt'];
	}
	else
	{
		return null;
	}

}

public function get_description()
{

	if(!empty($this->meta['items'][0]['snippet']['description']))
	{
		return $this->meta['items'][0]['snippet']['description'];
	}
	else
	{
		return null;
	}

}

public function get_name()
{
	if(!empty($this->meta['items'][0]['snippet']['title']))
	{
		return $this->meta['items'][0]['snippet']['title'];
	}
	else
	{
		return null;
	}

}


private function set_meta()
{
	$vidkey = $this->ID;

	$apikey = 'AIzaSyACEbxu3qpTax9gyd2WKCaSuctTyl8qokI';

   	$url = "https://www.googleapis.com/youtube/v3/videos?id=".$vidkey;
	$url .= "&part=snippet&key=".$apikey;

	$dur = file_get_contents($url);
	$VidMeta =json_decode($dur, true);


	$this->meta = $VidMeta;


}




} // class end
