<?php
defined('ABSPATH') or die('Access Denied!');

$section_id = 'contact_form';

$redux11->setSection(
    $opt_name, // This is your opt_name,
   array( // This is your arguments array
        'id'    => $section_id, // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'title' => 'Contact Form',
        'desc' => '',
        'heading' => '',
        'icon' => 'el el-th-list', // http://elusiveicons.com/icons/
        //'subsection' => true, // Enable this as subsection of a previous section
    ));

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'form_fields_number', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'Number of Fields' ,
        'validate'	=> 'numeric'
    ));


$redux11->setField(
     $opt_name , // This is your opt_name,
    	array( // This is your arguments array
        'id'         => 'form_action', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'Form Action' 
    ));


$redux11->setField(
     $opt_name , // This is your opt_name,
    	array( // This is your arguments array
        'id'         => 'form_submit_label', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'Submit Label' 
    ));

$bb_opts = get_option('bigbang');
$num_of_fields = (!empty($bb_opts['form_fields_number'])) ? $bb_opts['form_fields_number'] : 1;

 $redux11->setField(
     $opt_name , // This is your opt_name,
    	array( 
	     'id'   =>'divider_0',
	     'type' => 'divide',
	     'section_id' => $section_id,
	     'desc'    => '<br>'
		));


for ($i=1; $i <= $num_of_fields ; $i++) 
{ 
	$redux11->setField(
     $opt_name , // This is your opt_name,
    	array( // This is your arguments array
        'id'         => 'field_label_'.$i, // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'Field ' . $i . ' label' 
    ));

    $redux11->setField(
     $opt_name , // This is your opt_name,
    	array( // This is your arguments array
        'id'         => 'field_type_'.$i, // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'select',
        'section_id' => $section_id,
        'title'      => 'Field ' . $i . ' Type',
        'options' => array(
        	'text' => 'Text',
        	'email'=> 'Email',
        	'tele' => 'Telephone',
        	'url'  => 'Url',
        	'textarea' => 'Text Area'
        	)
    ));


    $redux11->setField(
     $opt_name , // This is your opt_name,
    	array( // This is your arguments array
        'id'         => 'required_'.$i, // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'checkbox',
        'section_id' => $section_id,
        'title'      => 'Field ' . $i . ' Required',
        'subtitle'	=> 'Check to make field required'
        ));


    $redux11->setField(
     $opt_name , // This is your opt_name,
    	array( // This is your arguments array
        'id'         => 'field_name_'.$i, // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => $section_id,
        'title'      => 'Field ' . $i . ' Name',
         'subtitle'	=> 'unique identifier - small letters and underscores only - no special characters',
         'validate' => 'no_special_chars'
    ));


	$redux11->setField(
	     $opt_name , // This is your opt_name,
	    	array( 
		     'id'   =>'divider_'.$i,
		     'type' => 'divide',
		     'section_id' => $section_id,
		     'desc'    => '<br>'
			));

}

