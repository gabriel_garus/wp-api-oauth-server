<?php
defined('ABSPATH') or die('Access Denied!');
//*********** Contact Details*************

$redux11->setSection(
    $opt_name, // This is your opt_name,
   array( // This is your arguments array
        'id'    => 'contact', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'title' => 'Footer',
        'desc' => 'Contact Details',
        'heading' => '',
        'icon' => 'el el-address-book', // http://elusiveicons.com/icons/
        //'subsection' => true, // Enable this as subsection of a previous section
    )
);

//--------------------------------------------------



//************Contact Section Fields****************



//--------------- CTA -----------------------------------

$redux11->setField(
     $opt_name , // This is your opt_name,
        array( // This is your arguments array
            'id'         => 'cta_title', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
            'type'       => 'text',
            'section_id' => 'contact',
            'title'      => 'CTA Title',
            'validate'   => 'text'
        ));

$redux11->setField(
     $opt_name , // This is your opt_name,
        array( // This is your arguments array
            'id'         => 'cta_text', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
            'type'       => 'textarea',
            'section_id' => 'contact',
            'title'      => 'CTA Text',
            'validate'   => 'text'
        ));


$redux11->setField(
     $opt_name , // This is your opt_name,
        array( // This is your arguments array
            'id'         => 'social_title', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
            'type'       => 'text',
            'section_id' => 'contact',
            'title'      => 'Social Links Title',
            'validate'   => 'text'
        ));


// --Telephone

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'telephone', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'contact',
        'title'      => 'Telephone',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);

//--------------------------------------------------



// --Email

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'email', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'contact',
        'title'      => 'Email',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'email'
    )
);

//--------------------------------------------------


// --Address Line 1

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'addressL1', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'contact',
        'title'      => 'Address Line 1',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);

//--------------------------------------------------

// --Address Line 2

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'addressL2', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'contact',
        'title'      => 'Address Line 2',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);

//--------------------------------------------------

// --Address

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'addressL3', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'contact',
        'title'      => 'Address Line 3',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);

//--------------------------------------------------

// --City

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'city', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'contact',
        'title'      => 'City',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);

//--------------------------------------------------

// --County

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'county', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'contact',
        'title'      => 'County',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);

//--------------------------------------------------

// --Area Code

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'areaCode', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'contact',
        'title'      => 'Post Code',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);

//--------------------------------------------------


// --Country

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'country', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'contact',
        'title'      => 'Country',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);

//--------------------------------------------------

// --Conuntry Code

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'countryCode', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'contact',
        'title'      => 'Country Code',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);




//--------------------------------------------------
//*************** Google maps *****************


// --longitude

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'longitude', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'contact',
        'title'      => 'Map - Longitude',
        'subtitle'   => 'company location map',
        'desc'       => 'Second parameter from google maps',
        'validate'   => 'text'
    )
);

//--------------------------------------------------
// --latitude

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'latitude', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'contact',
        'title'      => 'Map - Latitude',
        'subtitle'   => 'company location map',
        'desc'       => 'First parameter from google maps',
        'validate'   => 'text'
    )
);

//--------------------------------------------------

// --zoom

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'zoom', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'contact',
        'title'      => 'map - zoom',
        'subtitle'   => 'company location map',
        'desc'       => '0-19',
        'validate'   => 'numeric'
    )
);

//--------------------------------------------------
