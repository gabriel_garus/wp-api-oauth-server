<?php
defined('ABSPATH') or die('Access Denied!');


/*
* Redux award
*/

$redux11->setSection(
    $opt_name, // This is your opt_name,
   array( // This is your arguments array
        'id'    => 'awards', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'title' => 'Awards',
        'desc' => 'Awards image',
        'heading' => '',
        'icon' => 'el el-smiley', // http://elusiveicons.com/icons/ @TODO cahnge icon
        //'subsection' => true, // Enable this as subsection of a previous section
    )
);



//----------image upload------------
$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
          'id'       => 'img_upload_1',
          'type'     => 'media',
          'section_id' => 'awards',
          'url'      => false,// set to true for the image url in the database
          'title'    => __('Image Upload - 1', 'bigbang'),
          'desc'     => __('Choose an Image to Upload', 'bigbang'),
          'subtitle' => __('', 'bigbang')

    )
);

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
          'id'       => 'details_1',
          'type'     => 'text',
          'section_id' => 'awards',
          'title'    => __('Awdard Details', 'bigbang'),
          'desc'     => __('The Details of the Award', 'bigbang'),
          'subtitle' => __('', 'bigbang')

    )
);


$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
          'id'       => 'img_upload_2',
          'type'     => 'media',
          'section_id' => 'awards',
          'url'      => false,// set to true for the image url in the database
          'title'    => __('Image Upload - 2', 'bigbang'),
          'desc'     => __('Choose an Image to Upload', 'bigbang'),
          'subtitle' => __('', 'bigbang')

    )
);

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
          'id'       => 'details_2',
          'type'     => 'text',
          'section_id' => 'awards',
          'title'    => __('Awdard Details', 'bigbang'),
          'desc'     => __('The Details of the Award', 'bigbang'),
          'subtitle' => __('', 'bigbang')

    )
);

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
          'id'       => 'img_upload_3',
          'type'     => 'media',
          'section_id' => 'awards',
          'url'      => false,// set to true for the image url in the database
          'title'    => __('Image Upload - 3', 'bigbang'),
          'desc'     => __('Choose an Image to Upload', 'bigbang'),
          'subtitle' => __('', 'bigbang')

    )
);


$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
          'id'       => 'details_3',
          'type'     => 'text',
          'section_id' => 'awards',
          'title'    => __('Awdard Details', 'bigbang'),
          'desc'     => __('The Details of the Award', 'bigbang'),
          'subtitle' => __('', 'bigbang')

    )
);

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
          'id'       => 'img_upload_4',
          'type'     => 'media',
          'section_id' => 'awards',
          'url'      => false,// set to true for the image url in the database
          'title'    => __('Image Upload - 4', 'bigbang'),
          'desc'     => __('Choose an Image to Upload', 'bigbang'),
          'subtitle' => __('', 'bigbang')

    )
);


$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
          'id'       => 'details_4',
          'type'     => 'text',
          'section_id' => 'awards',
          'title'    => __('Awdard Details', 'bigbang'),
          'desc'     => __('The Details of the Award', 'bigbang'),
          'subtitle' => __('', 'bigbang')

    )
);
