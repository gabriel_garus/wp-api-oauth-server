<?php

defined('ABSPATH') or die('Access Denied!');

//*************** Config *****************

$redux11->setSection(
    $opt_name, // This is your opt_name,
   array( // This is your arguments array
        'id'    => 'config', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'title' => 'Config',
        'desc' => '',
        'heading' => '',
        'icon' => 'el el-adjust-alt', // http://elusiveicons.com/icons/
        //'subsection' => true, // Enable this as subsection of a previous section
    )
);

//--------------------------------------------------

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 's3bucket', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'config',
        'title'      => 'S3 Bucket',
        'subtitle'   => 'url to images folder on S3 bucket',
        'desc'       => '',
        'validate'   => 'url'
    )
);


$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'bb_prod_url', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'config',
        'title'      => 'Production URL',
        'subtitle'   => 'Server name ("example.com")',
        'desc'       => ''
       // 'validate'   => 'url'
    )
);

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'gtmId', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'config',
        'title'      => 'Google Tag Manager ID',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);


//--------------------------------------------------

// --Disqus ID

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'disqusId', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'config',
        'title'      => 'Disqus ID',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);


$redux11->setField(
     $opt_name , // This is your opt_name,
        array( // This is your arguments array
        'id'         => 'blog_list_intro_home', 
        'type'       => 'checkbox',
        'section_id' => 'config',
        'title'      => 'Blog List Excerpt (home)'
        ));


$redux11->setField(
     $opt_name , // This is your opt_name,
        array( // This is your arguments array
        'id'         => 'blog_list_intro_blog', 
        'type'       => 'checkbox',
        'section_id' => 'config',
        'title'      => 'Blog List Excerpt (blog)'
        ));