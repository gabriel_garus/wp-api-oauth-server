<?php
defined('ABSPATH') or die('Access Denied!');


/*
* Redux Affiliations
*/

$redux11->setSection(
    $opt_name, // This is your opt_name,
   array( // This is your arguments array
        'id'    => 'clients', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'title' => 'Client Logo',
        'desc' => 'Client Logo images',
        'heading' => '',
        'icon' => 'el el-asl', // http://elusiveicons.com/icons/
        //'subsection' => true, // Enable this as subsection of a previous section
    )
);



//----------image upload------------
$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
          'id'       => 'logo_upload_1',
          'type'     => 'media',
          'section_id' => 'clients',
          'url'      => false,// set to true for the image url in the database
          'title'    => __('Company Logo - 1', 'bigbang'),
          'desc'     => __('Choose an Image to Upload', 'bigbang'),
          'subtitle' => __('', 'bigbang')

    )
);
// --Company URL

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'company_url_1', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'clients',
        'title'      => 'Company URL - 1',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'url'
    )
);

//-----------------------New upload box------------------------




$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
          'id'       => 'logo_upload_2',
          'type'     => 'media',
          'section_id' => 'clients',
          'url'      => false,// set to true for the image url in the database
          'title'    => __('Company Logo - 2', 'bigbang'),
          'desc'     => __('Choose an Image to Upload', 'bigbang'),
          'subtitle' => __('', 'bigbang')

    )
);
// --Company URL

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'company_url_2', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'clients',
        'title'      => 'Company URL - 2',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'url'
    )
);

//-----------------------New upload box------------------------



$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
          'id'       => 'logo_upload_3',
          'type'     => 'media',
          'section_id' => 'clients',
          'url'      => false,// set to true for the image url in the database
          'title'    => __('Company Logo - 3', 'bigbang'),
          'desc'     => __('Choose an Image to Upload', 'bigbang'),
          'subtitle' => __('', 'bigbang')

    )
);
// --Company URL

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'company_url_3', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'clients',
        'title'      => 'Company URL - 3',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'url'
    )
);

//-----------------------New upload box------------------------


$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
          'id'       => 'logo_upload_4',
          'type'     => 'media',
          'section_id' => 'clients',
          'url'      => false,// set to true for the image url in the database
          'title'    => __('Company Logo - 4', 'bigbang'),
          'desc'     => __('Choose an Image to Upload', 'bigbang'),
          'subtitle' => __('', 'bigbang')

    )
);
// --Company URL

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'company_url_4', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'clients',
        'title'      => 'Company URL - 4',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'url'
    )
);
