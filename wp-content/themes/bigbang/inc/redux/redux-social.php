<?php

defined('ABSPATH') or die('Access Denied!');

/****** SOCIAL LINKS **///

$redux11->setSection(
    $opt_name, // This is your opt_name,
   array( // This is your arguments array
        'id'    => 'social', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'title' => 'Social Links',
        'desc' => 'Links to social media accounts .',
        'heading' => '',
        'icon' => 'el el-heart', // http://elusiveicons.com/icons/
        //'subsection' => true, // Enable this as subsection of a previous section
    )
);

// --- Social Section Fields

// -- Linked In
$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'linkedin', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'social',
        'title'      => 'Linkedin',
        'desc'       => 'Linkedin URL',
        'validate'   => 'url'
    )
);

//--------------------------------------------------
// -- Google+
$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'goolge+', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'social',
        'title'      => 'Google+',
        'desc'       => 'Google+ URL',
        'validate'   => 'url'
    )
);

//--------------------------------------------------

// -- Twitter
$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'twitter', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'social',
        'title'      => 'Twitter',
        'desc'       => 'Twitter Username'
    )
);

//--------------------------------------------------

// -- Facebook
$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'facebook', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'social',
        'title'      => 'Facebook',
        'desc'       => 'Facebook Page URL',
        'validate'   => 'url'
    )
);

//--------------------------------------------------

// -- Youtube
$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'youtube', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'social',
        'title'      => 'YouTube',
        'desc'       => 'YouTube URL',
        'validate'   => 'url'
    ));


// -- RSS
$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'rss', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'social',
        'title'      => 'RSS link',
        'desc'       => 'RSS feed link',
        'validate'   => 'text'
    ));

//--------------------------------------------------
