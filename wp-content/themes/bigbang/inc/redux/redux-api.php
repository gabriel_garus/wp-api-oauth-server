<?php
defined('ABSPATH') or die('Access Denied!');
//*************** API *****************

$redux11->setSection(
    $opt_name, // This is your opt_name,
   array( // This is your arguments array
        'id'    => 'api', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'title' => 'API',
        'desc' => 'Enter API Keys',
        'heading' => '',
        'icon' => 'el el-key', // http://elusiveicons.com/icons/
        //'subsection' => true, // Enable this as subsection of a previous section
    )
);

//--------------------------------------------------

// --Re-Captcha Public Key

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 're-captchaPublic', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'api',
        'title'      => 'Re-Captcha Public Key',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);


//--------------------------------------------------

// --Re-Captcha Private Key

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 're-captchaPrivate', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'api',
        'title'      => 'Re-Captcha Private Key',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);

//--------------------------------------------------

// --Mail Chimp Api Key

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'mailChimpApi', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'api',
        'title'      => 'Mail Chimp Api Key',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);


//--------------------------------------------------

// --Mail Chimp List ID

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'mailChimpList', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'api',
        'title'      => 'Mail Chimp List ID',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);

//--------------------------------------------------

// --Mail Chimp Api Key

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'mailChimpAction', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'api',
        'title'      => 'Mail Chimp Form Action',
        'subtitle'   => 'For embed forms',
        'desc'       => '',
        'validate'   => 'text'
    )
);
//--------------------------------

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'mailChimpSecretField', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'api',
        'title'      => 'Mail Chimp Secret Field',
        'subtitle'   => 'For embed forms',
        'desc'       => '',
        'validate'   => 'text'
    )
);
//--------------------------------

// --insightly API Key

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'insightlyApi', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'api',
        'title'      => 'Insightly API Key',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);

//--------------------------------------------------

// -- GTM ID

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'gtmId', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'api',
        'title'      => 'Google Tag Manager ID',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);


//--------------------------------------------------

// --Disqus ID

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'disqusId', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'api',
        'title'      => 'Disqus ID',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);
