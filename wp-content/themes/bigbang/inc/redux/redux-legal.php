<?php
defined('ABSPATH') or die('Access Denied!');
//*********** Legal Details*************

$redux11->setSection(
    $opt_name, // This is your opt_name,
   array( // This is your arguments array
        'id'    => 'legal', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'title' => 'Legal',
        'desc' => 'Legal Details',
        'heading' => '',
        'icon' => 'el el-pencil-alt', // http://elusiveicons.com/icons/
        //'subsection' => true, // Enable this as subsection of a previous section
    )
);

//--------------------------------------------------

//************Legal Section Fields****************

// --Trade Name

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'tradeName', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'legal',
        'title'      => 'Trade Name',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);

//--------------------------------------------------

// --Registration Number

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'registration', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'text',
        'section_id' => 'legal',
        'title'      => 'Company Number',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);

//--------------------------------------------------

// --Disclaimer

$redux11->setField(
     $opt_name , // This is your opt_name,
    array( // This is your arguments array
        'id'         => 'disclaimer', // Unique identifier for your panel. Must be set and must not contain spaces or special characters
        'type'       => 'textarea',
        'section_id' => 'legal',
        'title'      => 'Disclaimer Text',
        'subtitle'   => '',
        'desc'       => '',
        'validate'   => 'text'
    )
);

//--------------------------------------------------
