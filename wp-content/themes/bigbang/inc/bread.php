<?php
defined('ABSPATH') or die('Access Denied!');

function the_breadcrumb()
{
		global $wp_query;

		$query_vars = $wp_query->query_vars;
		//error_log(print_r($wp_query, true));
		$options = bb_get_options();

		$blog_page = ( defined('BLOG_ID') ) ? BLOG_ID : get_option( 'page_for_posts' );
		

  if ( !is_front_page() )
	{
		global $post;

		$title = '';
		$count = 2;
		echo '<div class="breadcrumbs section">' . PHP_EOL;
		echo '<div class="container">' . PHP_EOL;
		echo '<nav id="breadcrumbs">';
		echo '<ul>';
		echo '<li>';
		echo '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
		echo '<a href="' . rtrim(get_option('home'), '/') . '/" itemprop="url">';
		echo '<span itemprop="title">Home</span>';
		echo '</a>';
		echo '</span>';

	$is_tag = is_tag();

	if( is_singular('post') || $is_tag )
	{
	
		

		$page_title =  get_the_title( 	$blog_page );
		$the_post = get_post(	$blog_page );
		$page_slug = $the_post->post_name;

		bread_part('/'.$page_slug.'/', $page_title);
		$count++;

		

	}
   	elseif ( is_single() || is_category() || $is_tag )
	{
		//die('is_single() || is_category() || is_tag() ');
			$categories = get_the_category();
			foreach ($categories as $category)
			{
				$url = WP_HOME . '/'.$category->slug .'/';
			//	error_log('$url: ' . $url);
				bread_part($url , $category->cat_name);
				$count++;
			}
	}
	elseif ( is_page() || is_home())
	{

	 //print_r($post, false);
		//die('is_page');
		if(is_home())
		{
			$post = get_post( $blog_page );
		}

		if ($post->post_parent)
			{
				$ancestors = get_post_ancestors($post->ID);
				//error_log(print_r($ancestors, true));
				$ancestors = array_reverse($ancestors);
				foreach ($ancestors as $ancestor)
				{
					bread_part(get_page_link($ancestor), get_the_title($ancestor) );
					$count++;
				}
			}


	}

	//----------------------------------------




		if ( $is_tag )
		{
			$title =  single_tag_title('Tag: ');
		}
		else if ( is_day() )
		{
			$title = get_the_time('F jS, Y') . ' Archives';
		}
		else if ( is_month() )
		{
			$title = get_the_time('M') . ' Archives';
		}
		else if ( is_year() )
		{
			$title = get_the_time('Y') . ' Archives';
		}
		else if ( is_search() )
		{
			$title = 'Search Results';
		}
		else if ( is_author() )
		{
			$title = 'Page Not Found';
		}
		else if ( is_404() )
		{
			$title = 'Page Not Found';
		}
		else if ( is_author() )
		{
			//global $author;
			//$userdata = get_userdata($author)
			//$title = $userdata->display_name;
		}
		elseif( is_tax() )
		{
			$robot_type_slug = $query_vars['robot_type'];
			$robot_category_list = get_terms( 'robot_type' , array('slug' => $robot_type_slug ) );

			//error_log(print_r($robot_category, true ))	;
			$robot_category = $robot_category_list[0];
			$title = $robot_category->name;

		}
		else
		{
			$title =  get_the_title(PAGE_ID);
		}
		echo '<ul>';
		echo '<li class="current" itemprop="breadcrumb">';
		echo '<span>' . $title . '</span>';
		echo str_repeat('</li></ul>', $count) . PHP_EOL;
		echo '</nav>' . PHP_EOL;
		echo '</div>' . PHP_EOL;
		echo '</div>' . PHP_EOL;
	}

}



function bread_part($href, $label)
{
		echo '<ul>';
		echo '<li>';
		echo '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
		echo '<a href="'.$href.'" itemprop="url" >';
		echo '<span itemprop="title">'.$label.'</span>';
		echo '</a>';
		echo '</span>';
}
