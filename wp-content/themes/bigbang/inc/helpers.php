<?php
defined('ABSPATH') or die('Access Denied!');





function dump($sth, $line=true)
{
    if( is_array($sth) OR is_object($sth) )
    {
        echo '<ul>'.PHP_EOL;
        foreach ($sth as $key=>$value)
        {
            echo '<li>'.PHP_EOL;
            echo '["' . $key . '"] => ';
            if(is_array($value) OR is_object($value))
            {
               echo '<em> array </em>{ <br>'.PHP_EOL;
               dump($value, false ).PHP_EOL;
               echo ' }'.PHP_EOL;
            }
            elseif( is_null($value) )
            {
            echo '<em>null</em>';
            }
            elseif( (strpos($value,'http://') !== false) OR (strpos($value,'https://') !== false ) )
            {
            echo '<a href="'. $value .'" target="_blank">'.$value .'</a>';
            }
            else
            {
            echo var_dump($value);
            }
            echo '</li>'.PHP_EOL;

        }
        echo '</ul>'.PHP_EOL;
    }
   else
   {
     echo '<pre>' . var_dump($sth) . '</pre>';
   }

   if($line) echo'<hr>';

   return;
}


//--------------------


function bb_extract($meta)
{
    if( !is_array($meta) )
        return array('error' => 'Meta is not an array');;

    if( empty($meta['box_index'][0]) )
        return array('error' => 'Please re-save the post.');

    $index = unserialize($meta['box_index'][0]);

    $deserialised = array();
    foreach ($index as $key )
    {
        $string = $meta[$key][0];
        $arra = unserialize($string);
        $deserialised[$key] = $arra;

    }

    return $deserialised;
}


function empty_multi($array)
{
    $empty = array();
    if(is_array($array))
    {
        foreach ($array as $key)
        {
            if(is_array($key))
            {
                if(!empty_multi($key))
                {
                    $empty[] = 'x';
                }
            }
            else
            {
                if(!empty($key))
                {
                     $empty[] = 'x';
                }
            }
        }
    }
    else
    {
        if(!empty($array))
        {
             $empty[] = 'x';
        }
    }


    if(empty($empty))
    {
        return true;
    }
    else
    {
        return false;
    }

}


//----

function bb_change_post_type($old, $new)
{

    $posts = get_posts(array(
            'post_type' => $old,
            'post_per_page' => -1
        ));
    $result = array();
       foreach ($posts as $post )
        {
          $result[] = set_post_type( $post->ID, $new );
        }

    return $result;

}


function return_bytes($val) {
    $val = trim($val);
    $last = strtolower($val[strlen($val)-1]);
    switch($last) {
        // The 'G' modifier is available since PHP 5.1.0
        case 'g':
            $val *= 1024;
        case 'm':
            $val *= 1024;
        case 'k':
            $val *= 1024;
    }
    return $val;
}



function bb_get_page_id()
{

    if(is_home())
    {
        $thePostId = (defined('BLOG_ID')) ? BLOG_ID : get_option( 'page_for_posts' );
    }
    else
    {
        $thePostId = get_the_ID();
    }

    return $thePostId;

}

//-------------------------------------------


function bb_get_body_schema($page_title)
{
    if( is_front_page() )
    {
        $bodySchema = 'Organization';
    }
    elseif($page_title === 'Contact' )
    {
        $bodySchema = 'ContactPage';
    }
    elseif($page_title === 'Company' )
    {
        $bodySchema = 'AboutPage';
    }
    else
    {
        $bodySchema = 'WebPage';
    }

    return $bodySchema;
}


//-------------------------------------------


function bb_get_canonical()
{
    if(is_front_page())
    {
      $canonical = WP_HOME.'/';
    }
    else
    {
      $canonical = get_permalink();
    }

    return $canonical;
}


//-------------------------------------------



function bb_get_the_image($img_id, $opts = array() )
{
    if(empty($img_id))
    {
        return null;
    }
    
    $width = (!empty($opts['width'])) ? $opts['width'] : null;
    $height = (!empty($opts['height'])) ? $opts['height'] : null;
    $class = (!empty($opts['class'])) ? 'class="' . $opts['class'] . '" ' : null;
    $itemprop = (!empty($opts['itemprop']))? ' itemprop="'. $opts['itemprop'] . '" ' : null;
    $size = (!empty($opts['size']))?  $opts['size'] : null;


    $options = bb_get_options();

    $bb_image = array();



    if(!empty($img_id))
    {
        $image_post = get_post($img_id);
        $image_meta = wp_get_attachment_metadata($img_id);
        $the_image = wp_get_attachment_image_src( $img_id, $size );

        //dump($image_post);


        if(!empty($image_post->post_mime_type) AND $image_post->post_mime_type == 'image/svg+xml')
        {
            //die('bombasvg');
            $alt = (!empty($image_post->post_title)) ? $image_post->post_title : null;
            //dump($image_post);
            $img_url = $image_post->guid;
            $e = explode('/', $img_url);
            $img_file = end($e);
                    if(!empty($options['s3bucket']))
                    {
                        $img_src = rtrim($options['s3bucket'], '/') . '/' . $img_file;
                    }
                    else
                    {
                        $img_src = WP_HOME . '/' . UPLOADS . '/' . $img_file;
                    }

            
            $img_html = '<img ';
            $img_html .= $itemprop;
            $img_html .= 'src="'. $img_src  .'" ';
            $img_html .= 'alt="'. $alt . '" ';
            
            if($width !== null)
                {
                    $img_html .= 'width="'.$width .'" ';
                }

            if($height !== null)
                {
                    $img_html .= 'height="'. $height. '" ';
                }
                
            if(!empty($class))
            {
                $img_html .=  $class;
            }   
            

            $img_html .=' />';

            $bb_image['html'] = $img_html;
            $bb_image['mime'] = $image_post->post_mime_type;
            return $bb_image;

        }

        if(empty($image_meta) OR !is_array($image_meta) )
        {

            
            return;
        }



        // ---- get url --------------------------------
        if($size !== null)
        {
            if(!empty($image_meta['sizes'][$size]))
            {
                $file = $image_meta['sizes'][$size]['file'];
            }   
            else
            {
                $file = $image_meta['file'];
            }       
        }
        else
        {
            $file = $image_meta['file'];
        }


        if(!empty($options['s3bucket']))
        {
            $img_url = rtrim($options['s3bucket'], '/') . '/' . $file;
        }
        else
        {
            $img_url = $the_image[0];
        }

        $bb_image['url'] = $img_url;
        //-----------------------------------------
        
        $image_sizes = array();

        foreach ($image_meta['sizes'] as $key => $value) 
        {
            $image_sizes[] = $key;
        }



        



        // --- get Caption --------

        if(!empty($image_meta["image_meta"]['caption']))
        {
            $image_caption = $image_meta["image_meta"]['caption'];
        }
        elseif(!empty($image_post->post_excerpt))
        {
            $image_caption = $image_post->post_excerpt;
        }
        elseif(!empty($image_post->post_content))
        {
            $image_caption = bb_get_excerpt($img_id);
        }
        else
        {
            $image_caption = null;
        }

        $bb_image['caption'] = $image_caption;
        //----------------------------


        // --- get ALT --------

        $alt = get_post_meta( $img_id, '_wp_attachment_image_alt', true );
        $title = get_the_title($img_id);

        $bb_image['alt'] = (!empty($alt))? $alt : $title;

        if(!empty($opts['alt']))
        {
            $bb_image['alt'] = $opts['alt'];
        }

        //----------------------------------------------




        // Sizes

        // - width provided and height provided
        if($width != null AND $height != null)
        {
                $bb_image['width'] = $width;
                $bb_image['height'] = $height;
        } // - width provided
        elseif($width != null )
        {
            $ratio = $width /  $image_meta['width'];
            $height = round($image_meta['height'] * $ratio);
            $bb_image['width'] = $width;
            $bb_image['height'] = $height;
        }// - height provided
        elseif($height != null )
        {
            $ratio = $height /  $image_meta['height'];
            $width = round($image_meta['width'] * $ratio);
            $bb_image['width'] = $width;
            $bb_image['height'] = $height;
        }
        else
        {
            $bb_image['width'] = $the_image[1];
            $bb_image['height'] = $the_image[2];
        }

        if($width != null AND empty($image_meta['width']) )
        {
            $bb_image['width'] = $width;
        }


        if($height != null AND empty($image_meta['height']))
        {
            $bb_image['height'] = $height;
        }



                //----------------------------------------------


        $img_html = '<img ';
        $img_html .= $itemprop;
        $img_html .= 'src="'. $img_url  .'" ';
        $img_html .= 'alt="'.$bb_image['alt'] . '" ';
        $img_html .= 'width="'.$bb_image['width'] .'" ';
        $img_html .= 'height="'. $bb_image['height']. '" ';
        $img_html .= $class;

        $img_html .=' />';


        $bb_image['html'] = $img_html;
    }
    else
    {
        $bb_image = array();
    }

    $bb_image['mime'] = $image_post->post_mime_type;
    $bb_image['meta'] = $image_meta;

    $bb_image['sizes'] = $image_sizes;

    return $bb_image;
}



function bb_get_excerpt($id)
{
    $excerpt = '';
    $post = get_post($id);
    $leadin = get_post_meta($id, 'leadin');
    $content = $post->post_content;
    if(!empty($leadin[0]['text']))
    {
        $excerpt = $leadin[0]['text'];
    }
    elseif(!empty($post->post_excerpt))
    {
        $excerpt = $post->post_excerpt;
    }
    else
        {
            if (strlen($content) > 150)
            {
                $excerpt  = substr($content  , 0, 145) . ' ...';
            }
            else
            {
                $excerpt = $content;
            }

        }


    return $excerpt;
}



function bb_get_page_class($page_id, $page_title )
{
    
    $front_page = get_option( 'page_on_front' );
    if($front_page == $page_id)
    {
        return 'home';
    }
    else
    {
       $blog_page = (defined('BLOG_ID')) ? BLOG_ID : get_option( 'page_for_posts' );
       if( $blog_page == $page_id)
       {
            return 'blog';
       }
       else
       {
           return str_replace(' ', '-', trim($page_title) );
       }
    }
    
}





function get_user_browser()
{
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $ub = '';
    if(preg_match('/MSIE/i',$u_agent))
    {
        $ub = "ie";
    }
    elseif(preg_match('/Firefox/i',$u_agent))
    {
        $ub = "firefox";
    }
     elseif(preg_match('/Chrome/i',$u_agent))
    {
        $ub = "chrome";
    }
    elseif(preg_match('/Safari/i',$u_agent))
    {
        $ub = "safari";
    }
    elseif(preg_match('/Flock/i',$u_agent))
    {
        $ub = "flock";
    }
    elseif(preg_match('/Opera/i',$u_agent))
    {
        $ub = "opera";
    }

    return $ub;
}


function bb_get_options()
{   
    
    if(defined('bb_options'))
    {

        $options = unserialize(bb_options);
    } 
    else
    {
         $options =  get_option('bigbang');
         define('bb_options', serialize($options));

    } 

    return $options; 
}