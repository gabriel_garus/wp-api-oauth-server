<?php
defined('ABSPATH') or die('Access Denied!');
/*
*	
*	author: Gabriel Garus for Big Bang Design
*   gabriel.garus@gmail.com	
*/
class bbPage
{

	public $meta = array();
	public $meta_raw = array();
	public $ID = 0;
	public $post = '';
	public $thumbnail = 0;
	public $image = array();
	public $excerpt = '';
	public $content = '';
	public $title = '';
	public $field_list = array();
	public $thumbnail_size = 'thumbnail';
	public $wp_image = null;
	public $thumb_class = null;

	//----//----//----//----//----//----//----//----//----//----//----//


	public function __construct($id)
	{	
		$this->set_the_id($id);

		$this->set_the_meta();

		$this->post = get_post($id);

		$this->excerpt = $this->get_excerpt();

		$this->content = $this->post->post_content;

		$this->title = $this->get_title();

		$this->field_list = $this->get_field_list();

		$this->set_the_thumbnail();
	
		$this->wp_image = $this->get_wp_image($this->thumbnail);

		$this->image = $this->get_image( $this->thumbnail, array('size' => $this->thumbnail_size) );
	}
	//----//----//----//----//----//----//----//----//----//----//----//



	public function set_the_id($id)
	{
		$this->ID = $id;
	}
	//----//----//----//----//----//----//----//----//----//----//----//


	public function get_wp_image($id, $size = 'thumbnail')
	{
		
		$class = $this->thumb_class;

		if(empty($class ))
		{
			$class = $size;
		}
		return wp_get_attachment_image($id, $size, 0, array('class' => $class));
	}
	//----//----//----//----//----//----//----//----//----//----//----//




	public function set_the_thumbnail($th_id = null)
	{	
		if($th_id == null)
		{
			$th_id = get_post_thumbnail_id( $this->ID );
		}

		if(empty($th_id) OR $th_id == 0 )
		{
			$this->thumbnail = null;
		}
		else
		{
			$this->thumbnail = $th_id;
		}
		
	}
	//----//----//----//----//----//----//----//----//----//----//----//


	public function set_thumbnail_class($class = null)
	{	
		$this->thumb_class = $class;
		$this->image = $this->get_image( $this->thumbnail, array('size' => $this->thumbnail_size) );
		$this->wp_image = $this->get_wp_image( $this->thumbnail, $this->thumbnail_size );

	}

	private function set_the_meta()
	{
		$this->meta_raw = get_post_meta($this->ID);
		$this->meta = $this->get_meta();
		
	}
	//----//----//----//----//----//----//----//----//----//----//----//



	public function set_thumbnail_size($size)
	{
		if(!is_array($size))
		{
			$this->thumbnail_size = $size;
			$this->image = $this->get_image( $this->thumbnail, array('size' => $size) );
			$this->wp_image = $this->get_wp_image( $this->thumbnail,  $size );

		}
		elseif(!empty($size['width']) AND !empty($size['height']))
		{
			$this->image = $this->get_image( $this->thumbnail, $size );
		}
	
	}
	//----//----//----//----//----//----//----//----//----//----//----//
	public function set_image_size($size)
	{
		$this->set_thumbnail_size($size);
	}
	//----//----//----//----//----//----//----//----//----//----//----//



	private function get_title()
	{

		$meta = $this->meta;
		$post = $this->post;
		if(!empty($meta['headline']['title']))
		{
			$title = $meta['headline']['title'];
		}
		else
		{
			$title = $post->post_title;
		}

		return $title;

	}
	//----//----//----//----//----//----//----//----//----//----//----//



	public function get_meta($id = null)
	{
		if($id === null)
		{
			$raw_meta = $this->meta_raw;
			$the_meta = $this->extract($raw_meta);
		}
		else
		{
			$raw_meta = get_post_meta($id);
			$the_meta = bbPage::extract($raw_meta);
		}

		unset($raw_meta);
		return $the_meta;
	}
	//----//----//----//----//----//----//----//----//----//----//----//



	public function get_field_list()
	{
		$meta = $this->meta;
		$echo = '';

		$echo .= '<div style="text-align: left; padding: 20px; max-width: 300px; border: dotted 1px khaki; margin: 10px auto;">';
		$echo .= '<h3> Fields list for page ' . $this->post->post_title . ' (id:' . $this->ID .') </h3>';

		$echo .= '<ul>';

		foreach ($meta as $key => $value) 
		{
			$type = gettype($value);
			$echo .= '<li>';
			$echo .= $key;
			if(is_array($value))
			{
				$echo .= '<ul style="padding-left: 15px;">';
				
				foreach($value as $k=>$v)
				{	
					$type = gettype($v);
					$echo .= '<li>';
					$echo .= $k . ' <em style="font-size: 12px; color: #999;">('.$type.')</em>';
					$echo .= '</li>';
				}

				$echo .= '</ul>';
			}
			else
			{
				$echo .= ' ('.$type.')';
			}

			$echo .= '</li>';
		}

		$echo .= '</ul>';

		$echo .= '</div>';

		return $echo;
	}


	public function get_excerpt($id = null, $limit = 150)
	{
		$excerpt = '';

		if($id !== null)
		{
	
			$post = get_post($id);
			$excerpt_raw = $post->post_excerpt;
			$post_meta = get_post_meta( $id );
			$content = $post->post_content;
		}
		else
		{
			$post = $this->post;
			$excerpt_raw = $post->post_excerpt;
			$content = $post->post_content;
			$post_meta = $this->meta_raw;
		}

			$leadin = (!empty($post_meta['leadin'][0]['text'])) ? $post_meta['leadin'][0]['text'] : '';
			
			if(!empty($leadin))
			{
				$excerpt = $leadin;
			}
			elseif(!empty($excerpt_raw))
			{
				$excerpt = $excerpt_raw;
			}
			elseif (strlen($content) > $limit)
			{
				$excerpt  = substr($content  , 0, $limit-5) . ' ...';
			}
			else
			{
				$excerpt = $content;
			}
		
		return $excerpt;
	}
	//----//----//----//----//----//----//----//----//----//----//----//



	private static function extract($meta)
	{
		if( !is_array($meta) )
        return array('error' => 'Meta is not an array');;

    	if( empty($meta['box_index'][0]) )
        return array('error' => 'Please re-save the post.');

	    $index = unserialize($meta['box_index'][0]);

	    $deserialised = array();

	    foreach ($index as $key )
	    {
	        $string = $meta[$key][0];
	        $arra = unserialize($string);
	        $deserialised[$key] = $arra;

	    }

    	return $deserialised;
	}
	//----//----//----//----//----//----//----//----//----//----//----//



	public function get_field($id, $name)
	{	
		$meta = $this->meta;

		if(empty($meta[$id][$name]))
		{
			$value = null;
		}
		else
		{
			$value = $meta[$id][$name];
		}

		return $value;
	}
	//----//----//----//----//----//----//----//----//----//----//----//



	public function get_image($img_id, $opts = array())
	{
		if(empty($img_id))
		{
			$img_id = $this->thumbnail;
		}
		
		$width = (!empty($opts['width'])) ? $opts['width'] : null;
		$height = (!empty($opts['height'])) ? $opts['height'] : null;
		$class = (!empty($opts['class'])) ? 'class="' . $opts['class'] . '" ' : null;
		$itemprop = (!empty($opts['itemprop']))? ' itemprop="'. $opts['itemprop'] . '" ' : null;
		$size = (!empty($opts['size']))?  $opts['size'] : null;


		$options = bb_get_options();

		$bb_image = array();


		$image_post = get_post($img_id);
		

		if(!empty($options['s3bucket']))
		{
			$images_folder = rtrim($options['s3bucket'], '/') . '/';
		}
		elseif(!empty($image_post->guid))
		{
			$e = explode('/', $image_post->guid);
			$img_file = end($e);
			$images_folder = rtrim($image_post->guid, $img_file);
		}
		else
		{
			$images_folder = WP_HOME . '/' . UPLOADS . '/';
		}

		
		// --- SVG - no size check necessary
		if(!empty($image_post->post_mime_type) AND $image_post->post_mime_type == 'image/svg+xml')
		{
			$alt = (!empty($image_post->post_title)) ? $image_post->post_title : null;
				
			$e = explode('/', $image_post->guid);
			$img_file = end($e);

			$img_src = $images_folder . $img_file;
			
				
   			$img_html = '<img ';
			$img_html .= $itemprop;
			$img_html .= 'src="'. $img_src  .'" ';
			$img_html .= 'alt="'. $alt . '" ';
				
			if($width !== null)
			{
				$img_html .= 'width="'.$width .'" ';
			}

			if($height !== null)
			{
				$img_html .= 'height="'. $height. '" ';
			}
					
			if(!empty($class))
			{
				$img_html .=  $class;
			}	
				

			$img_html .=' />';

			$bb_image['html'] = $img_html;
			$bb_image['mime'] = $image_post->post_mime_type;
			return $bb_image;
		}
		// --- endof SVG
		
		$image_meta = wp_get_attachment_metadata($img_id);
		$the_image = wp_get_attachment_image_src( $img_id, $size );
		
		if(empty($image_meta) OR !is_array($image_meta) )
		{
			return false;
		}

		// ---- filename (size dependant) --------------------------------
		if($size !== null AND !empty($image_meta['sizes'][$size]))
		{
			$file = $image_meta['sizes'][$size]['file'];
		}
		else
		{
			$file = $image_meta['file'];
		}

	
		// --- get url
		if(!empty($options['s3bucket']))
		{
			$img_url = rtrim($options['s3bucket'], '/') . '/' . $file;
		}
		else
		{
			$img_url = $the_image[0];
		}

		$bb_image['url'] = $img_url;
		//-----------------------------------------
			


		$image_sizes = array();

		if(!empty($image_meta['sizes'] ))
		{
			foreach ($image_meta['sizes'] as $key => $value) 
			{
				$image_sizes[$key] = $value['width'] . ' x ' . $value['height'] ;
			}
		}

		$bb_image['sizes'] = $image_sizes;

		//--------------------------------------------



		// --- get Caption --------

		if(!empty($image_meta["image_meta"]['caption']))
		{
			$image_caption = $image_meta["image_meta"]['caption'];
		}
		elseif(!empty($image_post->post_excerpt))
		{
			$image_caption = $image_post->post_excerpt;
		}
		else
		{
			$image_caption = null;
		}

		$bb_image['caption'] = $image_caption;
		//----------------------------


		// --- get ALT --------

		$alt = get_post_meta( $img_id, '_wp_attachment_image_alt', true );
		$title = get_the_title($img_id);

		$bb_image['alt'] = (!empty($alt))? $alt : $title;

		if(!empty($opts['alt']))
		{
			$bb_image['alt'] = $opts['alt'];
		}

		//----------------------------------------------




		// Sizes - html width and height

		// - width provided and height provided
		if($width != null AND $height != null)
		{
			$bb_image['width'] = $width;
			$bb_image['height'] = $height;
		} // - width provided
		elseif($width != null )
		{
			$ratio = $width /  $image_meta['width'];
			$height = round($image_meta['height'] * $ratio);
			$bb_image['width'] = $width;
			$bb_image['height'] = $height;
		}// - height provided
		elseif($height != null )
		{
			$ratio = $height /  $image_meta['height'];
			$width = round($image_meta['width'] * $ratio);
			$bb_image['width'] = $width;
			$bb_image['height'] = $height;
		}
		else
		{
			$bb_image['width'] = $the_image[1];
			$bb_image['height'] = $the_image[2];
		}

		if($width != null AND empty($image_meta['width']) )
		{
			$bb_image['width'] = $width;
		}

		if($height != null AND empty($image_meta['height']))
		{
			$bb_image['height'] = $height;
		}


		//----------------------------------------------

		//------- Image HTML ---------------------------------------
		$img_html = '<img ';
		$img_html .= $itemprop;
		$img_html .= 'src="'. $img_url  .'" ';
		$img_html .= 'alt="'.$bb_image['alt'] . '" ';
		$img_html .= 'width="'.$bb_image['width'] .'" ';
		$img_html .= 'height="'. $bb_image['height']. '" ';
		$img_html .= $class;
		$img_html .=' />';
		//----------------------------------------------

		$bb_image['html'] = $img_html;


		$bb_image['mime'] = $image_post->post_mime_type;
		$bb_image['meta'] = $image_meta;




		return $bb_image;
	}

}
	//----//----//----//----//----//----//----//----//----//----//----//
	//----//----//----//----//----//----//----//----//----//----//----//
	//----//----//----//----//----//----//----//----//----//----//----//
	//----//----//----//----//----//----//----//----//----//----//----//
	//----//----//----//----//----//----//----//----//----//----//----//
	//----//----//----//----//----//----//----//----//----//----//----//
	//----//----//----//----//----//----//----//----//----//----//----//
