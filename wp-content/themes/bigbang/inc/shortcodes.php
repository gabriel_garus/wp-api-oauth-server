<?php
/*
*   Shortcodes 
*
*/

add_shortcode( 'main-image' , 'main_post_image' );

function main_post_image( $atts )
{
    global $post;

    $imaginarium = '';
    
    $thePost = new bb_post($post->ID);



    if(isset($atts['size']))
    {
    	$thePost->set_thumbnail_size( $atts['size'] );
    }
    else
    {
    	$thePost->set_thumbnail_size('hero-wide');
    }

     $img = $thePost->image;


    $imaginarium .= '<div class="image">';


    if(isset($atts['link']) AND $atts['link'] == true AND !empty($img['url']))
    {
    	 $imaginarium .= '<a href="'. $img['url'] . '" rel="nofollow" target="_blank">';
    }

    if($img['mime'] == 'image/svg+xml')
    {
        $thePost->set_thumbnail_size(array('width'=>617, 'height'=>316));
        $imaginarium .= $img['html'];
    }
    else
    {
        $imaginarium .= $thePost->wp_image;
    }

    if(isset($atts['link']) AND $atts['link'] == true AND !empty($img['url']))
   	{
   		$imaginarium .= '</a>';
   	}

    if(!empty($img['caption']))
    {
        $imaginarium .= '<p class="caption">' . $img['caption'] . '</p>';
    }

    $imaginarium .= '</div>';

    return $imaginarium;
}
//---------------------------------------------
//---------------------------------------------






add_shortcode('movie', 'post_video');

function post_video($atts)
{
    require_once INC_DIR . 'yt_video.php';

    $echo = '';

    if(!empty($atts['url']))
    {
        $video_url = $atts['url']; 
       
      // wp_enqueue_script( 'webshim' );
      
      /*
       if(!defined('VIDEO_CONTENT'))
        {
            define('VIDEO_CONTENT',true);
        }
    */

        $companyName = '';
        $companySite = WP_HOME;

        // Assign Raw link from DB

        $videoLink = $video_url;

        $V = new YT_Video($videoLink);

        if( empty($videoPoster) )
        {

            $videoPoster = ( !empty($video_poster) ) ? $video_poster : null;
        }


        if($V->ID !== null)
        {
            $videoName      = 'Name';
            $embedUrl       =  $V->get_embed_url();
            $videoSrc       = $V->get_watch_url();
            $contentUrl     = $V->get_watch_url();
            $thumbnailUrl = $V->get_thumbnail('mq');
            $videoPoster    = (empty($videoPoster)) ? ($V->get_thumbnail('hq')) : $videoPoster;
            $time               = $V->get_time();
            $publish_date = $V->get_publish_date();
            $description = $V->get_description();
            $video_name = $V->get_name();


            $description = str_replace('"', "'", $description);
            $video_name = str_replace('"', "'", $video_name);
            if(!is_front_page())
            {
                $vidItemProp = 'itemprop="video"';
            }

            else{
                $vidItemProp = null;
            }

            $echo .= '<div id="video" class="video" '. $vidItemProp;
            $echo .= ' itemscope itemtype="http://schema.org/VideoObject" >'.PHP_EOL;
            
            $echo .= '<div itemprop="publisher" itemscope itemtype="http://schema.org/Organization">';
            $echo .= '<meta itemprop="name" content="'. $companyName .'">'.PHP_EOL;
            $echo .= '<link itemprop="url" href="'.$companySite.'">'.PHP_EOL;
            $echo .= '</div>'.PHP_EOL;

            $echo .= '<div class="wrap">'.PHP_EOL;
            $echo .= '<div class="chc-video">'.PHP_EOL;
            $echo .= '<meta itemprop="thumbnailUrl" content="'.$thumbnailUrl.'" />'.PHP_EOL;
            $echo .= '<meta itemprop="contentUrl" content="' . $contentUrl . '" />'.PHP_EOL;
            $echo .= '<meta itemprop="embedURL" content="'. $embedUrl . '" />'.PHP_EOL;
            $echo .= '<meta itemprop="duration" content="'. $time . '" />'.PHP_EOL;
            $echo .= '<meta itemprop="description" content="'. $description . '" />'.PHP_EOL;
            $echo .= '<meta itemprop="name" content="'. $video_name . '" />'.PHP_EOL;
            $echo .= '<meta itemprop="uploadDate" content="' . $publish_date . '"  />' . PHP_EOL;
        //    $echo .= '<div class="mediaplayer ratio-16-9" >'.PHP_EOL;
            $echo .= '<iframe ';
            $echo .= 'width="512" height="360" ';
            $echo .=  'src="' . $embedUrl . '" ';
            $echo .= 'frameborder="0" allowfullscreen></iframe>';
            /*
            $echo .= '<video ';
            $echo .= 'poster="'.$videoPoster.'" ';
            $echo .= 'height="360" width="640" ';
            $echo .= 'style="height: 100%;" ';
            $echo .= 'controls preload ';
            $echo .= 'src="' . $videoSrc . '" type="video/youtube" ';
            $echo .= '>'.PHP_EOL;
            //$echo .= '<source type="video/mp4" src="' . $videoSrc . '" /> ' .PHP_EOL;
  			//$echo .= '<source type="video/ogg" src="' . $videoSrc . '">' . PHP_EOL;
  			//$echo .= '<source type="video/webm" src="' . $videoSrc . '" >' . PHP_EOL;
  			$echo .= '<source type="video/youtube" src="' . $videoSrc . '" >' . PHP_EOL;
            $echo .='</video>'.PHP_EOL . '</div>'.PHP_EOL;
            */
            $echo .='</div></div>'.PHP_EOL;
            $echo .='</div>'.PHP_EOL;

        }
    }

    return $echo; 
}
//---------------------------------------------
//---------------------------------------------




/*

add_shortcode('quote', 'post_quote');

function post_quote($atts, $content = null) 
{
	return '<div class="quote">' . $content . '</div>';
}

*/