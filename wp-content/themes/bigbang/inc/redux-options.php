<?php
defined('ABSPATH') or die('Access Denied!');

  $opt_name = 'bigbang';

$redux11 = new Redux();
$redux11->setArgs(
    $opt_name, // This is your opt_name,
   array( // This is your arguments array
       'display_name' => 'Bigbang Options',
       'display_version' => '1.0',
       'menu_title' => 'Big Bang',
       'admin_bar' => 'true',
       'page_slug' => 'bigbang_options_page', // Must be one word, no special characters, no spaces
       'menu_type' => 'menu', // Menu or submenu
       'allow_sub_menu' => true,
       'dev_mode'       => true

   )
);


//----------------------------------------------------------------------------

include_once INC_DIR . 'redux' . DIRECTORY_SEPARATOR . 'redux-social.php';
include_once INC_DIR . 'redux' . DIRECTORY_SEPARATOR . 'redux-contact.php';
include_once INC_DIR . 'redux' . DIRECTORY_SEPARATOR . 'redux-legal.php';
//include_once INC_DIR . 'redux' . DIRECTORY_SEPARATOR . 'redux-api.php';
include_once INC_DIR . 'redux' . DIRECTORY_SEPARATOR . 'redux-config.php';

// include_once INC_DIR . 'redux' . DIRECTORY_SEPARATOR . 'redux-contact-form.php';
