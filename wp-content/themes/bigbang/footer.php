<?php
defined('ABSPATH') or die('Access Denied!');

$options  = bb_get_options();


$title    = ( !empty($options['title']) ) ? $options['title'] : null;
$cta_title = ( !empty($options['cta_title']) ) ? $options['cta_title'] : null;
$cta_text = ( !empty($options['cta_text']) ) ? $options['cta_text'] : null;

$social_title =  ( !empty($options['social_title']) ) ? $options['social_title'] : null;

echo '</div>' . PHP_EOL; //.page-content


echo '<footer class="page-footer">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;


//------------- Footer CTA --------------------------

echo '<div class="footer-cta cf">' . PHP_EOL;

echo '<div class="left">' . PHP_EOL;



if($cta_title !== null)
{
	echo '<h4>'. $cta_title. '</h4>' . PHP_EOL;
}


if($cta_text !== null)
{
	echo '<p>'. $cta_text. '</p>' . PHP_EOL;
}

echo '</div>' . PHP_EOL; //.left
//----------

echo '<div class="right">' . PHP_EOL;

echo '<div class="footer-tele">';
if( !empty($options['telephone']))
{
	include PARTIALS_DIR . 'telephone-link.php';
}
echo '</div>' . PHP_EOL; //.footer-tele

echo '<div class="footer-email">';
if( !empty($options['email']))
{
		include PARTIALS_DIR . 'email-link.php';
}
echo '</div>'; //.footer-email

echo '</div>'; //.right
//-----------------------


echo '</div>'; //.footer-cta
//---------------------------------------

$logo_file = 'big-bang-logo';
//---------------------------------------
echo '<div class="mid cf">';


//---- LOGO ----
echo '<div class="footer-logo">';
echo '<a href="' . esc_url(home_url('/')) . '" rel="home">';
echo '<img alt="' . WP_HOME .' Logo" src="' . WP_HOME . '/img/logos/' . $logo_file .'.svg" ';
echo ' width="204" height="84">';
echo '</a>';
echo '</div>'; //.footer-logo
//-----------------

//------ Address ----------
echo '<div class="footer-address">';
if( !empty($options['addressL1']) OR !empty($options['addressL2']) OR !empty($options['addressL3']) )
{
	include PARTIALS_DIR . 'footer-address.php';
}
echo '</div>';//.footer-address
//---------------------




//--------- IIA --------
echo '<div class="footer-iia">';
echo '<a href="http://www.iia.ie/" rel="nofollow" target="_blank">';
echo '<img src="/img/icons/iia-footer.svg" alt="Irish Internet Association" width="68" height="82" />';
echo '</a>' ;
echo '</div>';
//---------------------


//--------------- Social -------------------------
echo '<div class="footer-social">';

if($social_title !== null)
{
	echo'<p class="social-title">' . $social_title . '</p>' ;
}

include PARTIALS_DIR . 'social-links.php';
echo '</div>'; //closes footer-social
//-----------------


echo '</div>' . PHP_EOL; // .mid
//---------------------------------------



//---------------------------------------
echo '<div class="bot cf">' . PHP_EOL;

echo '<div class="legal-info">'.PHP_EOL;
include PARTIALS_DIR . 'footer-legal.php';
echo '</div>';// .left


echo '<div class="legal-links">'.PHP_EOL;
bb_menu('legal');
echo '</div>';//closes div leftbot

echo '</div>';//closes div bottom

//echo '<div class="right"></div>'. PHP_EOL;



echo '<div class="back-to-top">';
echo '<a href="#head">Back to Top</a>'; 
echo '</div>'. PHP_EOL;



echo '</div>'; // .container
echo '</footer>' . PHP_EOL;


wp_footer();

include PARTIALS_DIR . 'footer-scripts.php';

echo '</div>' . PHP_EOL; // .snap-content

echo '</body>' . PHP_EOL;
echo '</html>' . PHP_EOL;


global $wp_scripts;
//echo var_dump($wp_scripts);
