<?php
defined('ABSPATH') or die('Access Denied!');
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package bigbang
 */

//$meta_data = bb_extract(get_post_meta(PAGE_ID));
//dump($meta_data);
if( !have_posts() )
{	
	header('Location: /');
}


get_header();


$theBlogId	= get_option( 'page_for_posts' );
$url 		= get_permalink($theBlogId);


echo '<div class="hero blog">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;
the_archive_title( '<h1 class="page-title">', '</h1>' );
the_archive_description( '<div class="taxonomy-description">', '</div>' );
echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL; //.container




	if ( have_posts() )
	{

			echo '<div class="section blog single-article"  itemtype="http://schema.org/Blog" >' . PHP_EOL;
				echo '<meta itemprop="url" content="'.$url.'">';
				echo '<meta itemprop="name" content="' . COMPANY_NAME . '">';

				echo '<section class="news">' . PHP_EOL;
		  			echo '<div class="container">' . PHP_EOL;
			  			echo '<div class="blog-list">' . PHP_EOL;

							while ( have_posts() )
							{
								the_post();
								require PARTIALS_DIR . 'blog-list-item.php';
							}

						echo '</div>' . PHP_EOL; // .blog list
					echo '</div>' . PHP_EOL; // .container
				echo '</section>' . PHP_EOL; //.news
			echo '</div>' . PHP_EOL; //.section blog single-article



	}
	else
	{
		echo 'No Posts Found';
	}



get_footer();
