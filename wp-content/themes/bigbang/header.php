<?php
defined('ABSPATH') or die('Access Denied!');
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package bigbang
 */

define( 'WP_USE_THEMES', '666' );
$browser = get_user_browser();

$page_id = bb_get_page_id();
define('PAGE_ID', $page_id);

$page_title 	= get_the_title( $page_id );

$body_schema 	= bb_get_body_schema($page_title);

//error_log('WPAPI Contstants: '. print_r( @get_defined_constants(), true ));

echo '<!DOCTYPE html>' . PHP_EOL;
echo '<html lang="en-IE" class="no-js">' . PHP_EOL;

echo '<head>' . PHP_EOL;

echo '<meta charset="utf-8" />' . PHP_EOL;

echo '<meta name="application-name" content="' . COMPANY_NAME . '" />' . PHP_EOL;
echo '<meta name="msapplication-tap-highlight" content="no" />' . PHP_EOL;
echo '<meta name="mobile-web-app-capable" content="yes" />' . PHP_EOL;
echo '<meta name="format-detection" content="telephone=no" />' . PHP_EOL;

echo '<link rel="dns-prefetch" href="//ajax.googleapis.com">' . PHP_EOL;
echo '<link rel="dns-prefetch" href="//dnjs.cloudflare.com">' . PHP_EOL;
echo '<link rel="dns-prefetch" href="//fonts.googleapis.com">' . PHP_EOL;
echo '<link rel="dns-prefetch" href="//dev.bigbang.ie.s3.amazonaws.com">' . PHP_EOL;
echo '<link rel="dns-prefetch" href="//s3-eu-west-1.amazonaws.com">' . PHP_EOL;


//echo '<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Ubuntu:regular,bold&subset=Latin"> ' . PHP_EOL;
echo '<link href="https://fonts.googleapis.com/css?family=Ubuntu:400,300,700,400italic,300italic" rel="stylesheet" type="text/css">' . PHP_EOL;

echo '<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, user-scalable=no" />' . PHP_EOL;

//echo '<link rel="canonical" href="' . $canonical .'" />'.PHP_EOL;
wp_head();



//include_once PARTIALS_DIR . 'schema-json.php';

echo '</head>' . PHP_EOL;

flush();

echo '<body itemscope itemtype="https://schema.org/' . $body_schema . '" class="' . $browser .'" >'.PHP_EOL;

//include_once PARTIALS_DIR . 'tag-manager.php';


include_once PARTIALS_DIR . 'navbar.php';

echo '<div id="page" class="snap-content">' . PHP_EOL;

include_once PARTIALS_DIR . 'logo.php';


if($body_schema == 'WebPage' OR $body_schema == 'Contactpage' OR $body_schema == 'AboutPage')
{
	$content_itemprop = ' itemprop="mainContentOfPage"';
}
else
{
	$content_itemprop = null;
}

$page_class = bb_get_page_class(PAGE_ID, $page_title);
echo '<div class="page-content '.$page_class.'"'. $content_itemprop . '>' . PHP_EOL;


the_breadcrumb();



