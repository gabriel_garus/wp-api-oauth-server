<?php
defined('ABSPATH') or die('Access Denied!');
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package bigbang
 */

get_header();

$bbPage = new bbPage(PAGE_ID);


$options  = bb_get_options();



include_once PARTIALS_DIR . 'hero-home.php';



$quote_id = $bbPage->get_field('quote_select', 'quote_id'); 

$news_heading = $bbPage->get_field('blog-list', 'title');

if($quote_id !== null)
{
	$quote_image = true;
	include PARTIALS_DIR . 'quote-big.php';
}

include_once PARTIALS_DIR . 'list-section.php';
include_once PARTIALS_DIR . 'services.php';
include_once PARTIALS_DIR . 'dual-home.php';

include_once PARTIALS_DIR . 'cta-section.php';
include_once PARTIALS_DIR . 'awards.php';
include_once PARTIALS_DIR . 'gallery-home.php';

  echo '<section class="news">' . PHP_EOL;
  echo '<div class="container">' . PHP_EOL;
    if($news_heading !== null)
    {
        echo '<div class="news-heading">' . PHP_EOL;
        echo '<h3 class="caps red">' . $news_heading  .'</h3>' . PHP_EOL;
        echo '</div>' . PHP_EOL;
    }

  //echo var_dump($options['blog_list_intro_home']);

  if(empty($options['blog_list_intro_home']) OR $options['blog_list_intro_home'] == '0')
  {
    $blog_list_intro = false;
  }
  else
  {
    $blog_list_intro = true; 
  }

	include_once PARTIALS_DIR . 'blog-list.php';

	echo '</div></section>' . PHP_EOL;

wp_reset_postdata();

get_footer();
