<?php

$page_set->add_metabox( array(
					'id' 	   => 'related' ,
					'title'    => 'Related Articles',
					'priority' => 'high'
					));

$page_set->add_field(
	'related', array(
		'type' => 'text',
		'label' => 'Title',
		'name' => 'heading'
	)
);

$page_set->add_field(
	'related', array(
		'type' => 'number',
		'label' => 'Number of related articles',
		'name' => 'nr'
	)
);

			$related_selects = get_post_meta( $post_id, 'related' );
			$related_selects_nr = (!empty($related_selects[0]['nr'])) ? $related_selects[0]['nr'] : 1;



			for ($i=1; $i <= $related_selects_nr  ; $i++)
			{
				$page_set->add_field(
				'related', array(
				'type' => 'content_select',
				'label' => 'Related Article',
				'post_type' => 'post',
				'name' => 'article_'.$i
				)
				);
			}

			//-------------------------------


/*
				$page_set->add_metabox( array(
						'id' => 'newsletter' ,
						'title' => 'Newsletter ',
						'context' => 'side',
						'priority' => 'low'
						));

				$page_set->add_field('newsletter', array(
								'type'  => 'text',
								'label' => 'Heading',
								'name'  => 'head'
								));


			$page_set->add_field('newsletter', array(
								'type'  => 'textarea',
								'label' => 'Intro',
								'name'  => 'text',
								'rows'	=> '6'
								));

					$page_set->add_field('newsletter', array(
								'type'  => 'text',
								'label' => 'Button Label',
								'name'  => 'label'
								));
					*/
                //---------------------------------------------

                $page_set->add_metabox( array(
                						'id' => 'follow' ,
                						'title' => 'Follow Us ',
                						'context' => 'side',
                						'priority' => 'low'
                						));

                			$page_set->add_field('follow', array(
                								'type'  => 'text',
                								'label' => 'Heading',
                								'name'  => 'head'
                								));

                			$page_set->add_field('follow', array(
                								'type'  => 'textarea',
                								'label' => 'Intro',
                								'name'  => 'text',
                								'rows'	=> '6'
                								));
