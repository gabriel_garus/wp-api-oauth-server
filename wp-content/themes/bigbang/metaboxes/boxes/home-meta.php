<?php
defined('ABSPATH') or die('Access Denied!');

$page_set->add_field('headline', array(
          'type'  => 'text',
          'label' => 'Subtitle',
          'name'  => 'hero-subtitle'
          )
        );

//---------------------------------------------------------------

  $page_set->add_metabox( array(
  			'id' => 'cta_hero' ,
  			'title' => 'CTA Button - Hero Section',
  			'priority' => 'high',
  		//	'context' => 'side'
  			));
  $page_set->add_field('cta_hero', array(
  					'type'  => 'cta_btn',
  					'label' => '',
  					'name'  => 'cta_hero'
  			));







//------------ Clients ----------------------------------

      $page_set->add_metabox( array(
      'id' => 'clients' ,
      'title' => 'Clients',
      'priority' => 'high',
      'label'=> 'Clients'
      ));

       $page_set->add_field('clients', array(
          'type' => 'text',
          'name' => 'title',
          'label' => 'Title'
          ));

        $page_set->add_field('clients', array(
          'type' => 'info',
          'info' => '<br />',
          ));

      for($a=1; $a <=4; $a++)
      {
        
         $page_set->add_field('clients', array(
          'type' => 'image',
          'label' => 'Client Logo',
          'name' => 'logo-'.$a
          ));

        $page_set->add_field('clients', array(
          'type' => 'info',
          'info' => '<br />'
          ));

      }
//----------------------------------------------



//----------------- Quote ----------------------------------------------


         $page_set->add_metabox( array(
            'id' => 'quote_select' ,
            'title' => 'Big Quote Select',
            'priority' => 'high',
            ));

          $page_set->add_field(
            'quote_select', array(
            'type' => 'content_select',
            'label' => '',
            'name' => 'quote_id',
            'post_type' => 'quotes'
          ));


//---------------------------




//---------------------------List Section -------

$box_id = 'list_section';
$page_set ->add_metabox( array(
  'id' => 'list_section' ,
  'title' => 'List Section',
  'priority' => 'high'
  ));


    $page_set->add_field(
       $box_id , array(
        'type' => 'text',
        'label' => 'Headline',
        'name' => 'title'
        ));

   

    $page_set->add_field(
       $box_id , array(
        'type' => 'number',
        'label' => 'Number of Sections',
        'name' => 'number'
        ));


      $list_sections = get_post_meta( $post_id, 'list_section');
      $switch_count = (!empty($list_sections[0]['number'])) ? $list_sections[0]['number'] : 0;

          $page_set->add_field( $box_id, array(
              'type' => 'info',
              'info' => '<br />',
              ));

    for ($i=1; $i <= $switch_count; $i++)
      {
         list_section_metabox($page_set, $box_id, $i);
        
         $page_set->add_field( $box_id, array(
              'type' => 'info',
              'info' => '<br />',
              ));
      }


//---------------------------
    





 
//--------------- Services -------------------------------
$box_id = 'services';

 $page_set ->add_metabox( array(
            'id' => $box_id ,
            'title' => 'Services',
            'priority' => 'high'
            ));


 $page_set->add_field(
       $box_id , array(
        'type' => 'text',
        'label' => 'Title',
        'name' => 'title'
        ));

 $page_set->add_field(
       $box_id , array(
        'type' => 'textarea',
        'label' => 'Intro',
        'name' => 'intro'
        ));


$page_set->add_field(
       $box_id , array(
        'type' => 'number',
        'label' => 'Number of Services',
        'name' => 'number',
        'max'=> 20,
        'default' => 6
        ));

   $page_set->add_field( $box_id, array(
              'type' => 'info',
              'info' => '<br />',
              ));

  $services_section = get_post_meta( $post_id, $box_id);
  $switch_count = (!empty($services_section[0]['number'])) ? $services_section[0]['number'] : 0;
  for ($i=1; $i <= $switch_count; $i++)
      {
       
        $page_set->add_field(
           $box_id , array(
            'type' => 'text',
            'label' => 'Service ' . $i . ' - Title',
            'name' => 'title_'.$i
            ));

          $page_set->add_field(
           $box_id , array(
            'type' => 'text',
            'label' => 'Service ' . $i . ' - Subtitle',
            'name' => 'subtitle_'.$i
            ));


        
         $page_set->add_field( $box_id, array(
              'type' => 'info',
              'info' => '<br />',
              ));
      }
//----------------------------------------------














//-------- Dual Section -------------------

$box_id = 'dual_section';
$page_set->add_metabox( array(
      'id' => $box_id ,
      'priority' => 'high',
      'title'=> 'Dual Section'
     ));

$page_set->add_field(
         $box_id, array(
            'type' => 'text',
            'label' => 'Heading',
            'name' => 'title'
          ));

$page_set->add_field(
          $box_id, array(
            'type' => 'rich',
            'label' => 'Paragraph',
            'name' => 'text',
            'rows' => 8
          ));

//-------------------------------------







// ---- CTA Section 
$box_id = 'cta_home';
 $page_set->add_metabox( array(
      'id' => $box_id ,
      'priority' => 'high',
      'title'=> 'CTA section',
      'containers' => array('left','right')
      ));

 $page_set->add_field(
       $box_id , array(
        'type' => 'text',
        'label' => 'Title',
        'name' => 'title',
        'container' => 'left'
        ));

 $page_set->add_field(
       $box_id , array(
        'type' => 'textarea',
        'label' => 'Intro',
        'name' => 'text',
        'rows' => 6,
        'container' => 'left'
        ));


$page_set->add_field($box_id, array(
      'type' => 'cta_btn',
      'label' => 'CTA Link',
      'container' => 'right'
      ));
      //----
//------------ Awards ----------------------------------

 $page_set->add_metabox( array(
      'id' => 'awards' ,
      'priority' => 'high',
      'title'=> 'Awards',
      'containers' => array('left','right')
      ));

for($x=1; $x<=4; $x++)
{
   if($x === 2 OR $x === 4)
        {
          $cont = 'right';
        }
        else
        {
          $cont = 'left';
        }

       $page_set->add_field('awards', array(
          'type' => 'image',
          'name' => 'award_image_' . $x,
          'label' => 'Award ' . $x . ' Image',
           'container' => $cont
          ));

         $page_set->add_field('awards', array(
          'type' => 'text',
          'name' => 'award_company_' . $x,
          'label' => 'Award ' . $x . ' Company',
           'container' => $cont
          ));

          $page_set->add_field('awards', array(
          'type' => 'text',
          'name' => 'award_title_' . $x,
          'label' => 'Award ' . $x . ' Category Title',
           'container' => $cont
          ));

          $page_set->add_field('awards', array(
          'type' => 'text',
          'name' => 'award_name_' . $x,
          'label' => 'Award ' . $x . ' Name',
           'container' => $cont
          ));

          $page_set->add_field('awards', array(
          'type' => 'text',
          'name' => 'award_position_' . $x,
          'label' => 'Award ' . $x . ' Position',
           'container' => $cont
          ));
}



//----------------------------------------------










// ---- Gallery -----


        $page_set->add_metabox( array(
        'id' => 'gallery_home' ,
        'title' => 'Gallery - Mini',
        'priority' => 'high',
        'containers'=> array('left', 'right')
        ));

      $page_set->add_field(
          'gallery_home', array(
            'type' => 'image',
            'label' => '',
            'name' => 'image_small',
            'container'=> 'left'

          ));

       $page_set->add_field(
          'gallery_home', array(
            'type' => 'image',
            'label' => '',
            'name' => 'image_big',
            'container'=> 'right'

          ));

// ------------------------------      


//---------------- Blog List -----------------------------------------------    

$page_set ->add_metabox( array(
  'id' => 'bloglist' ,
  'title' => 'Blog Posts',
  'priority' => 'high'
  ));

$page_set->add_field(
  'bloglist', array(
    'type' => 'text',
    'label' => 'Heading',
    'name' => 'title'
    ));

$page_set->add_field(
  'bloglist', array(
    'type' => 'number',
    'label' => 'Number of Posts',
    'name' => 'number'
    ));



