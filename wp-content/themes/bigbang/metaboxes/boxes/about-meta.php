<?php

defined('ABSPATH') or die('Access Denied!');


$box_id = 'middle_content';

$page_set->add_metabox( array(
      'id'      => $box_id ,
      'title'    => 'Middle Content',
      'priority' => 'high'
      ));

$page_set->add_field($box_id, array(
          'type'  => 'text',
          'label' => 'Title',
          'name'  => 'title'
          ));

$page_set->add_field($box_id, array(
          'type'  => 'rich',
          'label' => '',
          'name'  => 'text',
          'rows'  => 8
          ));


  $page_set->add_field($box_id, array(
            'type'  => 'cta_btn',
            'label' => '',
            'name'  => 'cta_hero'
        ));


$box_id = 'include_team';

$page_set->add_metabox( array(
      'id'      => $box_id ,
      'title'    => 'Team Section ',
      'priority' => 'high'
      ));

$page_set->add_field($box_id, array(
          'type'  => 'checkbox',
          'label' => 'Include team Section',
          'name'  => 'switch',
          'value' => 'on'
          ));