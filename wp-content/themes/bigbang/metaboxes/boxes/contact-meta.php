<?php
defined('ABSPATH') or die('Access Denied!');
/*
* 	Contact Page Meta
*/

$box_id = 'contact_intro';

$page_set->add_metabox( array(
			'id' 	   => $box_id ,
			'title'    => 'Contact Intro ',
			'priority' => 'high'
			));

$page_set->add_field($box_id, array(
					'type'  => 'text',
					'label' => 'Title',
					'name'  => 'title'
					));

$box_id = 'social_intro';

$page_set->add_metabox( array(
			'id' 	   => $box_id ,
			'title'    => 'Social Intro ',
			'priority' => 'high'
			));

$page_set->add_field($box_id, array(
					'type'  => 'text',
					'label' => 'Title',
					'name'  => 'title'
					));

$page_set->add_field($box_id, array(
					'type'  => 'textarea',
					'label' => 'Intro',
					'name'  => 'text',
					'rows'	=> 5
					));

$box_id = 'map_intro';

$page_set->add_metabox( array(
			'id' 	   => $box_id ,
			'title'    => 'Map Intro ',
			'priority' => 'high'
			));

$page_set->add_field($box_id, array(
					'type'  => 'text',
					'label' => 'Title',
					'name'  => 'title'
					));

$box_id = 'lead_in';
$page_set->add_metabox( array(
      'id'      => $box_id ,
      'title'    => 'Leadin Text ',
      'priority' => 'high'
      ));

$page_set->add_field($box_id, array(
          'type'  => 'text',
          'label' => 'label',
          'name'  => 'title'
          ));
