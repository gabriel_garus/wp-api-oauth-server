<?php

$box_id = 'www';

$page_set->add_metabox( array(
			'id' 	   => $box_id ,
			'title'    => 'Website',
			'priority' => 'high'
			));

$page_set->add_field($box_id, array(
					'type'  => 'text',
					'label' => 'URL',
					'name'  => 'url'
					));

$page_set->add_field($box_id, array(
					'type'  => 'checkbox',
					'label' => 'add rel="nofollow"',
					'value' => 'nofollow',
					'name'  => 'rel'
					));