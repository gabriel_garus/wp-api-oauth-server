<?php
/*
*Single expertise Meta
*
*/
defined('ABSPATH') or die('Access Denied!');
//===================Top Right Quote=======================


$box_id = 'middle_text';

$page_set->add_metabox( array(
      'id'      => $box_id ,
      'title'    => 'Middle Content ',
      'priority' => 'high'
      ));

$page_set->add_field($box_id, array(
          'type'  => 'rich',
          'label' => 'Text',
          'name'  => 'text',
          'rows'  => 20
          ));



$box_id = 'qoute_select';

$page_set->add_metabox( array(
          'id'       => $box_id  ,
          'title'    => 'Quote Select',
          'priority' => 'high',
          ));

$page_set->add_field($box_id , array(
          'type'      => 'content_select',
          'label'     => '',
          'name'      => 'quote_id',
          'post_type' => 'quotes'
          ));

$box_id = 'lead_in';
$page_set->add_metabox( array(
      'id'      => $box_id ,
      'title'    => 'Leadin Text ',
      'priority' => 'high'
      ));

$page_set->add_field($box_id, array(
          'type'  => 'text',
          'label' => 'label',
          'name'  => 'title'
          ));



$children = get_posts( array(
    'post_type' => 'page',
    'post_parent' => $post->post_parent,
    'posts_per_page' => -1,
    'post_status' => 'publish'
  ));







$box_id = 'next_article';

$page_set->add_metabox( array(
      'id'      => $box_id ,
      'title'    => 'Next Article',
      'priority' => 'high',
      'containers' => array('left', 'right')
      ));

$page_set->add_field($box_id, array(
          'type'  => 'text',
          'label' => 'Small Heading',
          'name'  => 'head',
          'container' => 'left'
          ));
$options = array();
$options['auto'] = 'Auto';


foreach ($children as $child)
{
  $options[$child->ID] = $child->post_title;
}
$options['other'] = 'Other';


$page_set->add_field($box_id, array(
          'type'  => 'radio',
          'label' => 'Select Page',
          'name'  => 'id',
          'options' => $options,
          'container' => 'right'
          ));

$page_set->add_field($box_id, array(
          'type'  => 'content_select',
          'label' => 'Other:',
          'name'  => 'other',
          'container' => 'right'
          ));

$page_set->add_field($box_id, array(
          'type'  => 'script',
          'label' => 'label',
          'script'  => '$(document).ready(function(){ 
              var drop = $(".field.other").find("select");
              var radio =  $(".field.id").find("input[type=radio]");
              
              var radio_val = "";
            
              radio.each(function(){
                if($(this).attr("checked") == "checked")
                {
                  radio_val = $(this).val();
                }
              });
                
              if(radio_val == "other" )
              { 
                
                drop.removeAttr("disabled");
                drop.trigger("chosen:updated");
              }
              else
              {
                drop.attr("disabled","disabled"); 
                drop.trigger("chosen:updated");
              }
                           

              radio.change(function(){
                var val = $(this).val();
                
                if(val == "other")
                { 
                  drop.removeAttr("disabled");
                  drop.trigger("chosen:updated");
                }
                else
                {
                  drop.attr("disabled","disabled"); 
                  drop.trigger("chosen:updated");
                }
                
              });
            });'
          ));


$page_set->add_field($box_id, array(
          'type'  => 'text',
          'label' => 'Button Label',
          'name'  => 'btn_label',
          'container' => 'left',
          ));