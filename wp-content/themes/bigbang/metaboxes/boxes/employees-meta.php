<?php
/*
*Employees meta
*  Set in Switch.php,
*
*/
defined('ABSPATH') or die('Access Denied!');


$page_set ->add_metabox( array(
  'id' => 'employee' ,
  'title' => 'Employee Details',
  'priority' => 'high'
  ));


$page_set->add_field(
    'employee', array(
    'type' => 'text',
    'label' => 'First Name',
    'name' => 'first_name'
    ));

  $page_set->add_field(
    'employee', array(
    'type' => 'text',
    'label' => 'Family Name',
    'name' => 'family_name'
    ));
  $page_set->add_field(
    'employee', array(
    'type' => 'text',
    'label' => 'Position',
    'name' => 'position'
  ));
  

$page_set ->add_metabox( array(
  'id' => 'social_profiles' ,
  'title' => 'Social Profiles',
  'priority' => 'high'
  ));

$sp = array('Linkedin', 'Facebook', 'Google Plus', 'Twitter');

foreach ($sp as $p) 
{
   $page_set->add_field(
    'social_profiles', array(
    'type' => 'text',
    'label' => $p,
    'name' => strtolower(str_replace(' ', '', $p))
  ));
}

  
$page_set ->add_metabox( array(
  'id' => 'user' ,
  'title' => 'Linke to User',
  'context' =>'side',
  'priority' => 'high'
  ));

  $page_set->add_field( 'user', array(
    'type' => 'user',
    'label' => 'User Id',
    'name' => 'id'
  ));