<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Expertise Meta Data
*
*/





$children = get_posts( array(
    'post_type' => 'page',
    'post_parent' => POST_ID,
    'posts_per_page' => -1,
    'post_status' => 'publish'
  ));

$a = '';
$b = 0;
if( sizeof($children) > 0 )
{
  foreach ($children as $child) 
  {
    
    $b++;
     $name = $child->post_title;
     $box_id = 'child_'.$child->ID; 
     $a .=  $child->ID;
     if($b != sizeof($children))
     {
      $a .= ':';
     }



     $page_set->add_metabox( array(
          'id' => $box_id ,
          'title' => $name . ' Section',
          'priority' => 'high',
          'containers' => array('left', 'right')
          ));

      $page_set->add_field( $box_id , array(
            'type' => 'text',
            'name' => 'title',
            'label' => 'Heading',
            'container' => 'left'
          ));

     $page_set->add_field( $box_id , array(
            'type'       => 'rich',
            'name'       => 'text',
            'rows'       => 14,
            'container'  => 'left'
          ));

       $page_set->add_field( $box_id , array(
            'type'  => 'hidden',
            'name'  => 'url',
            'value' => get_page_uri( $child->ID )
          ));

       $page_set->add_field( $box_id , array(
            'type'  => 'hidden',
            'name'  => 'page_id',
            'value' => $child->ID
          ));

 

         $page_set->add_field( $box_id , array(
            'type'         => 'heading',
            'heading'      => 'Perks List',
            'heading_type' => 'h1',
             'container'   => 'right'
          ));

       $page_set->add_field( $box_id , array(
            'type'       => 'number',
            'name'       => 'perks_number',
            'label'      => 'Number of Perks <br><em>( select and save/update to increase number of fields )</em>',
            'default'    => 1,
             'container' => 'right'
          ));

       $perks = get_post_meta( POST_ID, $box_id);
    
      $nr_of_perks = (!empty( $perks[0]['perks_number'] )) ? $perks[0]['perks_number'] : 1;

      $page_set->add_field( $box_id , array(
            'type'         => 'heading',
            'heading'      => 'list elements:',
            'heading_type' => 'h4',
             'container'   => 'right'
          ));

         for ($i=1; $i <=$nr_of_perks ; $i++) 
         { 
            $page_set->add_field( $box_id , array(
            'type'       => 'text',
            'name'       => 'perk_' . $i,
            'label'      => '',
             'container' => 'right'
          ));
         }

  }

    


}



  $box_id = 'dual_section';

 $page_set->add_metabox( array(
          'id'         => $box_id ,
          'title'      => 'Dual Section',
          'priority'   => 'high',
          'containers' => array('left', 'right')
          ));

      $page_set->add_field( $box_id , array(
            'type'      => 'textarea',
            'name'      => 'left_text',
            'label'     => 'Heading',
            'container' => 'left',
            'rows'      => 3
          ));

     $page_set->add_field( $box_id , array(
            'type'      => 'textarea',
            'name'      => 'right_text',
            'rows'      => 8, 
            'label'     => 'Heading',
            'container' => 'right'
          ));

$page_set->add_field( 'headline' , array(
          'type' => 'hidden',
          'name' => 'child_sections',
          'value' => $a
        ));

$box_id = null;
