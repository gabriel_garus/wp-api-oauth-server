<?php
defined('ABSPATH') or die('Access Denied!');


$page_set ->add_metabox( array(
  'id' => 'quote' ,
  'title' => 'Quote Details',
  'priority' => 'high'
  ));


$page_set->add_field(
    'quote', array(
    'type' => 'text',
    'label' => 'First Name',
    'name' => 'first_name'
    ));

  $page_set->add_field(
    'quote', array(
    'type' => 'text',
    'label' => 'Family Name',
    'name' => 'family_name'
    ));
  $page_set->add_field(
    'quote', array(
    'type' => 'text',
    'label' => 'Position',
    'name' => 'position'
  ));
  $page_set->add_field(
    'quote', array(
    'type' => 'text',
    'label' => 'Company',
    'name' => 'company_name'
    ));

    $page_set->add_field(
    'quote', array(
    'type' => 'text',
    'label' => 'Personal Profile Url',
    'name' => 'personal_url'
    ));

    $page_set->add_field(
    'quote', array(
    'type' => 'text',
    'label' => 'Company Url',
    'name' => 'company_url'
    ));
