<?php

defined('ABSPATH') or die('Access Denied!');

/*
*Work Page Meta
*
*/

$box_id = 'projects_list';
$page_set->add_metabox( array(
      'id'      => $box_id ,
      'title'    => 'Projects List Settings ',
      'priority' => 'high',
      'containers' => array('left', 'right')
      ));

$page_set->add_field($box_id, array(
          'type'  => 'radio',
          'label' => 'Order By',
          'name'  => 'orderby',
          'options' => array(
            'none' => 'No Order',
            'ID'  => 'the ID',
            'date' => 'Date',
            'title' => 'Title',
            'modified' => 'Last Modified Date',
            'rand'    => 'Random'
            ),
          'container' => 'left'

          ));


$page_set->add_field($box_id, array(
          'type'  => 'radio',
          'label' => 'Order',
          'name'  => 'order',
          'options' => array(
            'ASC' => 'Ascending',
            'DESC'  => 'Descending'
            ),
          'container' => 'right'
          ));


//========The Number of PRojects to be dispalyed==========
$box_id = 'projects';

$page_set->add_metabox( array(
      'id'      => $box_id ,
      'title'    => 'The Projects',
      'priority' => 'high'
      ));








$postOb = new bb_post(POST_ID);

$orderby = $postOb->get_field('projects_list','orderby');
$order= $postOb->get_field('projects_list', 'order');

$projects = get_posts(array(
      'posts_per_page' => -1,
      'post_type'      => 'projects',
      'orderby'        => $orderby,
      'order'          => $order
  ));






foreach ($projects as $project) 
{

    
  $page_set->add_field($box_id, array(
              'type'  => 'checkbox',
              'label' => $project->post_title . '(id: '. $project->ID .')',
              'value' => $project->ID,
              'name'  => $project->ID
              ));

 
}

