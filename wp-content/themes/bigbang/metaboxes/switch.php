<?php
defined('ABSPATH') or die('Access Denied!');

add_action( 'load-post.php', 'bb_metaboxes' );
add_action( 'load-post-new.php', 'bb_metaboxes' );


session_start();



function bb_metaboxes()
{
   
	//bb_remove_content_editor();

	// ---- get post ID ----------------------------------------

	$post_id = isset($_GET['post']) ? $_GET['post'] : (isset($_POST['post_ID']) ? $_POST['post_ID'] : '') ;

	if(!defined('POST_ID'))
		{
			define('POST_ID', $post_id);
		}
	//----------------------------------------------

	$post = get_post( $post_id );




	// ---- get post type ----------------------------------------
	if(!empty($post_id))
	{
		$post_type = get_post_type($post_id);
	}
	elseif(isset($_GET['post_type']) && !empty($_GET['post_type']))
	{
		$post_type = $_GET['post_type'];
	}
	else
	{
		$post_type = 'post';
	}



	// ---- get page -> get template name ----------------------------------------
	if($post_type === 'page')
	{
		if(!empty($post_id))
		{
			$template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
		}
		else
		{
			$template_file = 'default';
		}
	}

	// get blog page ID
	$blog_page_id = (defined('BLOG_ID')) ? BLOG_ID : get_option( 'page_for_posts' );
	$front_page_id = get_option( 'page_on_front' );


	define('BOXES', THEME_DIR . 'metaboxes/boxes/');

	$meta_data = bb_extract( get_post_meta($post_id) );

//--------------------------------------------------------------------------------------------
//  											THE SWITCH
//--------------------------------------------------------------------------------------------



//----------------------------------------------
//  				POSTS
//----------------------------------------------

$page_set = new mBoxSet();


if($post_type != 'employees' AND $post_type != 'quotes' AND $post_type != 'attachment')
{

	$page_set->add_metabox( array(
				'id' => 'headline' ,
				'title' => 'Headline',
				'priority' => 'high',
			//	'context' => 'side'
				));
	$page_set->add_field('headline', array(
						'type'  => 'text',
						'label' => '',
						'name'  => 'title'
						));

}
  // *** based on post type ***/
	if($post_type === 'quotes')
	{
		include BOXES . 'quotes-meta.php';
	}

	if($post_type === 'employees')
	{
		include BOXES . 'employees-meta.php';
	}

	elseif($post_type === 'projects')
	{
		include BOXES . 'projects-meta.php';
	}
	elseif($post_type === 'post')
	{
		include BOXES . 'article-meta.php';
	}





//----------------------------------------------
//  				PAGES
//----------------------------------------------


// *** based on page template ***/
	if($post_type === 'page')
	{
	  		if($post_id  == $front_page_id)
	  		{
	  			include BOXES . 'home-meta.php';	//$blog_page_id
	  		}
			elseif ($template_file ==='templates/work.php')
			{
				include BOXES . 'work-meta.php';
			}
			elseif ($template_file ==='templates/about.php')
			{
				include BOXES . 'about-meta.php';
			}
			elseif ($template_file ==='templates/single-expertise.php')
			{
				include BOXES . 'single-expertise-meta.php';
			}
			elseif ($template_file ==='templates/expertise.php')
			{
				include BOXES . 'expertise-meta.php';
			}
			elseif($template_file ==='templates/contact.php')
			{
				include BOXES . 'contact-meta.php';
			}
			elseif($post_id == $blog_page_id)
			{
				include BOXES . 'news-meta.php';
			}
			elseif ($template_file ==='templates/legal.php')
			{
			}
			else
			{
				//include BOXES . 'services-single-meta.php';
			}
		}


	$page_set->activate();
	}



require 'mBox_class.php';

require 'meta-helpers.php';


//----------------------------------------------
// move Yoast SEO to the bottom
//----------------------------------------------

add_filter( 'wpseo_metabox_prio', function() { return 'low';});

//----------------------------------------


//----------------------------------------------
// Reset metabox positions
//----------------------------------------------
$user_ID = get_current_user_id();
$dontResetMeta = get_user_meta($user_ID, 'noBoxReset');

if(empty($dontResetMeta)) :
update_user_meta( $user_ID, 'meta-box-order_post', '' );
update_user_meta( $user_ID, 'meta-box-order_page', '' );
endif;
//----------------------------------------
