<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Single
*/

get_header();


include_once PARTIALS_DIR . 'facebook-share-script.php';

echo '<script src="https://apis.google.com/js/platform.js" async defer></script>'; 


$thePost 	= new bb_post( PAGE_ID );
$meta_data  = $thePost->meta;
$title 		= $thePost->title;
$content  	=  $thePost->content;
$the_author = $thePost->author;

$thePost->set_img_itemprop('image');

$post_tags = $thePost->get_tag_list();
$gallery   = $thePost->gallery;

$middle_content = $thePost->get_field('middle_content', 'text');
$bottom_content = $thePost->get_field('bottom_content', 'text');

//dump($meta_data);
$theBlogId = (defined('BLOG_ID')) ? BLOG_ID : get_option( 'page_for_posts' );
$url = get_permalink(PAGE_ID);

$post_url = get_permalink(PAGE_ID);


//--------------------------------




echo '<div class="section single-article" itemscope itemtype="http://schema.org/Blog">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;

echo '<meta itemprop="name" content="' . COMPANY_NAME . '">';
echo '<meta itemprop="url" content="'.$url.'">';


echo '<div class="table">';

//--------- LEFT -----------------------
echo '<div class="left main">' . PHP_EOL;

//-------- Article ---------
echo '<article itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">'.PHP_EOL;

echo '<meta itemprop="url" content="'.$post_url.'">'.PHP_EOL;


//--------- Header -----------------------
echo '<header>' . PHP_EOL;
if(!empty($title))
{
	echo '<h1 itemprop="headline">' . $title . '</h1>' . PHP_EOL;
}


echo '<div class="article-meta">'.PHP_EOL;

//---- Author ----
//dump($the_author);
echo '<div class="article-author">'.PHP_EOL;
echo '<span class="title">by:&nbsp;</span>';

$check = $the_author['first_name'] . $the_author['last_name'];
if(!empty( $check ))
{
	if(!empty($the_author['first_name']))
	{
		echo '<span class="first-name">'. $the_author['first_name'] .'</span> ';
	}

	if(!empty($the_author['last_name']))
	{
		echo '<span class="last-name">&nbsp;'. $the_author['last_name'] . '</span>';
	}
}
elseif(!empty($the_author['nick_name']))
{
	echo '<span class="first-name">' . $the_author['nick_name'] .'</span>';
}
else
{
	echo '<span class="first-name">' . $the_author['user_name'] .'</span>';
}
echo '</div>'.PHP_EOL; // .article-author
//----------------------


echo '<div class="cf bot">';
echo '<div class="article-date left">' . PHP_EOL;
echo $thePost->get_date(). PHP_EOL;
echo '</div>' . PHP_EOL; //.article-date

echo '<div class="article-tags left">' . PHP_EOL;
echo '<span class="title">Tags:</span>'. $post_tags;
echo '</div>' . PHP_EOL; //.article-tags

echo '</div>' . PHP_EOL;



echo '</div>'.PHP_EOL; // .article-meta

echo '</header>' . PHP_EOL;
//--------------------------------




//----------- INTRO ---------------------
echo '<div class="post-content">' . PHP_EOL;
echo  do_shortcode( wpautop($content) );
echo '</div>' . PHP_EOL;
//--------------------------------



//------------- EVENT -------------------
/*$event = (!empty($meta_data['event'])) ? $meta_data['event'] : null;
if($event !== null AND !empty($event['name']))
{
	include PARTIALS_DIR . 'event.php';
}*/
//--------------------------------






//--------------------------------

//-----------Middle Content ---------------------

if( $middle_content != null )
{
		echo wpautop( $middle_content );
}
//--------------------------------






if(!empty($bottom_content))
{
	echo wpautop ( $bottom_content );
}



//--------- GALLERY -----------------------



if( !empty($gallery) )
{
	echo '<h3>';
	_e('Gallery' , 'bigbang');
	echo '</h3>'.PHP_EOL;

	echo '<div class="bb-gallery">' . PHP_EOL;
	include PARTIALS_DIR . 'gallery.php';
	echo '</div>' . PHP_EOL;
}

//--------------------------------



include PARTIALS_DIR . 'social-share-bb.php';
//--------------------------------





//------ Article Footer --------------------------
echo '<footer class="article-footer">' . PHP_EOL;




$author = $thePost->author;

include PARTIALS_DIR . 'article-author.php';
include PARTIALS_DIR . 'disqus.php';

echo '</footer>' . PHP_EOL;

echo '</article>' . PHP_EOL;

echo '</div>' . PHP_EOL; // . left
//--------------------------------






//----------Right Aside ----------------------
echo '<div class="right aside">' . PHP_EOL;

//----------- Related Articles ---------------------
include_once PARTIALS_DIR . 'related-articles.php';
//--------------------------------


//--------------- Social Follow -----------------
$blog_index_meta  = bb_extract(get_post_meta(get_option('page_for_posts')));
$follow_head = ( !empty($blog_index_meta['follow']['head']) ) ? $blog_index_meta['follow']['head'] : null;
$follow_text =  ( !empty($blog_index_meta['follow']['text']) ) ? $blog_index_meta['follow']['text'] : null;
require PARTIALS_DIR . 'follow-us.php';


echo '</div>' . PHP_EOL;// .right aside
//--------------------------------


echo '</div>' . PHP_EOL;// .table

$article_id = $thePost->get_field('next_article','id');
the_post();
if($article_id === null)
{
	$all_posts = get_posts(array('status' => 'any', 'exclude'=> PAGE_ID, 'posts_per_page' => -1 ) );
	$nnr = array_rand($all_posts, 1);
	$next_post = $all_posts[$nnr];
	//dump($all_posts);
	$article_id = $next_post->ID;
}

echo '<div class="next blog-list">';
echo '<h3 class="caps">Read Next</h3>';

include PARTIALS_DIR . 'blog-list-item.php';
echo '</div>';


echo '</div>' . PHP_EOL; //.container

echo '</div>' . PHP_EOL; // .section



get_footer();


