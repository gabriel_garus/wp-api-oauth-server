<?php
defined('ABSPATH') or die('Access Denied!');
/*
*CTA Section partial
*/

//-----Get meta_data---------

//-----------bb_extract()  located in the inc folder------------
if(empty($meta_data)){
  $meta_data = bb_extract( get_post_meta(PAGE_ID));
}

//-----Declare Variables------



$link_section = (!empty($meta_data['link_section'])) ? $meta_data['link_section'] : null ;

$link_title    = ( !empty($link_section['title']) ) ?  $link_section['title'] : null;
$link_text 	   = ( !empty($link_section['text']) ) ?  $link_section['text'] : null;
$link_btn_href = ( !empty($link_section['link']) ) ?  $link_section['link'] : null;
$link_btn_lbl  = ( !empty($link_section['label']) ) ?  $link_section['label'] : null;

if(!isset($cta_class))
{
	$cta_class = 'default';
}
echo '<section class="cta-section-company '. $cta_class .'">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;
echo '<div class="left">' . PHP_EOL;
if(!empty($link_title))
{
echo '<h3>' . $link_title . '</h3>' . PHP_EOL;
}
echo '</div>' . PHP_EOL; // .left
echo '<div class="right">' . PHP_EOL;
if(!empty($link_text))
{
echo '<p>' . $link_text . '</p>' . PHP_EOL;
}
if(!empty($link_btn_href))
{
echo '<a href="' . get_permalink($link_btn_href) .'" class="btn btn-secondary">' . $link_btn_lbl . '</a>' . PHP_EOL;
}
echo '</div>' . PHP_EOL; // .right
//echo '<hr class="grey divider">' . PHP_EOL;
echo '</div>' . PHP_EOL; // .container
echo '</section>' . PHP_EOL;
