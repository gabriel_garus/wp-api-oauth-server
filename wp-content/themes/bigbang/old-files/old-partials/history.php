<?php

defined('ABSPATH') or die('Access Denied!');
//added by gavin

$meta_data = bb_extract( get_post_meta(PAGE_ID) );



$history = (!empty($meta_data['history_list'])) ? $meta_data['history_list'] : null;


if($history !== null)
{


    $historic_title  = (!empty($history['heading_over_all']))   ? $history['heading_over_all'] : null;
    $historic_sections = array();

    for ($i=1; $i <= 6 ; $i++)
    {
      $section = array();
      $section['heading'] = (!empty($history['heading_'. $i])) ? $history['heading_'. $i] : null;
      $section['image'] = (!empty($history['historic_image_'. $i])) ? $history['historic_image_'. $i] : null;
      $section['text'] = (!empty($history['content_'. $i])) ? $history['content_'. $i] : null;
      
      $historic_sections['section_' . $i] = $section;
    }

    //*************************************************************

    echo '<div class="container company">' .PHP_EOL;
    echo '<h2>'. $historic_title .'</h2>';
    echo '</div>' . PHP_EOL;


    foreach ($historic_sections as $section)
    {
     include PARTIALS_DIR . 'switch-section.php';
    }
    
    //*****************************************************

}