<?php
/*
* Post for blog -list - homepage
*/



	$blog_post 	= new bb_post( $post->ID);
	$title   		= $blog_post->title;
	$excerpt 		= $blog_post->excerpt;
	$date    		= $blog_post->get_date();
	$url     		= get_permalink( $post->ID );

	$the_author = $blog_post->author;
	$thumb_id = get_post_thumbnail_id( $post->ID );
	$thumb = bb_get_the_image( $thumb_id,  array(
					'size' => 'thumbnail-large',
					'itemprop' => 'image',
					'class' => 'post-image'
	));

	echo '<article itemscope itemtype="http://schema.org/BlogPosting">'.PHP_EOL;

	echo '<header>'.PHP_EOL;
	if(!empty($url))
	{
	echo '<h2 itemprop="headline"><a href="'.$url.'">'.$title .'</a></h2>'.PHP_EOL;
	}
	echo '</header>'.PHP_EOL;
echo $thumb['html'];
	echo '<div class="article-meta">'.PHP_EOL;
	echo $date;
	
	echo $blog_post->get_tag_list();

	echo $blog_post->get_icons();

	echo '</div>'.PHP_EOL;





	echo '<div class="art-content" itemprop="articleBody">' . wpautop( $excerpt ) . '</div>'.PHP_EOL;

	echo '<div class="read-more-div">'.PHP_EOL;
	if(!empty($url))
	{
	echo '<a itemprop="url" href="'. $url .'"  class="btn btn-tertiary bg-navy">';
	_e('Read More' , 'bigbang');
	echo '</a>'.PHP_EOL;
	}
	echo '</div>'.PHP_EOL;

	echo '</article>'.PHP_EOL;
