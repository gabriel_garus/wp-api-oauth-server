<?php
defined('ABSPATH') or die('Access Denied!');
/*
*social share partial
*/
//------Options from Redux-------------

if(empty($options))
{
	$options = (!empty($_SESSION['bb_options'])) ? $_SESSION['bb_options'] : get_option('bigbang');
}

$title = get_the_title();
$aLink 							= get_permalink( PAGE_ID);
$exc 								= bb_get_excerpt(PAGE_ID);
$twitter = (!empty($options['twitter'])) ? $options['twitter'] : null;
$socials = array();

$fb = array();
$fb['class'] = 'facebook';
$fb['label'] = 'Facebook';
$fb['link']  = 'https://www.facebook.com/sharer/sharer.php?u=';
$fb['link'] .= $aLink;
$socials[] = $fb;

$twit = array();
$twit['class'] = 'twitter';
$twit['label'] = 'Twitter';
$twit['link']  = 'https://twitter.com/intent/tweet?text=';
$twit['link'] .= urlencode($title);
$twit['link'] .= '&amp;url=';
$twit['link'] .= urlencode($aLink);
if($twitter != null)
{
$twit['link'] .= '&amp;via=' . $twitter;
}
$socials[] = $twit;

$gp['class'] = 'google';
$gp['label'] = 'Google+';
$gp['link']  = 'https://plus.google.com/share?url=';
$gp['link'] .= urlencode($aLink);
$socials[] = $gp;

$li['class'] = 'linked';
$li['label'] = 'LinkedIn';
$li['link']  = 'https://www.linkedin.com/shareArticle?mini=true&amp;url=';
$li['link'] .=  urlencode($aLink);
$li['link'] .= '&amp;title=';
$li['link'] .= urlencode($title);
$li['link'] .= '&amp;source=';
$li['link'] .= urlencode($aLink);
$li['link'] .= '&amp;summary=';
$li['link'] .= urlencode($exc);
$socials[] = $li;

//------------------------------------------------------


echo '<div class="social-share share-links">'. PHP_EOL;
if(!empty($share_links_title))
{
echo '<h4>' . $share_links_title . '</h4>'  . PHP_EOL;
}
echo '<ul>'.PHP_EOL;

foreach ($socials as $soc)
{
	echo '<li class="'.$soc['class'].'">';
	echo '<a href="' . $soc['link'] .'" ';
	echo 'target="_blank" ';
	echo 'onclick="windowExplode(this.href,500,400); event.preventDefault()" >';
	echo $soc['label'].'</a>';

}

echo '</ul>' . PHP_EOL;
echo '</div>' . PHP_EOL; // .social share
