<?php

$services = get_posts(array(
			'post_type' => 'Services',
			'posts_per_page' => -1,
			'order' => 'ASC'

		));

	$special_title = (!empty($meta_data['heavy_title']['title'])) ? $meta_data['heavy_title']['title'] : null;




	echo '<section class="big-planes">' . PHP_EOL;
	echo '<div class="container">' . PHP_EOL;

	if($special_title !== null)
	{
		echo '<h2>' . $special_title . '</h2>' . PHP_EOL;
	}


	echo '<div>' . PHP_EOL;
	foreach($services as $service)
	{
		if($service->post_parent == PAGE_ID)
		{
			$meta_data = bb_extract( get_post_meta($service->ID) );
		//	dump($meta_data);

			if(!empty($meta_data['leadin']['title']))
			{
				$title = $meta_data['leadin']['title'];
			}
			else
			{
				$title = $service->post_title;
			}

			//--------------------------------

			$text = bb_get_excerpt($service->ID);
			$text = trim($text);
			$text = bb_post::cut_string($text, 145);

			if(!empty($meta_data['leadin']['image']) AND $meta_data['leadin']['image'] != 0)
			{
				$image_id = $meta_data['leadin']['image'];
			}
			else
			{
				$image_id = get_post_thumbnail_id( $service->ID );
			}

			if(!empty($image_id))
			{
				$image = bb_get_the_image($image_id, array('width' => 490, 'height' => 135 ));
			}
			else
			{
				$image = null;
			}




			echo '<div class="left">' . PHP_EOL;

			if($image != null)
			{
				echo $image['html'];
			}

			echo '<div class="text">' . PHP_EOL;

			echo '<h3>' . $title . '</h3>' . PHP_EOL;
		    echo '<hr class="hr-green">'. PHP_EOL;

		    echo '<div class="text">' . PHP_EOL;
		    echo '<p>' . $text . '</p>' . PHP_EOL;
		    echo '</div>' . PHP_EOL;

		    echo '<a class="read-more" href="' . get_permalink( $service->ID ) . '">Read More</a>' . PHP_EOL;

			echo '</div>' . PHP_EOL;
			echo '</div>' . PHP_EOL;


		}
	}

	echo '</div>' . PHP_EOL;
	echo '</div>' . PHP_EOL; //.container
	echo '</section>'; //.big-planes
