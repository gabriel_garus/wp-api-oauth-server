<?php
defined('ABSPATH') or die('Access Denied!');
//added by gavin


if(empty($meta_data))
{
  $meta_data = bb_extract( get_post_meta(PAGE_ID));
}


if(!empty($meta_data['aircraft_types']))
{
    $aircraft_types = $meta_data['aircraft_types'];


    echo '<div class="aircraft-types">' . PHP_EOL;
    echo '<div class="header">' . PHP_EOL;
    echo "<h3>Aircraft Types</h3>". PHP_EOL;
    echo '<hr class="hr-green">'. PHP_EOL;
    echo '</div>' . PHP_EOL;

    echo '<div class="grid-4 types">';

    for($a=1; $a<=4; $a++)
  {
    $image = (!empty($aircraft_types['image_'.$a]))? $aircraft_types['image_'.$a] : null;
    $airtype = (!empty($aircraft_types['air_type_'.$a]))? $aircraft_types['air_type_'.$a] : null;

     echo '<div class="grid-element">' . PHP_EOL;

    if($image !== null)
    {
      $the_image = bb_get_the_image($image, array('width'=> 272, 'height' => 114, 'class' => 'contour'));
      echo $the_image['html'];
    //  dump($the_image);
    }


    if($airtype !== null)
    {
      echo '<h3>' . $airtype . '</h3>';
    }

     echo '</div>';

  }



  echo '</div>' . PHP_EOL; //. cf


 echo '<div class="grid-4 taglines">';
  for($a=1; $a<=4; $a++)
  {
    $tagline = (!empty($aircraft_types['tag_line_'.$a]))? $aircraft_types['tag_line_'.$a] : null;
    echo '<div class="grid-element">' . PHP_EOL;
      if($tagline !== null)
      {

        echo '<h4>' . $tagline . '</h4>';
      }
    echo '</div>' ;
  }

  echo '</div>' . PHP_EOL;
  echo '</div>' . PHP_EOL;//."aircraft-types
}
