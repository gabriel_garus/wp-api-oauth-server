<?php
defined('ABSPATH') or die('Access Denied!');



get_header();

$meta_data = bb_extract( get_post_meta(PAGE_ID) );

echo '<div itemscope itemtype="https://schema.org/Service">' . PHP_EOL;

$hero_title_itemscope = 'serviceType name';
$hero_text_itemscope = 'description';
include PARTIALS_DIR . 'hero.php';

if(!empty($meta_data['special_type']['type']) AND $meta_data['special_type']['type'] ==  'heavy_mainetnance')
{

	include PARTIALS_DIR . 'heavy-maintenance.php';
}
else
{
	include PARTIALS_DIR . 'page-default.php';
}
echo '</div>' . PHP_EOL;


get_footer();