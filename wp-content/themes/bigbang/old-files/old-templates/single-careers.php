<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Single Careers Page
*/




get_header();
//----bb_extract()...A function to get all of the meta data for the page
if(empty($meta_data)){
  $meta_data = bb_extract( get_post_meta(PAGE_ID) );
}
//bb_extract()  located in the inc folder

if(empty($options))
{
	$options = get_option('bigbang');
}

$heading = (!empty($meta_data['headline']['title'])) ?  $meta_data['headline']['title'] : get_the_title();
$content = get_the_content();

$role = (!empty($meta_data['job_spec']['role'])) ?  $meta_data['job_spec']['role'] : null;
$salary = (!empty($meta_data['job_spec']['salary_range'])) ?  $meta_data['job_spec']['salary_range'] : null;
$location = (!empty($meta_data['job_spec']['location'])) ?  $meta_data['job_spec']['location'] : null;
$contact_first_name = (!empty($meta_data['job_spec']['contact_first_name'])) ?  $meta_data['job_spec']['contact_first_name'] : null;
$contact_last_name = (!empty($meta_data['job_spec']['contact_last_name'])) ?  $meta_data['job_spec']['contact_last_name'] : null;
$posted = get_the_date();
$description = (!empty($meta_data['job_description']['content'])) ?  $meta_data['job_description']['content'] : null;


//------------------------------------------------------
//------------------------------------------------------
//------------------------------------------------------




echo '<section class="the_job" itemscope itemtype="https://schema.org/JobPosting">' . PHP_EOL;
echo '<div class ="container">' . PHP_EOL;
	echo '<div class="left main">' . PHP_EOL;

		echo '<div class="top">' . PHP_EOL;
			echo '<h1 itemprop="name">'. $heading . '</h1>' . PHP_EOL;
			echo wpautop($content) . PHP_EOL;
		echo '</div>' . PHP_EOL;// .top

		echo '<meta itemprop="datePosted" content="'.$posted.'">';

		// @TODO: check if industry rich snippet for job posting is correct.
		echo '<meta itemprop="industry" content="Aviation">';

		// @TODO: check if hiringOrganization rich snippet for job posting is correct.
		if(!empty($options['tradeName']))
		{
			echo '<meta itemprop="hiringOrganization" content="' . $options['tradeName'] . '">';
		}


		//@done: add translatable strings to hardcoded text

		echo '<table class="job-spec">' . PHP_EOL;

		echo '<tr class="first">' . PHP_EOL ;

		echo '<td class="title">';
		echo '<h4>' . PHP_EOL;
		_e('Role:' , 'bigbang');
		echo '</h4>' . PHP_EOL;
		echo '</td>' . PHP_EOL;

		echo '<td class="desc">' . PHP_EOL;
		echo '<p itemprop="title">'. $role . '</p>' . PHP_EOL;
		echo '</td>' . PHP_EOL;
		echo '</tr>' . PHP_EOL;


		echo '<tr>' . PHP_EOL ;

		echo '<td class="title">';
		echo '<h4>' . PHP_EOL;
		_e('Salary Range:' , 'bigbang');
		echo '</h4>' . PHP_EOL;
		echo '</td>' . PHP_EOL;

		echo '<td class="desc">' . PHP_EOL;
		echo '<p itemprop="baseSalary">'. $salary .'</p>' . PHP_EOL;
		echo '</td>' . PHP_EOL;
		echo '</tr>' . PHP_EOL;

		echo '<tr>' . PHP_EOL ;

		echo '<td class="title">';
		echo '<h4>' . PHP_EOL;
		_e('Location:' , 'bigbang');
		echo '</h4>' . PHP_EOL;
		echo '</td>' . PHP_EOL;

		echo '<td class="desc">' . PHP_EOL;
		echo '<p itemprop="jobLocation">'. $location .'</p>' . PHP_EOL;
		echo '</td>' . PHP_EOL;
		echo '</tr>' . PHP_EOL;

		echo '<tr>' . PHP_EOL ;

		echo '<td class="title">';
		echo '<h4>' . PHP_EOL;
		_e('Contact:' , 'bigbang');
		echo '</h4>' . PHP_EOL;
		echo '</td>' . PHP_EOL;

		echo '<td class="desc">' . PHP_EOL;
		echo '<p><span>'. $contact_first_name . '</span>' . PHP_EOL;
		echo '<span>' . $contact_last_name . '</span></p>' . PHP_EOL;
		echo '</td>' . PHP_EOL;
		echo '</tr>' . PHP_EOL;

		echo '<tr class="last">' . PHP_EOL ;

		echo '<td class="title">';
		echo '<h4>' . PHP_EOL;
		_e('Posted:' , 'bigbang');
		echo '</h4>' . PHP_EOL;
		echo '</td>' . PHP_EOL;

		echo '<td class="desc">' . PHP_EOL;
		echo '<p>'. $posted .'</p>' . PHP_EOL;
		echo '</td>' . PHP_EOL;
		echo '</tr>' . PHP_EOL;

		echo '</table>' . PHP_EOL;// .job-spec
		//-------------------------------------------------------------------------


		echo '<h3 class="grey">Share this job</h3>' . PHP_EOL;
		include PARTIALS_DIR . 'social-share.php';
		echo '<div class="bot">' . PHP_EOL;

			echo '<h4>' . PHP_EOL;
			_e('Job Description:' , 'bigbang');
			echo '</h4>' . PHP_EOL;
			echo '<div itemprop="description">' ,PHP_EOL;
				echo wpautop( $description , true);
			echo '</div>' . PHP_EOL; //.


				$job_post = new bb_post( PAGE_ID );
				$author = $job_post->author;
				$author_title = 'Job Posted by:';
				include PARTIALS_DIR . 'author-short.php';
				//@done contact-form goes here

				$form_id = ( !empty($meta_data['form']['form_id']) ) ? $meta_data['form']['form_id'] : null;

			if( $form_id !== null )
			{
				$formH     =  '[contact-form-7 id="'. $form_id .'" html_name="contact-form"]';

				echo '<h4 class="green">' . PHP_EOL;
					_e('Apply for this job:' , 'bigbang');
				echo '</h4>' . PHP_EOL;

				echo '<div class="apply-form">'.PHP_EOL;
					echo do_shortcode($formH);
				echo '</div>'.PHP_EOL;

			}

		echo '</div>' . PHP_EOL; // .bot

	echo '</div>' . PHP_EOL; // .left
echo '</div>' . PHP_EOL; // .container
echo '</section>' . PHP_EOL;
get_footer();
