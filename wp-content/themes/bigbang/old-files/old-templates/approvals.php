<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Template Name: Approvals
*/



get_header();
$meta_data = bb_extract( get_post_meta(PAGE_ID) );

//------------------bb_extract()  located in the inc folder--------------

include PARTIALS_DIR . 'hero.php';

$services = get_posts(array(
			'post_type' => 'Approvals',
			'posts_per_page' => -1,
			'order' => 'ASC'
		));



$special_title = (!empty($meta_data['heavy_title']['title'])) ? $meta_data['heavy_title']['title'] : null;




	echo '<section class="big-planes approvals">' . PHP_EOL;
	echo '<div class="container">' . PHP_EOL;

	if($special_title !== null)
	{
		echo '<h2>' . $special_title . '</h2>' . PHP_EOL;
	}


	echo '<div>' . PHP_EOL;
	foreach($services as $service)
	{
		$service_meta_data = bb_extract( get_post_meta($service->ID) );
		//	dump($meta_data);

			if(!empty($service_meta_data['leadin']['title']))
			{
				$title = $service_meta_data['leadin']['title'];
			}	
			else
			{
				$title = $service->post_title;
			}

			//--------------------------------

			$text = bb_get_excerpt($service->ID);

			if(!empty($service_meta_data['leadin']['image']) AND $service_meta_data['leadin']['image'] != 0)
			{
				$image_id = $service_meta_data['leadin']['image'];
			}
			else
			{
				$image_id = get_post_thumbnail_id( $service->ID );
			}

			if(!empty($image_id))
			{
				$image = bb_get_the_image($image_id, array('size' => 'small_portrait'));
			}
			else
			{
				$image = null;
			}



			echo '<div class="left">' . PHP_EOL;

			if($image != null)
			{
				echo $image['html'];
			}

			echo '<div class="text">' . PHP_EOL;

			echo '<h3>' . $title . '</h3>' . PHP_EOL;
		   
		    echo '<div class="text">' . PHP_EOL;
		    echo wpautop( $text ) . PHP_EOL;
		    echo '</div>' . PHP_EOL;

		    echo '<a class="read-more" href="' . get_permalink( $service->ID ) . '">View Certificate</a>' . PHP_EOL;

			echo '</div>' . PHP_EOL;
			echo '</div>' . PHP_EOL;


		
	}

	echo '</div>' . PHP_EOL;
	echo '</div>' . PHP_EOL; //.container
	echo '</section>'; //.big-planes

	include PARTIALS_DIR . 'cta-section.php';


get_footer();
