<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Template Name: Careers
*/



get_header();
$meta_data = bb_extract( get_post_meta(PAGE_ID) );

//------------------bb_extract()  located in the inc folder--------------

include PARTIALS_DIR . 'hero.php';



$job_headline = (!empty($meta_data['job']['title'])) ?  $meta_data['job']['title'] : null;



$careers = get_posts( array('post_type' => 'careers', 'post_per_page' => -1) );

if(!empty($careers) AND is_array($careers))
{
  echo '<section class ="jobs">' . PHP_EOL;
  echo '<div class = "container">' . PHP_EOL;
	echo '<div class="job-list">' .PHP_EOL;

  echo '<h2>' . $job_headline . '</h2>' . PHP_EOL;

	foreach ($careers as $job)
	{
		$post_lbl = 'Read More';
    	$post_excerpt = bb_get_excerpt($job->ID);

		echo '<div class="job-posting" itemscope itemtype="http://schema.org/JobPosting">' . PHP_EOL;
		echo '<h3 itemprop="title"> '. $job->post_title . '</h3>'.PHP_EOL;
		echo '<hr class="hr-green" />' . PHP_EOL;
		echo '<div itemprop="description">' . PHP_EOL;
       	echo wpautop( $post_excerpt );
       	echo '</div>' . PHP_EOL;
    	echo '<h5 class="blue">';
    	echo '<a class="read-more upper" itemprop="url" href="' . get_permalink($job->ID) . '">' . $post_lbl . '</a></h5>' . PHP_EOL;


		echo '</div>' . PHP_EOL;// .job-posting
	}

	echo '</div>' . PHP_EOL;// .job-list
  echo '</div>' . PHP_EOL;// .container
  echo '</section>' . PHP_EOL;
}


get_footer();
