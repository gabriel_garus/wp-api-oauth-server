<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Template Name: Services
*/


get_header();

$meta_data = bb_extract( get_post_meta(PAGE_ID) );

//------------bb_extract()  located in the inc folder----------------


include PARTIALS_DIR . 'hero.php';


$service_posts = get_posts(array(
	'post_type' => 'services',
	'posts_per_page' => -1,
	'post_parent' => 0
	));

$grid_title = (!empty($meta_data['grid']['title'])) ?  $meta_data['grid']['title'] : null;


// ---- GRID -------

echo '<section class="the-grid services-grid">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;

if(!empty($grid_title))
{
	echo '<h2>' . $grid_title . '</h2>' . PHP_EOL;
}

echo '<div class="grid-list">' . PHP_EOL;

$counter = 0;
$rows = ceil( sizeof( $service_posts ) / 3 );
$row_counter = 0;

foreach ($service_posts as $service)
{
	$counter++;
	if($counter === 1)
	{
		$row_counter++;

		if($row_counter === 1)
		{
			$row_class = ' first';
		}
		elseif($row_counter == $rows)
		{
			$row_class = ' last';
		}
		else
		{
			$row_class = '';
		}

		echo '<div class="grid-row'.$row_class .'">';

	}

	$grid_object = $service;
	include PARTIALS_DIR . 'grid-element.php';

	if($counter === 3)
	{
		echo '</div>';
		$counter = 0;
	}

}

echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL; // .container
echo '</section>' . PHP_EOL;


get_footer();
