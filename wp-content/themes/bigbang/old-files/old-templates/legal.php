<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Template Name: Legal
*/


get_header();

$meta_data = bb_extract( get_post_meta(PAGE_ID) );

$hero_title     = (!empty($meta_data['headline']['title'])) ?  $meta_data['headline']['title'] : get_the_title();
$hero_text      = wpautop( get_the_content() );

echo '<div class="section legal">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;

echo '<div class="left main">' . PHP_EOL;
echo '<div class="content">' . PHP_EOL; 
echo '<h1>' . $hero_title . '</h1>' . PHP_EOL;
echo $hero_text;
echo '</div>' . PHP_EOL;

echo '</div>' . PHP_EOL;



echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL;
get_footer();

