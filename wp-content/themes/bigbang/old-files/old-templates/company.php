<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Template Name: Company
*/



get_header();
$meta_data = bb_extract( get_post_meta(PAGE_ID) );

//--------------bb_extract()  located in the inc folder---------------

include PARTIALS_DIR . 'hero.php';

$company_heading = (!empty($meta_data['company_heading']['heading'])) ? $meta_data['company_heading']['heading'] : null;
$section = (!empty($meta_data['switch_top'])) ? $meta_data['switch_top'] : null;

//echo '<div>' . PHP_EOL;
echo '<div class="container company">' .PHP_EOL;
echo '<h2>' . $company_heading . '</h2>' . PHP_EOL;
echo '</div>' . PHP_EOL;//.container
include PARTIALS_DIR . 'switch-section.php';
$section = (!empty($meta_data['switch_bottom'])) ? $meta_data['switch_bottom'] : null;
include PARTIALS_DIR . 'switch-section.php';
//echo '</div>' . PHP_EOL;
echo '<div class="container"> ' . PHP_EOL;
echo '<hr class="grey" />' . PHP_EOL;
echo '</div>' . PHP_EOL;
include PARTIALS_DIR . 'blog-list.php';
wp_reset_postdata();
get_footer();
