<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// A loop that creates 4 meta boxes with image upload.  change names where needed
$page_set ->add_metabox( array(
    'id' => 'aircraft_types',
    'title' => 'Aircraft Types' ,
    'priority' => 'high',
      ));

for ($i=1; $i < 5 ; $i++)
{

   $page_set->add_field(
    'aircraft_types', array(
    'type' => 'info',
    'info' => '<h2>Aircraft Type '. $i . '</h2>'
      ));
  $page_set->add_field(
    'aircraft_types', array(
    'type' => 'image',
    'label' => 'image - ' . $i ,
    'name' => 'image_'.$i
      ));

  $page_set->add_field(
    'aircraft_types', array(
    'type' => 'text',
    'label' => 'Air Type - ' . $i,
    'name' => 'air_type_'.$i
      ));

    $page_set->add_field(
        'aircraft_types', array(
        'type' => 'text',
        'label' => 'Tag Line - ' . $i,
        'name' => 'tag_line_' . $i
          ));
}


//=============================================================/////

$page_set ->add_metabox( array(
  'id' => 'home_switch_sections' ,
  'title' => 'Number of Sections',
  'priority' => 'high'
  ));


$page_set->add_field(
  'home_switch_sections', array(
    'type' => 'text',
    'label' => 'Title',
    'name' => 'title'
    ));

$page_set->add_field(
  'home_switch_sections', array(
    'type' => 'text',
    'label' => 'Subtitle',
    'name' => 'subtitle'
    ));

$page_set->add_field(
  'home_switch_sections', array(
    'type' => 'number',
    'label' => '',
    'name' => 'number'
    ));


  $home_sections = get_post_meta( $post_id, 'home_switch_sections');
  $switch_count = (!empty($home_sections[0]['number'])) ? $home_sections[0]['number'] : 1;


for ($i=1; $i <= $switch_count; $i++)
  {
  switch_section_metabox($page_set, $i, 'Home Switch Section ');
  }



  //=====================================================///////



  $page_set ->add_metabox( array(
    'id' => 'large_middle_section',
    'title' => 'Large Middle Section',
    'priority' => 'high',
  	'containers' => array('left', 'right'	)
    ));

  $page_set->add_field(
    'large_middle_section', array(
      'type' => 'text',
      'label' => 'Headline',
      'name' => 'heading',
  		'container' => 'left'
  		));
  $page_set->add_field(
    'large_middle_section', array(
  	  'type' => 'text',
  	  'label' => 'Sub Heading',
  	  'name' => 'sub_heading',
  		'container' => 'left'
  	  ));

  $page_set->add_field(
    'large_middle_section', array(
  	  'type' => 'textarea',
  	  'label' => 'Main Text',
  	  'name' => 'main_text',
  		'container' => 'right',
  		'rows' 		=> 5
  	  ));
  $page_set->add_field(
  		'large_middle_section', array(
  		'type'  => 'cta_btn',
  		'label' => 'Link Text',
  		'name'  => 'link',
  		'container' => 'left'
  ));

  $page_set->add_field('large_middle_section', array(
  				'type' => 'image',
  				'label' => 'Image Select',
  				'container' => 'right',
  				'name' => 'image'
  				));


//=============================================================================////
