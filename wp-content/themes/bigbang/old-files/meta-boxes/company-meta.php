<?php
defined('ABSPATH') or die('Access Denied!');



switch_section_metabox($page_set, 'top');
switch_section_metabox($page_set, 'bottom');



$page_set->add_metabox( array(
			'id' => 'company_heading' ,
			'title' => 'Company Listing Title',
			'priority' => 'high',
			));
$page_set->add_field('company_heading', array(
					'type'  => 'text',
					'label' => '',
					'name'  => 'heading'
					));

$page_set ->add_metabox( array(
  'id' => 'bloglist' ,
  'title' => 'Blog Posts',
  'priority' => 'high'
  ));

$page_set->add_field(
  'bloglist', array(
    'type' => 'text',
    'label' => 'Heading',
    'name' => 'title'
    ));

$page_set->add_field(
  'bloglist', array(
    'type' => 'number',
    'label' => 'Number of Posts',
    'name' => 'number'
    ));
