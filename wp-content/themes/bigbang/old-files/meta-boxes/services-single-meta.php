<?php
defined('ABSPATH') or die('Access Denied!');


//switch_section_metabox($page_set, 'single');
//check_list_section($page_set);
//four_part_section($page_set);


$page_set->add_metabox( array(
			'id' => 'top_section' ,
			'title' => 'Top Section',
			'priority' => 'high',
			));
$page_set->add_field('top_section', array(
					'type'  => 'text',
					'label' => 'Service Headline',
					'name'  => 'title'
					));
$page_set->add_field('top_section', array(
					'type'  => 'rich',
					'label' => 'Service List',
					'name'  => 'list'
					));


$page_set->add_metabox( array(
			'id' => 'mid_section' ,
			'title' => 'Middle Section',
			'priority' => 'high',
			'containers' => array('left', 'right'	)
			));

$page_set->add_field('mid_section', array(
					'type'  => 'textarea',
					'label' => 'Main Text',
					'name'  => 'mid_section_text',
					'container' => 'left'
					));

$page_set->add_field('mid_section', array(
					'type'  => 'image',
					'label' => 'Image',
					'name'  => 'mid_section_image',
					'container' => 'right'
					));

$page_set->add_metabox( array(
			'id' => 'bottom_section' ,
			'title' => 'Bottom Section',
			'priority' => 'high',
			));
$page_set->add_field('bottom_section', array(
					'type'  => 'text',
					'label' => 'Bottom Headline',
					'name'  => 'title'
					));
$page_set->add_field('bottom_section', array(
					'type'  => 'rich',
					'label' => 'Bottom List',
					'name'  => 'bottom_list'
					));


$page_set->add_metabox( array(
			'id' => 'bottom_2_section' ,
			'title' => 'Bottom Section 2',
			'priority' => 'high',
			));
$page_set->add_field('bottom_2_section', array(
					'type'  => 'text',
					'label' => 'Bottom Headline 2',
					'name'  => 'title'
					));
$page_set->add_field('bottom_2_section', array(
					'type'  => 'rich',
					'label' => 'Bottom List 2',
					'name'  => 'list'
					));
link_section_metabox($page_set);
