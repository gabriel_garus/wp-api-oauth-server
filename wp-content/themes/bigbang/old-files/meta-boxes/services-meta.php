<?php
defined('ABSPATH') or die('Access Denied!');

$page_set->add_metabox( array(
			'id' => 'cta_hero' ,
			'title' => 'CTA Hero',
			'priority' => 'high',
		//	'context' => 'side'
			));
$page_set->add_field('cta_hero', array(
					'type'  => 'cta_btn',
					'label' => '',
					'name'  => 'cta_hero'
					));


testimonial_section($page_set);

$page_set->add_metabox( array(
			'id' => 'grid' ,
			'title' => 'Services Listing Title',
			'priority' => 'high',
			));
$page_set->add_field('grid', array(
					'type'  => 'text',
					'label' => '',
					'name'  => 'title'
					));


link_section_metabox($page_set);
