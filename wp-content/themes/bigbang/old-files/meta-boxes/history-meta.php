<?php
//added by Gavin
defined('ABSPATH') or die('Access Denied!');

			switch_section_metabox($page_set, 'top');
			link_section_metabox($page_set);

			$page_set ->add_metabox( array(
			  'id' => 'history_list' ,
			  'title' => 'History Sections',
			  'priority' => 'high',
				'containers' => array('left', 'right')
			  ));

					$page_set->add_field('history_list', array(
						'type'  => 'text',
						'label' => 'Title ',
						'name'  => 'heading_over_all',
						'container'=> 'left'
						));

				for ($i=1; $i < 7 ; $i++)
				{
					$page_set->add_field('history_list', array(
						'type'  => 'text',
						'label' => 'Title '. $i,
						'name'  => 'heading_'. $i,
						'container'=> 'left'
						));
					$page_set->add_field('history_list', array(
						'type'  => 'textarea',
						'label' => 'content '. $i,
						'name'  => 'content_'. $i,
						'container'=> 'left',
						'rows'=> 5
						));
					$page_set->add_field('history_list', array(
						'type'  => 'image',
						'label' => 'image '. $i,
						'name'  => 'historic_image_'. $i,
						'container'=> 'right'
						));

				}
