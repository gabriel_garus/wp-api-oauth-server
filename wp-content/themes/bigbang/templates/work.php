<?php

/*
*Template Name: Work
*
*/

defined('ABSPATH') or die('Access Denied!');


get_header();

$bbPage = new bbPage(PAGE_ID);

$projects_nr = $bbPage->get_field('project_sections', 'number');

include PARTIALS_DIR . 'hero.php';


echo '<div class="section projects">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;




$meta = $bbPage->meta;

$projects = $meta['projects'];

foreach ($projects as $project_id) 
{
	$the_project = new bb_post($project_id);

	$the_project->set_thumbnail_size('hero-wide');

	$p_name  = $the_project->post->post_title;
	$p_title = $the_project->get_field('headline','title');
	$p_url   = $the_project->get_field('www','url');
	$p_tags  = $the_project->get_tag_list(array('links' => false, 'title_li'=>'We Did:') );

	$rel = $the_project->get_field('www','rel');

	echo '<div class="the-project cf">' . PHP_EOL;
	echo '<h2 class="caps">' . $p_name . '</h2>';

	if( $p_title !== null)
	{
		echo '<h3>' . $p_title . '</h3>';
	}
	 

	echo $the_project->wp_image . PHP_EOL;


	echo '<div class="foot cf">';

	if(!empty($p_tags))
	{
		echo '<div class="left">';
		echo $p_tags;
		echo '</div>' ;
	}



	echo '<div class="btn-wrap right">';

	if($p_url !== null)
	{
		echo '<a href="'.$p_url .'" target="_blank" class="btn btn-secondary"';

		if($rel AND $rel == 'nofollow')
		{
			echo ' rel="nofollow"';
		}
		echo '>View Project</a>' . PHP_EOL;
	}
	
	
	echo '</div>' ;
	
	echo '</div>'; // .foot

	echo '</div>' . PHP_EOL; // .the-project
}

echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL;

get_footer();
