<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Template Name: Contact
*/


get_header();
//--------------Get Meta Data----------------
$bbPage = new bbPage(PAGE_ID);


include_once PARTIALS_DIR . 'hero.php';



$options = bb_get_options();


$telephone = (!empty($options['telephone'])) ? $options['telephone'] : null;
$email 	= ( !empty($options['email']) ) ? $options['email'] : null;

if($telephone != null)
{
	$telephone = trim($telephone);
	$tele_short = str_replace(' ', '', $telephone);
}

$social_intro_title = $bbPage->get_field('social_intro','title');
$social_intro_text = $bbPage->get_field('social_intro','text');
$contact_title = $bbPage->get_field('contact_intro','title');

//------------------------------------
if( empty($map) )
{

	if( !empty($meta_data['map'][0]) )
	{
	    $map = unserialize($meta_data['map'][0]);
	}
	elseif( !empty ($meta_data['map']) )
	{
	$map = $meta_data['map'];
	}
	elseif( !empty( $options['longitude'] ) AND !empty( $options['latitude'] ) )
	{
		$map = array();
		$map['longitude'] = $options['longitude'];
		$map['latitude']  = $options['latitude'];
		$map['zoom'] 	  = (!empty($options['zoom'])) ? $options['zoom'] : '7';
	}
	else
	{
		$map = array();

	}

}


$longitude      = ( !empty($map['longitude']) ) ?  $map['longitude'] : '';
$latitude       = ( !empty($map['latitude']) ) ?  $map['latitude'] : '';
$zoom           = ( !empty($map['zoom']) ) ?  $map['zoom'] : '';

$company        = ( !empty($options['title']) ) ? $options['title'] : null;
$address_line_1 = ( !empty($options['addressL1']) ) ? $options['addressL1'] : null;
$address_line_2 = ( !empty($options['addressL2']) ) ? $options['addressL2'] : null;
$address_line_3 = ( !empty($options['addressL3']) ) ? $options['addressL3'] : null;
$city 			= ( !empty($options['city']) ) ? $options['city'] : null;
$area_code 		= ( !empty($options['areaCode']) ) ? $options['areaCode'] : null;


if( !empty($latitude) AND !empty($longitude) )
{


	$map_href = 'https://maps.google.com/maps?q=loc:'.$latitude . ',' . $longitude;

	$map_url = '//maps.googleapis.com/maps/api/staticmap?center=';
	$map_url .= $latitude . ',' . $longitude;
	$map_url .= '&amp;zoom=' . $zoom . '&amp;size=1040x440&amp;scale=2&amp;locations=BigBang';
	$map_url .= '&amp;markers=color:red%7Clabel:B%7C'.$latitude.','.$longitude;
}
else
{
	$map_url = false;
}

$map_title = $bbPage->get_field('map_intro','title');
//---------------------------------------










echo '<div class="contact section">' . PHP_EOL;
echo '<div class="container cf">' . PHP_EOL;

echo '<div class="left main ">' . PHP_EOL;

if($contact_title !== null)
{
	echo '<h3 class="caps">' . $contact_title . '</h3>' . PHP_EOL;
}


if($telephone !== null)
{
	echo '<a href="tel:'.$tele_short.'" class="telephone">'. $telephone . '</a> '.PHP_EOL;
}


if( $email !== null )
{
	echo '<a class="email" href="mailto:'. $email .'" >'. $email .'</a>'.PHP_EOL;
}



echo '<div class="address caps">' . PHP_EOL;
echo '<span>' . $company . '</span>' . PHP_EOL;
echo '<span>' . $address_line_1 . '</span>' . PHP_EOL;
echo '<span>' . $address_line_2 . '</span>' . PHP_EOL;
echo '<span>' . $address_line_3 . '</span>' . PHP_EOL;
echo '<span>' . $city . ' ' . $area_code . '</span>' . PHP_EOL;
echo '</div>' . PHP_EOL; //. address




echo '</div>' . PHP_EOL; //.left



echo '<div class="right aside">' . PHP_EOL;
	
	if( $social_intro_title !== null )
	{
		echo '<h3 class="caps">' . $social_intro_title . '</h3>';
	}

	if ( $social_intro_text )
	{
		echo '<p>' . $social_intro_text . '</p>' . PHP_EOL;
	}
	

	include_once PARTIALS_DIR . 'social-links-invert.php';





echo '</div>' . PHP_EOL; //right main


echo '</div>' . PHP_EOL; //.container
echo '</div>' . PHP_EOL; //.contact-section




if( $map_url )
{
	echo '<div class="section location">';
	echo '<div class="container">' . PHP_EOL;

	if($map_title )
	{
		echo '<h3 class="caps">' . $map_title . '</h3>' . PHP_EOL;
	}
	
	echo '<div class="google-map">';
	echo '<a href="' . $map_href .'" target="_blank">';
	echo '<img src="' . $map_url . '" alt="Location Map" />';
	echo '</a></div>'. PHP_EOL;

	echo '</div>';
	echo '</div>' . PHP_EOL;

}

get_footer();
