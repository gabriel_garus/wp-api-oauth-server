<?php
defined('ABSPATH') or die('Access Denied!');
/**
*Template Name: Full Width 
 */

 

get_header();

if(!isset($bbPage))
{
  $bbPage = new bbPage(PAGE_ID);
}


$title     = $bbPage->title;
$text      = $bbPage->content;


echo '<div class="section full-width">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;
echo '<h1>' . $title  . '</h1>';

echo do_shortcode(wpautop( $text ) );

echo '</div></div>' . PHP_EOL;


echo '<script type="text/javascript"> ' . PHP_EOL;
echo '	$(document).ready(function(){ ' . PHP_EOL;

echo '	});'. PHP_EOL;
echo '</script> ' . PHP_EOL;
get_footer();