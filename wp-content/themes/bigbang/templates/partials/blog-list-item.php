<?php
defined('ABSPATH') or die('Access Denied!');
/*
*News list partial
*/




// initialize post object.
if(empty($article_id))
{
	$article_id = $post->ID;
}




$blog_post 	= new bb_post( $article_id );
//dump($blog_post);


// get variables
$meta = $blog_post->meta;

$post_title   			= $blog_post->leadin_title;
$post_excerpt 			= strip_tags($blog_post->excerpt);
$post_date  			= $blog_post->get_date(array('sup'=> false ));
$post_href  			= $blog_post->url;
$post_icons    			= $blog_post->get_icons(28, array('li_class'=> false));
$post_author_first_name = $blog_post->author['first_name'];
$post_author_last_name  = $blog_post->author['last_name'];
$post_tags 				= $blog_post->get_tag_list();
$the_author 			= $blog_post->author;
$thumb_id  				= $blog_post->leadin_image;


$article_class = '';

if(!isset($blog_list_intro))
{
	$blog_list_intro = false; 
}

if($blog_list_intro)
{
	$article_class .= 'with-intro';
}


echo '<article';
if(!empty($article_class))
{
	echo ' class="'.$article_class.'" ';
}
echo '>' . PHP_EOL;
echo '<header class="cf">'.PHP_EOL;
echo '<div class="left">' . PHP_EOL;
echo '<h2 itemprop="headline"><a href="' . $post_href . '">' . $post_title . '</a></h2>'.PHP_EOL;

// --- post meta
echo '<div class="article-meta cf">'.PHP_EOL;
	echo '<div class="article-date">' . PHP_EOL;
	echo $post_date;
	echo '</div>' . PHP_EOL;

	echo '<div class="article-tags bold">';
	echo '<span class="title">Tags:</span>'.$post_tags;
	echo '</div>';
echo '</div>';
//---------------------------


if($blog_list_intro)
{
	echo wpautop( $post_excerpt );
}
echo '</div>' . PHP_EOL; //.left
//---------------------------

if(!empty($thumb_id))
{	
	echo '<div class="post-thumbnail">' . PHP_EOL;
	echo $blog_post->get_wp_leadin_image();
	echo '</div>'.PHP_EOL;
}


echo '</header>'.PHP_EOL;
//---------------------------
// --- Read More button

	if(!empty($post_href))
	{
		echo '<div class="btn-wrap read-more">';
		echo '<a itemprop="url" href="'. $post_href .'"  class="btn btn-secondary">';
			_e('Read Article' , 'bigbang');
		echo '</a>';
		echo '</div>' . PHP_EOL;
	}

echo '</article>'.PHP_EOL;

$article_id = null; 
