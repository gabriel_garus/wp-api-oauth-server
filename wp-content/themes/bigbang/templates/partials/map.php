<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Map partial
*/

//----get meta_data from big bang Redux-------

if(empty($options))
{
	$options = (!empty($_SESSION['bb_options'])) ? $_SESSION['bb_options'] : get_option('bigbang');
}


if( !empty( $options['longitude'] ) AND !empty( $options['latitude'] ) )
{
	$map = array();
	$map['longitude'] = $options['longitude'];
	$map['latitude'] = $options['latitude'];
	$map['zoom'] = (!empty($options['zoom'])) ? $options['zoom'] : '7';
}
else
{
	$map = array();
}

//-------declare Variables-------------


$longitude = ( !empty($map['longitude']) ) ?  $map['longitude'] : '';
$latitude = ( !empty($map['latitude']) ) ?  $map['latitude'] : '';
$zoom = ( !empty($map['zoom']) ) ?  $map['zoom'] : '';
