<?php

if(is_front_page())
{
	if(empty($options))
	{
		$options = bb_get_options();
	}

	$telephone = (!empty($options['telephone'])) ? $options['telephone'] : null;
	
	if($telephone != null)
	{
		$telephone = trim($telephone);
		$tele_short = str_replace(' ', '-', $telephone);
		$CompanyName = (!empty($options['tradeName'])) ? $options['tradeName'] : null;

		echo '<script type="application/ld+json">' . PHP_EOL;
		echo '{' . PHP_EOL;
		echo '	"@context" : "http://schema.org",' . PHP_EOL;
		echo '	"@type" : "Organization",' . PHP_EOL;
		echo '	"url" : "'. WP_HOME .'",' . PHP_EOL;
		
		if($CompanyName !== null)
		{
			echo '	"name" : "'. $CompanyName .'",' . PHP_EOL;
		}

		echo '	"contactPoint" : [{' . PHP_EOL;
		echo '	  "@type" : "ContactPoint",' . PHP_EOL;
		echo '	  "telephone" : "' . $tele_short . '",' . PHP_EOL;
		echo '	  "contactType" : "customer service"' . PHP_EOL;
		echo '	}]' . PHP_EOL;
		echo '}' . PHP_EOL;
		echo '</script>' . PHP_EOL;
	}
}