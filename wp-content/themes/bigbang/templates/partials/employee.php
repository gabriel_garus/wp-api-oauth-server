<?php
defined('ABSPATH') or die('Access Denied!');
/*
* Employee Partial
*/


//--------------Get Meta Data----------------

$args = array( 'post_type' => 'employees', 'posts_per_page' => -1, 'order'=>'ASC');
$team = get_posts( $args );

echo '<div class="employee-section">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;
// echo '<h3 class="caps">The Team</h3>' . PHP_EOL;

echo '<div class="the-grid">' . PHP_EOL;


foreach ($team as $member)

{
  //dump($member);

  $employee_post = new bbPage($member->ID);
  $employee_meta = $employee_post->meta;



  echo '<div class="grid-element">' . PHP_EOL;

  $first_name  = $employee_post->get_field('employee', 'first_name' );
  $family_name = $employee_post->get_field('employee', 'family_name' );
  $position    = $employee_post->get_field('employee', 'position' );

  $employee_post->set_thumbnail_size('avatar');


  echo $employee_post->wp_image;

  echo '<div itemscope itemtype="http://schema.org/Person">' . PHP_EOL;
  echo '<div class="name" itemprop="name">' . PHP_EOL;

  $check = $first_name . $family_name;

  if( !empty( $check  ) )
  {
     if( $first_name !== null )
     {
       echo '<span itemprop="givenName">' . $first_name . '</span>' . PHP_EOL;
     }

     if( $family_name !== null )
     {
       echo '<span itemprop="familyName">' . $family_name . '</span>' . PHP_EOL;
     }
  }
  else
  {
     echo $member->post_title;
  }

  
 
  echo '</div>' . PHP_EOL; //.name

  if( $position !== null )
  {
    echo '<span itemprop="jobtitle">' . $position . '</span>' . PHP_EOL;
  }

  echo '</div>' . PHP_EOL;//.person
  echo '</div>'; //.grid-element
}
echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL; //.container
echo '</div>' . PHP_EOL; //.employee-section
