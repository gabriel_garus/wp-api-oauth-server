<?php

defined('ABSPATH') or die('Access Denied!');
/*
*Quotes Partial
*
*/



//--------------Get Meta Data----------------

if(!empty($quote_id))
{

	$quote_post = new bbPage($quote_id);
	$post_meta 	= $quote_post->meta;


	$my_post_id = $quote_id;//This is page id or post id
	$content = $quote_post->content;

	$first_name   = $quote_post->get_field('quote','first_name');
	$family_name  = $quote_post->get_field('quote','family_name');
	$position 	  = $quote_post->get_field('quote','position');
	$company 	  = $quote_post->get_field('quote','company_name');
	$company_url  = $quote_post->get_field('quote','company_url');
	$personal_url = $quote_post->get_field('quote','personal_url');
	//$quote_post->set_thumbnail_size(array('width'=> 200, 'height'=> 40 )); 
	$quote_post->set_thumbnail_class('quote-thumb');

	echo '<div class="section quote big">' . PHP_EOL;
	echo '<div class="container">' . PHP_EOL;
	echo '<div class="wrap">' . PHP_EOL;

	echo '<div class="the-quote">'.PHP_EOL;
	echo wpautop($content) . PHP_EOL;
	echo '</div>' . PHP_EOL;

	echo '<div class="bottom">' . PHP_EOL;


	


	echo '<div itemscope itemtype="http://schema.org/Person" class="person">' . PHP_EOL;

	echo '<div itemprop="name" class="name">' . PHP_EOL;
	if($personal_url !== null)
	{
		echo '<a href="'. $personal_url . '" rel="nofollow" target="_blank" class="blue">';
	}
	echo '<span itemprop="givenName">' . $first_name . '</span>' . PHP_EOL;
	echo '<span itemprop="familyName">' . $family_name . '</span>,' . PHP_EOL;
	if($personal_url !== null)
	{
		echo '</a>';
	}

	echo '</div>' . PHP_EOL; //.name

	echo '<span itemprop="jobtitle">' . $position . '</span>' . PHP_EOL;
	
	echo '</div>' . PHP_EOL;//.person


	if(!empty($quote_post->thumbnail))
	{
		$company_logo = $bbPage->get_image($quote_post->thumbnail, array('width'=>200, 'height'=>40 ));

		if($company_url !== null)
		{
			echo '<a href="'. $company_url . '" rel="nofollow" target="_blank" class="image">';
		}
	 	echo $company_logo['html'];
	 	if($company_url !== null)
		{
			echo '</a>';
		}

	}


	echo '</div>' . PHP_EOL;//.bottom


	echo '</div>'; //.wrap
	echo '</div>'; //.container
	echo '</div>' . PHP_EOL; //.quote

		







}
else
{
	echo '<!-- Quote ID is missing -->' . PHP_EOL;
}