<?php
defined('ABSPATH') or die('Access Denied!');
/*
*hero about partial
*/


if(empty($bbPage) OR !is_object($bbPage))
{
  $bbPage = new bbPage(PAGE_ID);
}

//----------bb_extract()  located in the inc folder-----------

$hero_title     = $bbPage->title;
$hero_text      = $bbPage->content;

$hero_img  = $bbPage->wp_image;




//----------------------------------------------------------------------------


echo '<section class="hero multi">'  .  PHP_EOL;

echo '<div class="container">' . PHP_EOL;

echo '<div class="left">' . PHP_EOL;

echo '<h1>' . $hero_title . '</h1>' . PHP_EOL;

echo wpautop( do_shortcode( $hero_text ) );

echo $hero_img;

echo '</div>' . PHP_EOL; // .left

echo '<div class="right">' . PHP_EOL;

$quote_id = $bbPage->get_field('hero_quote_select', 'quote_id'); 
include_once PARTIALS_DIR . 'quote-aside.php';

echo '</div>' . PHP_EOL; //.right

echo '</div>' . PHP_EOL; //.container
echo '</section>' . PHP_EOL; //.hero
