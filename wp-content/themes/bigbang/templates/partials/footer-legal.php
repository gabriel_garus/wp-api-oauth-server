<?php
defined('ABSPATH') or die('Access Denied!');
/*
*footer legal partial
*/


//-----get meat data from bigbang Redux----------
if(empty($options))
{
	$options = bb_get_options();
}


//-------declare Variables--------------
$trade_name = ( !empty($options['tradeName']) ) ? $options['tradeName'] : null;
$country_code = ( !empty($options['registration']) ) ? $options['registration'] : null;
$copyright = ( !empty($options['disclaimer']) ) ? $options['disclaimer'] : null;

$copyright = trim($copyright);

if( empty($copyright) )
{
	$copyright = 'All rights reserved';
}

echo '<div class="legal">' . PHP_EOL;
if( ( is_front_page() ) )
{
	echo '<span itemprop=\'legalName\'>';
}

else
{
	echo '<span>' . PHP_EOL;
}

echo '<span>Copyright ';
echo '<span class="copy">&copy;</span> ';
echo date("Y") . ' ';
echo $trade_name . ' (' . $country_code . ')';
echo ' '. $copyright;
echo '</span>';




echo '</div>' . PHP_EOL; // .legal
