<?php
echo '<ul class="article-social">';

echo '<li class="facebook">' . PHP_EOL;
echo '<div class="fb-like" data-href="'. $post_url .'" ';
echo 'data-layout="box_count" data-action="like" ';
echo 'data-show-faces="false" data-share="false"></div>';
echo '</li>';

echo '<li class="twitter">';
echo '<a class="twitter-share-button" href="https://twitter.com/share" ';
echo 'data-url="' . $post_url .'" data-count="vertical">Tweet</a>' . PHP_EOL;

echo '<script>';
echo '!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';';
echo 'if(!d.getElementById(id)){js=d.createElement(s);';
echo 'js.id=id;js.src=p+\'://platform.twitter.com/widgets.js\';';
echo 'fjs.parentNode.insertBefore(js,fjs);}}(document, \'script\', \'twitter-wjs\');';
echo '</script>' . PHP_EOL;
echo '</li>';

echo '<li class="linkedin">';
echo '<script src="//platform.linkedin.com/in.js" type="text/javascript">';
echo ' lang: en_US ' . PHP_EOL;
echo '</script>' . PHP_EOL;
      
echo '<script type="IN/Share" data-url="' . $post_url . '" data-counter="top"> </script>' . PHP_EOL;
echo '</li>';

echo '<li class="googleplus">';
echo '<div class="g-plus" data-action="share" data-annotation="vertical-bubble" data-height="70"></div>';
echo '</li>';
echo '</ul>';