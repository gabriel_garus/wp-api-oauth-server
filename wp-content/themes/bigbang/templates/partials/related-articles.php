<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Related articles partial
*/


//-----Get meta_data---------


//bb_extract()  located in the inc folder

if(empty($meta_data))
{

  $meta_data = bb_extract( get_post_meta(PAGE_ID));
}



$related 	= (!empty($meta_data['related'])) ? $meta_data['related'] : null;
$heading 	= ( !empty($related['heading']) ) ? $related['heading'] : 'Most Popular';
$nr 		= ( !empty($related['nr']) ) ? $related['nr'] : 3;


	echo '<div class="related ws-sticky blog-list">' . PHP_EOL;


	if($heading !== null)
	{
		echo '<h2 class="caps">'. $heading .'</h2>'. PHP_EOL;
	}


	if($related !== null AND ( !empty($related['article_1']) OR !empty($related['article_2']) OR !empty($related['article_3']) ) )
	{

		for ( $i=1; $i <= $nr; $i++ )
		{
			$article_name = 'article_' . $i;
			if(!empty($related[ $article_name ]) AND $related[ $article_name ] != '0')
			{
				$article_id = $related[ $article_name ];
	
				// bb_rel_post($id);
				include PARTIALS_DIR . 'blog-list-item.php';
	   
			}
		}

	}
	else
	{
		$rel_articles = get_posts( array(
				'post_type' => 'post',
				'posts_per_page' => $nr,
				'exclude' => PAGE_ID
			));

		foreach ($rel_articles as $article) 
		{

			$article_id = $article->ID;
			include PARTIALS_DIR . 'blog-list-item.php';
	   
		}


	}

	echo '</div>' . PHP_EOL;



function bb_rel_post($id)
{
	$rel_post = new bb_post($id);

	$url   = $rel_post->url;
	$title = $rel_post->leadin_title;
	$date  = $rel_post->get_date();

	$rel_post->set_thumbnail_size('th-small');
			
	echo '<article>';
	
		echo '<table><tr>';
		if(!empty($url))
		{
			echo '<td class="head"><h3><a href="' . $url . '">' . $title . '</a></h3></td>';
		}

		echo '<td class="img">' . $rel_post->wp_image . '</td>';
		echo '</tr></table>' . PHP_EOL;
		echo $date;
				

	echo '</article>' . PHP_EOL;
}

?>