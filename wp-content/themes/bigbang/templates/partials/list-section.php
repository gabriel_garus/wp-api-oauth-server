<?php

defined('ABSPATH') or die('Access Denied!');

if(empty($bbPage) OR !is_object($bbPage))
{
  $bbPage = new bbPage(PAGE_ID);
}

$list_id = 'list_section';

$list_count   = $bbPage->get_field($list_id,'number'); 
$headline     = $bbPage->get_field($list_id,'title'); 


echo '<div class="section list-section">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;

if($headline !== null)
{
	echo '<h2>' . $headline . '</h2>' . PHP_EOL;
}


echo '<div class="the-list">' . PHP_EOL;



for ($i=1; $i <= $list_count; $i++)
{

	
	echo '<div class="box">'. PHP_EOL;
	$li_icon  		= $bbPage->get_field($list_id, 'icon_' . $i);
	$li_heading     = $bbPage->get_field($list_id, 'heading_'.$i);
 	$li_sub_heading = $bbPage->get_field($list_id, 'sub_heading_'.$i);
 	$li_text        = $bbPage->get_field($list_id, 'text_'.$i);

 	echo '<div class="li-head cf">' ;
 	if($li_icon !== null)
 	{
 		$icon_url = wp_get_attachment_url( $li_icon );
 		echo '<img src="'.$icon_url .'" alt="colour icon" width="80" height="80" />';
 	}

 	echo '<div class="li-headers">';
	echo '<h3>' . $li_heading . '</h3>';
	echo '<h4>' .$li_sub_heading. '</h4>';
	echo '</div>' . PHP_EOL;
	echo '</div>' . PHP_EOL;
	echo '<p>' . $li_text . '</p>' . PHP_EOL;
	echo '</div>' . PHP_EOL;

	if($i != $list_count)
	{
		echo '<div class="li-divider"></div>';
	}
}

echo '</div>';




echo '</div>' ; //.container
echo '</div>' . PHP_EOL;// .list-section
