<?php
/*
* Pagination
* by: Gabriel Garus
* for: Big Bang Design Limited
*/


$page_size = get_option('posts_per_page');

$all =  wp_count_posts();
$all_posts = $all->publish;

$pages = $all_posts / $page_size;
$pages = ceil($pages);

//$thePostId = get_option( 'page_for_posts' );
$blogUrl = get_permalink( PAGE_ID );

$uri = $_SERVER['REQUEST_URI'];
$ex = explode('page/', $uri);
$current = (int)rtrim( end($ex), '/');
//echo 'Pages: ' . $pages . '<br>';
$prevLink = (get_previous_posts_link('')) ? get_previous_posts_link('') : '' ;
$nextLink = (get_next_posts_link( '' )) ? get_next_posts_link( '' ) : '';

$l_state = (empty($prevLink)) ? 'disabled' : 'active';
$r_state = (empty($nextLink)) ? 'disabled' : 'active';

/*
error_log('$page_size: ' . $page_size);
error_log('$all_posts: ' . $all_posts);
error_log('$blogUrl: ' . $blogUrl);
error_log('$prevLink: ' . $prevLink);
error_log('$nextLink: ' . $nextLink);
error_log('$uri: ' . $uri);
error_log('$current: ' .  $current);
*/

if($pages > 1 )
{
	echo '<nav class="pagination">'.PHP_EOL;
	echo '<div class="navi navi-left '.$l_state.'">';
	echo $prevLink;
	echo '</div>'.PHP_EOL;

	for($e=1; $e <= $pages; $e++)
	{

		if($e === $current || ($e === 1) && ($current === 0))
		{
			$state = 'disabled';
			$in = '<span>' . $e . ' </span>';
		}
		else
		{
			$state = 'active';
			$pagi_link = ($e === 1) ? $blogUrl : $blogUrl . 'page/'. $e . '/';
			$in = '<a href="'.$pagi_link.'" >'. $e . '</a> ';
		}

		echo '<div class="nr '.$state.'">'.$in.'</div>' . PHP_EOL;

	}



	echo '<div class="navi navi-right '.$r_state.'">';
	echo $nextLink;
	echo '</div>'.PHP_EOL;
	echo '</nav>';
}
