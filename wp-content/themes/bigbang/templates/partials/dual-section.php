<?php

defined('ABSPATH') or die('Access Denied!');

if(!isset($bbPage))
{
	$bbPage = new bbPage(PAGE_ID);
}

$this_section = 'underline';

$left_heading 	  = $bbPage->get_field( $this_section , 'left_heading' );
$left_sub_heading = $bbPage->get_field( $this_section , 'left_sub_heading' );
$right_heading 	  = $bbPage->get_field( $this_section , 'right' );

$chk = $left_heading . $left_sub_heading . $right_heading;
if( !empty( $chk ) )
{
	echo '<div class="dual-section">' . PHP_EOL;
	echo '<div class="container">' . PHP_EOL;
	echo '<div class="left">' . PHP_EOL;
	
	if($left_heading !== null)
	{
		echo '<h3>' . $left_heading . '</h3>' . PHP_EOL;
	}

	if($left_sub_heading !== null )
	{
		echo  '<p>' . $left_sub_heading . '</p>' . PHP_EOL;
	}
	

	echo '</div>' . PHP_EOL; //.left
	echo '<div class="right">' . PHP_EOL;

	if($right_heading !== null)
	{
		echo '<h3>' . $right_heading . '</h3>' . PHP_EOL;
	}


	echo '</div>'; //.right
	echo '</div>'; //.container
	echo '</div>' . PHP_EOL; //.dual-section
}


