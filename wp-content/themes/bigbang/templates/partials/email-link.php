<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Email link partial
*/


//------conditional for rich snippets--------
if ( is_front_page() )
{
	$md_email = 'itemprop="email"';
}
else
{
	$md_email = null;
}

//-------get meta data from bigbang Redux-------

if( empty($options) )
{
	$options = bb_get_options();
}

//--------Declare Variables---------------
$email 	= ( !empty($options['email']) ) ? $options['email'] : null;

//------------------------------------

if( $email !== null )
{
	echo '<a '.$md_email .' href="mailto:'.$email.'" >' . $email . '</a>'.PHP_EOL;
}