<?php

defined('ABSPATH') or die('Access Denied!');


/*
*Switch Section for the work page
*
*/




echo '<div class="switch">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;
echo '<div class="left">' . PHP_EOL;
echo '<h3>' . $switch_title . '</h3>' . PHP_EOL;
echo  wpautop( $switch_text ) . PHP_EOL;
echo '<a class="btn btn-secondary" target="_blank" href="'. $switch_url .'">View Website</a>' . PHP_EOL;
echo '</div>' . PHP_EOL; //.left
echo '<div class="right">' . PHP_EOL;
echo wp_get_attachment_image($switch_img, 'hero');
echo '</div>' . PHP_EOL; //.right
echo '</div>' . PHP_EOL; //.container
echo '</div>' . PHP_EOL; //.work-switch


