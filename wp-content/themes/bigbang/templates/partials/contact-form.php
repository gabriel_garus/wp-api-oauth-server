<?php
if(empty($bbPage) OR !is_object($bbPage))
{
  $bbPage = new bbPage(PAGE_ID);
}

if(empty($options))
{
	$options = bb_get_options();
}



$submit_label = $bbPage->get_field( $section_id, 'submit_label' );
$form_action =  $bbPage->get_field( $section_id, 'form_action' );
$form_fields_nr = (!empty($options['form_fields_number'])) ? $options['form_fields_number'] : 0;

if(empty($form_action))
{
	$form_action = (!empty($options['form_action'])) ? $options['form_action'] : '#';
}

if(empty($submit_label))
{
	$submit_label = (!empty($options['form_submit_label'])) ? $options['form_submit_label'] : 'send';
}

echo '<form class="contact-form" action="' . $form_action . '">' . PHP_EOL;

for ($i=1; $i <= $form_fields_nr ; $i++) 
{ 
	$label_id = 'field_label_'.$i;
	$field_label = (!empty($options[$label_id])) ? $options[$label_id] : null;

	$field_type_id = 'field_type_'.$i;
	$field_type = (!empty($options[$field_type_id])) ? $options[$field_type_id] : null;

	$field_name_id = 'field_name_'.$i;
	$field_name = (!empty($options[$field_name_id])) ? strtolower($options[$field_name_id]) : null;
	
	$field_req_id = 'required_'.$i;
	$field_required = (!empty($options[$field_req_id]) AND $options[$field_req_id] == '1') ? true : false;


	if($field_name === null)
	{
		$field_name = strtolower( str_replace(' ', '_', $field_label) );
	}
	$required = ($field_required) ? ' required' : '';
	echo '<div class="field'.$required.'">'.PHP_EOL;
	echo '<label for="'.$field_name.'">'.$field_label.'</label>';
	echo '<input type="'.$field_type.'" id="'.$field_name.'" name="'.$field_name.'"'.$required.' />';
	echo '</div>' . PHP_EOL;

}


echo '<div class="field">'.PHP_EOL;
echo '<input type="submit" value="' .$submit_label . '" />';
echo '</div>' . PHP_EOL;


echo '</form>' . PHP_EOL;
