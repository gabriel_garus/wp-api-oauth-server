<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Switch section partial
*/

//------Check $hr_divider from front-page.php
if(!isset($hr_divider))
{
  $hr_divider = true;
}



//-------Declare Variables-----------


if(!empty($section))
{	
	$switch_title    = $bbPage->get_field( $section, 'heading');
	$switch_text     = $bbPage->get_field( $section, 'text');
	$switch_btn_href = $bbPage->get_field( $section, 'link');
	$switch_btn_lbl  = $bbPage->get_field( $section, 'label');
	$switch_img_id   = $bbPage->get_field( $section, 'image');



	if($switch_img_id  !== '0')
	{
		$switch_img =  wp_get_attachment_image( $switch_img_id,  'switch-home' );
	}
	else
	{
	 	$switch_img = null;
	}


	$btn_class = 'more';
	//---------------------------------------------------------------

	echo '<div class="home-switch">' . PHP_EOL;

	echo '<div class="container">' . PHP_EOL;
	echo '<div class="left">' . PHP_EOL;
  
  	if(!empty($switch_title))
	{
		echo '<h3>' . $switch_title . '</h3>' . PHP_EOL;
	}

	if(!empty($switch_text))
	{
		echo '<p>' . $switch_text . '</p>' . PHP_EOL;
	}

	if(!empty($switch_btn_href))
	{
		echo '<a href ="' . get_permalink($switch_btn_href) . '" class="'. $btn_class .'">' . $switch_btn_lbl . '</a>' . PHP_EOL;
	}


	echo '</div>'; // .left  !Do not add PHP_EOL here as these divs need to be positioned as inline-block (gg)

	echo '<div class="right">' . PHP_EOL;
  if($switch_img != null)
  {
    echo  $switch_img . PHP_EOL;
  }

//===============



	echo '</div>' . PHP_EOL; // .right
	echo '</div>' . PHP_EOL; // .container

	echo '</div>' . PHP_EOL; //.home-switch

}
