<?php
defined('ABSPATH') or die('Access Denied!');
/*
* expertise hero partial
*/


//--------------Get Meta Data----------------
if(empty($meta_data))
{
  $meta_data = bb_extract( get_post_meta(PAGE_ID) );
}
//----------bb_extract()  located in the inc folder-----------

$hero_title     = (!empty($meta_data['headline']['title'])) ?  $meta_data['headline']['title'] : get_the_title();
$hero_text      = do_shortcode(get_the_content());
$hero_btn_href  = (!empty($meta_data['cta_hero']['cta_hero_link'])) ?  $meta_data['cta_hero']['cta_hero_link'] : null;
$hero_btn_lbl   = (!empty($meta_data['cta_hero']['cta_hero_label'])) ?  $meta_data['cta_hero']['cta_hero_label'] : null;
$the_class      = (!empty($hero_class)) ? ' ' . $hero_class : null;

$hero_img_id  = get_post_thumbnail_id();

$img_class = 'expertise-img';



$is_front = (is_front_page()) ? true : false;

if(!$is_front)
{
	$hero_img_itemprop = 'primaryImageOfPage';
}
else
{
	$hero_img_itemprop = null;
}

$hero_image = bb_get_the_image( $hero_img_id  , array(
 'class' => 'expertise-img',
 'size' => 'large'
 ));

//----------------------------------------------------------------------------


echo '<section class="expertise-hero">'  .  PHP_EOL;

echo '<div class="container">' . PHP_EOL;

echo '<div class="left">' . PHP_EOL;
if($is_front)
  {
  	echo '<h2>' . $hero_title . '</h2>' . PHP_EOL;
  }
else
  {
 	 echo '<h1>' . $hero_title . '</h1>' . PHP_EOL;
  }

	echo '<p>' . $hero_text . '</p>';


  if(!empty($hero_image['html']))
  {
    echo $hero_image['html'];
  }


/* CTA Button
if($hero_btn_href != null)
{
  echo '<a href="' . get_permalink($hero_btn_href) . '"  class="btn btn-primary">' . $hero_btn_lbl . '</a>' . PHP_EOL;
}
*/
echo '</div>' . PHP_EOL; // .left
echo '<div class="right">' . PHP_EOL;
