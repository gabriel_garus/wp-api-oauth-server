<?php
defined('ABSPATH') or die('Access Denied!');
/*
*To be included in the header
*/


echo '<header class="snap-drawers section">' . PHP_EOL;
echo '<div class="snap-drawer snap-drawer-right container">' . PHP_EOL;

echo '<div id="mobile-menu" class="ws-sticky">' . PHP_EOL;
echo '<button id="snap-close" type="button" aria-label="Close"><span aria-hidden="true">×</span></button>' . PHP_EOL;

bb_menu( 'primary', array('search' => false) );

echo '</div>'; 

echo '</div>' . PHP_EOL; // END: .snap-drawer
echo '</header>' . PHP_EOL; // END: .snap-drawers

//------------------------------------------------
