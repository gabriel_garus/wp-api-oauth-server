<?php
defined('ABSPATH') or die('Access Denied!');
/*
*hero partial
*/


//--------------Get Meta Data----------------
if(empty($bbPage) OR !is_object($bbPage))
{
  $bbPage = new bbPage(PAGE_ID);
}
//----------bb_extract()  located in the inc folder-----------

$hero_title     =  $bbPage->title;


$hero_subtitle  = $bbPage->get_field('headline', 'hero-subtitle'); 

$hero_text      = $bbPage->content;

$hero_btn_href  = $bbPage->get_field('cta_hero', 'cta_hero_link'); 
$hero_btn_lbl   =  $bbPage->get_field('cta_hero', 'cta_hero_label');

$hero_img_id = $bbPage->thumbnail;


//----------------------------------------------------------------------------


//***********************************************************************************************


echo '<section class="hero-home">' ;
echo '<div class="container cf">' . PHP_EOL;


echo '<div class="left">';

if($hero_title !== null)
{
	echo '<h2>' . $hero_title . '</h2>';
}

if(!empty($hero_text))
{
	echo '<div class="hero-text">';
	echo wpautop( do_shortcode( $hero_text ), true );
	echo '</div>';
}

if($hero_btn_href != null)
{
  echo '<div class="cta-btn btn-wrap">';
  echo '<a href="' . get_permalink($hero_btn_href) . '"  class="btn btn-primary">' . $hero_btn_lbl . '</a>';
  echo '</div>';
}

echo '</div>'  . PHP_EOL; // .left


// ------

echo '<div class="right">';
if( !empty($hero_img_id))
{
	$hero_img = $bbPage->get_image( null , array('width'=>577, 'height'=>431));
	echo $hero_img['html'];
}
echo '</div>' . PHP_EOL; //right


// ------

echo '</div>' . PHP_EOL; // .container

include PARTIALS_DIR . 'clients.php';





echo '</section>' . PHP_EOL; // .hero


