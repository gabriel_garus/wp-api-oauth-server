<?php

//======================SWITCH SECTION=======================================
$sections_count      = $bbPage->get_field('home_sections', 'number');
$sections_heading    = $bbPage->get_field('home_sections', 'title');
$sections_subheading = $bbPage->get_field('home_sections', 'sections-subtitle');

if(empty($sections_count))
{
	$sections_count = 3;
}


echo '<div class="home-sections">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;

if($sections_heading !== null)
{
	echo '<h3>' . $sections_heading .'</h3>' . PHP_EOL;
}

if($sections_subheading !== null)
{
	echo '<h5>' . $sections_subheading . '</h5>' . PHP_EOL;
}


for ($i=1; $i <= $sections_count; $i++)
{
	$section = 'switch_'.$i;

	include PARTIALS_DIR . 'home-section.php';
	
}


echo '</div>' . PHP_EOL;// . container

echo '</div>' . PHP_EOL;// .home sections


//=============================Lsit Section=============================================
