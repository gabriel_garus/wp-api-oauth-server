<?php
defined('ABSPATH') or die('Access Denied!');
/*
*article author partial
*/


if( empty($author) )
{
	$author = array();
}
//Get meta data


$author_img_id 		= ( !empty($author['img_id']) ) ? $author['img_id'] :  null;
$author_first_name  = ( !empty($author['first_name']) ) ? $author['first_name'] : null;
$author_sir_name 	= ( !empty($author['last_name']) ) ? $author['last_name'] : null;
$author_position	= ( !empty($author['position']) ) ? $author['position'] : null;

if($author_img_id !== null)
{
	$author_img = wp_get_attachment_image( $author_img_id 	, 'avatar-small' );
}
else { $author_img = null; }


if( !empty($author_first_name) AND !empty($author_sir_name) )
{
	$full_name = '<span itemprop="givenName">' . $author_first_name .  '</span> <span itemprop="familyName">' . $author_sir_name . '</span>';
	$full_name_raw = $author_first_name . ' ' . $author_sir_name;
}

else
{
	$full_name = '<span itemprop="givenName">' . get_the_author() . '</span>';
	$full_name_raw = get_the_author();
}

$author_text	= ( !empty($author['bio']) ) ? $author['bio'] : null;
$author_href	= ( !empty($author['social_url']) ) ? $author['social_url'] : null;


$author_social_icon_url = '/img/icons/social/linkedin.svg';






echo '<div itemprop="author" itemscope="" itemtype="https://schema.org/Person" class="author table">' . PHP_EOL;

if($author_img !== null)
{


	echo '<div class="author-img cell">' . PHP_EOL;
	echo $author_img;
	echo '</div>' . PHP_EOL;
}

echo '<div class="bio cell">' . PHP_EOL;
echo '<h3>' . $full_name . '</h3>' . PHP_EOL;
echo '<span itemprop="description">' . $author_text . '</span>';
echo '</p>' . PHP_EOL;
echo '</div>'. PHP_EOL; // .bio

echo '</div>' . PHP_EOL; // .author
