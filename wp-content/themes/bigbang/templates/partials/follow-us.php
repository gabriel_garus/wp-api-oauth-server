<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Flat section with left side text and img to the right
*/



if( empty($follow_head) )
{
	$company = get_bloginfo('blog_name');
	$follow_head = 'Follow ' . $company ;
}

	echo '<div class="social invert">'.PHP_EOL;

if(!empty($follow_head))
{
	echo '<h3>' . $follow_head . '</h3>'.PHP_EOL;
}
if( !empty($follow_text) )
{
	echo '<p>' . $follow_text . '</p>'.PHP_EOL;
}

include PARTIALS_DIR . 'social-links-invert.php';
echo '</div>'.PHP_EOL;
