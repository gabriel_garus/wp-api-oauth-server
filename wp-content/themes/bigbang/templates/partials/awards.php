<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Awards partial
*/


if(empty($bbPage) OR !is_object($bbPage))
{
  $bbPage = new bbPage(PAGE_ID);
}


//----Declare Variables------

$section_id = 'awards';

echo '<div class="award-section">'  . PHP_EOL;
echo '<div class ="container">' . PHP_EOL;

echo '<ul class="awards">' . PHP_EOL;
for($i=1; $i<5; $i++)
{

	$award_img_id   = $bbPage->get_field( $section_id, 'award_image_' . $i );
	$award_company  = $bbPage->get_field( $section_id, 'award_company_' . $i );
	$award_title    = $bbPage->get_field( $section_id, 'award_title_' . $i );
	$award_position = $bbPage->get_field( $section_id, 'award_position_' . $i );
	$award_name     = $bbPage->get_field( $section_id, 'award_name_' . $i );

	$chk = $award_company.$award_title.$award_position;
	if( $award_img_id !== null AND $award_img_id > 0 AND !empty( $chk ))
	{
		echo '<li>' . PHP_EOL;
	
		if($award_img_id !== null)
		{	
			$img_src = wp_get_attachment_url($award_img_id);
			echo '<div class="image"><img src="' . $img_src . '" alt="'.$award_title.'" width="160" height="50" /></div>' . PHP_EOL;
		}

		echo '<div class="description">';
		if( $award_company !== null )
		{
			echo '<p class="company">' .$award_company . '</p>';
		}

		if( $award_title !== null )
		{
			echo '<p class="title">' .$award_title . '</p>';
		}


		if( $award_name !== null )
		{
			echo '<p class="name">' .$award_name . '</p>';
		}

		if( $award_position !== null )
		{
			echo '<p class="position">' .$award_position . '</p>';
		}
		echo '</div>';
		
		echo '</li>' . PHP_EOL;
	}
	

}
echo '</ul>' . PHP_EOL;

echo '</div>' . PHP_EOL; // .container
echo '</div>' . PHP_EOL; // .award-section
