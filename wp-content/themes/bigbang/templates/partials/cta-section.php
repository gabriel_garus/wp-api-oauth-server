<?php
defined('ABSPATH') or die('Access Denied!');
/*
*CTA Section partial
*/
if(empty($bbPage) OR !is_object($bbPage))
{
  $bbPage = new bbPage(PAGE_ID);
}





$cta_title    = $bbPage->get_field('cta_home', 'title');
$cta_text 	  = $bbPage->get_field('cta_home', 'text');
$cta_btn_href = $bbPage->get_field('cta_home', 'link');
$cta_btn_lbl  = $bbPage->get_field('cta_home', 'label');

if($cta_btn_lbl === null AND $cta_btn_href !== null)
{
	$cta_btn_lbl = get_the_title( $cta_btn_href );
}

if($cta_btn_href === null)
{
	$cta_btn_href = $bbPage->get_field('cta_home', 'custom_link');
}

$check = $cta_title . $cta_text . $cta_btn_href;







if(!empty($check))
{
	echo '<section class="cta-section">' . PHP_EOL;
	echo '<div class="container cf">' . PHP_EOL;
	echo '<div class="left">' . PHP_EOL;

	if(!empty($cta_title))
	{
		echo '<h3>' . $cta_title . '</h3>' . PHP_EOL;
	}

	if(!empty($cta_text))
	{
		echo '<p>' . $cta_text . '</p>' . PHP_EOL;
	}


	echo '</div>' . PHP_EOL; // .left
	echo '<div class="right">' . PHP_EOL;



	if(!empty($cta_btn_href))
	{
		echo '<div class="cta-btn btn-wrap">';
		echo '<a href="' . get_permalink($cta_btn_href) .'" class="btn btn-primary">' . $cta_btn_lbl . '</a>' . PHP_EOL;
		echo '</div>' . PHP_EOL;
	}

	echo '</div>'; // .right

	echo '</div>'; // .container
	echo '</section>' . PHP_EOL;
}
else
{
	if(WP_DEBUG)
	{
		echo '<!--';
		echo 'CTA EMPTY';
		echo '-->' . PHP_EOL;
	}
}
