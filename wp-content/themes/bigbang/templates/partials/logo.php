<?php
defined('ABSPATH') or die('Access Denied!');
/*
* TOP Logo and mobile menu opening button
*/

$logo_file = 'big-bang-logo';


echo '<div id="head">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;

if ( is_front_page() )
{
	$logo_wrapper_tag = 'h1';
}
else
{
	$logo_wrapper_tag = 'aside';
}


echo '<' . $logo_wrapper_tag . ' id="logo">' . PHP_EOL;
echo '<a href="' . esc_url(home_url('/')) . '" itemprop="url" rel="home">';
echo '<img alt="' . COMPANY_NAME . ' Logo" itemprop="logo" title="' . COMPANY_NAME . '" ';
echo 'id="logo-big" src="/img/logos/'. $logo_file . '.svg" ';
echo ' width="213" height="60">';
echo '</a>' . PHP_EOL;
echo '<meta itemprop="name" content="' . COMPANY_NAME . '">' . PHP_EOL;
echo '</'.$logo_wrapper_tag.'>' . PHP_EOL;



echo '<button id="snap-open" type="button" aria-label="Open"></button>' . PHP_EOL;


echo '</div>' . PHP_EOL; // END: .wrap
echo '</div>' . PHP_EOL; // END: .head
