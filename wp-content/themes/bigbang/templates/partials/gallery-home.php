<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Swiper gallery
*/
//$meta_data = bb_extract( get_post_meta(PAGE_ID) );

//$gallery = ( !empty($meta_data['gallery']['images'])) ? $meta_data['gallery']['images']: null;


if(empty($bbPage) OR !is_object($bbPage))
{
  $bbPage = new bbPage(PAGE_ID);
}


$image_1 = $bbPage->get_field('gallery_home', 'image_small');
$image_2 = $bbPage->get_field('gallery_home', 'image_big');


echo '<section class="gallery-home">' . PHP_EOL;
	echo '<div class="container">' . PHP_EOL;
	echo $bbPage->get_wp_image($image_1, 'port-crop');
	echo $bbPage->get_wp_image($image_2, 'pan-crop');
	echo '</div>' . PHP_EOL; // .container
echo '</section>' . PHP_EOL;