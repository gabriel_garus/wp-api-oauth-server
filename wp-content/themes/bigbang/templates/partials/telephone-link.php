<?php
defined('ABSPATH') or die('Access Denied!');
/*
*telephone link partial
*/


//--------Conditional for rich snippets-----------
if ( is_front_page() )
{
	$md_tele  = 'itemprop="telephone"';
}

else
{
	$md_tele = null;
}

//-----Get Meta data from bigbang Redux-----------
if(empty($options))
{
	$options = (!empty($_SESSION['bb_options'])) ? $_SESSION['bb_options'] : get_option('bigbang');
}


//----------Declare Variables--------------------
$telephone = (!empty($options['telephone'])) ? $options['telephone'] : null;
$telephone = trim($telephone);

//---------------------------------------------

if($telephone !== null)
{
	$tele_short = str_replace(' ', '', $telephone);
	$tele_short = ltrim($tele_short, '0');
	$tele_short = '+353' . $tele_short; 

	echo '<a href="tel:'.$tele_short.'" class="telephone"><span ' . $md_tele . '>' . $telephone . '</span></a> '.PHP_EOL;
}
