<?php
defined('ABSPATH') or die('Access Denied!');
/*
*hero partial
*/


if(!isset($bbPage))
{
  $bbPage = new bbPage(PAGE_ID);
}





$hero_title     = $bbPage->title;
$hero_text      = $bbPage->content;
$hero_btn_href  = $bbPage->get_field('cta_hero','cta_hero_link');
$hero_btn_lbl   = $bbPage->get_field('cta_hero','cta_hero_label');


$the_class      = (!empty($hero_class)) ? ' ' . $hero_class : null;


$img = $bbPage->image;

if($img['mime'] == 'image/svg+xml')
{
 	$bbPage->set_thumbnail_size(array('width'=>1040, 'height'=>400));
 	$imaginarium = $img['html'];
}
else
{
	$bbPage->set_thumbnail_size('hero-wide');
	$imaginarium = $bbPage->wp_image;
}







//----------------------------------------------------------------------------


echo '<section class="hero">'  .  PHP_EOL;

echo '<div class="container">' . PHP_EOL;

echo '<h1>' . $hero_title . '</h1>' . PHP_EOL;

echo '<div class="text">' . PHP_EOL;
echo wpautop( $hero_text );
echo '</div>' . PHP_EOL;

if( !empty($imaginarium) )
{
	echo $imaginarium;
}


//--------------------------------------------

echo '</div>' . PHP_EOL; // .container


echo '</section>' . PHP_EOL; // .hero
