<?php

defined('ABSPATH') or die('Access Denied!');
/*
*Quotes Partial
*
*/



//--------------Get Meta Data----------------

if(!empty($quote_id))
{

	$quote_post = new bb_post($quote_id);
	$post_meta 	= $quote_post->meta;


	$my_post_id = $quote_id;//This is page id or post id
	$content = $quote_post->content;

	$first_name  = $quote_post->get_field('quote','first_name');
	$family_name = $quote_post->get_field('quote','family_name');
	$position 	 = $quote_post->get_field('quote','position');
	$company 	 = $quote_post->get_field('quote','company_name');



	$quote_post->set_thumbnail_size('avatar-small');



	//---------------------------------------------------------
	echo '<div class="quote aside">' . PHP_EOL;

	// - top - 
	echo '<div class="top">' . PHP_EOL;
	echo '<p>' .  trim( $content ) . '</p>' . PHP_EOL;
 	echo '</div>' . PHP_EOL; //.top
 	// - - - -

 	// - bottom - 
	echo '<div itemscope itemtype="http://schema.org/Person" class="bottom cf">' . PHP_EOL;
	
	// - image - 
	echo '<div class="person-image">' . PHP_EOL;
	if(!empty($quote_post->wp_image))
	{
	 	echo  $quote_post->wp_image;	
	}
	echo '</div>' . PHP_EOL; // - image
	// - - - 

	// - text - 
	echo '<div class="person-text">' . PHP_EOL;
	
	if(!empty($first_name ) OR !empty( $family_name ))
	{
		echo '<div itemprop="name" class="name">';
		echo '<span itemprop="givenName">' . $first_name . ' </span>';
		echo '<span itemprop="familyName">' . $family_name . '</span>';
		echo '</div>' . PHP_EOL; //.name
	}



	echo '<div class="person-meta">' . PHP_EOL;

	if($position !== null)
	{
		echo '<span itemprop="jobtitle">' . $position . '</span>, ' . PHP_EOL;
	}
	
	if($company !== null)
	{
		echo '<div itemscope itemtype="http://schema.org/Organization">' ;
		echo '<span itemprop="name">' . $company . '</span>';
		echo '</div>'; //.organisation
	}

	echo '</div>'; //.person-meta

	echo '</div>'; //.person text
	// - - - 

	echo '</div>';//.bottom
	// - - - -


	echo '</div>' . PHP_EOL; //.quote aside
	//---------------------------------------------------------



}
elseif(WP_DEBUG)
{
	echo '<!-- Quote ID is missing -->' . PHP_EOL;
}