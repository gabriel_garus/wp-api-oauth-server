<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Awards partial
*/


if(empty($bbPage) OR !is_object($bbPage))
{
  $bbPage = new bbPage(PAGE_ID);
}


//----Declare Variables------

$section_id = 'services';

$s_title     = $bbPage->get_field($section_id,'title'); 
$s_intro     = $bbPage->get_field($section_id,'intro'); 

$s_number   = $bbPage->get_field($section_id,'number'); 

echo '<div class="services section">'  . PHP_EOL;
echo '<div class ="container">' . PHP_EOL;

if($s_title !== null)
{
	echo '<h3>' . $s_title . '</h3>' . PHP_EOL;
}


if($s_intro !== null)
{
	echo wpautop($s_intro);
}

if($s_number > 0)
{	
	echo '<ul class="services-list">' . PHP_EOL;

	for ($i=1; $i <= $s_number ; $i++) 
	{ 
		$title = $bbPage->get_field($section_id,'title_' . $i); 
		$sub   = $bbPage->get_field($section_id,'subtitle_' . $i); 

		$li_class = trim( str_replace( ' ', '-', strtolower( $title ) ) );
		
		echo '<li class="'.$li_class.'">' . PHP_EOL;

	

		if($title !== null)
		{
			echo '<h5>'. $title . '</h5>' . PHP_EOL;
		}

		if($sub !== null)
		{
			echo '<p>'. $sub .'</p>' . PHP_EOL;
		}
		echo '</li>' . PHP_EOL;
	}

}

echo '</div>' . PHP_EOL; // .container
echo '</div>' . PHP_EOL; // .services