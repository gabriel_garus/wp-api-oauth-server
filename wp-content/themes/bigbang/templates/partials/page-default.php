<?php

if(empty($meta_data))
{
  $meta_data = bb_extract( get_post_meta(PAGE_ID));
}


$top_title = ( !empty($meta_data['top_section']['title']) ) ? $meta_data['top_section']['title'] : null;
$top_list = ( !empty($meta_data['top_section']['list']) ) ? $meta_data['top_section']['list'] : null;

$mid_text = ( !empty($meta_data['mid_section']['mid_section_text']) ) ? $meta_data['mid_section']['mid_section_text'] : null;
$mid_img_id = ( !empty($meta_data['mid_section']['mid_section_image']) ) ? $meta_data['mid_section']['mid_section_image'] : '0';
if($mid_img_id  !== '0')
{
	
	$mid_img = bb_get_the_image( $mid_img_id , array(
	 'class' => 'mid-image',
	 'size' => 'medium_400'
	 ) );
}
else
{
  $mid_img = null;
}

$bottom_title = ( !empty($meta_data['bottom_section']['title']) ) ? $meta_data['bottom_section']['title'] : null;
$bottom_list = ( !empty($meta_data['bottom_section']['bottom_list']) ) ? $meta_data['bottom_section']['bottom_list'] : null;

$bottom_2_title = ( !empty($meta_data['bottom_2_section']['title']) ) ? $meta_data['bottom_2_section']['title'] : null;
$bottom_2_list =  ( !empty($meta_data['bottom_2_section']['list']) ) ?  $meta_data['bottom_2_section']['list'] : null;
//bb_extract()  located in the inc folder

echo '<div class="content">' . PHP_EOL;
echo '<section class="top-list">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;
echo '<h2>' . $top_title . '</h2>' . PHP_EOL;

echo   wpautop( $top_list );
echo '</div>' . PHP_EOL; // .container
echo '</section>' . PHP_EOL;


echo '<section class="mid">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;
if(!empty($mid_text))
{
	if(empty($mid_img['html']))
	{
		echo '<strong class="teal">' . $mid_text . '</strong>' . PHP_EOL;
	}
	else
	{
		echo '<div class="left">' . PHP_EOL;
		echo wpautop( $mid_text );
		echo '</div>' . PHP_EOL; // .left
	}
}
if(!empty($mid_img['html']))
{
	echo '<div class="left">' . PHP_EOL;
	echo $mid_img['html'];
	echo '</div>' . PHP_EOL; // .left
}
echo ' </div>' . PHP_EOL; // .container
echo '</section>' . PHP_EOL;


echo '<section class="bottom">' . PHP_EOL;
echo '<div class ="container">' . PHP_EOL;
echo '<h2>' . $bottom_title . '</h2>' . PHP_EOL;
echo  wpautop( $bottom_list );
echo '</div>' . PHP_EOL;
echo '</section>' . PHP_EOL;


echo '<section class="bottom-2">' . PHP_EOL;
echo '<div class="container">' . PHP_EOL;
echo '<h2>' . $bottom_2_title . '</h2>' . PHP_EOL;
echo wpautop( $bottom_2_list );
echo '</div>' . PHP_EOL; // .container
echo '</section>' . PHP_EOL;

echo '</div>' . PHP_EOL;

include PARTIALS_DIR . 'cta-section.php';