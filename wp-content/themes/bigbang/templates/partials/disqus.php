<?php
defined('ABSPATH') or die('Access Denied!');

// **** DISQUS ***********************


//$disqus_shortname = 'taxhugblogtest';
if(empty($options))
{
	$options = bb_get_options();
}
$disqus_shortname = $options['disqusId'];


if(!empty($disqus_shortname))
{
	echo '<div class="disqus">'.PHP_EOL;

	echo '<div id="disqus_thread"></div>'.PHP_EOL;

	echo '<script type="text/javascript">' .PHP_EOL;
	    /* * * CONFIGURATION VARIABLES * * */
	echo 'var disqus_shortname = \''.$disqus_shortname.'\';';

	    /* * * DON'T EDIT BELOW THIS LINE * * */
	echo '(function() {';
	echo 'var dsq = document.createElement(\'script\'); dsq.type = \'text/javascript\'; dsq.async = true;';
	echo 'dsq.src = \'//\' + disqus_shortname + \'.disqus.com/embed.js\';';
	echo '(document.getElementsByTagName(\'head\')[0] || document.getElementsByTagName(\'body\')[0]).appendChild(dsq);';
	echo '})();'.PHP_EOL;
	echo '</script>'.PHP_EOL;
	echo '<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>';

	echo '</div>'.PHP_EOL; // . disqus
}
//----------------------------------------------------------------
