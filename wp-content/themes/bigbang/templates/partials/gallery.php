<?php
defined('ABSPATH') or die('Access Denied!');
/*
*Swiper gallery
*/
//$meta_data = bb_extract( get_post_meta(PAGE_ID) );

//$gallery = ( !empty($meta_data['gallery']['images'])) ? $meta_data['gallery']['images']: null;

$gallery_post = new bb_post(PAGE_ID);

$gallery_post->set_gallery_img_size('gallery');
$gallery_items = $gallery_post->get_gallery_srcs();
echo '<div id="gallery" class="swiper-container">'.PHP_EOL;
//<!-- Additional required wrapper -->
echo '<div class="swiper-wrapper">'.PHP_EOL;

foreach($gallery_items as $img)
	{
	echo '<div class="swiper-slide">';
	echo $img;
	echo '</div>'.PHP_EOL;
	}

echo '</div>' . PHP_EOL; // .swiper-wrapper

echo '<div class="swiper-pagination"></div>' . PHP_EOL;
//           <!-- Add Arrows -->
echo '<div class="swiper-button-next swiper-button-white"></div>'.PHP_EOL;
echo '<div class="swiper-button-prev swiper-button-white"></div>'.PHP_EOL;
echo '</div>'.PHP_EOL; // #gallery



if(!defined('gallery'))
{
	define('gallery', true);
}
