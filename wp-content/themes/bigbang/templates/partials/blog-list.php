<?php

defined('ABSPATH') or die('Access Denied!');
/*
*Blog List partial
*/


if(empty($bbPage) OR !is_object($bbPage))
{
  $bbPage = new bbPage(PAGE_ID);
}

$section_id = 'bloglist';



$post_nr = $bbPage->get_field($section_id, 'number'); 

if($post_nr === null)
{
  $post_nr = get_option('posts_per_page');
} 



if(!isset( $blog_list_intro ) )
{
  $blog_list_intro = false;
}

  echo '<div class="blog-list">' . PHP_EOL;




if(is_home())
{
  while(have_posts())
  {
    the_post();
    include PARTIALS_DIR . 'blog-list-item.php';
  }
}
else
{
  $args = array(  'post_type' => 'post', 'posts_per_page' => $post_nr );
  $the_query = new WP_Query( $args );
  
  while ( $the_query->have_posts() )
  {
    $the_query->the_post();
    include PARTIALS_DIR . 'blog-list-item.php';

  }

}

echo '</div>' . PHP_EOL;

echo '</div>' . PHP_EOL; // .container
echo '</section>' . PHP_EOL;