<?php
defined('ABSPATH') or die('Access Denied!');
/*
* Clients partial
*/


if(empty($bbPage) OR !is_object($bbPage))
{
  $bbPage = new bbPage(PAGE_ID);
}


$d_title = $bbPage->get_field('dual_section', 'title');
$d_text  = $bbPage->get_field('dual_section', 'text');



echo '<div class="section dual-section">' . PHP_EOL;
echo '<div class="container cf">' . PHP_EOL;

echo '<div class="left">' . PHP_EOL;
if($d_title !== null)
{
	echo '<h3>' . $d_title . '</h3>' . PHP_EOL;
}

echo '</div>' . PHP_EOL;

echo '<div class="right">' . PHP_EOL;
if($d_text !== null)
{
	echo wpautop( $d_text ) . PHP_EOL;
}

echo '</div>';

echo '</div>'; //.container
echo '</div>' . PHP_EOL; //.section