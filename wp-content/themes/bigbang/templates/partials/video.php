<?php
defined('ABSPATH') or die('Access Denied!');
/* HTML5 Video Markup
* require $video_url
*/

//wp_enqueue_script( 'shimVideo' );



$companyName = '';
$companySite = WP_HOME;

// Assign Raw link from DB

$videoLink = $video_url;

$V = new YT_Video($videoLink);

if( empty($videoPoster) )
{

	$videoPoster = ( !empty($video_poster) ) ? $video_poster : null;
}


if($V->ID !== null)
{
	$videoName 		= 'Name';
	$embedUrl 		=  $V->get_embed_url();
	$videoSrc 		= $V->get_watch_url();
	$contentUrl 	= $V->get_watch_url();
	$thumbnailUrl = $V->get_thumbnail('mq');
	$videoPoster 	= (empty($videoPoster)) ? ($V->get_thumbnail('hq')) : $videoPoster;
	$time 				= $V->get_time();
	$publish_date = $V->get_publish_date();
	$description = $V->get_description();
	$video_name = $V->get_name();


	$description = str_replace('"', "'", $description);
	$video_name = str_replace('"', "'", $video_name);
	if(!is_front_page())
	{
		$vidItemProp = 'itemprop="video"';
	}

	else{
		$vidItemProp = null;
	}

	echo '<div id="video" class="video" '. $vidItemProp;
	echo ' itemscope itemtype="http://schema.org/VideoObject" >'.PHP_EOL;

	echo '<div itemprop="publisher" itemscope itemtype="http://schema.org/Organization">';
	echo '<meta itemprop="name" content="'. $companyName .'">'.PHP_EOL;
	echo '<link itemprop="url" href="'.$companySite.'">'.PHP_EOL;
	echo '</div>'.PHP_EOL;

	echo '<div class="wrap">'.PHP_EOL;
	echo '<div class="chc-video">'.PHP_EOL;

	echo '<meta itemprop="thumbnailUrl" content="'.$thumbnailUrl.'" />'.PHP_EOL;

	echo '<meta itemprop="contentUrl" content="' . $contentUrl . '" />'.PHP_EOL;
	echo '<meta itemprop="embedURL" content="'. $embedUrl . '" />'.PHP_EOL;
	echo '<meta itemprop="duration" content="'. $time . '" />'.PHP_EOL;
	echo '<meta itemprop="description" content="'. $description . '" />'.PHP_EOL;
	echo '<meta itemprop="name" content="'. $video_name . '" />'.PHP_EOL;


	echo '<meta itemprop="uploadDate" content="' . $publish_date . '"  />' . PHP_EOL;

	echo '<div class="mediaplayer ratio-16-9" >'.PHP_EOL;
	echo '<video poster="'.$videoPoster.'" height="360" width="640" style="height: 100%;" controls>'.PHP_EOL;
	echo '<source type="video/youtube" src="' . $videoSrc . '" /> ' .PHP_EOL;

	echo'</video>'.PHP_EOL . '</div>'.PHP_EOL;
	echo'</div></div>'.PHP_EOL;
	echo'</div>'.PHP_EOL;

}





class YT_Video
{

public $embedLink = '';
public $watchLink = '';
public $ID = '';
public $meta = '';

public function __construct($video_link)
{
	$this->set_ID($video_link);
	$this->set_meta();
}

private function set_ID($video_link)
{
	$id = '';

	if (strpos($video_link,'watch') !== false)
	{
		$ex = explode('v=', $video_link);
		$id = end($ex);
	}
	elseif(strpos($video_link,'youtu.be') !== false)
	{
		$ex = explode('.be/', $video_link);
		$id = end($ex);
	}
	elseif(strpos($video_link,'embed') !== false)
	{
		$ex = explode('embed/', $video_link);
		if(strpos($ex[1],'"') !== false)
		{
			$ex2 = explode('"', $ex[1]);
			$id = $ex2[0];
		}
		else
		{
		$id = end($ex);
		}
	}
	else
	{
		$id = null;
	}
	$this->ID = $id;

}


public function get_id()
{
	return $this->ID;
}

public function get_thumbnail($size = 'default')
{
	$thumb = '';
	$id = $this->ID;

	if($id === null)
	return;

	$path = 'http://img.youtube.com/vi/'.$id.'/';

	switch ($size) {
		case 'full':
			$img = '0';
			break;

		case 'hq':
			$img = 'hqdefault';
			break;

		case 'mq' :
			$img = 'mqdefault';
			break;

		case 'small' :
			$img = '1';
			break;

		default:
			$img = 'default';
			break;
	}
	$thumb = $path . $img .'.jpg';

	return $thumb;
}

public function get_watch_url()
{
 $id = $this->ID;

	if($id === null)
	return;

 $watchUrl = 'https://www.youtube.com/watch?v=';
 $watchUrl .= $id. '&amp;vq=large';
 return  $watchUrl;
}


public function get_embed_url()
{
 $id = $this->ID;

 $embedUrl = 'https://www.youtube.com/embed/';
 $embedUrl .= $id;
 return  $embedUrl;
}


public function get_time()
{

	$vidkey = $this->ID;


	if($vidkey === null)
	return;
	$apikey = 'AIzaSyACEbxu3qpTax9gyd2WKCaSuctTyl8qokI';

	$url = "https://www.googleapis.com/youtube/v3/videos?id=".$vidkey;
	$url .= "&part=contentDetails&key=".$apikey;
	$dur = file_get_contents($url);
	$VidDuration =json_decode($dur, true);

	//dump($VidDuration);
	$theTime = $VidDuration['items'][0]['contentDetails']['duration'];




	return $theTime;

}
public function get_publish_date()
{

	if(!empty($this->meta['items'][0]['snippet']['publishedAt']))
	{
		return $this->meta['items'][0]['snippet']['publishedAt'];
	}
	else
	{
		return null;
	}

}

public function get_description()
{

	if(!empty($this->meta['items'][0]['snippet']['description']))
	{
		return $this->meta['items'][0]['snippet']['description'];
	}
	else
	{
		return null;
	}

}

public function get_name()
{
	if(!empty($this->meta['items'][0]['snippet']['title']))
	{
		return $this->meta['items'][0]['snippet']['title'];
	}
	else
	{
		return null;
	}

}


private function set_meta()
{
	$vidkey = $this->ID;

	$apikey = 'AIzaSyACEbxu3qpTax9gyd2WKCaSuctTyl8qokI';

   	$url = "https://www.googleapis.com/youtube/v3/videos?id=".$vidkey;
	$url .= "&part=snippet&key=".$apikey;

	$dur = file_get_contents($url);
	$VidMeta =json_decode($dur, true);


	$this->meta = $VidMeta;


}




} // class end
