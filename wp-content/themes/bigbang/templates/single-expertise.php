<?php
/*
*Template Name: Single Expertise
*
*/
defined('ABSPATH') or die('Access Denied!');
get_header();




$bbPage = new bbPage(PAGE_ID) ;

$middle_content = $bbPage->get_field('middle_text','text');

$quote_id = $bbPage->get_field('qoute_select','quote_id');

//----------------------------------------------------
$next_article_id = $bbPage->get_field('next_article','id');



if($next_article_id == 'auto' OR empty($next_article_id))
{
	$siblings = get_pages( array('parent' => $post->post_parent) );

	$current_mark = 0;
	
	foreach ($siblings as $counter => $page) 
	{
		if($page->ID == $post->ID)
		{
			$current_mark = $counter;
		}
	}

	if($current_mark == (sizeof($siblings) -1 ))
	{
		$next_mark = 0;
	}
	else
	{
		$next_mark = $current_mark + 1;
	}

	$next = $siblings[$next_mark];
	$next_article_id = $next->ID;
}
elseif($next_article_id == 'other')
{
	$other_id = $bbPage->get_field('next_article','other');

	if($other_id !== null)
	{
		$next_article_id = $other_id;
	}
}


$next_article_url = get_permalink( $next_article_id );
$next_article_leadin = get_post_meta( $next_article_id, 'lead_in');
$lead_title = (!empty($next_article_leadin[0]['title'])) ? $next_article_leadin[0]['title'] : get_the_title($next_article_id);
$btn_label_pre = $bbPage->get_field('next_article','btn_label');
$the_heading = $bbPage->get_field('next_article','head');

if(isset($other_id))
{
	if($btn_label_pre !== null)
	{
		$btn_label = $btn_label_pre;
	}
	else
	{
		$btn_label = 'Read about <strong>' . get_the_title($next_article_id) . '</strong>';
	}
}
else
{
	if($btn_label_pre !== null)
	{
		$btn_label =  $btn_label_pre. '<strong>' . get_the_title($next_article_id) . '</strong>';
	}
	else
	{
		$btn_label = 'Read about <strong>' . get_the_title($next_article_id) . '</strong>';
	}
}



//------------------------------------------------------------------------------------------------------------------------------



include_once PARTIALS_DIR . 'hero.php';

echo '<div class="section expertise single">' . PHP_EOL;
echo '<div class="container cf">' . PHP_EOL;

echo '<div class="left main">' . PHP_EOL;
echo do_shortcode( wpautop( $middle_content, true ) );
echo '</div>' . PHP_EOL;

echo '<div class="right aside">' . PHP_EOL;
include_once PARTIALS_DIR . 'quote-aside.php';
echo '</div>' . PHP_EOL;

echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL;




echo '<div class="section read-next">' . PHP_EOL;
echo '<div class="container cf">' . PHP_EOL;
echo '<h4 class="caps cf">';
if($the_heading !== null)
{
	echo $the_heading;
}
else
{
	echo 'Read Next';
}

echo '</h4>' . PHP_EOL;
echo '<div class="left">' . PHP_EOL;


echo '<h2>' . $lead_title . '</h2>';

echo '</div>';

echo '<div class="right">' . PHP_EOL;
if(isset($other_id))
{	
	echo '<div class="btn-wrap">' . PHP_EOL;
	echo '<a class="btn btn-primary" href="' .$next_article_url.'">' . $btn_label .'</a>' . PHP_EOL;
	echo '</div>' . PHP_EOL;
}
else
{
	echo '<a class="blue" href="' .$next_article_url.'">' . $btn_label .'</a>' . PHP_EOL;
}



echo '</div>' . PHP_EOL;

echo '</div>' . PHP_EOL;
echo '</div>' . PHP_EOL;


get_footer();
