<?php
defined('ABSPATH') or die('Access Denied!');

/*
*Template Name: Expertise
*
*/

get_header();



$bbPage = new bbPage(PAGE_ID) ;



include_once PARTIALS_DIR . 'hero.php';



$children_raw = $bbPage->get_field('headline','child_sections');



if( $children_raw !== null )
{
	$children = explode(':', $children_raw);

	foreach ($children as $ch_id) 
	{
		$box_id = 'child_' . $ch_id;

		$title = $bbPage->get_field($box_id,'title');
		$conte = $bbPage->get_field($box_id,'text');
		$linke = $bbPage->get_field($box_id,'url');

		if($title === null)
		{
			$title = get_the_title( $ch_id );
		}



		echo '<div class="section expertise">' . PHP_EOL;
		echo '<div class="container cf">' . PHP_EOL;

		echo '<div class="cf">' . PHP_EOL;
		echo '<h3>'.$title.'</h3>' . PHP_EOL;
		echo '</div>' . PHP_EOL;

		echo '<div class="left">' . PHP_EOL;

		if($conte !== null)
		{
			echo wpautop( $conte );
		}



		echo '</div>' . PHP_EOL;

		echo '<div class="right">' . PHP_EOL;

		$nr_of_perks = intval( $bbPage->get_field($box_id,'perks_number') );

		if($nr_of_perks > 0)
		{
			echo '<ul class="perks">';
			for($p = 1; $p<=$nr_of_perks; $p++)
			{
				$perk = $bbPage->get_field($box_id,'perk_'.$p);
				if($perk !== null)
				{
					echo '<li>' . $perk . '</li>';
				}
				
			}
			echo '</ul>' . PHP_EOL;
		}

		echo '</div>' . PHP_EOL;

		if($linke !== null)
		{
			echo '<div class="btn-wrap">' . PHP_EOL;
			echo '<a class="btn btn-tertiary" href="'. WP_HOME .'/' . $linke .'/">Read More</a>' . PHP_EOL;
			echo '</div>' . PHP_EOL;
		}

		echo '</div>' . PHP_EOL;
		echo '</div>' . PHP_EOL;

	

	}
}



$box_id = 'dual_section';

$left_text  = $bbPage->get_field($box_id,'left_text');
$right_text = $bbPage->get_field($box_id,'right_text');

if($left_text !== null OR $right_text !== null)
{
	echo '<div class="section dual">' . PHP_EOL;
	echo '<div class="container cf">' . PHP_EOL;


	echo '<div class="left">' . PHP_EOL;
		if ($left_text !== null)
		{
			echo '<p>'. $left_text .'</p>'. PHP_EOL;
		}
	echo '</div>' . PHP_EOL;

	echo '<div class="right">' . PHP_EOL;
	if ($right_text !== null)
		{
			echo '<p>'. $right_text .'</p>'. PHP_EOL;
		}
	echo '</div>' . PHP_EOL;

	echo '</div>' . PHP_EOL;
	echo '</div>' . PHP_EOL;
}

get_footer();
