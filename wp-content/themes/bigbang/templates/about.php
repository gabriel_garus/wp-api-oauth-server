<?php
/*
*Template Name: About
*
*/
defined('ABSPATH') or die('Access Denied!');

get_header();

$bbPage = new bbPage(PAGE_ID) ;


include PARTIALS_DIR . 'hero.php';

$cta_btn_link = $bbPage->get_field('middle_content', 'cta_hero_custom_link');
if($cta_btn_link === null)
{
	$cta_btn_link = $bbPage->get_field('middle_content', 'cta_hero_link');
}


$cta_btn_lbl  = $bbPage->get_field('middle_content', 'cta_hero_label');

//--------------------------------------------
$middle['title'] = $bbPage->get_field('middle_content','title');
$middle['text'] = $bbPage->get_field('middle_content','text');

if(!max($middle) !== null )
{
	echo '<div class="section middle">' . PHP_EOL;
	echo '<div class="container cf">' . PHP_EOL;
	
	echo '<div class="left">' . PHP_EOL;
	if($middle['title'] !== null)
	{
		echo '<h3>' . $middle['title'] .'</h3>';
	}

	if($middle['text'] !== null)
	{
		echo wpautop( $middle['text'] );
	}
	echo '</div>' . PHP_EOL;

	echo '<div class="right">' . PHP_EOL;
	if($cta_btn_link !== null)
	{
		echo '<div class="cta-btn btn-wrap">';
		echo '<a href="' . get_permalink($cta_btn_link) .'" class="btn btn-primary">' . $cta_btn_lbl . '</a>' . PHP_EOL;
		echo '</div>' . PHP_EOL;
	}
	echo '</div>' . PHP_EOL;

	echo '</div>' . PHP_EOL;
	echo '</div>' . PHP_EOL;

}


$team_switch = $bbPage->get_field('include_team','switch');


if($team_switch !== null)
{
	include PARTIALS_DIR . 'employee.php';
}



get_footer();
